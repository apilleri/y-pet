/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "phantoms.h"

CylinderPhantom::CylinderPhantom(std::string config, std::string tag)
{
  XMLDocument* xml_doc = xmlLoad(config);
  
  // --- Parsing CYLINDER or NORM_CYLINDER element
  const XMLElement *cylinder_el = xmlGetElement(xml_doc, tag.c_str());
  
  pht_min_r = xmlGetNumber<SpaceType>(cylinder_el, "PHANTOM_MIN_RADIUS");
  pht_max_r= xmlGetNumber<SpaceType>(cylinder_el, "PHANTOM_MAX_RADIUS");
  pht_border_h = xmlGetNumber<SpaceType>(cylinder_el, "PHANTOM_BORDER_HEIGHT");
  
  active_min_r = xmlGetNumber<SpaceType>(cylinder_el, "ACTIVE_MIN_RADIUS");
  active_max_r = xmlGetNumber<SpaceType>(cylinder_el, "ACTIVE_MAX_RADIUS");
  active_z = xmlGetNumber<SpaceType>(cylinder_el, "ACTIVE_Z");

  centre_x = xmlGetNumber<SpaceType>(cylinder_el, "CENTRE_X");
  centre_y = xmlGetNumber<SpaceType>(cylinder_el, "CENTRE_Y");
  centre_z = xmlGetNumber<SpaceType>(cylinder_el, "CENTRE_Z");
  activity_density_v = xmlGetNumber<float>(cylinder_el, "ACTIVITY_DENSITY");
}

void CylinderPhantom::print() const
{
  std::cout << "Printing configuration:" << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "PHANTOM_MIN_RADIUS: " << pht_min_r << std::endl;
  std::cout << "PHANTOM_MAX_RADIUS: " << pht_max_r << std::endl;
  std::cout << "PHANTOM_Z_BORDER: " << pht_border_h << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "ACTIVE_MIN_RADIUS: " << active_min_r << std::endl;
  std::cout << "ACTIVE_MAX_RADIUS: " << active_max_r << std::endl;
  std::cout << "ACTIVE_Z: " << active_z << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "CENTRE_X: " << centre_x << std::endl;
  std::cout << "CENTRE_Y: " << centre_y << std::endl;
  std::cout << "CENTRE_Z: " << centre_z << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "ACTIVITY_DENSITY: " << activity_density_v << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "Computed total activity: ";
  std::cout << activeVolume()*activityDensity()/1e6 << " MBq" << std::endl;
  std::cout << "---" << std::endl;
}
