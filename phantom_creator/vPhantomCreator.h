/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef VPHANTOMCREATOR_H
#define VPHANTOMCREATOR_H

#include <iostream>
#include <fstream>
#include <cstdint>
#include <string>
#include <vector>

#include <cmath>

#include "dataTypes.h"
#include "f_utils.h"
#include "fieldOfView.h"
#include "phantoms.h"

#include "tinyxml2.h"
using namespace tinyxml2;

typedef enum {Positron, BackToBack} SourceType;
// Enumeration to control if files are saved in text or in binary mode.

class vPhantomCreator {

protected:
  std::string xml_config;
  FieldOfView *FOV;
  
  std::string g_fn;
  std::string p_fn;
  
  std::string o_fn;
  
  SourceType source_type;
  FloatType energy;
  FloatType unstable;
  
  bool build_phantom;
  // flag to build the attenuation phantom
  bool emits_2D;
  
  uint32_t vis_points;
  
  float time_length;
  
  uint32_t seed;
  
  // the list must be populated by the sourceConstruction method
  std::vector<std::string> sources_list;
  
  
public:
  vPhantomCreator(const std::string& xml_config_p, const std::string& fov_tag_p)
  : FOV(nullptr), g_fn(""), p_fn(""), o_fn(""), source_type(Positron),
  energy(0), unstable(-1), build_phantom(true), emits_2D(false), vis_points(0),
    time_length(0), seed(0)
  { 
    xml_config = xml_config_p;
    FOV = new FieldOfView(xml_config, fov_tag_p);
  }
  
  void setGeometryFile(std::string f)
  {
    g_fn = f;
  }
  
  void setPhysicsDigitizerFile(std::string f)
  {
    p_fn = f;
  }
  
  void setOutFile(std::string f)
  {
    o_fn = f;
  }
  
  void setSourceType(std::string s_type)
  {
    if (s_type == "BackToBack")
      source_type = BackToBack;
    else if (s_type == "Positron")
      source_type = Positron;
    else
      throw std::string("Unknown source type, choose among Positron and BackToBack");
    
    std::cout << "SOURCE_TYPE: " << s_type << std::endl;
  }
  
  void setEnergy(FloatType en) { energy = en; }
  
  void setUnstable(FloatType hl) { unstable = hl; }
  
  void setVisualization(uint32_t n_points) { vis_points = n_points; }
  
  void setAcquisitionLength(float t) { time_length = t; }
  
  void setSeed(uint32_t s) { seed = s; }
  
  void setNoPhantom() { build_phantom = false; }
  
  void set2D() { emits_2D = true; }

  static vPhantomCreator* init(std::string p_type_p,
                               std::string config_p,
                               std::string tag_p,
                               std::string fov_tag_p
                              );

  
  void construct();
  
  void appendGeometry();
  
  void appendPhysics();
  
  void appendVisualization(uint16_t nPoints);
  
  void appendRun(std::string root_sim_name);
  
protected:
  
  void createCylSource(std::ofstream& out_f, std::string name, 
                       FloatType activity_p, FloatType radius,
                       FloatType halfz, const ThreeVector& center);
  
  virtual void phantomConstruction() = 0;
  
  virtual void sourceConstruction() = 0;
  
  virtual void simulationConstruction() = 0;

public:
  virtual void imageConstruction() = 0;
};



class PhantomCreatorCylinder  : public vPhantomCreator {
  
  CylinderPhantom *pht;
  
  void phantomConstruction() override;
  
  void sourceConstruction() override;
  
  void simulationConstruction() override;
  
  void imageConstruction() override;
  
public:
  PhantomCreatorCylinder(std::string config, std::string tag, const std::string& fov_tag_p);
  
  ~PhantomCreatorCylinder()
  {
    if (pht != nullptr)
      delete pht;
  }
};


class PhantomCreatorDerenzo  : public vPhantomCreator {
  
  DerenzoPhantom *pht;
    
  void phantomConstruction() override;
  
  void sourceConstruction() override;
  
  void simulationConstruction() override;
  
  void imageConstruction() override;
  
public:
  PhantomCreatorDerenzo(std::string config, std::string tag, const std::string& fov_tag_p);
  
  ~PhantomCreatorDerenzo()
  {
    if (pht != nullptr)
      delete pht;
  }
};


class PhantomCreatorPlanar  : public vPhantomCreator {
  
  PlanarPhantom *pht;
  
  AngleType cur_angle;
  
  void phantomConstruction() override;
  
  void sourceConstruction() override;
  
  void simulationConstruction() override;
  
  void imageConstruction() override;
  
public:
  PhantomCreatorPlanar(std::string config, std::string tag, const std::string& fov_tag_p);
  
  ~PhantomCreatorPlanar()
  {
    if (pht != nullptr)
      delete pht;
  }
};


class PhantomCreatorRodsGrid  : public vPhantomCreator {
  
  SpaceType phantom_radius;
  // radius of the attenuation phantom (mm) 
  SpaceType phantom_height;
  // height of the attenuation phantom  (mm) in z
  SpaceType safety_distance;
  // safety distance from the border of the cylinder phantom (XY plane)
  SpaceType rod_height;
  // height of every cylinder source (mm)
  SpaceType rod_radius;
  // diameter of every cylinder source (mm)
  float activity_density;
  // activity density (Bequerell)
  float spacing_factor;
  // spacing factor between the centers of two consecutive rods in x and y.
  // The distance between the centers of two consecutive rods is equal to
  // distance = spacing_factor*2*rod_radius
  
  std::vector<ThreeVector> cyl_positions;
  
  void phantomConstruction() override;
  
  void sourceConstruction() override;
  
  void simulationConstruction() override;
  
  void imageConstruction() override;
  
public:
  PhantomCreatorRodsGrid(std::string config, std::string tag, const std::string& fov_tag_p);
};


#endif // VPHANTOMCREATOR_H
