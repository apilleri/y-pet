/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "phantoms.h"

DerenzoPhantom::DerenzoPhantom(std::string config, std::string tag, SpaceType p_r)
{
  XMLDocument* xml_doc = xmlLoad(config);
  
  // --- Parsing DERENZO element
  const XMLElement *derenzo_el = xmlGetElement(xml_doc, tag.c_str());
  
  pht_max_r = p_r;
  pht_h = xmlGetNumber<SpaceType>(derenzo_el, "PHANTOM_Z");
  
  forbid_r = xmlGetNumber<SpaceType>(derenzo_el, "FORBID_RADIUS");
  inner_cyl_rad = xmlGetNumber<SpaceType>(derenzo_el, "INNER_CYL_RADIUS");
  safety_distance = xmlGetNumber<SpaceType>(derenzo_el, "SAFETY_DISTANCE");
  start_angle = xmlGetNumber<SpaceType>(derenzo_el, "START_ANGLE");
  rod_h = xmlGetNumber<SpaceType>(derenzo_el, "ROD_Z");
  min_rod_r = xmlGetNumber<SpaceType>(derenzo_el, "MIN_ROD_RADIUS");
  inc_r = xmlGetNumber<SpaceType>(derenzo_el, "INC_RADIUS");
  spacing_factor = xmlGetNumber<float>(derenzo_el, "SPACING_FACTOR");
  activity_density = xmlGetNumber<float>(derenzo_el, "ACTIVITY_DENSITY");
  
  if (inner_cyl_rad > forbid_r)
    std::cout << "Warning: INNER_CYL_RADIUS > FORBID_RADIUS";
  
  uint16_t n_sectors = 6;
  AngleType step_angle = 2*M_PI/n_sectors;
  AngleType half_angle = step_angle/2;
  std::vector<uint16_t> n_rods(n_sectors, 0);
  
  for (uint16_t sector=0; sector<n_sectors; sector++) {
    AngleType cur_angle = start_angle + sector*step_angle;
    SpaceType cur_radius = min_rod_r + sector*inc_r;
    sectors_radii.push_back(cur_radius);
    float distance = spacing_factor*2*cur_radius;
    float x_max = (pht_max_r-safety_distance-cur_radius);
    std::vector<ThreeVector> sector_centers;
    
    float x_pos = forbid_r + cur_radius;
    float y_pos = 0;
    float cur_x_max = 0;
    uint16_t n_rod_s = 1;
    
    do {
      cur_x_max = x_pos;
      float temp_y = y_pos;
      for (uint16_t i=0; i<n_rod_s; i++) {
        sector_centers.push_back( ThreeVector(x_pos, temp_y, 0) );
        temp_y -= distance;
      }
      n_rod_s++;
      y_pos += distance*std::sin(half_angle);
      x_pos += distance*std::cos(half_angle);
      
    } while (x_pos < x_max);
    
    for (uint16_t i=0; i<sector_centers.size(); i++) {
      SpaceType shift = x_max-cur_x_max;
      ThreeVector shifted = sector_centers[i] + ThreeVector(shift, 0, 0);
      sector_centers[i] = shifted.rotateZnotInPlace(cur_angle);
    }
    
    cyl_positions.push_back(sector_centers);    
    
  }
  
}

void DerenzoPhantom::print() const
{
  std::cout << "Printing configuration:" << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "PHANTOM_Z: " << pht_h << std::endl;
  std::cout << "FORBID_RADIUS: " << forbid_r << std::endl;
  std::cout << "INNER_CYL_RADIUS: " << inner_cyl_rad << std::endl;
  std::cout << "SAFETY_DISTANCE: " << safety_distance << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "START_ANGLE: " << start_angle << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "ROD_Z: " << rod_h << std::endl;
  std::cout << "MIN_ROD_RADIUS: " << min_rod_r << std::endl;
  std::cout << "INC_RADIUS: " << inc_r << std::endl;
  std::cout << "SPACING_FACTOR: " << spacing_factor << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "ACTIVITY_DENSITY: " << activity_density << std::endl;
}
