/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vPhantomCreator.h"

vPhantomCreator* vPhantomCreator::init(std::string p_type_p,
                                       std::string config_p,
                                       std::string tag_p,
                                       std::string fov_tag_p
                                      )
{
  if (p_type_p == "cylinder")
    return new PhantomCreatorCylinder(config_p, tag_p, fov_tag_p);
  else if (p_type_p == "derenzo")
    return new PhantomCreatorDerenzo(config_p, tag_p, fov_tag_p);
  else if (p_type_p == "rodsgrid")
    return new PhantomCreatorRodsGrid(config_p, tag_p, fov_tag_p);
  else if (p_type_p == "planar")
    return new PhantomCreatorPlanar(config_p, tag_p, fov_tag_p);
  else
    throw std::string("Unknown phantom type");
}



void vPhantomCreator::construct()
{
  if (g_fn == "")
    throw std::string("construct(), geometry filename not set");
  if (p_fn == "")
    throw std::string("construct(), physics and digitizer filename not set");
  if (o_fn == "")
    throw std::string("construct(), out filename not set");
  if (time_length == 0 && !vis_points)
    throw std::string("construct(), time_length not set");
  
  simulationConstruction();
}

void vPhantomCreator::appendGeometry()
{
  std::cout << "Appending geometry..." << std::endl;
  
  std::ifstream g_file;
  g_file.open(g_fn);
  if (!g_file.is_open())
    throw std::string("Error opening " + g_fn + " for reading");
  
  std::ofstream out_file;
  out_file.open(o_fn + ".mac");
  if (!out_file.is_open())
    throw std::string("Error opening " + o_fn + ".mac for writing");
  
  std::string content = "";
  
  while ( ! g_file.eof() )
    content += g_file.get();
  
  content.erase(content.end()-1);     // erase last character
  g_file.close();
  
  out_file << content;                 // output
  out_file.close();
}


void vPhantomCreator::appendPhysics()
{
  std::cout << "Appending physics and digitizer..." << std::endl;
  
  
  std::ifstream p_file;
  p_file.open(p_fn);
  if (!p_file.is_open())
    throw std::string("Error opening " + p_fn + " for reading");
  
  std::ofstream out_file;
  // truncating file
  out_file.open(o_fn + ".mac", std::fstream::app);
  if (!out_file.is_open())
    throw std::string("Error opening " + o_fn + ".mac for writing");
  
  std::string content = "";
  
  while ( ! p_file.eof() )
    content += p_file.get();
  
  content.erase(content.end()-1);     // erase last character
  p_file.close();
  
  out_file << content;                 // output
  out_file.close();
}


void vPhantomCreator::appendVisualization(uint16_t nPoints)
{
  std::cout <<"Appending visualization lines..." << std::endl;
  
  
  std::ofstream out_file;
  out_file.open(o_fn + ".mac", std::fstream::app);
  if (!out_file.is_open())
    throw std::string("Error opening " + o_fn + ".mac for appending");
  
  out_file << "#=====================================================";
  out_file << std::endl;
  out_file << "# VISUALIZATION" << std::endl;
  out_file << "#=====================================================";
  out_file << std::endl;
  
  out_file << "/vis/open OGLI" << std::endl;
  out_file << "/vis/drawVolume" << std::endl;
  out_file << "/vis/viewer/set/viewpointThetaPhi 0 0" << std::endl;
  out_file << "/vis/viewer/zoom 1" << std::endl;
  out_file << "/tracking/storeTrajectory 1" << std::endl;
  out_file << "/vis/scene/add/trajectories" << std::endl;
  out_file << "/vis/scene/add/hits" << std::endl;
  out_file << "/vis/scene/add/axes" << std::endl;
  out_file << "/vis/scene/endOfEventAction accumulate" << std::endl;
  out_file << "/vis/viewer/set/background black" << std::endl;
  out_file << std::endl;
  
  for (std::string source : sources_list) {
    out_file << "/gate/source/" << source << "/visualize " << nPoints;
    out_file<< " green 1" << std::endl;
  }
  
  out_file << std::endl << std::endl;
  
  out_file.close();
}


void vPhantomCreator::appendRun(std::string root_sim_name)
{
  std::cout <<"Appending run lines..." << std::endl;
  
  std::ofstream out_file;
  out_file.open(o_fn + ".mac", std::fstream::app);
  if (!out_file.is_open())
    throw std::string("Error opening " + o_fn + ".mac for appending");
  
  out_file << "#=====================================================";
  out_file << std::endl;
  out_file << "# ACQUISITION" << std::endl;
  out_file << "#=====================================================";
  out_file << std::endl;
  // Output
  out_file << "/gate/output/verbose                        2" << std::endl;
  out_file << "/gate/output/ascii/disable" << std::endl;
  out_file << "/gate/output/root/enable" << std::endl;
  out_file << "/gate/output/root/setFileName               " << root_sim_name;
  out_file << "_seed_" << std::to_string(seed) << "" << std::endl;
  out_file << "/gate/output/root/setRootHitFlag            0" << std::endl;
  out_file << "/gate/output/root/setRootSinglesFlag        0" << std::endl;
  out_file << "/gate/output/root/setRootCoincidencesFlag   1" << std::endl;
  // #/gate/output/root/setRootdelayFlag                      {AcquireRandom}
  // Random JamesRandom Ranlux64 MersenneTwister
  out_file << "/gate/random/setEngineName       MersenneTwister" << std::endl;
  out_file << "/gate/random/setEngineSeed       " << seed << std::endl;
  out_file << "/gate/random/verbose             0" << std::endl;
  // Start
  out_file << "/gate/application/setTimeStart       0 s" << std::endl;
  out_file << "/gate/application/setTimeStop        " << time_length;
  out_file << " s" << std::endl;
  out_file << "/gate/application/startDAQ" << std::endl;
}

void vPhantomCreator::createCylSource(std::ofstream& out_f,
                                      std::string name,
                                      FloatType activity_p,
                                      FloatType radius,
                                      FloatType halfz, 
                                      const ThreeVector& center)
{
  out_f << "/gate/source/addSource " << name << std::endl;
  out_f << "/gate/source/"<<name<<"/setActivity " << activity_p;
  out_f << " Bq" << std::endl;
  if (source_type == BackToBack) {
    out_f << "/gate/source/"<<name<<"/setType backtoback" << std::endl;
    out_f << "/gate/source/"<<name<<"/gps/particle gamma" << std::endl;
    out_f << "/gate/source/"<<name<<"/gps/energytype Mono" << std::endl;
    out_f << "/gate/source/"<<name<<"/gps/monoenergy 0.511 MeV" << std::endl;
  } 
  else if (source_type == Positron) {
    out_f << "/gate/source/"<<name<<"/gps/particle e+" << std::endl;
    
    if (energy) {
      out_f << "/gate/source/"<<name<<"/gps/energytype Mono" << std::endl;
      out_f << "/gate/source/"<<name<<"/gps/monoenergy " << energy << "MeV" << std::endl;
    }
    else
      out_f << "/gate/source/"<<name<<"/gps/energytype Fluor18" << std::endl;
  }
  
  if (unstable > 0) {
    out_f << "/gate/source/"<<name<<"/setForcedUnstableFlag true" << std::endl;
    out_f << "/gate/source/"<<name<<"/setForcedHalfLife " << unstable << " s" << std::endl;
  }
  out_f << "/gate/source/"<<name<<"/gps/type Volume" << std::endl;
  out_f << "/gate/source/"<<name<<"/gps/shape Cylinder" << std::endl;
  out_f << "/gate/source/"<<name<<"/gps/radius " << radius << " mm" << std::endl;
  out_f << "/gate/source/"<<name<<"/gps/halfz " << halfz << " mm" << std::endl;
  out_f << "/gate/source/"<<name<<"/gps/angtype iso" <<std::endl;
  if (emits_2D) {
    out_f << "/gate/source/"<<name<<"/gps/mintheta 90. deg" <<std::endl;
    out_f << "/gate/source/"<<name<<"/gps/maxtheta 90. deg" <<std::endl;
    out_f << "/gate/source/"<<name<<"/gps/minphi 0. deg" <<std::endl;
    out_f << "/gate/source/"<<name<<"/gps/maxphi 360. deg" <<std::endl;
  }
  out_f << "/gate/source/" << name << "/gps/centre";
  out_f << " " << center.x();
  out_f << " " << center.y();
  out_f << " " << center.z() << " mm" << std::endl;
  out_f << std::endl;
}
