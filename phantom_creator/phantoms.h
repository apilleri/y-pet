/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef PHANTOM_CREATOR_PHANTOMS_H
#define PHANTOM_CREATOR_PHANTOMS_H

#include "dataTypes.h"
#include "f_utils.h"

#include "tinyxml2.h"
using namespace tinyxml2;

class CylinderPhantom {
  
  SpaceType pht_min_r;
  // min radius of the attenuation phantom (mm) 
  SpaceType pht_max_r;
  // max radius of the attenuation phantom (mm) 
  SpaceType pht_border_h;
  // height of the attenuation phantom (mm) 
  
  SpaceType active_min_r;
  // min radius of the source (mm) 
  SpaceType active_max_r;
  // max radius of the source (mm) 
  SpaceType active_z;
  // height of the source (mm) 
  
  SpaceType centre_x, centre_y, centre_z;
  // center of the cylinder
  
  FloatType activity_density_v;
  // total activity of the source (Bequerell)
  
public:
  CylinderPhantom(std::string config, std::string tag);
  
  SpaceType pMinR() const { return pht_min_r; }
  
  SpaceType pMaxR() const { return pht_max_r; }
  
  SpaceType pBrdH() const { return pht_border_h; }

  SpaceType aMinR() const { return active_min_r; }

  SpaceType aMaxR() const { return active_max_r; }

  SpaceType aZ() const { return active_z; }

  SpaceType cX() const { return centre_x; }

  SpaceType cY() const { return centre_y; }
  
  SpaceType cZ() const { return centre_z; }
  
  FloatType activityDensity() const { return activity_density_v; }
  
  SpaceType activeVolume() const 
  {
    return  M_PI*active_z*(active_max_r*active_max_r - active_min_r*active_min_r);
    
  }
  
  void print() const;
  
};

class DerenzoPhantom {
  
  SpaceType pht_max_r;
  // radius of the attenuation phantom (mm), computed from the FOV
  SpaceType pht_h;
  // height of the attenuation phantom  (mm) in z
  SpaceType forbid_r;
  // safety distance from the center of the phantom (XY plane)
  SpaceType inner_cyl_rad;
  // radius of the uniform central cylinder (mm)
  SpaceType safety_distance;
  // safety distance from the border of the cylinder phantom (XY plane)
  AngleType start_angle;
  // start angle of the first sector
  SpaceType rod_h;
  // height of every cylinder source (mm)
  SpaceType min_rod_r;
  // radius of the cylinder source with minimum radius (mm)
  SpaceType inc_r;
  // increment step radius (mm)
  float spacing_factor;
  // spacing factor between the centers of two consecutive rods in x and y.
  float activity_density;
  // activity density (Bequerell)
  
  std::vector< std::vector<ThreeVector> > cyl_positions;
  std::vector<SpaceType> sectors_radii;
  
public:
  DerenzoPhantom(std::string config, std::string tag, SpaceType p_r);
  
  SpaceType pR() const { return pht_max_r; }
  
  SpaceType pH() const { return pht_h; }
  
  SpaceType forbidR() const { return forbid_r; }
  
  SpaceType innerCylR() const { return inner_cyl_rad; }
  
  SpaceType safetyDistance() const { return safety_distance; }
  
  SpaceType startAngle() const { return start_angle; }
  
  SpaceType rodH() const { return rod_h; }
  
  SpaceType rodMinR() const { return min_rod_r; }
  
  SpaceType spacingF() const { return spacing_factor; }
  
  SpaceType activityDensity() const { return activity_density; }
  
  const std::vector< std::vector<ThreeVector> >& positions() const
  {
    return cyl_positions;
  }
  
  const std::vector<SpaceType>& radii() const
  {
    return sectors_radii;
  }
  
  void print() const;
};



class DerenzoSpherePhantom {
  
  SpaceType pht_r;
  // radius of the attenuation phantom (mm) 
  SpaceType pht_h;
  // height of the attenuation phantom  (mm) in z
  SpaceType forbid_r;
  // safety distance from the center of the phantom (XY plane)
  SpaceType safety_distance;
  // safety distance from the border of the cylinder phantom (XY plane)
  AngleType start_angle;
  // start angle of the first sector
  SpaceType min_sphere_r;
  // radius of the spherical source with minimum radius (mm)
  SpaceType inc_r;
  // increment step radius (mm)
  float spacing_factor;
  // spacing factor between the centers of two consecutive rods in x and y.
  float activity_density;
  // activity density (Bequerell)
  
  std::vector< std::vector<ThreeVector> > spheres_positions;
  std::vector<SpaceType> sectors_radii;
  
public:
  DerenzoSpherePhantom(std::string config, std::string tag, SpaceType p_r);
  
  SpaceType pR() const { return pht_r; }
  
  SpaceType pH() const { return pht_h; }
  
  SpaceType forbidR() const { return forbid_r; }
  
  SpaceType safetyDistance() const { return safety_distance; }
  
  SpaceType startAngle() const { return start_angle; }
  
  SpaceType sphereMinR() const { return min_sphere_r; }
  
  SpaceType spacingF() const { return spacing_factor; }
  
  SpaceType activityDensity() const { return activity_density; }
  
  const std::vector< std::vector<ThreeVector> >& positions() const
  {
    return spheres_positions;
  }
  
  const std::vector<SpaceType>& radii() const
  {
    return sectors_radii;
  }
  
  void print() const;
};




class PlanarPhantom {
  
  // planar size along the three dimensions
  SpaceType p_x, p_y, p_z;
  // size of the active along the three dimensions
  SpaceType a_x, a_y, a_z;
  // number  of planar views
  uint16_t n_views;
  // angle of the first planar
  AngleType start_angle;
  //number of sector to skip between every view.
  uint16_t sector_skip;
  //number of sectors to erase next to the planar
  uint16_t sector_erase;
  //direction of rotation (CLOKWISE or ANTICLOCKWISE)
  std::string rotation_v;  
  
  FloatType activity_v;
  
  std::vector<AngleType> views_v;
  
  std::vector< std::vector<detID_t> > discarded_sectors;
  
public:
  PlanarPhantom(std::string config, std::string tag);
  
  SpaceType pX() const { return p_x; }
  
  SpaceType pY() const { return p_y; }
  
  SpaceType pZ() const { return p_z; }
  
  SpaceType aX() const { return a_x; }
  
  SpaceType aY() const { return a_y; }

  SpaceType aZ() const { return a_z; }
  
  uint16_t nViews() const { return n_views; }
  
  AngleType startAngle() const { return start_angle; }
  
  uint16_t sectorSkip() const { return sector_skip; }
  
  uint16_t sectorErase() const { return sector_erase; }
  
  std::string rotation() const { return rotation_v; }
  
  FloatType activity() const { return activity_v; }
  
  const std::vector<AngleType>& views() const { return views_v; }
  
  const std::vector< std::vector<detID_t> >& discardedSectors() const
  {
    return discarded_sectors;
  }
  
  void print() const;
};


class RodsGridPhantom {
  
};


#endif // PHANTOM_CREATOR_PHANTOMS_H
