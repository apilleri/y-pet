/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vPhantomCreator.h"

PhantomCreatorCylinder::PhantomCreatorCylinder(std::string config, std::string header, const std::string& fov_tag_p)
: vPhantomCreator(config, fov_tag_p)
{
  xml_config = config;
  pht = new CylinderPhantom(config, header);
  pht->print();
}


void PhantomCreatorCylinder::phantomConstruction()
{
  std::cout << "Generating the attenuation phantom..." << std::endl;
  
  std::ofstream out_file(o_fn + ".mac", std::fstream::app);
  if (!out_file.is_open())
    throw std::string("Error opening " + o_fn + ".mac for appending");
  
  
  out_file << "#=====================================================" << std::endl;
  out_file << "# PHANTOM" << std::endl;
  out_file << "#=====================================================" << std::endl;
  
  if (build_phantom) {
    // Definition of the plastic cylinder phantom
    out_file << "/gate/world/daughters/name PlasticCyl"<<std::endl;
    out_file << "/gate/world/daughters/insert cylinder"<<std::endl;
    out_file << "/gate/PlasticCyl/setMaterial PMMA"<<std::endl;
    out_file << "/gate/PlasticCyl/vis/setColor grey"<<std::endl;
    out_file << "/gate/PlasticCyl/vis/forceWireframe" <<std::endl;
    out_file << "/gate/PlasticCyl/geometry/setRmin " << pht->pMinR();
    out_file << " mm" << std::endl;
    out_file << "/gate/PlasticCyl/geometry/setRmax " << pht->pMaxR();
    out_file << " mm"<<std::endl;
    out_file << "/gate/PlasticCyl/geometry/setHeight " << pht->aZ();
    out_file << " mm"<<std::endl;
    out_file << "/gate/PlasticCyl/placement/setTranslation";
    out_file << " " << pht->cX();
    out_file << " " << pht->cY();
    out_file << " " << pht->cZ() << " mm" << std::endl;
    out_file << "/gate/PlasticCyl/attachPhantomSD" << std::endl;
    out_file << std::endl;
    
    // Definition of the plastic borders of the cylinder phantom
    out_file << "/gate/world/daughters/name PlasticBrd1"<<std::endl;
    out_file << "/gate/world/daughters/insert cylinder"<<std::endl;
    out_file << "/gate/PlasticBrd1/setMaterial PMMA"<<std::endl;
    out_file << "/gate/PlasticBrd1/vis/setColor grey"<<std::endl;
    out_file << "/gate/PlasticBrd1/vis/forceWireframe" <<std::endl;
    out_file << "/gate/PlasticBrd1/geometry/setRmin 0 mm" << std::endl;
    out_file << "/gate/PlasticBrd1/geometry/setRmax " << pht->pMaxR();
    out_file << " mm"<<std::endl;
    out_file << "/gate/PlasticBrd1/geometry/setHeight " << pht->pBrdH();
    out_file << " mm"<<std::endl;
    out_file << "/gate/PlasticBrd1/placement/setTranslation";
    out_file << " " << pht->cX();
    out_file << " " << pht->cY();
    out_file << " " << pht->cZ() + pht->aZ()/2 + pht->pBrdH()/2;
    out_file << " mm" << std::endl;
    out_file << "/gate/PlasticBrd1/attachPhantomSD" << std::endl;
    out_file << std::endl;
    
    out_file << "/gate/world/daughters/name PlasticBrd2"<<std::endl;
    out_file << "/gate/world/daughters/insert cylinder"<<std::endl;
    out_file << "/gate/PlasticBrd2/setMaterial PMMA"<<std::endl;
    out_file << "/gate/PlasticBrd2/vis/setColor grey"<<std::endl;
    out_file << "/gate/PlasticBrd2/vis/forceWireframe" <<std::endl;
    out_file << "/gate/PlasticBrd2/geometry/setRmin 0 mm" << std::endl;
    out_file << "/gate/PlasticBrd2/geometry/setRmax " << pht->pMaxR();
    out_file << " mm"<<std::endl;
    out_file << "/gate/PlasticBrd2/geometry/setHeight " << pht->pBrdH();
    out_file << " mm"<<std::endl;
    out_file << "/gate/PlasticBrd2/placement/setTranslation";
    out_file << " " << pht->cX();
    out_file << " " << pht->cY();
    out_file << " " << pht->cZ() - pht->aZ()/2 - pht->pBrdH()/2;
    out_file << " mm" << std::endl;
    out_file << "/gate/PlasticBrd2/attachPhantomSD" << std::endl;
    out_file << std::endl;
    
    // Definition of the water cylinder phantom containing the activity
    out_file << "/gate/world/daughters/name water_cyl"<<std::endl;
    out_file << "/gate/world/daughters/insert cylinder"<<std::endl;
    out_file << "/gate/water_cyl/setMaterial Water"<<std::endl;
    out_file << "/gate/water_cyl/vis/setColor white"<<std::endl;
    out_file << "/gate/water_cyl/vis/forceWireframe" <<std::endl;
    out_file << "/gate/water_cyl/geometry/setRmin " << pht->aMinR();
    out_file << " mm" << std::endl;
    out_file << "/gate/water_cyl/geometry/setRmax " << pht->aMaxR();
    out_file << " mm"<<std::endl;
    out_file << "/gate/water_cyl/geometry/setHeight " << pht->aZ();
    out_file << " mm"<<std::endl;
    out_file << "/gate/water_cyl/placement/setTranslation";
    out_file << " " << pht->cX();
    out_file << " " << pht->cY();
    out_file << " " << pht->cZ() << " mm" << std::endl;
    out_file <<"/gate/water_cyl/attachPhantomSD" << std::endl;
    out_file << std::endl;
  }
  
  if (pht->aMinR() > 0) {
    out_file <<"/gate/world/daughters/name cold_area" << std::endl;
    out_file <<"/gate/world/daughters/insert cylinder" << std::endl;
    out_file <<"/gate/cold_area/setMaterial Air" << std::endl;
    out_file <<"/gate/cold_area/vis/forceWireframe" << std::endl;
    out_file <<"/gate/cold_area/vis/setColor green " << std::endl;
    out_file <<"/gate/cold_area/geometry/setRmin 0.0 mm" << std::endl;
    out_file <<"/gate/cold_area/geometry/setRmax " << pht->aMinR();
    out_file << " mm" << std::endl;
    out_file <<"/gate/cold_area/geometry/setHeight " << pht->aZ();
    out_file << " mm" << std::endl;
    out_file <<"/gate/cold_area/placement/setTranslation";
    out_file << " " << pht->cX();
    out_file << " " << pht->cY();
    out_file << " " << pht->cZ() << " mm" << std::endl;
    out_file << std::endl;
  }
  

  
  out_file.close();
}

void PhantomCreatorCylinder::sourceConstruction()
{
  std::cout <<"Generating the sources..." << std::endl;
  
  std::ofstream out_file;
  out_file.open(o_fn + ".mac", std::fstream::app);
  if (!out_file.is_open())
    throw std::string("Error opening " + o_fn + ".mac for appending");
  
  out_file << "#=====================================================" << std::endl;
  out_file << "# SOURCE" << std::endl;
  out_file << "#=====================================================" << std::endl;

  sources_list.push_back("source_cyl");
  ThreeVector center(pht->cX(), pht->cY(), pht->cZ());
  FloatType tot_activity = pht->activeVolume()*pht->activityDensity();
  createCylSource(out_file, "source_cyl", tot_activity, pht->aMaxR(),
                  pht->aZ()/2, center);
  
  if (pht->aMinR() > 0) 
    out_file << "/gate/source/source_cyl/gps/Forbid cold_area" << std::endl;
  out_file << std::endl;
  
  out_file.close();
}

void PhantomCreatorCylinder::simulationConstruction()
{
  appendGeometry();
  
  phantomConstruction();
  
  appendPhysics();
  
  sourceConstruction();
  
  if (vis_points)
    appendVisualization(vis_points);
  else
    appendRun(o_fn);
}

void PhantomCreatorCylinder::imageConstruction()
{
  if (!FOV)
    throw std::string("FOV not initialized!");
  
  ImageType cylinder;
  ThreeVector center(pht->cX(), pht->cY(), pht->cZ());
  
  FloatType vox_activity = pht->activityDensity()*FOV->voxelVolume();
  
  if (pht->aMinR() == 0)
    cylinder = FOV->createCylinder(pht->aMaxR(), center, pht->aZ(), vox_activity);
  else {
    cylinder = FOV->createInitImage(0);
    
    ImageType inner = FOV->createCylinder(pht->aMinR(), center, pht->aZ(), vox_activity);
    ImageType outer = FOV->createCylinder(pht->aMaxR(), center, pht->aZ(), vox_activity);
    for (uint32_t vx=0; vx<cylinder.size(); vx++)
      cylinder[vx] = (outer[vx] - inner[vx]);
  }
  
  FOV->saveImage(o_fn, cylinder, false);
}
