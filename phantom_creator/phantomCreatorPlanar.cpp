/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vPhantomCreator.h"

PhantomCreatorPlanar::PhantomCreatorPlanar(std::string config, std::string tag, const std::string& fov_tag_p)
: vPhantomCreator(config, fov_tag_p)
{
  xml_config = config;
  pht = new PlanarPhantom(config, tag);
  pht->print(); 
}


void PhantomCreatorPlanar::phantomConstruction()
{
  std::cout << "Generating the attenuation phantom..." << std::endl;
  
  std::ofstream out_file(o_fn + ".mac", std::fstream::app);
  if (!out_file.is_open())
    throw std::string("Error opening " + o_fn + ".mac for appending");
  
  
  out_file << "#=====================================================" << std::endl;
  out_file << "# PHANTOM" << std::endl;
  out_file << "#=====================================================" << std::endl;
  
  // Definition of the plastic phantom
  out_file<<"/gate/world/daughters/name PlasticBox"<<std::endl;
  out_file<<"/gate/world/daughters/insert box"<<std::endl;
  out_file<<"/gate/PlasticBox/setMaterial PMMA"<<std::endl;
  out_file<<"/gate/PlasticBox/vis/setColor red"<<std::endl;
  out_file<<"/gate/PlasticBox/vis/forceWireframe" <<std::endl;
  out_file<<"/gate/PlasticBox/vis/setVisible 1" <<std::endl;
  out_file<<"/gate/PlasticBox/geometry/setXLength " << pht->pX() << " mm" << std::endl;
  out_file<<"/gate/PlasticBox/geometry/setYLength " << pht->pY() << " mm" << std::endl;
  out_file<<"/gate/PlasticBox/geometry/setZLength " << pht->pZ() << " mm" << std::endl;
  out_file<<"/gate/PlasticBox/placement/setRotationAxis 0 0 1" << std::endl;
  out_file<<"/gate/PlasticBox/placement/setRotationAngle " << cur_angle << " deg" << std::endl;
  out_file << "/gate/PlasticBox/placement/setTranslation 0 0 0 mm" << std::endl;
  out_file << "/gate/PlasticBox/attachPhantomSD" << std::endl << std::endl;
  
  // Definition of the water phantom
  out_file<<"/gate/PlasticBox/daughters/name WaterBox"<<std::endl;
  out_file<<"/gate/PlasticBox/daughters/insert box"<<std::endl;
  out_file<<"/gate/WaterBox/setMaterial Water"<<std::endl;
  out_file<<"/gate/WaterBox/vis/setColor white"<<std::endl;
  out_file<<"/gate/WaterBox/vis/forceWireframe" <<std::endl;
  out_file<<"/gate/WaterBox/vis/setVisible 1" <<std::endl;
  out_file<<"/gate/WaterBox/geometry/setXLength " << pht->aX() << " mm" << std::endl;
  out_file<<"/gate/WaterBox/geometry/setYLength " << pht->aY() << " mm" << std::endl;
  out_file<<"/gate/WaterBox/geometry/setZLength " << pht->aZ() << " mm" << std::endl;
  out_file<<"/gate/WaterBox/placement/setRotationAxis 0 0 1" << std::endl;
  out_file<<"/gate/WaterBox/placement/setRotationAngle 0 deg" << std::endl;
  out_file << "/gate/WaterBox/placement/setTranslation 0 0 0 mm" << std::endl;
  out_file << "/gate/WaterBox/attachPhantomSD" << std::endl;
  
}

void PhantomCreatorPlanar::sourceConstruction()
{
  std::cout <<"Generating the sources..." << std::endl;
  
  std::ofstream out_file(o_fn + ".mac", std::fstream::app);
  if (!out_file.is_open())
    throw std::string("Error opening " + o_fn + ".mac for appending");
  
  out_file << "#=====================================================" << std::endl;
  out_file << "# SOURCE" << std::endl;
  out_file << "#=====================================================" << std::endl;
  
  sources_list.push_back("planar_src");
  
  out_file << "/gate/source/addSource planar_src" << std::endl;
  out_file << "/gate/source/planar_src/setType backtoback" << std::endl;
  out_file << "/gate/source/planar_src/setActivity " << pht->activity() << " becquerel" << std::endl;
  if (source_type == BackToBack) {
    out_file << "/gate/source/planar_src/setType backtoback" << std::endl;
    out_file << "/gate/source/planar_src/gps/particle gamma" << std::endl;
    out_file << "/gate/source/planar_src/gps/energytype Mono" << std::endl;
    out_file << "/gate/source/planar_src/gps/monoenergy 0.511 MeV" << std::endl;
  } 
  else if (source_type == Positron) {
    out_file << "/gate/source/planar_src/gps/particle e+" << std::endl;
    
    if (energy) {
      out_file << "/gate/source/planar_src/gps/energytype Mono" << std::endl;
      out_file << "/gate/source/planar_src/gps/monoenergy " << energy << "MeV" << std::endl;
    }
    else
      out_file << "/gate/source/planar_src/gps/energytype Fluor18" << std::endl;
  }
  
  if (unstable > 0) {
    out_file << "/gate/source/planar_src/setForcedUnstableFlag true" << std::endl;
    out_file << "/gate/source/planar_src/setForcedHalfLife " << unstable << " s" << std::endl;
  }
  out_file << "/gate/source/planar_src/gps/type 			Volume" << std::endl;
  out_file << "/gate/source/planar_src/gps/shape 			Para" << std::endl;
  out_file << "/gate/source/planar_src/gps/halfx " << pht->aX()/2 << " mm" << std::endl;
  out_file << "/gate/source/planar_src/gps/halfy " << pht->aY()/2 << " mm" << std::endl;
  out_file << "/gate/source/planar_src/gps/halfz " << pht->aZ()/2 << " mm" << std::endl;
  out_file << "/gate/source/planar_src/gps/angtype iso" << std::endl;
  if (emits_2D) {
    out_file << "/gate/source/planar_src/gps/mintheta 90. deg" <<std::endl;
    out_file << "/gate/source/planar_src/gps/maxtheta 90. deg" <<std::endl;
    out_file << "/gate/source/planar_src/gps/minphi 0. deg" <<std::endl;
    out_file << "/gate/source/planar_src/gps/maxphi 360. deg" <<std::endl;
  }
  out_file << "/gate/source/planar_src/gps/centre 0.0 0.0 0.0 mm" << std::endl;
  out_file << "/gate/source/planar_src/attachTo WaterBox" << std::endl;
}

void PhantomCreatorPlanar::simulationConstruction()
{
  std::string base_name = o_fn;
  
  const std::vector<AngleType>& active_views = pht->views();
  
  for (uint16_t i=0; i<active_views.size(); i++) {
    
    cur_angle = active_views[i];
    o_fn = base_name + std::to_string(i);
    
    appendGeometry();
    
    if (build_phantom)
      phantomConstruction();
    
    appendPhysics();
    
    sourceConstruction();
    
    if (vis_points)
      appendVisualization(vis_points);
    else
      appendRun( o_fn + "_p" + std::to_string(i) );
    
    o_fn = base_name;
  }
}

void PhantomCreatorPlanar::imageConstruction()
{
  if (!FOV)
    throw std::string("FOV not initialized!");
  
  const std::vector<AngleType>& active_views = pht->views();
  
  for (uint16_t i=0; i<active_views.size(); i++) {
    ThreeVector active_size = ThreeVector(pht->aX(), pht->aY(), pht->aZ());  
    ImageType planar = FOV->createPlanarPhantom(ThreeVector(0,0,0),
                                                active_size,
                                                active_views[i]);
    
    FOV->saveImage(o_fn+ std::to_string(i), planar, false);
  }

}
