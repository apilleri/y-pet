/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "phantoms.h"

PlanarPhantom::PlanarPhantom(std::string config, std::string tag)
{
  XMLDocument* xml_doc = xmlLoad(config);
  
  // --- Parsing PLANAR_PHANTOM element
  const XMLElement *planar_el = xmlGetElement(xml_doc, tag.c_str());
  p_x = xmlGetNumber<SpaceType>(planar_el, "PHANTOM_X_SIZE");
  p_y = xmlGetNumber<SpaceType>(planar_el, "PHANTOM_Y_SIZE");
  p_z = xmlGetNumber<SpaceType>(planar_el, "PHANTOM_Z_SIZE");
  a_x = xmlGetNumber<SpaceType>(planar_el, "ACTIVE_X_SIZE");
  a_y = xmlGetNumber<SpaceType>(planar_el, "ACTIVE_Y_SIZE");
  a_z = xmlGetNumber<SpaceType>(planar_el, "ACTIVE_Z_SIZE");
  n_views = xmlGetNumber<uint16_t>(planar_el, "N_VIEWS");
  start_angle = xmlGetNumber<AngleType>(planar_el, "START_ANGLE");
  sector_skip = xmlGetNumber<uint16_t>(planar_el, "SECTOR_SKIP");
  sector_erase = xmlGetNumber<uint16_t>(planar_el, "SECTOR_ERASE");
  activity_v = xmlGetNumber<FloatType>(planar_el, "ACTIVITY");
  rotation_v = xmlGetText(planar_el, "ROTATION");
  
  const XMLElement *scanner_el = xmlGetElement(xml_doc, "SCANNER");
  std::string scanner_type = xmlGetText(scanner_el, "TYPE");
  
  if (scanner_type == "PLANAR") 
    views_v.push_back(start_angle);
  else if (scanner_type == "CYLINDRICAL") {
    uint16_t n_scanner_sectors = xmlGetNumber<uint16_t>(scanner_el, "SECTORS_NUMBER");
    
    AngleType step_angle = 360.0/n_scanner_sectors;
    
    views_v.push_back(start_angle);
    
    uint16_t i=1+sector_skip;   
    while ( views_v.size() < n_views ) {
      AngleType temp;
      if (rotation_v == "CLOCKWISE")
        temp = start_angle - i*step_angle;
      else if (rotation_v == "ANTICLOCKWISE")
        temp = start_angle + i*step_angle;
      else
        throw std::string("PlanarPhantom::PlanarPhantom Unknown rotation direction");
      
      if (temp < 0)
        temp += 360;
      if (temp >= 360)
        temp -= 360;
      
      views_v.push_back(temp);
      
      i += 1+sector_skip;
    }
    
    
  } else
    throw std::string ("Unknown Scanner Type");
  
  if (scanner_type == "CYLINDRICAL") {
    
    uint16_t n_scanner_sectors = xmlGetNumber<uint16_t>(scanner_el, "SECTORS_NUMBER");
    AngleType step_angle = 360.0/n_scanner_sectors;
    
    for (AngleType angle : views_v) {
      std::vector<uint16_t> local_vec;
      for (int16_t se=-sector_erase/2; se<=sector_erase/2; se++) {
        int16_t discarded_sector_1 = std::round( angle / step_angle)+se;
        if (discarded_sector_1<0)
          discarded_sector_1 += n_scanner_sectors;
        if (discarded_sector_1 >= n_scanner_sectors)
          discarded_sector_1 -= n_scanner_sectors;
        uint16_t discarded_sector_2 = (discarded_sector_1 + n_scanner_sectors/2 );
        discarded_sector_2 = discarded_sector_2 % n_scanner_sectors;
        local_vec.push_back(discarded_sector_1);
        local_vec.push_back(discarded_sector_2);
      }

      discarded_sectors.push_back(local_vec);
    }
  }
 
  
  
  
}

void PlanarPhantom::print() const
{
  std::cout << "Printing configuration:" << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "PHANTOM_X_SIZE: " << p_x << std::endl;
  std::cout << "PHANTOM_Y_SIZE: " << p_y << std::endl;
  std::cout << "PHANTOM_Z_SIZE: " << p_z << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "ACTIVE_X_SIZE: " << a_x << std::endl;
  std::cout << "ACTIVE_Y_SIZE: " << a_y << std::endl;
  std::cout << "ACTIVE_Z_SIZE: " << a_z << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "N_VIEWS: " << n_views << std::endl;
  std::cout << "START_ANGLE: " << start_angle << std::endl;
  std::cout << "SECTOR_SKIP: " << sector_skip << std::endl;
  std::cout << "SECTOR_ERASE: " << sector_erase << std::endl;
  std::cout << "ROTATION: " << rotation_v << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "ACTIVITY: " << activity_v << std::endl << std::endl;
  
  std::cout << "Angles and discarded sectors:" << std::endl;
  for (uint16_t v=0; v<n_views; v++) {
    std::cout << std::setw(3) << views_v[v] << "\t";
    for (uint16_t s : discarded_sectors[v])
      std::cout << std::setw(3) << s;
    std::cout << std::endl;
  }
    
}

