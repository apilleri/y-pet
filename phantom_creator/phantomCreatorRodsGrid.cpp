/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vPhantomCreator.h"

PhantomCreatorRodsGrid::PhantomCreatorRodsGrid(std::string config, std::string tag, const std::string& fov_tag_p)
: vPhantomCreator(config, fov_tag_p)
{  
  XMLDocument* xml_doc = xmlLoad(config);
  xml_config = config;
  
  // --- Parsing GRID element
  const XMLElement *grid_el = xmlGetElement(xml_doc, tag.c_str());
  phantom_radius = xmlGetNumber<SpaceType>(grid_el, "PHANTOM_RADIUS");
  phantom_height = xmlGetNumber<SpaceType>(grid_el, "PHANTOM_Z");
  safety_distance = xmlGetNumber<SpaceType>(grid_el, "SAFETY_DISTANCE");
  rod_radius = xmlGetNumber<SpaceType>(grid_el, "ROD_RADIUS");
  rod_height = xmlGetNumber<SpaceType>(grid_el, "ROD_Z");
  spacing_factor = xmlGetNumber<float>(grid_el, "SPACING_FACTOR");
  activity_density = xmlGetNumber<float>(grid_el, "ACTIVITY_DENSITY");

  std::cout << "Printing configuration:" << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "PHANTOM_RADIUS: " << phantom_radius << std::endl;
  std::cout << "PHANTOM_Z: " << phantom_height << std::endl;
  std::cout << "SAFETY_DISTANCE: " << safety_distance << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "ROD_Z: " << rod_height << std::endl;
  std::cout << "ROD_RADIUS: " << rod_radius << std::endl;
  std::cout << "SPACING_FACTOR: " << spacing_factor << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "ACTIVITY_DENSITY: " << activity_density << std::endl;
  
  float distance = spacing_factor*2*rod_radius;
  uint16_t n_rods = 0;
  // the algorithm iterates over the first quadrant and clones the cylinders 
  // into the other quadrant of x or y are different from zero.
  ThreeVector cur_pos;
  float x_pos = 0, y_pos = 0;
  do { // Y step
    
    x_pos = 0;
    
    do { // X step
      cur_pos = ThreeVector(x_pos, y_pos, 0);
      
      if (cur_pos.r() < (phantom_radius - safety_distance - rod_radius) ) {
        
        cyl_positions.push_back(cur_pos);
        n_rods++;
        if (x_pos != 0) {
          cyl_positions.push_back(ThreeVector(-x_pos, y_pos, 0));
          n_rods++;
        }
        if (y_pos != 0) {
          cyl_positions.push_back(ThreeVector(x_pos, -y_pos, 0));
          n_rods++;
        }
        if (x_pos != 0 && y_pos != 0) {
          cyl_positions.push_back(ThreeVector(-x_pos, -y_pos, 0));
          n_rods++;
        }
      }
      x_pos += distance;
      
    } while( cur_pos.r() < (phantom_radius - safety_distance) );
    
    y_pos += distance;
    
  } while( y_pos + rod_radius < (phantom_radius - safety_distance) );
  
  std::cout << "NUMBER OF RODS: " << n_rods << std::endl;
  
}

void PhantomCreatorRodsGrid::phantomConstruction()
{
  std::cout << "Generating the attenuation phantom..." << std::endl;
  std::ofstream out_file(o_fn + ".mac", std::fstream::app);
  if (!out_file.is_open())
    throw std::string("Error opening " + o_fn + ".mac for appending");
  
  
  out_file << "#=====================================================" << std::endl;
  out_file << "# PHANTOM" << std::endl;
  out_file << "#=====================================================" << std::endl;
  
  if (build_phantom) {
    // Definition of the plastic cylinder phantom
    out_file<<"/gate/world/daughters/name PlasticBox"<<std::endl;
    out_file<<"/gate/world/daughters/insert cylinder"<<std::endl;
    out_file<<"/gate/PlasticBox/setMaterial PMMA"<<std::endl;
    out_file<<"/gate/PlasticBox/vis/setColor grey"<<std::endl;
    out_file<<"/gate/PlasticBox/geometry/setRmin 0.0 mm" << std::endl;
    out_file<<"/gate/PlasticBox/geometry/setRmax "<< phantom_radius << " mm"<<std::endl;
    out_file<<"/gate/PlasticBox/geometry/setHeight "<< phantom_height << " mm"<<std::endl;
    out_file<<"/gate/PlasticBox/attachPhantomSD" << std::endl;
    out_file<<"/gate/PlasticBox/vis/forceWireframe"<<std::endl<<std::endl;
    
    for (uint16_t cur_cyl=0; cur_cyl < cyl_positions.size(); cur_cyl++) {
      out_file << "/gate/PlasticBox/daughters/name water_cyl" << cur_cyl << std::endl;
      out_file << "/gate/PlasticBox/daughters/insert cylinder" << std::endl;
      out_file << "/gate/water_cyl" << cur_cyl << "/setMaterial Water" << std::endl;
      out_file << "/gate/water_cyl" << cur_cyl << "/vis/setColor white" << std::endl;
      out_file << "/gate/water_cyl" << cur_cyl << "/geometry/setRmin 0.0 mm" << std::endl;
      out_file << "/gate/water_cyl" << cur_cyl << "/geometry/setRmax " << rod_radius << " mm" << std::endl;
      out_file << "/gate/water_cyl" << cur_cyl << "/geometry/setHeight " << rod_height << " mm" << std::endl;
      out_file << "/gate/water_cyl" << cur_cyl << "/placement/setTranslation";
      out_file << " " << cyl_positions[cur_cyl].x();
      out_file << " " << cyl_positions[cur_cyl].y();
      out_file << " " << 0 << " mm" << std::endl;
      out_file << "/gate/water_cyl" << cur_cyl << "/attachPhantomSD" << std::endl;
      out_file << std::endl;
    }
  }
  out_file.close();
}

void PhantomCreatorRodsGrid::sourceConstruction()
{
  std::cout <<"Generating the sources..." << std::endl;
  
  std::ofstream out_file(o_fn + ".mac", std::fstream::app);
  if (!out_file.is_open())
    throw std::string("Error opening " + o_fn + ".mac for appending");
  
  out_file << "#=====================================================" << std::endl;
  out_file << "# SOURCE" << std::endl;
  out_file << "#=====================================================" << std::endl;
  
  float rod_volume = M_PI * rod_radius*rod_radius * rod_height;
  float activity = activity_density  * rod_volume;
  
  for (uint16_t cur_cyl=0; cur_cyl < cyl_positions.size(); cur_cyl++) {
    
    std::string name = "source_cyl" + std::to_string(cur_cyl);
    sources_list.push_back(name);
    
    ThreeVector center(0,0,0);
    center.setX(cyl_positions[cur_cyl].x());
    center.setY(cyl_positions[cur_cyl].y());
    
    createCylSource(out_file, name, activity, rod_radius,
                    rod_height/2, center);
  }
  
  out_file << std::endl;
  out_file.close();
  
  std::cout << "Total active volume: " << rod_volume*cyl_positions.size();
  std::cout << " mm^3" << std::endl;
  std::cout << "Total activity: " << activity*cyl_positions.size()/1e6;
  std::cout << " MBq" << std::endl;
}

void PhantomCreatorRodsGrid::simulationConstruction()
{
  appendGeometry();
  
  phantomConstruction();
  
  appendPhysics();
  
  sourceConstruction();
  
  if (vis_points)
    appendVisualization(vis_points);
  else
    appendRun(o_fn);
  
  // dump of the positions of the cylinders, useful for data analisys
  
  std::ofstream out_txt(o_fn + "_centers.txt");
    
  for (const ThreeVector& entry : cyl_positions)
    out_txt << entry.x() << " " << entry.y() << std::endl;
  
  out_txt.close();
}

void PhantomCreatorRodsGrid::imageConstruction()
{
  if (!FOV)
    throw std::string("FOV not initialized!");
  
  ImageType grid = FOV->createCylindricalGrid(cyl_positions, rod_radius, rod_height);
  FOV->saveImage(o_fn, grid, false);
}
