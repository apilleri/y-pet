/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vPhantomCreator.h"

#include "progress_bar.hpp"

PhantomCreatorDerenzo::PhantomCreatorDerenzo(std::string config, std::string tag, const std::string& fov_tag_p)
: vPhantomCreator(config, fov_tag_p)
{
  pht = new DerenzoPhantom(config, tag, std::floor(FOV->vxN(0)*FOV->vxL(0)/2.0));
}


void PhantomCreatorDerenzo::phantomConstruction()
{
  std::cout << "Generating the attenuation phantom..." << std::endl;
  std::ofstream out_file(o_fn + ".mac", std::fstream::app);
  if (!out_file.is_open())
    throw std::string("Error opening " + o_fn + ".mac for appending");
  
  const std::vector< std::vector<ThreeVector> >& cyl_positions = pht->positions();
  const std::vector<SpaceType>& sectors_radii = pht->radii();
  
  out_file << "#=====================================================" << std::endl;
  out_file << "# PHANTOM" << std::endl;
  out_file << "#=====================================================" << std::endl;

  // Definition of the plastic cylinder phantom
  out_file<<"/gate/world/daughters/name PlasticBox"<<std::endl;
  out_file<<"/gate/world/daughters/insert cylinder"<<std::endl;
  out_file<<"/gate/PlasticBox/setMaterial PMMA"<<std::endl;
  out_file<<"/gate/PlasticBox/vis/setColor grey"<<std::endl;
  out_file<<"/gate/PlasticBox/geometry/setRmin "<<pht->innerCylR() << " mm" << std::endl;
  out_file<<"/gate/PlasticBox/geometry/setRmax "<<pht->pR() << " mm"<<std::endl;
  out_file<<"/gate/PlasticBox/geometry/setHeight "<<pht->pH() << " mm"<<std::endl;
  out_file<<"/gate/PlasticBox/attachPhantomSD" << std::endl;
  out_file<<"/gate/PlasticBox/vis/forceWireframe"<<std::endl<<std::endl;
  
  if ( pht->innerCylR()) {
    // Definition of the water cylinder of the inner centered cyl
    out_file<<"/gate/world/daughters/name water_inner_cyl"<<std::endl;
    out_file<<"/gate/world/daughters/insert cylinder"<<std::endl;
    out_file<<"/gate/water_inner_cyl/setMaterial Water"<<std::endl;
    out_file<<"/gate/water_inner_cyl/vis/setColor grey"<<std::endl;
    out_file<<"/gate/water_inner_cyl/geometry/setRmin 0 mm" << std::endl;
    out_file<<"/gate/water_inner_cyl/geometry/setRmax "<<pht->innerCylR() << " mm"<<std::endl;
    out_file<<"/gate/water_inner_cyl/geometry/setHeight "<<pht->pH() << " mm"<<std::endl;
    out_file<<"/gate/water_inner_cyl/attachPhantomSD" << std::endl;
    out_file<<"/gate/water_inner_cyl/vis/forceWireframe"<<std::endl<<std::endl;
  }
  
  for (uint16_t sector=0; sector<cyl_positions.size(); sector++) {
    for (uint16_t cyl=0; cyl < cyl_positions[sector].size(); cyl++) {
      std::string name = std::to_string(sector) + "_" + std::to_string(cyl);
      out_file << "/gate/PlasticBox/daughters/name water_cyl" << name << std::endl;
      out_file << "/gate/PlasticBox/daughters/insert cylinder" << std::endl;
      out_file << "/gate/water_cyl" << name << "/setMaterial Water" << std::endl;
      out_file << "/gate/water_cyl" << name << "/vis/setColor white" << std::endl;
      out_file << "/gate/water_cyl" << name << "/geometry/setRmin 0.0 mm" << std::endl;
      out_file << "/gate/water_cyl" << name << "/geometry/setRmax " << sectors_radii[sector] << " mm" << std::endl;
      out_file << "/gate/water_cyl" << name << "/geometry/setHeight " << pht->rodH() << " mm" << std::endl;
      out_file << "/gate/water_cyl" << name << "/placement/setTranslation";
      out_file << " " << cyl_positions[sector][cyl].x();
      out_file << " " << cyl_positions[sector][cyl].y();
      out_file << " " << 0 << " mm" << std::endl;
      out_file << "/gate/water_cyl" << name << "/attachPhantomSD" << std::endl;
      out_file << std::endl;
    }
  }
  
  out_file.close();
}

void PhantomCreatorDerenzo::sourceConstruction()
{
  std::cout <<"Generating the sources..." << std::endl;
  
  std::ofstream out_file(o_fn + ".mac", std::fstream::app);
  if (!out_file.is_open())
    throw std::string("Error opening " + o_fn + ".mac for appending");
  
  out_file << "#=====================================================" << std::endl;
  out_file << "# SOURCE" << std::endl;
  out_file << "#=====================================================" << std::endl;
  
  FloatType total_activity = 0;

  if ( pht->innerCylR()) {
    
    sources_list.push_back("inner_cyl");
    
    float activity = pht->activityDensity() * M_PI * pht->innerCylR()
                      * pht->innerCylR() * pht->rodH();
    total_activity += activity;
    
    createCylSource(out_file, "inner_cyl", activity, pht->innerCylR(),
                    pht->rodH()/2, ThreeVector(0,0,0));
  }
  
  // Creating the sectors
  const std::vector< std::vector<ThreeVector> >& cyl_positions = pht->positions();
  const std::vector<SpaceType>& sectors_radii = pht->radii();
  
  for (uint16_t sector=0; sector<cyl_positions.size(); sector++) {
    for (uint16_t cyl=0; cyl < cyl_positions[sector].size(); cyl++) {
    
      std::string name = "source_cyl" + std::to_string(sector);
      name += "_" + std::to_string(cyl);
      sources_list.push_back(name);
      
      float activity = pht->activityDensity() * M_PI * sectors_radii[sector]*sectors_radii[sector] * pht->rodH();
      total_activity += activity;
      
      ThreeVector center(0,0,0);
      center.setX(cyl_positions[sector][cyl].x());
      center.setY(cyl_positions[sector][cyl].y());
      createCylSource(out_file, name, activity, sectors_radii[sector],
                      pht->rodH()/2, center);
    }
  }
  
  out_file << std::endl;
  out_file.close();
  
  std::cout << "Total activity: " << total_activity/1e6 << " MBq" << std::endl;
}

void PhantomCreatorDerenzo::simulationConstruction()
{
  appendGeometry();
  
  if (build_phantom)
    phantomConstruction();
  
  appendPhysics();
  
  sourceConstruction();
  
  if (vis_points)
    appendVisualization(vis_points);
  else
    appendRun(o_fn);
  
  // dump of the positions of the cylinders, useful for data analisys
  
  std::ofstream out_txt(o_fn + "_centers.txt");
  
  const std::vector< std::vector<ThreeVector> >& cyl_positions = pht->positions();
  const std::vector<SpaceType>& sectors_radii = pht->radii();

  out_txt << "#Sectors radii" << std::endl;
  for (uint16_t sector=0; sector<cyl_positions.size(); sector++)
    out_txt << sectors_radii[sector] << " ";
  out_txt << std::endl;
  
  for (uint16_t sector=0; sector<cyl_positions.size(); sector++) {
    out_txt << "\n\n#Sector " << sector << std::endl;
    
    out_txt << "#x" << std::endl;
    for (const ThreeVector& entry : cyl_positions[sector] )
      out_txt << entry.x() << " ";
    out_txt << "\n#y" << std::endl;
    for (const ThreeVector& entry : cyl_positions[sector] )
      out_txt << entry.y() << " ";

  }
  
  out_txt.close();
  
}

void PhantomCreatorDerenzo::imageConstruction()
{
  if (!FOV)
    throw std::string("FOV not initialized!");
  
  FloatType vox_activity = pht->activityDensity()*FOV->voxelVolume();
  
  const std::vector< std::vector<ThreeVector> >& cyl_positions = pht->positions();
  const std::vector<SpaceType>& sectors_radii = pht->radii();
  
  ImageType image = FOV->createInitImage(0);
  
  ImageType inner = FOV->createCylinder(pht->innerCylR(),
                                        ThreeVector(0,0,0), pht->rodH(), vox_activity);
  for (uint32_t i=0; i<image.size(); i++)
    image[i] += inner[i];
  
  uint64_t n_cylinders = 0;
  for (uint16_t sector=0; sector<cyl_positions.size(); sector++)
    n_cylinders += cyl_positions[sector].size();
  
  
  
  ProgressBar progress(n_cylinders);
  for (uint16_t sector=0; sector<cyl_positions.size(); sector++) {
    uint16_t n_rods = cyl_positions[sector].size();
    #pragma omp parallel for
//     for (const ThreeVector& c : cyl_positions[sector]) {
    for (uint16_t c_cyl=0; c_cyl<n_rods; c_cyl++) {
      const ThreeVector& c = cyl_positions[sector][c_cyl];
      SpaceType cur_r = sectors_radii[sector];
      ImageType cur_cyl = FOV->createCylinder(cur_r, c, pht->rodH(), vox_activity);
      // TODO sum of ImageType
      for (uint32_t i=0; i<image.size(); i++)
        image[i] += cur_cyl[i];
      #pragma omp critical
      {
        ++progress;
      }
    }
  }
  
  FOV->saveImage(o_fn, image, false);
}
