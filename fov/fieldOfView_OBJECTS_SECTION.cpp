/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "fieldOfView.h"

// ImageType FieldOfView::createPolygon(const ThreeVector& poly_radius,
//                                      const std::vector<ThreeVector> &poly_points) const
// {
//   // polygon_radius is the diagonal
//   using PolygonType = itk::PolygonSpatialObject< ImageDimension >;
//   using SpatialObjectToImageFilterType = itk::SpatialObjectToImageFilter< PolygonType, itkImageType >;
//   
//   PolygonType::Pointer polygon = PolygonType::New();
//   
//   itkImageType::SizeType size;
//   itkImageType::SpacingType spacing;
//   itkImageType::PointType center;
//   std::tie(size, spacing, center) = generateItkImageInfo();
//   
//   // discarding third dimension (z)
//   size[2] = 1;
//   spacing[2] = 1;
//   center[2] = 0;
//   
//   typename PolygonType::PointType point;
//   typename PolygonType::PolygonPointType polygonPoint;
//   
//   point[2] = 0;
//   for (uint16_t i=0; i<poly_points.size(); i++) {
//     ThreeVector direction = poly_points[i] / poly_points[i].r();
//     point[0] = direction.x() * poly_radius.x() + center[0];
//     point[1] = direction.y() * poly_radius.y() + center[1];
//     polygonPoint.SetPositionInObjectSpace(point);
//     polygon->GetPoints().push_back(polygonPoint);
//   }
//   
//   polygon->SetIsClosed(true);
//   polygon->Update();
//   
//   SpatialObjectToImageFilterType::Pointer imageFilter = SpatialObjectToImageFilterType::New();
//   imageFilter->SetInput(polygon);
//   imageFilter->SetSize(size);
//   imageFilter->SetSpacing(spacing);
//   imageFilter->SetUseObjectValue(true);
//   imageFilter->Update();
//   
//   uint32_t n_vox_xy = voxels_N[0] * voxels_N[1];
//   ImageType polygon_img(voxels_N);
//   
//   for (uint16_t i = 0; i<voxels_N[2];  i++) {
//     std::copy(imageFilter->GetOutput()->GetBufferPointer(),
//               imageFilter->GetOutput()->GetBufferPointer() + n_vox_xy,
//               polygon_img.begin() + i*n_vox_xy
//     );
//   }
//   
//   return polygon_img;
// }

// ImageType FieldOfView::createPolygon(const ThreeVector& poly_radius,
//                                      const std::vector<ThreeVector> &poly_points) const
// {
//   // polygon_radius is the diagonal
//   using PolygonType = itk::PolygonSpatialObject< ImageDimension >;
//   using SpatialObjectToImageFilterType = itk::SpatialObjectToImageFilter< PolygonType, itkImageType >;
//   
//   PolygonType::Pointer polygon = PolygonType::New();
//   
//   itkImageType::SizeType size;
//   itkImageType::SpacingType spacing;
//   itkImageType::PointType center;
//   std::tie(size, spacing, center) = generateItkImageInfo();
//   
//   // discarding third dimension (z)
//   size[2] = 1;
//   spacing[2] = 1;
//   center[2] = 0;
//   
//   typename PolygonType::PointType point;
//   typename PolygonType::PolygonPointType polygonPoint;
//   
//   point[2] = 0;
//   for (uint16_t i=0; i<poly_points.size(); i++) {
// //     ThreeVector direction = poly_points[i] / poly_points[i].r();
// //     point[0] = direction.x() * poly_radius.x() + center[0];
// //     point[1] = direction.y() * poly_radius.y() + center[1];
//     point[0] = poly_points[i].x() + center[0];
//     point[1] = poly_points[i].y() + center[1];
//     polygonPoint.SetPositionInObjectSpace(point);
//     polygon->GetPoints().push_back(polygonPoint);
//   }
//   
//   polygon->SetIsClosed(true);
//   polygon->Update();
//   
//   SpatialObjectToImageFilterType::Pointer imageFilter = SpatialObjectToImageFilterType::New();
//   imageFilter->SetInput(polygon);
//   imageFilter->SetSize(size);
//   imageFilter->SetSpacing(spacing);
//   imageFilter->SetUseObjectValue(true);
//   imageFilter->Update();
//   
//   uint32_t n_vox_xy = voxels_N[0] * voxels_N[1];
//   ImageType polygon_img(total_voxels_N);
//   
//   for (uint16_t i = 0; i<voxels_N[2];  i++) {
//     std::copy(imageFilter->GetOutput()->GetBufferPointer(),
//               imageFilter->GetOutput()->GetBufferPointer() + n_vox_xy,
//               polygon_img.begin() + i*n_vox_xy
//     );
//   }
//   
//   return polygon_img;
// }

ImageType FieldOfView::createCylinder(SpaceType radius,
                                      ThreeVector cyl_center,
                                      SpaceType height,
                                      FloatType value) const
{
  using TubeType = itk::TubeSpatialObject<ImageDimension>;
  using SpatialObjectToImageFilterType = itk::SpatialObjectToImageFilter<TubeType, itkImageType>;
  
  itkImageType::SizeType size;
  itkImageType::SpacingType spacing;
  itkImageType::PointType center;
  std::tie(size, spacing, center) = generateItkImageInfo();
  
  TubeType::Pointer    tube = TubeType::New();
  
//   SpaceType z_step = spacing[2] / 1;
  SpaceType start_z = center[2] + cyl_center[2] - height/2;
//   uint16_t n_points = height/z_step;
  
  typename TubeType::PointType         point;
  typename TubeType::TubePointType     tubePoint;
  typename TubeType::TubePointListType tubePointList;
  
//   for (uint16_t i=0; i<n_points+1; i++) {
//     point[0] = center[0] + cyl_center[0];
//     point[1] = center[1] + cyl_center[1];
//     point[2] = start_z +i*z_step;
// //     std::cout << start_z +i*z_step << std::endl;
//     tubePoint.SetPositionInObjectSpace(point);
//     tubePoint.SetRadiusInObjectSpace(radius);
//     tubePointList.push_back(tubePoint);
//   }
  

  point[0] = center[0] + cyl_center[0];
  point[1] = center[1] + cyl_center[1];
  point[2] = start_z;
  //     std::cout << start_z +i*z_step << std::endl;
  tubePoint.SetPositionInObjectSpace(point);
  tubePoint.SetRadiusInObjectSpace(radius);
  tubePointList.push_back(tubePoint);
  
  SpaceType stop_z = center[2] + cyl_center[2] + height/2;
  point[0] = center[0] + cyl_center[0];
  point[1] = center[1] + cyl_center[1];
  point[2] = stop_z;
  //     std::cout << start_z +i*z_step << std::endl;
  tubePoint.SetPositionInObjectSpace(point);
  tubePoint.SetRadiusInObjectSpace(radius);
  tubePointList.push_back(tubePoint);
  
  tube->SetPoints(tubePointList);
  tube->Update();
  
  SpatialObjectToImageFilterType::Pointer imageFilter = SpatialObjectToImageFilterType::New();
  imageFilter->SetInput(tube);
  imageFilter->SetSize(size);
  imageFilter->SetSpacing(spacing);
  imageFilter->SetUseObjectValue(true);
  imageFilter->Update();
  
  ImageType cylinder_img(total_voxels_N);
  std::copy(imageFilter->GetOutput()->GetBufferPointer(),
            imageFilter->GetOutput()->GetBufferPointer() + total_voxels_N,
            cylinder_img.begin());
  
  for (uint32_t i=0; i<total_voxels_N; i++)
    cylinder_img[i] *= value;
  
  return cylinder_img;
}


ImageType
FieldOfView::createCylindricalGrid(std::vector<ThreeVector> centers,
                                   SpaceType radius,
                                   SpaceType height
                                  ) const
{
  ImageType grid = createInitImage(0);
  
  for (const ThreeVector& c : centers) {
    ImageType cur_cyl = createCylinder(radius, c, height, 1);
    
    // TODO sum of ImageType
    for (uint32_t i=0; i<total_voxels_N; i++)
      grid[i] += cur_cyl[i];
  }
  
  return grid;
}
                           

ImageType FieldOfView::createPlanarPhantom(const ThreeVector& phantom_center,
                                           const ThreeVector& phantom_size,
                                           AngleType angle_deg) const
{
  itkImageType::SizeType size;
  itkImageType::SpacingType spacing;
  itkImageType::PointType center;
  std::tie(size, spacing, center) = generateItkImageInfo();
  
  using BoxType = itk::BoxSpatialObject< ImageDimension >;
  using SpatialObjectToImageFilterType = itk::SpatialObjectToImageFilter< BoxType, itkImageType >;
  using BoxTransformType = BoxType::TransformType;
  
  
  BoxType::Pointer box=BoxType::New();
  BoxType::SizeType CubeSize;
  CubeSize[0] = phantom_size.x();
  CubeSize[1] = phantom_size.y();
  CubeSize[2] = phantom_size.z();
  box->SetSizeInObjectSpace(CubeSize);
  BoxTransformType::Pointer transform = BoxTransformType::New();
  transform->SetIdentity();
  
  
  
  BoxTransformType::OutputVectorType  translation;
  translation[ 0 ] = -CubeSize[0]/2.0;
  translation[ 1 ] = -CubeSize[1]/2.0;
  translation[ 2 ] = -CubeSize[2]/2.0;
  transform->Translate( translation, false );
  
  const AngleType angle_rad = angle_deg * std::atan(1.0) / 45.0;
  TransformType::OutputVectorType axis;
  axis[0] = 0;
  axis[1] = 0;
  axis[2] = 1;
  transform->Rotate3D(axis, angle_rad, false);
  
  translation[ 0 ] = phantom_center.x()+center[0];
  translation[ 1 ] = phantom_center.y()+center[1];
  translation[ 2 ] = phantom_center.z()+center[2];
  transform->Translate( translation, false );
  
  
  box->SetObjectToParentTransform(transform);
  box->SetDefaultInsideValue(1);
  box->SetDefaultOutsideValue(0);
  box->Update();
  
  
  SpatialObjectToImageFilterType::Pointer imageFilter = SpatialObjectToImageFilterType::New();
  imageFilter->SetInput(box);
  imageFilter->SetSize(size);
  imageFilter->SetSpacing(spacing);
//   imageFilter->SetOrigin(origin);
  imageFilter->SetUseObjectValue(true);
  imageFilter->Update();
  
  ImageType planar(total_voxels_N);
  std::copy(imageFilter->GetOutput()->GetBufferPointer(),
            imageFilter->GetOutput()->GetBufferPointer() + total_voxels_N,
            planar.begin()
  );
  
//   rotateImage(planar.data(), planar.data(), angle, false);
  
  return planar;
  
}

