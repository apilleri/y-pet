/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "fieldOfView.h"
#include "timer.hpp"

#define EPSILON 1e-3


FieldOfView::FieldOfView(const std::string& config, const std::string& tag_p)
{
  XMLDocument* xml_doc = xmlLoad(config);
   
  // --- Parsing FOV child of the XML tree
  std::cout << "loading FOV: " << tag_p << std::endl;
  const XMLElement *fov_el = xmlGetElement(xml_doc, tag_p.c_str());
  voxels_N[0] = xmlGetNumber<uint16_t>(fov_el, "VOXEL_NUMBER_X");
  voxels_N[1] = xmlGetNumber<uint16_t>(fov_el, "VOXEL_NUMBER_Y");
  voxels_N[2] = xmlGetNumber<uint16_t>(fov_el, "VOXEL_NUMBER_Z");
  voxel_lengths[0] = xmlGetNumber<SpaceType>(fov_el, "VOXEL_LENGTH_X");
  voxel_lengths[1] = xmlGetNumber<SpaceType>(fov_el, "VOXEL_LENGTH_Y");
  voxel_lengths[2] = xmlGetNumber<SpaceType>(fov_el, "VOXEL_LENGTH_Z");

  
  total_voxels_N = voxels_N[0]*voxels_N[1]*voxels_N[2];
  
  fov_half_lengths[0] = voxels_N[0]*voxel_lengths[0]/2;
  fov_half_lengths[1] = voxels_N[1]*voxel_lengths[1]/2;
  fov_half_lengths[2] = voxels_N[2]*voxel_lengths[2]/2;
  
//   itk::MultiThreaderBase::SetGlobalDefaultNumberOfThreads( 20 );
  
  // computing masks;
  image_mask = createInitImage(1);
  rot_mask = createInitImage(1);
  
  const XMLElement *scanner_el = xmlGetElement(xml_doc, "SCANNER");
  std::string scanner_type = xmlGetText(scanner_el, "TYPE");
  
  if (scanner_type == "CYLINDRICAL") {
    uint16_t n_sectors = xmlGetNumber<uint16_t>(scanner_el, "SECTORS_NUMBER");
    AngleType angle_step = 360.0/n_sectors;
    AngleType cur_angle = angle_step;
    
    ImageType uniform = createInitImage(1);
    ImageType temp = createInitImage(0);
    
    timer msec_timer("msec");
    msec_timer.start("computing FOV rot mask", "\t", true);
    while( cur_angle < 90 ) {
      rotateImage2D(uniform.data(), temp.data(), cur_angle, false);
      for (uint32_t vx=0; vx<uniform.size(); vx++) {
        rot_mask[vx] *= temp[vx];
        rot_mask[vx] = std::floor(rot_mask[vx]);
      }
      cur_angle += angle_step;
    }
    msec_timer.stop();
    
    ThreeVector fov_p1, fov_p2;
    fov_p1.setX(+fov_half_lengths[0]);
    fov_p1.setY(-fov_half_lengths[1]);
    fov_p1.setZ(0);
    fov_p2.setX(+fov_half_lengths[0]);
    fov_p2.setY(+fov_half_lengths[1]);
    fov_p2.setZ(0);
    
    ThreeVector rot_fov_p1 = fov_p1.rotateZnotInPlace(-angle_step*M_PI/180);
    ThreeVector fot_fov_p2 = fov_p2.rotateZnotInPlace(-angle_step*M_PI/180);
    
    ThreeVector start = intersectionPoint(fov_p1, fov_p2, rot_fov_p1, fot_fov_p2);
    polygon_points.push_back(start);
    
    cur_angle = angle_step;
    while (cur_angle < 360) {
      start.rotateZ( angle_step*M_PI/180 );
      polygon_points.push_back(start);
      cur_angle += angle_step;
    }
    
    
//     for (uint16_t i=0; i<n_sectors/2; i++) {
//       ImageType temp = createInitImage(0);
//       rotateImage(uniform.data(), temp.data(), cur_angle, false);
// //       saveImage("rot"+std::to_string(i), temp);
//       for (uint32_t vx=0; vx<uniform.size(); vx++) {
//         rot_mask[vx] *= temp[vx];
//         rot_mask[vx] = std::floor(rot_mask[vx]);
//       }
//       cur_angle += angle_step;
//     }
    
//     saveImage("rot", rot_mask);
    
    //THIS IS IMPORTANT OTHERWISE SPIKES FROM THE CORNER OF THE ROTATED IMAGE
    //PROPAGATES TO THE IMAGE AFTER SOME ITERATIONS
    #define CYL_PAD 5
    mask_radius = (voxels_N[0]/2.0 - CYL_PAD) * voxel_lengths[0];
    image_mask = createCenteredCylinder(mask_radius);
  } 
  else {
    ThreeVector start;
    start.setX(-fov_half_lengths[0]);
    start.setY(-fov_half_lengths[1]);
    start.setZ(0);
    polygon_points.push_back(start);
    
    start.rotateZ( M_PI/2 );
    polygon_points.push_back(start);
    
    start.rotateZ( M_PI/2 );
    polygon_points.push_back(start);
    
    start.rotateZ( M_PI/2 );
    polygon_points.push_back(start);
    
  }
  
  SpaceType half_x = voxels_N[0]*voxel_lengths[0]/2;
  SpaceType half_y = voxels_N[1]*voxel_lengths[1]/2;
  outer_radius = std::sqrt( std::pow(half_x, 2) + std::pow(half_y, 2) );

  std::cout << "Fov initialized." << std::endl;
}


void FieldOfView::printInfo(const std::string& filename) const
{
  std::ofstream txt_file;
  txt_file.open(filename);
  txt_file << "FOV size:\n";
  txt_file << "X:" << std::setw(7) << voxels_N[0] << std::endl;
  txt_file << "Y:" << std::setw(7) << voxels_N[1] << std::endl;
  txt_file << "Z:" << std::setw(7) << voxels_N[2] << std::endl;
  txt_file << "Voxels number: " << std::setw(10) << total_voxels_N << std::endl;
  txt_file << "\nFOV spacing:\n";
  txt_file << "X:" << std::setw(7) << voxel_lengths[0] << std::endl;
  txt_file << "Y:" << std::setw(7) << voxel_lengths[1] << std::endl;
  txt_file << "Z:" << std::setw(7) << voxel_lengths[2] << std::endl;
  txt_file << "\nFOV physical lengths:\n";
  txt_file << "X:" << std::setw(7) << voxels_N[0]*voxel_lengths[0] << std::endl;
  txt_file << "Y:" << std::setw(7) << voxels_N[1]*voxel_lengths[1] << std::endl;
  txt_file << "Z:" << std::setw(7) << voxels_N[2]*voxel_lengths[2] << std::endl;
  txt_file.close();
}


void
FieldOfView::saveLOR(const std::string& name, std::vector<MatrixEntry> row) const
{
  ImageType fov = createInitImage(0);
  
  for (const MatrixEntry& entry : row)
    fov[entry.col] = entry.v;

  this->saveImage(name, fov, false);
}

std::tuple<itkImageType::SizeType, itkImageType::SpacingType, itkImageType::PointType>
FieldOfView::generateItkImageInfo() const
{
  itkImageType::SizeType size;
  size[0] = voxels_N[0];  // size along X
  size[1] = voxels_N[1];  // size along Y
  size[2] = voxels_N[2];  // size along Z
  itkImageType::SpacingType spacing;
  spacing[0] = voxel_lengths[0];
  spacing[1] = voxel_lengths[1];
  spacing[2] = voxel_lengths[2];
  itkImageType::PointType center;
  center[0] = spacing[0] * (size[0]-1) / 2.0;
  center[1] = spacing[1] * (size[1]-1) / 2.0;
  center[2] = spacing[2] * (size[2]-1) / 2.0;
  
  return std::make_tuple(size, spacing, center);
}


ImageType FieldOfView::createInitImage(FloatType value) const
{
  ImageType image;
  image.resize(total_voxels_N, value);

  return image;
}


void FieldOfView::saveImage(const std::string& filename,
                            const FloatType *image,
                            bool useImageMask) const
{
  std::string vx_size = std::to_string(voxels_N[0]) + "x" + std::to_string(voxels_N[1]) + "x" +  std::to_string(voxels_N[2]);
  std::string def_name = filename + "_" + vx_size + ".raw";
  
  std::ofstream out_raw;
  out_raw.open(def_name);
  if ( !out_raw.is_open() ) 
    throw std::string ( "Unable to open image file for creation: " + def_name);
  
  if (useImageMask) {
    ImageType to_save = createInitImage(0);
    for (uint32_t i=0; i<total_voxels_N; i++) {
      to_save[i] = image_mask[i] * image[i];
      if (to_save[i] < 0 )
        to_save[i] = 0;
    }
    const char *pointer = reinterpret_cast<const char *>(to_save.data());
    out_raw.write(pointer, total_voxels_N*sizeof(FloatType) );
  } else {
    const char *pointer = reinterpret_cast<const char *>(image);
    out_raw.write(pointer, total_voxels_N*sizeof(FloatType) ); 
  }
  
  //TODO check correct writing
  out_raw.close();
}


void FieldOfView::fixCorners(ImageType& image) const
{
  ImageType corners = createInitImage(0);
  #pragma omp parallel for
  for (uint32_t i=0; i<total_voxels_N; i++) 
    corners[i] = (1-image_mask[i]) * image[i];
  
  std::vector<FloatType> nne;
  for (uint32_t i=0; i<total_voxels_N; i++) {
    if (corners[i])
      nne.push_back( corners[i] );
  }
  FloatType median_value = median(nne);
  
  #pragma omp parallel for
  for (uint32_t i=0; i<total_voxels_N; i++) {
    if (corners[i])
      image[i] = median_value;
  }
}


void FieldOfView::applyRotMask(FloatType* image) const
{
//   #pragma omp parallel for
  for (uint32_t i=0; i<total_voxels_N; i++) 
    image[i] *= rot_mask[i];
}


ImageType FieldOfView::loadImage(const std::string& filename) const
{
  // when the image is loaded from disk, it is supposed the size is the one
  // of the real FOV, so it needs to be padded to the internal FOV size for 
  // a correct computation
  ImageType img;
  img.resize(total_voxels_N);
  
  std::ifstream image_file;
  image_file.open(filename);
  if ( !image_file.is_open() )
    throw std::string ( "Unable to open file " + filename + " for reading");
  
  image_file.read(reinterpret_cast<char *>(img.data()), total_voxels_N*sizeof(FloatType) );
  if (!image_file) {
    std::string error("Error reading image: expected ");
    error += std::to_string(total_voxels_N*sizeof(FloatType));
    error += " bytes but read only ";
    error += std::to_string(image_file.gcount());
    throw error;
  }
  image_file.close();
  
  
  return img;
}

ImportFilterType::Pointer FieldOfView::importImage(FloatType* img) const
{
  ImportFilterType::Pointer importFilter = ImportFilterType::New();
  // This filter requires the user to specify the size of the image to be
  // produced as output.  The `SetRegion()` method is used to this end.
  // The image size should exactly match the number of pixels available in the
  // locally allocated buffer.
  ImportFilterType::SizeType  size;
  size[0] = voxels_N[0];  // size along X
  size[1] = voxels_N[1];  // size along Y
  size[2] = voxels_N[2];  // size along Z
  
  ImportFilterType::IndexType start;
  start.Fill( 0 );
  ImportFilterType::RegionType region;
  region.SetIndex(start);
  region.SetSize(size);
  importFilter->SetRegion(region);
  
  itkImageType::SpacingType spacing;
  spacing[0] = voxel_lengths[0];
  spacing[1] = voxel_lengths[1];
  spacing[2] = voxel_lengths[2];
  importFilter->SetSpacing(spacing);
  
  // The buffer is passed to the ImportImageFilter with the
  // `SetImportPointer()` method. Note that the last argument of this method
  // specifies who will be responsible for deleting the memory block once it
  // is no longer in use. A `true` value, will allow the
  // filter to delete the memory block upon destruction of the import filter.
  //
  // For the `ImportImageFilter` to appropriately delete the
  // memory block, the memory must be allocated with the C++
  // `new()` operator. Memory allocated with other memory
  // allocation mechanisms, such as C `malloc` or `calloc`, will not
  // be deleted properly by the `ImportImageFilter`. In
  // other words, it is the application programmer's responsibility
  // to ensure that `ImportImageFilter` is only given
  // permission to delete the C++ `new` operator-allocated memory.
  const bool importImageFilterWillOwnTheBuffer = false;
  importFilter->SetImportPointer(img, total_voxels_N, importImageFilterWillOwnTheBuffer );

  return importFilter;
}

void FieldOfView::gaussFilter(FloatType* img, FloatType sigma, uint16_t width) const
{
  using GaussFilter = itk::DiscreteGaussianImageFilter<itkImageType, itkImageType>;
//   using GaussFilter = itk::RecursiveGaussianImageFilter<itkImageType, itkImageType>;
  
  itkImageType::SizeType size;
  itkImageType::SpacingType spacing;
  itkImageType::PointType center;
  std::tie(size, spacing, center) = generateItkImageInfo();
  
  ImportFilterType::Pointer importFilter = importImage(img);
  importFilter->Update();

  GaussFilter::Pointer filter = GaussFilter::New();
  filter->SetInput(importFilter->GetOutput());
  filter->SetVariance(sigma*sigma);
  filter->SetMaximumKernelWidth(width);
  
  filter->Update();
  FloatType * p = filter->GetOutput()->GetBufferPointer();
  for (uint32_t vx=0; vx<total_voxels_N; vx++)
    img[vx] = p[vx];
//   std::copy(std::execution::par_unseq, p, p+total_voxels_N, img.begin());
}


void FieldOfView::medianFilter(FloatType* img, uint16_t width) const
{
  using MedianFilter = itk::MedianImageFilter<itkImageType, itkImageType>;
  //   using GaussFilter = itk::RecursiveGaussianImageFilter<itkImageType, itkImageType>;
  
  itkImageType::SizeType size;
  itkImageType::SpacingType spacing;
  itkImageType::PointType center;
  std::tie(size, spacing, center) = generateItkImageInfo();
  
  ImportFilterType::Pointer importFilter = importImage(img);
  importFilter->Update();
  
  MedianFilter::Pointer filter = MedianFilter::New();
  filter->SetInput(importFilter->GetOutput());
  MedianFilter::InputSizeType radius;
  radius[0] = width*voxel_lengths[0];
  radius[1] = width*voxel_lengths[1];
  radius[2] = width*voxel_lengths[2];
  
  filter->SetRadius(radius);
  
  filter->Update();
  FloatType * p = filter->GetOutput()->GetBufferPointer();
  for (uint32_t vx=0; vx<total_voxels_N; vx++)
    img[vx] = p[vx];
  //   std::copy(std::execution::par_unseq, p, p+total_voxels_N, img.begin());
}
