/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef GEOMETRIES_FIELDOFVIEW_H_
#define GEOMETRIES_FIELDOFVIEW_H_

#include <iostream>
#include <iomanip>
#include <vector>
#include <cstdint>
#include <array>
#include <tuple>
#include <map>

#include "euclideanSpace.h"
#include "dataTypes.h"
#include "f_utils.h"
#include "modelT.h"
#include "symmetriesT.h"

#include <itkImage.h>
#include <itkFlipImageFilter.h>
#include <itkResampleImageFilter.h>
#include <itkAffineTransform.h>
#include <itkImportImageFilter.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkBSplineInterpolateImageFunction.h>
#include <itkNearestNeighborInterpolateImageFunction.h>
#include <itkThresholdImageFilter.h>
#include <itkMultiThreaderBase.h>

#include <itkBoxSpatialObject.h>
// #include <itkPolygonSpatialObject.h>
#include <itkTubeSpatialObject.h>
#include <itkSpatialObjectToImageFilter.h>

#include <itkMultiThreaderBase.h>

#include "itkDiscreteGaussianImageFilter.h"
#include "itkRecursiveGaussianImageFilter.h"
#include "itkMedianImageFilter.h"

const uint16_t    ImageDimension = 3;
using itkImageType = itk::Image<FloatType, ImageDimension>;
using ImportFilterType = itk::ImportImageFilter<FloatType, ImageDimension>;
using ResampleFilterType =  itk::ResampleImageFilter<itkImageType, itkImageType>;
using TransformType = itk::AffineTransform<double, ImageDimension>;
using ThresholdImageFilterType = itk::ThresholdImageFilter<itkImageType>;


/*! \class FieldOfView
 *  \brief Field of View class.
 *
 *  This class contains the information about the Field Of View of the acquisition 
 *  system. These information must be specified in the scanner XML file:
 *
 *  <FOV>\n
 *    < VOXEL_NUMBER_X>integer</VOXEL_NUMBER_X>\n
 *    <VOXEL_NUMBER_Y>integer</VOXEL_NUMBER_Y>\n
 *    <VOXEL_NUMBER_Z>integer</VOXEL_NUMBER_Z>\n
 *    <VOXEL_LENGTH_X>SpaceType (in mm)</VOXEL_LENGTH_X>\n
 *    <VOXEL_LENGTH_Y>SpaceType (in mm)</VOXEL_LENGTH_Y>\n
 *    <VOXEL_LENGTH_Z>SpaceType (in mm)</VOXEL_LENGTH_Z>\n
 *  </FOV>\n
 */
class FieldOfView{

private:
  //! number of voxels along the three directions.
  std::array<uint16_t, 3>   voxels_N;
//   std::array<uint16_t, 3>   real_voxels_N;
  
  //! total number of voxels of the FOV.
  uint32_t                  total_voxels_N;
  //! lenght of the voxels along the three directions.
  std::array<SpaceType, 3>  voxel_lengths;
  
  //! half lenght of the FOV along the three directions.
  std::array<SpaceType, 3>  fov_half_lengths;

  ImageType rot_mask;
  std::vector<ThreeVector> polygon_points;
  
  //used for the projectors to avoid computation of useless elements
  SpaceType outer_radius;
  
  SpaceType mask_radius;
  ImageType image_mask;
  
public:
  explicit FieldOfView(const std::string& config, const std::string& tag_p);
  

  /*!
   *  \brief Compute the unique linear index for a voxel of the FOV from the 
   *  values of indexes of the three axes.
   *
   *  As it is stored as a uint32_t it should be able to accurately represent
   *  a FOV with size 1500x1500x1500
   *
   *  \param x index along the x axis
   *  \param y index along the y axis
   *  \param z index along the z axis
   * 
   *  \return linearIndex
   */
  
  const std::vector<ThreeVector>& polygonPoints() const
  {
    return polygon_points;
  }
  
  uint32_t computeLinearIndex(uint16_t x, uint16_t y, uint16_t z) const
  {
    uint32_t xn=x, yn=y, zn=z;
    return (xn + yn*voxels_N[0] + zn*voxels_N[0]*voxels_N[1]);
  }
  
  /*! \brief Compute the x axis index for a voxel, given the unique linear index.
   * 
   *  \param idx unique linear index
   * 
   *  \return x axis index
   */
  uint16_t getX(const uint32_t idx) const
  {
    return idx%voxels_N[0];
  }
  
  /*! \brief Compute the y axis index for a voxel, given the unique linear index.
   * 
   *  \param idx unique linear index
   * 
   *  \return y axis index
   */
  uint16_t getY(const uint32_t idx) const
  {
    return (idx/voxels_N[0])%voxels_N[1];
  }
  
  /*! \brief Compute the z axis index for a voxel, given the unique linear index.
   * 
   *  \param idx unique linear index
   * 
   *  \return z axis index
   */
  uint16_t getZ(const uint32_t idx) const
  {
    return (idx/voxels_N[0])/voxels_N[1];
  }
  
  /*! \brief Length of the voxel along a specified direction.
   * 
   *  \param axis direction
   * 
   *  \return voxel length
   */
  SpaceType vxL(uint16_t axis) const
  {
    return voxel_lengths[axis];
  }
  
  /*! \brief Number of voxels along a specified direction.
   * 
   *  \param axis direction
   * 
   *  \return number of voxels
   */
  uint16_t vxN(uint16_t axis) const
  {
    return voxels_N[axis];
  }
  
  /*! \brief Physical position of a plane in the space
   * 
   *  \param axis direction perpendicular to the plane
   *  \param idx index of the plane to be computed
   * 
   *  \return physical position of the plane in mm
   */
  SpaceType plane(uint16_t axis, uint16_t idx) const
  {
    return idx*voxel_lengths[axis] - fov_half_lengths[axis];
  }
  
  SpaceType halfLength(uint16_t axis) const
  {
    return fov_half_lengths[axis];
  }
  
  /*! \brief Physical position of a voxel in the space
   * 
   *  \param x index along x axis
   *  \param y index along y axis
   *  \param z index along z axis
   * 
   *  \return physical position of the voxel as a ThreeVector (mm, mm, mm)
   */
  ThreeVector vx2pos(uint16_t x, uint16_t y, uint16_t z) const
  {
    SpaceType x_v =  voxel_lengths[0]*( x + 0.5) - fov_half_lengths[0];
    SpaceType y_v =  voxel_lengths[1]*( y + 0.5) - fov_half_lengths[1];
    SpaceType z_v =  voxel_lengths[2]*( z + 0.5) - fov_half_lengths[2];
    return ThreeVector(x_v, y_v, z_v);
  }
  
  std::tuple<uint16_t, uint16_t, uint16_t> pos2vx (const ThreeVector& pos) const
  {
    uint16_t x = std::round( (pos.x() + fov_half_lengths[0])/voxel_lengths[0] - 0.5);
    uint16_t y = std::round( (pos.y() + fov_half_lengths[1])/voxel_lengths[1] - 0.5);
    uint16_t z = std::round( (pos.z() + fov_half_lengths[2])/voxel_lengths[2] - 0.5);
    return std::make_tuple(x,y,z);
  }
  
  /*! \brief Physical position of a voxel in the space
   * 
   *  \param idx unique linear index of the voxel
   * 
   *  \return physical position of the voxel as a ThreeVector (mm, mm, mm)
   */
  ThreeVector vx2pos(uint32_t idx) const
  {
    return vx2pos(getX(idx), getY(idx), getZ(idx));
  }
  
  /*! \brief Physical position of a voxel in the space along a specified axis
   * 
   *  \param axis direction
   *  \param idx index of the voxel along the axis
   * 
   *  \return physical position of the voxel in mm along the axis direction
   */
  SpaceType vx2pos(uint16_t axis, uint16_t idx) const
  {
    return voxel_lengths[axis]*( 2*idx + 1 - voxels_N[axis] )/2;
  }
  
  /*! \brief Total number of voxels of the FOV
   * 
   *  Total number of voxels of the FOV: vxN(0) * vxN(1) * vxN(2)
   *  \return number of voxels
   */
  uint32_t nVoxels() const
  {
    return total_voxels_N;
  }
  
  FloatType voxelVolume() const
  {
    return voxel_lengths[0]*voxel_lengths[1]*voxel_lengths[2];
  }
  
  FloatType mask(uint32_t i) const
  {
    return rot_mask[i];
  }
  
  SpaceType outerRadius() const
  {
    return outer_radius;
  }
  
  void printInfo(const std::string& filename) const;
  
//   void computeMasks();
//   void computeMasks(const std::string& scanner_type, const ViewsList& views);
  
  void saveLOR(const std::string& name, std::vector<MatrixEntry> row) const;
  
  std::tuple<itkImageType::SizeType, itkImageType::SpacingType, itkImageType::PointType>
  generateItkImageInfo() const ;
  
  ImageType createInitImage(FloatType value) const;

  void saveImage(const std::string& filename,
                 const FloatType *image,
                 bool useImageMask) const;
  void saveImage(const std::string& filename,
                 const ImageType& image,
                 bool useImageMask) const
  {
    saveImage(filename, image.data(), useImageMask);
  }

  ImageType loadImage(const std::string& filename) const;
  
  void applyRotMask(ImageType& image) const
  {
    applyRotMask( image.data() );
  }
  void applyRotMask(FloatType* image) const;
  
  void fixCorners(ImageType& image) const;
  
  void gaussFilter(FloatType* img, FloatType sigma, uint16_t width) const;
  void gaussFilter(ImageType& img, FloatType sigma, uint16_t width) const
  {
    gaussFilter(img.data(), sigma, width);
  }
  
  void medianFilter(FloatType* img, uint16_t width) const;
  void medianFilter(ImageType& img, uint16_t width) const
  {
    medianFilter(img.data(), width);
  }
  
private:
  ImportFilterType::Pointer importImage(FloatType* img) const;
  ImportFilterType::Pointer importImage(ImageType& img) const
  {
    return importImage( img.data() );
  }
 
  

  // SYM SECTION
public:
  void rotateImage(FloatType *vec, FloatType *out, AngleType angle_in_deg, bool add) const;
  void rotateImage2D(FloatType *vec, FloatType *out, AngleType angle_in_deg, bool add) const;
  
  void shiftImage(FloatType *vec, FloatType *out, const ThreeVector& transl, bool add) const;
  
  void flipImage(FloatType *vec, FloatType *out, bool x, bool y, bool z, bool add) const;
  
//   void shiftAndFlip(FloatType *vec, FloatType *out, const ThreeVector& transl, bool x, bool y, bool z) const;
//   void flipAndShift(FloatType *vec, FloatType *out, const ThreeVector& transl, bool x, bool y, bool z) const;
  
  // End of SYM SECTION
  
  
  // OBJECTS SECTION
public:
//   ImageType createPolygon(const ThreeVector& poly_radius,
//                           const std::vector<ThreeVector> &poly_points) const;
  
  ImageType createCylinder(SpaceType radius,
                           ThreeVector cyl_center,
                           SpaceType height,
                           FloatType value) const;
  
  ImageType createCenteredCylinder(SpaceType radius) const
  {
    return createCylinder(radius, ThreeVector(0, 0, 0), fov_half_lengths[2]*2, 1);
  }
  
//   ImageType createCylinderGrid(SpaceType radius, SpaceType spacing_factor) const;
  
  ImageType createCylindricalGrid(std::vector<ThreeVector> centers,
                                  SpaceType radius,
                                  SpaceType height
                                 ) const;
                                 
  ImageType createPlanarPhantom(const ThreeVector& phantom_center,
                                const ThreeVector& phantom_size,
                                AngleType angle_deg) const;
                                  
  
  // End of OBJECTS SECTION
};

#endif  // GEOMETRIES_FIELDOFVIEW_H_

