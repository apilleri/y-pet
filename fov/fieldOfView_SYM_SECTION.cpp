/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "fieldOfView.h"

#define SPLINE_ORDER 3

/*! this function assumes that the vector vec contains a 3D image of dimensions
 *  x_N * y_N * z_N. The function rotates the image around its center of the
 *  quantity angle_in_deg around the Z-axis. The ITK reference frame is left-handed. 
 *  The result of the rotation is added or copied (see add parameter) to the vector out.
 *  The vec and out vectors must be properly initialized.
 */
void FieldOfView::rotateImage(FloatType *vec, FloatType *out, AngleType angle_in_deg, bool add) const
{
//   itk::MultiThreaderBase::SetGlobalMaximumNumberOfThreads(8);
//   std::cout << "threads: " << itk::MultiThreaderBase::GetGlobalMaximumNumberOfThreads()
//   << std::endl;
//     using InterpolatorType = itk::LinearInterpolateImageFunction<itkImageType, double>;
//   using InterpolatorType = itk::NearestNeighborInterpolateImageFunction<itkImageType, double>;
  using InterpolatorType = itk::BSplineInterpolateImageFunction<itkImageType, double>;
  
  if ( fpAreEqual<AngleType>(angle_in_deg, 0, 1e-1) || fpAreEqual<AngleType>(angle_in_deg, 360, 1e-1) ) {
    if (add)
      std::transform(std::execution::par_unseq,
                     vec, vec+total_voxels_N,
                     out,
                     out,
                     std::plus<FloatType>() );
    else
      std::copy(std::execution::par_unseq, vec, vec+total_voxels_N, out);
    return;
  }

  itkImageType::SizeType size;
  itkImageType::SpacingType spacing;
  itkImageType::PointType center;
  std::tie(size, spacing, center) = generateItkImageInfo();
  
  ImportFilterType::Pointer importFilter = importImage(vec);
  importFilter->Update();
  
  ResampleFilterType::Pointer filter = ResampleFilterType::New();
  TransformType::Pointer transform = TransformType::New();
  InterpolatorType::Pointer interpolator = InterpolatorType::New();
  interpolator->SetSplineOrder(SPLINE_ORDER);
  filter->SetInterpolator(interpolator);
  filter->SetDefaultPixelValue(0);
  filter->SetOutputSpacing(spacing);
  filter->SetOutputDirection( importFilter->GetOutput()->GetDirection() );
  filter->SetSize( size );
  
  filter->SetInput( importFilter->GetOutput() );
  
  TransformType::OutputVectorType translation;
  translation[0] =   -center[0];
  translation[1] =   -center[1];
  translation[2] =   -center[2];
  transform->Translate(translation);
  
  const AngleType angle = angle_in_deg * std::atan(1.0) / 45.0;
  
  TransformType::OutputVectorType axis;
  axis[0] = 0;
  axis[1] = 0;
  axis[2] = 1;
  transform->Rotate3D(axis, angle, false);
  transform->Translate(-translation, false);
  
  filter->SetTransform(transform);
  
  FloatType min=vec[0];
  for (uint32_t vx=1;vx<total_voxels_N;vx++)
    if ( vec[vx] < min)
      min = vec[vx];
    
  ThresholdImageFilterType::Pointer ThresholdFilter= ThresholdImageFilterType::New();
  ThresholdFilter->SetInput(filter->GetOutput());
  ThresholdFilter->ThresholdBelow (min/100);
  ThresholdFilter->SetOutsideValue(0.0);
  ThresholdFilter->Update();
  
  FloatType * p = ThresholdFilter->GetOutput()->GetBufferPointer();
  if (add)
    std::transform(std::execution::par_unseq,
                   p, p+total_voxels_N,
                   out,
                   out,
                   std::plus<FloatType>() );
  else
    std::copy(std::execution::par_unseq, p, p+total_voxels_N, out);
  
//   applyRotMask(out);

}

void FieldOfView::rotateImage2D(FloatType *vec, FloatType *out, AngleType angle_in_deg, bool add) const
{
  const uint16_t ImageDimension = 2;
  using itkImageType = itk::Image<FloatType, ImageDimension>;
  using ImportFilterType = itk::ImportImageFilter<FloatType, ImageDimension>;
  using ResampleFilterType =  itk::ResampleImageFilter<itkImageType, itkImageType>;
  using TransformType = itk::AffineTransform<double, ImageDimension>;
  using ThresholdImageFilterType = itk::ThresholdImageFilter<itkImageType>;
  
  using InterpolatorType = itk::BSplineInterpolateImageFunction<itkImageType, double>;
  
  if ( fpAreEqual<AngleType>(angle_in_deg, 0, 1e-1) ) {
    if (add)
      std::transform(std::execution::par_unseq,
                     vec, vec+total_voxels_N,
                     out,
                     out,
                     std::plus<FloatType>() );  
      else
        std::copy(std::execution::par_unseq, vec, vec+total_voxels_N, out);
      return;
  }
  
  itkImageType::SizeType size;
  size[0] = voxels_N[0];  // size along X
  size[1] = voxels_N[1];  // size along Y
//   size[2] = voxels_N[2];  // size along Z
  itkImageType::SpacingType spacing;
  spacing[0] = voxel_lengths[0];
  spacing[1] = voxel_lengths[1];
//   spacing[2] = voxel_lengths[2];
  itkImageType::PointType center;
  center[0] = spacing[0] * (size[0]-1) / 2.0;
  center[1] = spacing[1] * (size[1]-1) / 2.0;
//   center[2] = spacing[2] * (size[2]-1) / 2.0;
  
  uint32_t nvx_xy = voxels_N[0]*voxels_N[1];
  
  #pragma omp parallel for
  for (uint32_t z=0; z<voxels_N[2]; z++) {
    ImportFilterType::Pointer importFilter = ImportFilterType::New();
    ImportFilterType::IndexType start;
    start.Fill( 0 );
    ImportFilterType::RegionType region;
    region.SetIndex(start);
    region.SetSize(size);
    importFilter->SetRegion(region);
    importFilter->SetSpacing(spacing);
    const bool importImageFilterWillOwnTheBuffer = false;
    importFilter->SetImportPointer(vec+z*nvx_xy, nvx_xy, importImageFilterWillOwnTheBuffer );
    importFilter->Update();

    ResampleFilterType::Pointer filter = ResampleFilterType::New();
    TransformType::Pointer transform = TransformType::New();
    InterpolatorType::Pointer interpolator = InterpolatorType::New();
    interpolator->SetSplineOrder(SPLINE_ORDER);
    filter->SetInterpolator(interpolator);
    filter->SetDefaultPixelValue(0);
    filter->SetOutputSpacing(spacing);
    filter->SetOutputDirection( importFilter->GetOutput()->GetDirection() );
    filter->SetSize( size );
    filter->SetInput( importFilter->GetOutput() );
    
    TransformType::OutputVectorType translation;
    translation[0] =   -center[0];
    translation[1] =   -center[1];
//     translation[2] =   -center[2];
    const AngleType angle = angle_in_deg * std::atan(1.0) / 45.0;
    transform->Translate(translation);
    transform->Rotate2D(angle, false);
    transform->Translate(-translation, false);
    filter->SetTransform(transform);
    
    ThresholdImageFilterType::Pointer ThresholdFilter= ThresholdImageFilterType::New();
    ThresholdFilter->SetInput(filter->GetOutput());
    ThresholdFilter->ThresholdBelow (0);
    ThresholdFilter->SetOutsideValue(0.0);
    
    ThresholdFilter->Update();
    FloatType * p = ThresholdFilter->GetOutput()->GetBufferPointer();
    
//     filter->Update();
//     FloatType * p = filter->GetOutput()->GetBufferPointer();
    
    if (add)
      std::transform(p, p+nvx_xy, out+z*nvx_xy, out+z*nvx_xy, std::plus<FloatType>() );
    else
      std::copy(p, p+nvx_xy, out+z*nvx_xy);
  }

}



void FieldOfView::flipImage(FloatType *vec, FloatType *out, bool x, bool y, bool z, bool add) const
{
//   for (uint32_t vx=0; vx<total_voxels_N; vx++) {
//     uint32_t cx = getX(vx);
//     uint32_t cy = getY(vx);
//     uint32_t cz = getZ(vx);
//     
//     if (x)
//       cx = vxN(0) - 1 - cx;
//     if (y)
//       cy = vxN(1) - 1 - cy;
//     if (z)
//       cz = vxN(2) - 1 - cz;
//     
//     uint32_t n_idx = computeLinearIndex(cx, cy, cz);
//     
//     if (add)
//       out[n_idx] += vec[vx];
//     else
//       out[n_idx] = vec[vx];
//   }
  
  ImportFilterType::Pointer importFilter = importImage(vec);
  
  using FlipImageFilterType = itk::FlipImageFilter< itkImageType >;
  FlipImageFilterType::Pointer flipFilter = FlipImageFilterType::New ();
  flipFilter->SetInput( importFilter->GetOutput() );
  
  FlipImageFilterType::FlipAxesArrayType flipAxes;
  flipAxes[0] = x;
  flipAxes[1] = y;
  flipAxes[2] = z;
  flipFilter->SetFlipAxes( flipAxes );
  flipFilter->Update();
  
  FloatType* p = flipFilter->GetOutput()->GetBufferPointer();
  if (add)
    std::transform(p, p+total_voxels_N, out, out, std::plus<FloatType>() );
  else
    std::copy(p, p+total_voxels_N, out);
  
}


void FieldOfView::shiftImage(FloatType *vec, FloatType *out, const ThreeVector& transl, bool add) const
{
  using InterpolatorType = itk::LinearInterpolateImageFunction<itkImageType, double>;
  // using InterpolatorType = itk::NearestNeighborInterpolateImageFunction<itkImageType, double>;
//   using InterpolatorType = itk::BSplineInterpolateImageFunction<itkImageType, double>;
  
  // it is better to use ITK instead of a simple voxel shift because it performs
  // a physical translation and a resample.
  // It is useful in the presence of offsets between modules or blocks that are not 
  // integral multiple of the pixel spacing.
  
  ImportFilterType::Pointer importFilter = importImage(vec);
  
  ResampleFilterType::Pointer filter = ResampleFilterType::New();
  
  TransformType::Pointer transform = TransformType::New();
  filter->SetTransform( transform );
  
  InterpolatorType::Pointer interpolator = InterpolatorType::New();
//   interpolator->SetSplineOrder(3);
  filter->SetInterpolator(interpolator);
  filter->SetDefaultPixelValue(0);
  
  filter->SetOutputOrigin( importFilter->GetOutput()->GetOrigin() );
  filter->SetOutputSpacing( /*spacing*/importFilter->GetSpacing() );
  filter->SetOutputDirection( importFilter->GetOutput()->GetDirection() );
  filter->SetSize(importFilter->GetRegion().GetSize() /*size*/);
  
  filter->SetInput( importFilter->GetOutput() );
  
  TransformType::OutputVectorType translation;
  translation[0] = -transl.x();
  translation[1] = -transl.y();
  translation[2] = -transl.z();
  transform->Translate(translation);
  
  ThresholdImageFilterType::Pointer ThresholdFilter= ThresholdImageFilterType::New();
  ThresholdFilter->SetInput(filter->GetOutput());
  ThresholdFilter->ThresholdBelow (0);
  ThresholdFilter->SetOutsideValue(0.0);
  ThresholdFilter->Update();
  FloatType * p = ThresholdFilter->GetOutput()->GetBufferPointer();
  
//   filter->Update();
//   FloatType * p = filter->GetOutput()->GetBufferPointer();
  
  if (add)
    std::transform(p, p+total_voxels_N, out, out, std::plus<FloatType>() );
  else
    std::copy(p, p+total_voxels_N, out);
}


// void FieldOfView::shiftAndFlip(FloatType *vec, FloatType *out, const ThreeVector& transl, bool x, bool y, bool z) const
// {
//   ImportFilterType::Pointer importFilter = importImage(vec, true);
//   
//   using InterpolatorType = itk::LinearInterpolateImageFunction<itkImageType, double>;
//   InterpolatorType::Pointer interpolator = InterpolatorType::New();
//   // using InterpolatorType = itk::NearestNeighborInterpolateImageFunction<itkImageType, double>;
//   // using InterpolatorType = itk::BSplineInterpolateImageFunction<itkImageType, double>;
//   
//   // it is better to use ITK instead of a simple voxel shift because it performs
//   // a physical translation and a resample.
//   // It is useful in the presence of offsets between modules or blocks that are not 
//   // integral multiple of the pixel spacing.
//   
//   TransformType::OutputVectorType translation;
//   // the itk cordinate system is left-ended
//   translation[0] = -transl.x();
//   translation[1] = -transl.y();
//   translation[2] = -transl.z();
//   TransformType::Pointer transform = TransformType::New();
//   transform->Translate(translation);
//   
//   ResampleFilterType::Pointer shiftFilter = ResampleFilterType::New();
//   shiftFilter->SetInput( importFilter->GetOutput() );
//   shiftFilter->SetOutputOrigin( importFilter->GetOutput()->GetOrigin() );
//   shiftFilter->SetOutputSpacing( importFilter->GetSpacing() );
//   shiftFilter->SetOutputDirection( importFilter->GetOutput()->GetDirection() );
//   shiftFilter->SetSize(importFilter->GetRegion().GetSize() );
//   shiftFilter->SetTransform( transform );
//   shiftFilter->SetInterpolator(interpolator);
//   shiftFilter->SetDefaultPixelValue(0);
//   shiftFilter->Update();
//   
//   using FlipImageFilterType = itk::FlipImageFilter< itkImageType >;
//   FlipImageFilterType::Pointer flipFilter = FlipImageFilterType::New ();
//   flipFilter->SetInput( shiftFilter->GetOutput() );
//   FlipImageFilterType::FlipAxesArrayType flipAxes;
//   flipAxes[0] = x;
//   flipAxes[1] = y;
//   flipAxes[2] = z;
//   flipFilter->SetFlipAxes( flipAxes );
//   flipFilter->Update();
//   
//   FloatType * p = flipFilter->GetOutput()->GetBufferPointer();
//   std::copy(p, p+total_voxels_N, out);
// 
// }
// 
// void FieldOfView::flipAndShift(FloatType *vec, FloatType *out, const ThreeVector& transl, bool x, bool y, bool z) const
// {
//   ImportFilterType::Pointer importFilter = importImage(vec, true);
//   
//   using FlipImageFilterType = itk::FlipImageFilter< itkImageType >;
//   FlipImageFilterType::Pointer flipFilter = FlipImageFilterType::New ();
//   flipFilter->SetInput( importFilter->GetOutput() );
//   FlipImageFilterType::FlipAxesArrayType flipAxes;
//   flipAxes[0] = x;
//   flipAxes[1] = y;
//   flipAxes[2] = z;
//   flipFilter->SetFlipAxes( flipAxes );
//   flipFilter->Update();
//   
//   
//   using InterpolatorType = itk::LinearInterpolateImageFunction<itkImageType, double>;
//   InterpolatorType::Pointer interpolator = InterpolatorType::New();
//   TransformType::OutputVectorType translation;
//   // the itk cordinate system is left-ended
//   translation[0] = -transl.x();
//   translation[1] = -transl.y();
//   translation[2] = -transl.z();
//   TransformType::Pointer transform = TransformType::New();
//   transform->Translate(translation);
//   
//   ResampleFilterType::Pointer shiftFilter = ResampleFilterType::New();
//   
//   shiftFilter->SetInput( flipFilter->GetOutput() );
//   shiftFilter->SetOutputOrigin( flipFilter->GetOutput()->GetOrigin() );
//   shiftFilter->SetOutputSpacing( importFilter->GetSpacing() );
//   shiftFilter->SetOutputDirection( flipFilter->GetOutput()->GetDirection() );
//   shiftFilter->SetSize(importFilter->GetRegion().GetSize() );
//   shiftFilter->SetTransform( transform );
//   shiftFilter->SetInterpolator(interpolator);
//   shiftFilter->SetDefaultPixelValue(0);
//   shiftFilter->Update();
//   
//   
//   FloatType * p = shiftFilter->GetOutput()->GetBufferPointer();
//   std::transform(p, p+total_voxels_N, out, out, std::plus<FloatType>() );
// }
