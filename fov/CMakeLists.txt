file(GLOB FOV_SOURCE "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

set( fov_files ${FOV_SOURCE} )

add_library( fov_lib ${fov_files} )

target_link_libraries( fov_lib ${ITK_LIBRARIES} tbb)
