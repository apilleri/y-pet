#ifndef TASKS_TASKBASE_H_
#define TASKS_TASKBASE_H_

#include "cxxopts.hpp"

#include "vGeometry.h"

#include "fieldOfView.h"
#include "vProjector.h"
#include "dataTypes.h"
#include "HistogramsMap.h"

#include "timer.hpp"

#include <omp.h>

class TaskBase {
  
protected:
  std::string xml_config;
  
  vGeometry *scanner;
  FieldOfView *fov;
  vProjector* projector;
  
  bool use_z_model;
  HistogramsMap Z_model;
  
  bool use_g_model;
  std::map<detPair_t, SparseModel> G_models;
  std::map<detPair_t, SparseModel> G_models_tr;
  bool use_transposed_G;
  
  bool use_d_model;
  std::map<detPair_t, SparseModel> D_models;
  std::map<detPair_t, SparseModel> D_models_tr;
  
  std::string output_dir;
  
  ImageType image;
  ImageType composed_img;
  
  std::vector<AngleType> unique_angles;
  std::map<AngleType, ImageType> angle_views;
  std::vector<FloatType*> estim_views;
  std::vector<FloatType*> output_views;
  
  uint16_t n_threads;
  
public:
  TaskBase(const std::string& config, const cxxopts::ParseResult& cmd_line);
  
  virtual ~TaskBase()
  { }
  
  virtual void apply() = 0;

  
protected:
    
  void initProjector(const std::string& proj_tag_p);
  
  void loadNormalizationModel(std::string& z_model_str, std::string& hist_type);
  
  void loadGeometricModels(std::string& g_dir, bool use_tr_models);
  
  void loadDetectorModels(std::string& d_dir, bool use_tr_models);

  void createAnglesAndInitViews();
  
  void createViews();
  
  void clearOutputViews();
  
  void composeViews();
  
  FloatType projectModelRow(const FloatType* vec,
                            const SparseModel& model,
                            lorIDX_t lor_id);
  

};

#endif // TASKS_TASKBASE_H_
