/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TASKS_RECONSTRUCTION_VRECONSTRUCTION_H_
#define TASKS_RECONSTRUCTION_VRECONSTRUCTION_H_

#include "taskBase.h"

class vIterativeReconstruction : public TaskBase {

protected:
  std::string start_image; //non si può continuare una ricostruzione se c'era il background perchè ha tolto la roba sotto lo zero e quindi non è più consistente.
  uint16_t start_iter;
  
  StringVec output_files;
  std::string out_filename;
  
  std::string input_hist_type;
  StringVec input_files;
  HistogramsMap in_hist;

  StringVec random_files;
  HistogramsMap random;
  
  StringVec attenuation_files;
  HistogramsMap attenuation;
  
  uint16_t n_iterations;
  uint16_t save_every;
  
  SubsestList subsets;
  SubsestList rnd_subsets;
  
  HistogramsMap background;
  FloatType bg_value;
  

  
  FloatType psf_sigma;
  uint16_t psf_width;
  // 0 - both, 1 - pre-only, 2 - post-only
  uint16_t psf_type;
  
  // REGULARIZATION
  // simple Gaussian filtering
  FloatType smooth_sigma;
  uint16_t smooth_width;
  // Wang-Patch Regularization
  FloatType wang_beta;
  FloatType wang_delta;
  
  
public:
  vIterativeReconstruction(const std::string& config,
                           const cxxopts::ParseResult& cmd_line)
  : TaskBase(config, cmd_line),
    n_iterations(1), save_every(1), bg_value(0),
    psf_sigma(0), psf_width(0), psf_type(0),
    smooth_sigma(0), smooth_width(0),
    wang_beta(0), wang_delta(0)
  {
    out_filename = "";
    
    if (cmd_line.count("start-image") )
      start_image = cmd_line["start-image"].as<std::string>();
    else
      start_image = "";
    
    if (cmd_line.count("start-iter") )
      start_iter = cmd_line["start-iter"].as<uint16_t>();
    else
      start_iter = 0;
    
    output_files = cmd_line["output"].as< StringVec >();
    
    input_files = cmd_line["input"].as< StringVec >();
    

    if ( cmd_line.count("activate-vg") ) 
      input_hist_type = "LOFs";
    else
      input_hist_type = "LORs";
    
    
    if ( cmd_line.count("number") )
      n_iterations = cmd_line["number"].as<uint16_t>();
    
    if ( cmd_line.count("every") )
      save_every = cmd_line["every"].as<uint16_t>();
    
    
    if ( cmd_line.count("random") ) {
      random_files = cmd_line["random"].as< StringVec >();
    }
    
    if ( cmd_line.count("attenuation") ) {
      attenuation_files = cmd_line["attenuation"].as< StringVec >();
    }
    
    if (cmd_line.count("bg-h") ) {
      background.allocate(scanner->getDetectorsList(),
                          scanner->getCoincidenceSchema(),
                          input_hist_type, 0);
      background.load(cmd_line["bg-h"].as<std::string>());
      
      bg_value = cmd_line["bg-v"].as<FloatType>();
    }
    
    
    if (cmd_line.count("psf") ) {
      std::vector<FloatType > v = cmd_line["psf"].as< std::vector<FloatType> >();
      if (v.size() != 3)
        throw std::string("psf option: you must provide three values");
      
      psf_sigma = v[0];
      psf_width = static_cast<uint16_t>(v[1]);
      psf_type = v[2];
    }
    
    // REGULARIZATION 
    if (cmd_line.count("filter") ) {
      std::vector<FloatType > v = cmd_line["filter"].as< std::vector<FloatType> >();
      if (v.size() != 2)
        throw std::string("filter option: you must provide two values");
      
      smooth_sigma = v[0];
      smooth_width = static_cast<uint16_t>(v[1]);
    }
    
    if (cmd_line.count("wang-reg") ) {
      std::vector<FloatType > v = cmd_line["wang-reg"].as< std::vector<FloatType> >();
      if (v.size() != 2)
        throw std::string("Wang Patch regularization option: you must provide two values");
      
      wang_beta = v[0];
      wang_delta = v[1];
    }

  }
  
  virtual ~vIterativeReconstruction()
  { }

  
  void apply() override
  {
    for (uint16_t i=0; i<input_files.size(); i++) {
      in_hist.allocate(scanner->getDetectorsList(),
                       scanner->getCoincidenceSchema(),
                       input_hist_type, 0);
      in_hist.load( input_files[i] );
      std::cout << "Input Histogram: number of lors: " << in_hist.nLors();
      std::cout << std::endl;
      std::cout << "Input Histogram: number of not null Lors: ";
      std::cout << in_hist.notNull() << " (" ;
      std::cout << ( (float)in_hist.notNull() )/in_hist.nLors() * 100;
      std::cout << "%)" << std::endl;
      
      if (bg_value)
        in_hist += background;
      
      out_filename = output_dir + "/" + output_files[i];
      
      if ( random_files.size() ) {
        random.allocate(scanner->getDetectorsList(),
                        scanner->getCoincidenceSchema(),
                        input_hist_type, 0);
        random.load( random_files[i] );
      }
      
      if ( attenuation_files.size() ) {
        attenuation.allocate(scanner->getDetectorsList(),
                            scanner->getCoincidenceSchema(),
                            input_hist_type, 0);
        attenuation.load( attenuation_files[i] );
      }
      
      performRecon();
    }
    

  }
  
protected:
  virtual void performRecon() = 0;
  
  void WangPatchRegularization(const ImageType& cur_sensitivity);
  void computeWeight(uint32_t vx, uint32_t vy, uint32_t vz,
                     std::vector<float>& weights,
                     uint32_t idx);
};


#endif // TASKS_RECONSTRUCTION_VRECONSTRUCTION_H_
