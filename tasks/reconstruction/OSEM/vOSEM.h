/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TASKS_RECONSTRUCTION_VOSEM_H_
#define TASKS_RECONSTRUCTION_VOSEM_H_

#include "vIterativeReconstruction.h"


class vOSEM : public vIterativeReconstruction {
  
protected:
  StringVec sens_files;
  std::vector<ImageType> sensitivity;
  
  uint32_t seed;
  uint16_t n_subsets;
  bool save_all_subsets;
  
public:
  vOSEM(const std::string& config, const cxxopts::ParseResult& cmd_line)
  : vIterativeReconstruction(config, cmd_line),
    seed(0), n_subsets(1), save_all_subsets(false)
  { 
    if ( cmd_line.count("subsets") )
      n_subsets = cmd_line["subsets"].as<uint16_t>();
    
    save_all_subsets = cmd_line.count("save-all-subsets");
    
    // MODELS
    if (cmd_line.count("zmodel")) {
      std::string str = cmd_line["zmodel"].as<std::string>();
      loadNormalizationModel(str, input_hist_type);
    }
    
    if (cmd_line.count("gmodel")) {
      std::string str = cmd_line["gmodel"].as<std::string>();
      loadGeometricModels(str, false);
    }
    
  }
  
protected:
  void prepareSensitivity(const cxxopts::ParseResult& cmd_line)
  {
    if ( !cmd_line.count("compute-sensitivity") ) {
      
      sens_files = cmd_line["sensitivity"].as< StringVec >();
      for (const std::string& str : sens_files) 
        sensitivity.push_back( fov->loadImage(str) );
      
    } else {
      computeSensitivity();
      if (n_subsets > 1) {
        ImageType full_sens = fov->createInitImage(0);
        for (ImageType subset : sensitivity) {
          for (uint32_t vx=0; vx < subset.size(); vx++)
            full_sens[vx] += subset[vx];
        }
        std::string psf_str = "";
        if (psf_sigma) {
          psf_str += "_psf-" + std::to_string(psf_sigma);
          psf_str += "-" + std::to_string(psf_width);
        }
        if (projector)
          fov->saveImage(output_dir + "/full_sens_" + projector->type() + psf_str, full_sens, false);
        else
          fov->saveImage(output_dir + "/full_sens" + psf_str , full_sens, false);
      }
    }
    
    //INVERTING Sensitivity
    for (uint16_t s_sub=0; s_sub<n_subsets; s_sub++) {
      #pragma omp parallel for
      for(uint32_t i=0; i<fov->nVoxels(); i++) {
        sensitivity[s_sub][i] = 1/sensitivity[s_sub][i];
        if (! std::isnormal(sensitivity[s_sub][i]) )
          sensitivity[s_sub][i] = 0;
      }
    }
  }
  
  void saveToDisk(uint16_t iter, uint16_t s_sub)
  {
    if (iter % save_every == 0 || iter == 1) {
      std::string name = out_filename;
      name += "_" + std::to_string(iter+start_iter);
      if (save_all_subsets) 
        name += "_" + std::to_string(s_sub+1);
      if (save_all_subsets || (s_sub+1) == n_subsets) {
        //removing background before saving
        if (bg_value) {
          ImageType tmp_image(image.size(), -bg_value);
          for (uint32_t vx=0; vx<image.size(); vx++) {
            tmp_image[vx] += image[vx];
            if ( tmp_image[vx] < 0 )
              tmp_image[vx] = 0;
          }
          fov->saveImage(name, tmp_image, true);
          
        } else
          fov->saveImage(name, image, true);
      }
    } 
  }
  
  virtual void computeSensitivity() = 0;
};


class osemFullZG : public vOSEM {
  
public:
  osemFullZG(const std::string& config, const cxxopts::ParseResult& cmd_line)
  : vOSEM(config, cmd_line)
  { 
    prepareSensitivity(cmd_line);
  }
  
protected:
  void performRecon() override;
  void computeSensitivity() override;
  
};


class osemSymZG : public vOSEM {
  
public:
  osemSymZG(const std::string& config, const cxxopts::ParseResult& cmd_line)
  : vOSEM(config, cmd_line)
  {
    createAnglesAndInitViews();
    prepareSensitivity(cmd_line);
  }
  
protected:
  void performRecon() override;
  void computeSensitivity() override;
  
};



#endif // TASKS_RECONSTRUCTION_VOSEM_H_
