/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vOSEM.h"

#include <unordered_map>

void osemSymZG::performRecon()
{  
  timer msec_timer("msec"), msec_timer_partial("msec");
  
  if (start_image != "")
    image = fov->loadImage(start_image);
  else
    image = fov->createInitImage(1);
  
  const CoincidenceSchema& cs = scanner->getCoincidenceSchema();
    
  scanner->reflectHistograms(in_hist, input_hist_type);
  in_hist.generateSubsets(subsets, n_subsets, seed);
  
  if ( random_files.size() ) {
    scanner->reflectHistograms(random, input_hist_type);
    random.generateSubsets(rnd_subsets, n_subsets, seed);
  }
  
  ImageType backup_image = fov->createInitImage(0);
  for (uint16_t iter=1; iter <= n_iterations; iter++) {
    msec_timer.start("Iteration: " + std::to_string(iter), "", false);
    
    for (uint16_t s_sub=0; s_sub<n_subsets; s_sub++) {
      
      if (psf_sigma && (psf_type == 0 || psf_type == 1) ) {
        msec_timer_partial.start("convolution image", "\t", true);
        for (uint32_t vx=0; vx<image.size(); vx++)
          backup_image[vx] = image[vx];
        fov->gaussFilter(image, psf_sigma, psf_width);
        msec_timer_partial.stop();
      }
      
      createViews();
      
      if (psf_sigma && (psf_type == 0 || psf_type == 1)) {
        for (uint32_t vx=0; vx<image.size(); vx++)
          image[vx] = backup_image[vx];
      }
      
      msec_timer_partial.start("proj & back", "\t", true);
      
      #pragma omp parallel for 
      for (lorIDX_t i=0; i<subsets[s_sub].size(); i++) {
        
        lorIDX_t cur_lor = subsets[s_sub][i].first;
        detPair_t p_id; 
        lorIDX_t rel_lof;
        
        std::tie(p_id, rel_lof) = in_hist.identifyLOR(cur_lor);
        
        
        const uint16_t& view_id = cs.at(p_id).uniqueView();
        FloatType* view = estim_views[ view_id ];
        const detPair_t& model_id = cs.at(p_id).symID();
        const SparseModel& model = G_models.at(model_id);
        
//         const CountsType& entry = subsets[s_sub][i];
//         detPair_t p_id = entry.pairID();
//         const uint16_t& view_id = cs.at(p_id).uniqueView();
//         FloatType* view = estim_views[ view_id ];
//         const detPair_t& model_id = cs.at(p_id).symID();
//         const SparseModel& model = G_models.at(model_id);
//         uint32_t n_cry2 = scanner->getDetector(entry.det2()).nCrystals();
//         lorIDX_t rel_lof = entry.cry1()*n_cry2 + entry.cry2();
        
        FloatType projection = projectModelRow(view, model, rel_lof);
        if ( random_files.size() ) 
          projection += rnd_subsets[s_sub][i].second;
        
        if (projection) {
          MatrixEntry *bp = model.rowPointer(rel_lof);
          MatrixEntry *ep = bp + model.rowElementsN(rel_lof);
          
          FloatType counts = subsets[s_sub][i].second;
          while(bp < ep) {
            uint32_t vx = bp->col;
            FloatType value = bp->v;

            FloatType update = value*counts/projection;
            #pragma omp atomic update
            output_views[view_id][vx] += update;
            
            bp++;
          }
        }
        
      }
      msec_timer_partial.stop();
      
      composeViews();
      clearOutputViews();
      
      
      if (psf_sigma && (psf_type == 0 || psf_type == 2)) {
        msec_timer_partial.start("convolution composed_img", "\t", true);
        fov->gaussFilter(composed_img, psf_sigma, psf_width);
        msec_timer_partial.stop();
      }
      
      msec_timer_partial.start("updating", "\t", true);
      #pragma omp parallel for
      for (uint32_t vx=0; vx<image.size(); vx++) {
        if ( std::isnormal (composed_img[vx]) )
          image[vx] *= composed_img[vx]*sensitivity[s_sub][vx];
      }
      msec_timer_partial.stop();
      
      // REGULARIZATION
      
      if (smooth_sigma) {// && (s_sub+1) == n_subsets) {
        msec_timer_partial.start("filtering", "\t", true);
        fov->gaussFilter(image, smooth_sigma, smooth_width);
        msec_timer_partial.stop();
      }
      
      if (wang_beta) {
        msec_timer_partial.start("applying wang patch-based regularization", "\t", true);
        WangPatchRegularization(sensitivity[s_sub]);
        msec_timer_partial.stop();
      }
      
      if (scanner->type() == "CYLINDRICAL")
        fov->fixCorners(image);
      
      saveToDisk(iter, s_sub);
    }
    msec_timer.stop();
  }
}


void osemSymZG::computeSensitivity()
{
  std::string name_tosave = scanner->name() + "_Sensitivity";

  timer msec_timer("msec");
  timer msec_timer_partial("msec");
  
  msec_timer.start("Computing sensitivity", "", false);
  
  if (!use_z_model) {
    std::cout << "\tAllocating normalization histogram" << std::endl;
    Z_model.allocate(scanner->getDetectorsList(),
                     scanner->getCoincidenceSchema(),
                     input_hist_type, 1);
    name_tosave += "_OSEM_Sym_G";
  }
  else {
    scanner->reflectHistograms(Z_model, input_hist_type);
    name_tosave += "_osemSymZG";
  }
    
  
//   SubsetsList subsets = scanner->generateLorList(Z_model, n_subsets, seed);
//   Z_model.clear();
//   
//   const CoincidenceSchema& cs = scanner->getCoincidenceSchema();
//   for (uint16_t s_sub=0; s_sub<n_subsets; s_sub++) {
//     msec_timer_partial.start("subset: " + std::to_string(s_sub), "\t", true);
//     sensitivity.push_back( fov->createInitImage(0) );
//     std::cout << "size: " << subsets[s_sub].size() << std::endl;
//     #pragma omp parallel for
//     for (lorIDX_t i=0; i<subsets[s_sub].size(); i++) {
//       const CountsType& entry = subsets[s_sub][i];
//       // retrieving TOR
//       const detPair_t& p_id = entry.pairID();
//       const detPair_t& model_id = cs.at(p_id).symID();
//       const uint16_t& view_id = cs.at(p_id).uniqueView();
//       uint32_t n_cry2 = scanner->getDetector(entry.det2()).nCrystals();
//       lorIDX_t rel_lof = entry.cry1()*n_cry2 + entry.cry2();
//       MatrixEntry *start_pointer, *stop_pointer;
//       start_pointer = G_models.at(model_id).rowPointer(rel_lof);
//       stop_pointer = start_pointer + G_models.at(model_id).rowElementsN(rel_lof);
//       while(start_pointer < stop_pointer) {
//         uint32_t vx = (*start_pointer).col;
//         FloatType value = (*start_pointer).v;
//         #pragma omp atomic update
//         output_views[view_id][vx] += value*entry.counts();
//         start_pointer++;
//       }
//     }
  
  if ( attenuation_files.size() ) {
    std::cout << "\tUsing attenuation correction" << std::endl;
    Z_model *= attenuation;
  }
  
  msec_timer_partial.start("Generating subsets", "\t", true);
  Z_model.generateSubsets(subsets, n_subsets, seed);
  msec_timer_partial.stop();
  
//     to generate one subset for each detector pair, DEBUG
//     Z_model.generateDetSubsets(subsets);
//     n_subsets = subsets.size();
  
  const CoincidenceSchema& cs = scanner->getCoincidenceSchema();
  for (uint16_t s_sub=0; s_sub<n_subsets; s_sub++) {
    msec_timer_partial.start("subset: " + std::to_string(s_sub), "\t", false);
    sensitivity.push_back( fov->createInitImage(0) );
    #pragma omp parallel for
    for (lorIDX_t i=0; i<subsets[s_sub].size(); i++) {
      
      lorIDX_t cur_lor = subsets[s_sub][i].first;
      FloatType counts = subsets[s_sub][i].second;
      detPair_t p_id;
      lorIDX_t rel_lof;
      std::tie(p_id, rel_lof) = Z_model.identifyLOR(cur_lor);
      const uint16_t& view_id = cs.at(p_id).uniqueView();
      const detPair_t& model_id = cs.at(p_id).symID();

      MatrixEntry *start_p = G_models.at(model_id).rowPointer(rel_lof);
      MatrixEntry *stop_p = start_p + G_models.at(model_id).rowElementsN(rel_lof);

      while(start_p < stop_p) {
        uint32_t vx = (*start_p).col;
        FloatType value = (*start_p).v;
        #pragma omp atomic update
        output_views[view_id][vx] += value* counts;
        start_p++;
      }
    }
    
    composeViews();
    
    std::string psf_str = "";
    if (psf_sigma && (psf_type == 0 || psf_type == 2) ) {
      fov->gaussFilter(composed_img, psf_sigma, psf_width);
      psf_str += "_psf-" + std::to_string(psf_sigma);
      psf_str += "-" + std::to_string(psf_width);
    }

    
    #pragma omp parallel for
    for(uint32_t i=0; i<fov->nVoxels(); i++)
      sensitivity[s_sub][i] = composed_img[i];
    
    clearOutputViews();
    
    fov->applyRotMask(sensitivity[s_sub]);
    
    msec_timer_partial.stop();
    
    fov->saveImage(output_dir + "/" + name_tosave + psf_str + "_SUB" + std::to_string(s_sub+1),
                   sensitivity[s_sub], false);

  }
  
  msec_timer.stop();
}
