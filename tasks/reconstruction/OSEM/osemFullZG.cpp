/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vOSEM.h"

#pragma omp declare reduction(vec_float_plus : std::vector<FloatType> : \
std::transform(omp_out.begin(), omp_out.end(), omp_in.begin(), omp_out.begin(), std::plus<FloatType>())) \
initializer(omp_priv = decltype(omp_orig)(omp_orig.size()))


void osemFullZG::performRecon()
{
//   if (use_normalization_model) {
//     for (auto k : Z_model.keys()) {
//       for (uint32_t idx=0; idx<Z_model.data[k].size(); idx++) {
//         if (Z_model.data[k][idx] == 0)
//           Z_model.data[k][idx] = 1;
//         in_hist.data[k][idx] *= Z_model.data[k][idx];
//       }
//     }
//   }
  
  timer msec_timer("msec"), msec_timer_partial("msec");
  
  if (start_image != "")
    image = fov->loadImage(start_image);
  else
    image = fov->createInitImage(1);
  
  ImageType backup_image = fov->createInitImage(1);
  
  ImageType back_proj = fov->createInitImage(0);
  
  in_hist.generateSubsets(subsets, n_subsets, seed);
  if ( random_files.size() ) 
    random.generateSubsets(rnd_subsets, n_subsets, seed);
  
  for (uint16_t iter=1; iter <= n_iterations; iter++) {
    msec_timer.start("Iteration: " + std::to_string(iter), "", false);
    for (uint16_t s_sub=0; s_sub<n_subsets; s_sub++) {
      
      msec_timer_partial.start("Subset: " + std::to_string(s_sub), "\t", true);
      
      if (psf_sigma && (psf_type == 0 || psf_type == 1) ) {
//         msec_timer_partial.start("convolution image", "\t", true);
        for (uint32_t vx=0; vx<image.size(); vx++)
          backup_image[vx] = image[vx];
        fov->gaussFilter(image, psf_sigma, psf_width);
//         msec_timer_partial.stop();
      }
      
      
      #pragma omp parallel for reduction( vec_float_plus : back_proj )
      for (lorIDX_t i=0; i<subsets[s_sub].size(); i++) {
        
        lorIDX_t cur_lor = subsets[s_sub][i].first;

        detPair_t p_id, id1, id2;
        detCryID_t cry1, cry2;
        lorIDX_t rel_lof;
        
        std::tie(p_id, rel_lof) = in_hist.identifyLOR(cur_lor);
        std::tie(id1, cry1, id2, cry2) = scanner->getLORFullInfo(p_id, rel_lof);
        
        // computing TOR
        std::vector<MatrixEntry> row;
        const PlanarDetectorType& det1 = scanner->getDetector(id1);
        const PlanarDetectorType& det2 = scanner->getDetector(id2);
        
        
        projector->project(det1, cry1, det2, cry2, row);
        
        //computing projection
        FloatType projection = 0;
        for (const MatrixEntry& r : row) 
          projection += image[r.col] * r.v;
        
        if ( random_files.size() ) 
          projection += rnd_subsets[s_sub][i].second;
        
        // computing backprojection
        if (projection) {
          FloatType counts = subsets[s_sub][i].second;
          for (const MatrixEntry& t : row) {
            FloatType update = t.v*counts/projection;
//             #pragma omp atomic update
            back_proj[t.col] += update; 
          }
        }

      }

      
      if (psf_sigma && (psf_type == 0 || psf_type == 2) ) {
//         msec_timer_partial.start("convolution update", "\t", true);
        fov->gaussFilter(back_proj, psf_sigma, psf_width);
//         msec_timer_partial.stop();
        if (psf_type == 0 || psf_type == 1) {
          //restoring image prior of performing the update
          for (uint32_t vx=0; vx<image.size(); vx++)
            image[vx] = backup_image[vx];
        }
      }
      
      #pragma omp parallel for
      for (uint32_t vx=0; vx<image.size(); vx++) {
        if ( std::isnormal (back_proj[vx]) )
          image[vx] *= back_proj[vx];
        image[vx] *= sensitivity[s_sub][vx];
        back_proj[vx] = 0;
      }
      msec_timer_partial.stop();

      // REGULARIZATION
      if (smooth_sigma) {// && (s_sub+1) == n_subsets) {
        msec_timer_partial.start("filtering", "\t", true);
        fov->gaussFilter(image, smooth_sigma, smooth_width);
        msec_timer_partial.stop();
      }
      
      if (wang_beta) {
        msec_timer_partial.start("applying wang patch-based regularization", "\t", true);
        WangPatchRegularization(sensitivity[s_sub]);
        msec_timer_partial.stop();
      }
      

      for (uint32_t vx=0; vx<image.size(); vx++) 
        image[vx] *= fov->mask(vx);

      saveToDisk(iter, s_sub);      
    }
    msec_timer.stop();
  }
}


void osemFullZG::computeSensitivity()
{
  std::string name_tosave = scanner->name() + "_Sensitivity";

  timer msec_timer("msec"), msec_timer_partial("msec");
  
  msec_timer.start("Computing sensitivity", "", false);
  
  if (!use_z_model) {
    std::cout << "\tAllocating normalization histogram" << std::endl;
    Z_model.allocate(scanner->getDetectorsList(),
                     scanner->getCoincidenceSchema(),
                     input_hist_type, 1);
    name_tosave += "_OSEM_Full_G";
  }
  else 
    name_tosave += "_osemFullZG";
  
  name_tosave += "_" + projector->type();

  if ( attenuation_files.size() ) {
    std::cout << "\tUsing attenuation correction" << std::endl;
    Z_model *= attenuation;
  }
  
  msec_timer_partial.start("Generating subsets", "\t", true);
  Z_model.generateSubsets(subsets, n_subsets, seed);
  msec_timer_partial.stop();
  
//   to generate one subset for each detector pair, DEBUG
//   Z_model.generateDetSubsets(subsets);
//   n_subsets = subsets.size();
  
  for (uint16_t s_sub=0; s_sub<n_subsets; s_sub++) {
    
    msec_timer_partial.start("subset: " + std::to_string(s_sub), "\t", true);
    
    ImageType local_sens = fov->createInitImage(0);
    
    #pragma omp parallel for reduction( vec_float_plus : local_sens )
    for (lorIDX_t i=0; i<subsets[s_sub].size(); i++) {
      
      lorIDX_t cur_lor = subsets[s_sub][i].first;
      FloatType counts = subsets[s_sub][i].second;
      
      detPair_t p_id, id1, id2;
      detCryID_t cry1, cry2;
      lorIDX_t rel_lof;
      
      std::tie(p_id, rel_lof) = Z_model.identifyLOR(cur_lor);
      std::tie(id1, cry1, id2, cry2) = scanner->getLORFullInfo(p_id, rel_lof);
      
      const PlanarDetectorType& det1 = scanner->getDetector(id1);
      const PlanarDetectorType& det2 = scanner->getDetector(id2);
      
      std::vector<MatrixEntry> row;
      projector->project(det1, cry1, det2, cry2, row);
      
      for (const MatrixEntry& t : row)
        local_sens[t.col] += t.v*counts;
      
    }
    
    std::string psf_str = "";
    if (psf_sigma && (psf_type == 0 || psf_type == 2) ) {
      fov->gaussFilter(local_sens, psf_sigma, psf_width);
      psf_str += "_psf-" + std::to_string(psf_sigma);
      psf_str += "-" + std::to_string(psf_width);
    }
    
    sensitivity.push_back(local_sens);
    msec_timer_partial.stop();
    
    fov->saveImage(output_dir + "/" + name_tosave + psf_str + "_SUB" + std::to_string(s_sub+1),
                   local_sens, false);
  }
  
  msec_timer.stop();
  
}



