#include "sirtFullZG.h"

#pragma omp declare reduction(vec_float_plus : std::vector<FloatType> : \
std::transform(omp_out.begin(), omp_out.end(), omp_in.begin(), omp_out.begin(), std::plus<FloatType>())) \
initializer(omp_priv = decltype(omp_orig)(omp_orig.size()))

void SirtFullZG::performRecon()
{
  
  timer msec_timer("msec"), msec_timer_partial("msec");
  image = fov->createInitImage(0);
  ImageType backup_image = fov->createInitImage(0);
  
  ImageType back_proj = fov->createInitImage(0);
  
  msec_timer.start("Generating subsets", "", true);
  in_hist.generateSubsets(subsets, n_subsets, seed);
  if ( random_files.size() ) 
    random.generateSubsets(rnd_subsets, n_subsets, seed);
  msec_timer.stop();
  
  for (uint16_t iter=1; iter <= n_iterations; iter++) {
    msec_timer.start("Iteration: " + std::to_string(iter), "", false);
    for (uint16_t s_sub=0; s_sub<n_subsets; s_sub++) {
      
      msec_timer_partial.start("Subset: " + std::to_string(s_sub), "\t", true);
      
      if (psf_sigma) {
        for (uint32_t vx=0; vx<image.size(); vx++)
          backup_image[vx] = image[vx];
        fov->gaussFilter(image, psf_sigma, psf_width);
      }
      
      #pragma omp parallel for reduction( vec_float_plus : back_proj )
      for (lorIDX_t i=0; i<subsets[s_sub].size(); i++) {
        
        lorIDX_t cur_lor = subsets[s_sub][i].first;
        
        detPair_t p_id, id1, id2;
        detCryID_t cry1, cry2;
        lorIDX_t rel_lof;
        
        std::tie(p_id, rel_lof) = in_hist.identifyLOR(cur_lor);
        std::tie(id1, cry1, id2, cry2) = scanner->getLORFullInfo(p_id, rel_lof);
        
        // computing TOR
        std::vector<MatrixEntry> row;
        const PlanarDetectorType& det1 = scanner->getDetector(id1);
        const PlanarDetectorType& det2 = scanner->getDetector(id2);
        
        
        projector->project(det1, cry1, det2, cry2, row);
        
        //computing projection
        FloatType projection = 0, sum_p = 0;
        for (const MatrixEntry& r : row) {
          projection += image[r.col] * r.v;
          sum_p += r.v;
        }
        if (use_normalization_model)
          projection *= Z_model.data.at(p_id).at(rel_lof);
        // moltiplicare la proiezione per l'hist della norm)
        
        if ( random_files.size() ) 
          projection += rnd_subsets[s_sub][i].second;
        
        // computing backprojection
        if (sum_p > 0) {
          FloatType ratio = (subsets[s_sub][i].second - projection)/sum_p;
          for (const MatrixEntry& t : row) {
            FloatType update = lambda*t.v*ratio;
            back_proj[t.col] += update; 
          
          }
        }
      }
      
      if (bpconv_sigma) {
        fov->gaussFilter(back_proj, bpconv_sigma, bpconv_width);
      }
      
      if (psf_sigma) {
        fov->gaussFilter(back_proj, psf_sigma, psf_width);
        for (uint32_t vx=0; vx<image.size(); vx++)
          image[vx] = backup_image[vx];
      }
      
      
      #pragma omp parallel for
      for (uint32_t vx=0; vx<image.size(); vx++) {
        if ( std::isnormal (back_proj[vx]) )
          image[vx] += back_proj[vx]*sensitivity[s_sub][vx];
        back_proj[vx] = 0;
      }
      msec_timer_partial.stop();
      
      
      if (gauss_sigma) {// && (s_sub+1) == n_subsets) {
        msec_timer_partial.start("filtering", "\t", true);
        fov->gaussFilter(image, gauss_sigma, gauss_width);
        msec_timer_partial.stop();
      }

      for (uint32_t vx=0; vx<image.size(); vx++) 
        image[vx] *= fov->mask(vx);
      
      saveToDisk(iter, s_sub);
      
    }
    msec_timer.stop();
  }

}



void SirtFullZG::computeSensitivity()
{
  std::string name_tosave = scanner->name() + "_Sensitivity";
  if (scanner->type() == "CYLINDRICAL") 
    name_tosave += "_1vs" + std::to_string( ((CylindricalGeometry*) scanner)->getCoincidenceSectorStep()*2+1 );
  
  timer msec_timer("msec"), msec_timer_partial("msec");
  
  msec_timer.start("Computing sensitivity", "", false);
  
  if (use_normalization_model) {
//     std::cout << "Performing normalization correction" << std::endl;
//     
//     for (detPair_t k : in_hist.keys() )
//       for (uint32_t idx=0; idx < in_hist.data.at(k).size(); idx++)
//         in_hist.data.at(k).at(idx) /= Z_model.data.at(k).at(idx);
//   
    name_tosave += "_SIRT_Full_ZG";
  }
  else {
    std::cout << "Allocating uniform normalization histogram" << std::endl;
    Z_model.allocate(scanner->getDetectorsList(),
                     scanner->getCoincidenceSchema(),
                     input_hist_type, 1);
    name_tosave += "_SIRT_Full_G";
  }

  name_tosave += "_" + projector->type();

  Z_model.generateSubsets(subsets, n_subsets, seed);
  
  for (uint16_t s_sub=0; s_sub<n_subsets; s_sub++) {
    
    msec_timer_partial.start("subset: " + std::to_string(s_sub), "\t", false);
    
    ImageType local_sens = fov->createInitImage(0);
    
    #pragma omp parallel for reduction( vec_float_plus : local_sens )
    for (lorIDX_t i=0; i<subsets[s_sub].size(); i++) {
      
      lorIDX_t cur_lor = subsets[s_sub][i].first;
      FloatType counts = subsets[s_sub][i].second;
      
      detPair_t p_id, id1, id2;
      detCryID_t cry1, cry2;
      lorIDX_t rel_lof;
      
      std::tie(p_id, rel_lof) = Z_model.identifyLOR(cur_lor);
      std::tie(id1, cry1, id2, cry2) = scanner->getLORFullInfo(p_id, rel_lof);
      
      const PlanarDetectorType& det1 = scanner->getDetector(id1);
      const PlanarDetectorType& det2 = scanner->getDetector(id2);
      
      std::vector<MatrixEntry> row;
      projector->project(det1, cry1, det2, cry2, row);
      
      for (const MatrixEntry& t : row)
        local_sens[t.col] += t.v*counts;
      
    }
    
    if (bpconv_sigma)
      fov->gaussFilter(local_sens, bpconv_sigma, bpconv_width);
    
    std::string psf_str = "";
    if (psf_sigma) {
      fov->gaussFilter(local_sens, psf_sigma, psf_width);
      psf_str += "_psf-" + std::to_string(psf_sigma);
      psf_str += "-" + std::to_string(psf_width);
    }
    
    sensitivity.push_back(local_sens);
    msec_timer_partial.stop();
    
    
    
    fov->saveImage(output_dir + "/" + name_tosave + psf_str + "_SUB" + std::to_string(s_sub+1),
                   local_sens);
  }
  
  msec_timer.stop();
  
}
