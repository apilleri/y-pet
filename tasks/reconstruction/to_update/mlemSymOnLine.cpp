/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "mlemSymOnLine.h"
#include "CylindricalGeometry.h"

// This works only with the geometric matrix of the real scanner, it's not 
// possible to use it with the virtual geometry (we may use it with the two-step)

void MlemSymOnLine::performRecon()
{
  scanner->reflectHistograms(in_hist, "LORs");
  
  timer msec_timer("msec"), msec_timer_partial("msec");
  
  if (start_image != "")
    image = fov->loadImage(start_image);
  else
    image = fov->createInitImage(1);
  
  const GroupedCS& grouped_cs = scanner->getGroupedCS();
  std::vector< std::mutex> locks(fov->nVoxels());
  
  for (uint16_t iter=1; iter <= n_iterations; iter++) {
    msec_timer.start("Iteration: " + std::to_string(iter), "", false);
    createViews();
    
    
    for (uint16_t f_pair=0; f_pair<grouped_cs.size(); f_pair++) {
      detID_t f_id1 = grouped_cs[f_pair][0].id1();
      detID_t f_id2 = grouped_cs[f_pair][0].id2();
      
      uint32_t n_ac1 = scanner->getDetector(f_id1).aCryN();
      uint32_t n_ac2 = scanner->getDetector(f_id2).aCryN();
      lorIDX_t n_LORs = n_ac1 * n_ac2;
      
      uint16_t n_views = grouped_cs[f_pair].size();
      
      msec_timer_partial.start("proj", "\t", true);
      std::vector< std::vector<FloatType> > projections;
      projections.resize(n_views);
      for (uint16_t v=0; v<n_views; v++) 
        projections[v].resize(n_LORs, 0);
      
      #pragma omp parallel for
      for (lorIDX_t c_lor=0; c_lor<n_LORs; c_lor++) {
        uint16_t c1 = c_lor / n_ac2;
        uint16_t c2 = c_lor % n_ac2;
        // computing TOR
        std::vector<MatrixEntry> row;
        const PlanarDetectorType& det1 = scanner->getDetector(f_id1);
        const PlanarDetectorType& det2 = scanner->getDetector(f_id2);
        projector->project(det1, c1, det2, c2, row);
        for (uint16_t v=0; v<n_views; v++) {
          const detPair_t& pair_id = grouped_cs[f_pair][v].pairID();
          FloatType value = in_hist.data[pair_id][c_lor]; 
          if (value) {
            const uint16_t& view_id = grouped_cs[f_pair][v].uniqueView();
            for (const MatrixEntry& r : row)
              projections[v][c_lor] += estim_views[view_id][r.col] * r.v;
          }
          
        }
      }
      msec_timer_partial.stop();
      
      
      for (uint16_t v=0; v<n_views; v++) {
        #pragma omp parallel for
        for (lorIDX_t c_lor=0; c_lor<n_LORs; c_lor++) {
          detPair_t pair_id = grouped_cs[f_pair][v].pairID();
          FloatType value = in_hist.data[pair_id][c_lor];
          FloatType temp = value/projections[v][c_lor];
          if ( std::isnan(temp) || std::isinf(temp))
            projections[v][c_lor] = 0;
          else
            projections[v][c_lor] = temp;
        }
      }
      
      
      
      msec_timer_partial.start("backproj", "\t", true);
      #pragma omp parallel for
      for (lorIDX_t c_lor=0; c_lor<n_LORs; c_lor++) {
        uint16_t c1 = c_lor / n_ac2;
        uint16_t c2 = c_lor % n_ac2;
        // computing TOR
        std::vector<MatrixEntry> row;
        const PlanarDetectorType& det1 = scanner->getDetector(f_id1);
        const PlanarDetectorType& det2 = scanner->getDetector(f_id2);
        projector->project(det1, c1, det2, c2, row); 
        
        std::vector<uint16_t> nnviews;
        nnviews.reserve(n_views);
        for (uint16_t v=0; v<n_views; v++)
          if (projections[v][c_lor])
            nnviews.push_back(v);
          
        for (const MatrixEntry& t : row) {
          locks[t.col].lock();
          for (uint16_t v : nnviews) {
            const uint16_t& view_id = grouped_cs[f_pair][v].uniqueView();
            output_views[view_id][t.col] += t.v*projections[v][c_lor];
          }
          locks[t.col].unlock();
        }
      }
      
      msec_timer_partial.stop();
    }
    
    composeViews();
    
    // updating 
    msec_timer_partial.start("updating", "\t", true);
    #pragma omp parallel for
    for (uint32_t vx=0; vx<image.size(); vx++)
      image[vx] *= composed_img[vx]*sensitivity[vx];
    msec_timer_partial.stop();
    
    msec_timer.stop();
    
    if (scanner->type() == "CYLINDRICAL")
      fov->fixCorners(image);
    
    if (iter % save_every == 0 || iter == 1)
      fov->saveImage(out_filename + "_" + std::to_string(iter), image);
  }
  
}

void MlemSymOnLine::computeSensitivity()
{
  std::string name_tosave = scanner->name() + "_Sensitivity";
  if (scanner->type() == "CYLINDRICAL") 
    name_tosave += "_1vs" + std::to_string( ((CylindricalGeometry*) scanner)->getCoincidenceSectorStep()*2+1 );
  
  name_tosave += "_SYM_OnLine";
  
  timer msec_timer("msec");
  timer msec_timer_partial("msec");
  
  if (!use_normalization_model) {
    std::cout << "Allocating normalization histogram" << std::endl;
    Z_model.allocate(scanner->getDetectorsList(),
                     scanner->getCoincidenceSchema(),
                     input_hist_type, 1);
  }
  data_type& nd = Z_model.data;
  
  msec_timer.start("Computing sensitivity", "", false);
  
  const GroupedCS& grouped_cs = scanner->getGroupedCS();
  
  for (uint16_t f_pair=0; f_pair<grouped_cs.size(); f_pair++) {

    detID_t f_id1 = grouped_cs[f_pair][0].id1();
    detID_t f_id2 = grouped_cs[f_pair][0].id2();
    
    uint32_t n_ac1 = scanner->getDetector(f_id1).aCryN();
    uint32_t n_ac2 = scanner->getDetector(f_id2).aCryN();
    lorIDX_t n_LORs = n_ac1 * n_ac2;
    
    msec_timer_partial.start("backproj " + std::to_string(f_pair), "\t", true);
    #pragma omp parallel for
    for (lorIDX_t c_lor=0; c_lor<n_LORs; c_lor++) {
      uint16_t c1 = c_lor / n_ac2;
      uint16_t c2 = c_lor % n_ac2;
      // computing TOR
      std::vector<MatrixEntry> row;
      const PlanarDetectorType& det1 = scanner->getDetector(f_id1);
      const PlanarDetectorType& det2 = scanner->getDetector(f_id2);
      projector->project(det1, c1, det2, c2, row); 
      
      for (uint16_t ip=0; ip<grouped_cs[f_pair].size(); ip++) {
        const detPair_t& pair_id = grouped_cs[f_pair][ip].pairID();
        const uint16_t& view_id = grouped_cs[f_pair][ip].uniqueView();
        for (const MatrixEntry& t : row) {
          FloatType value = nd[pair_id][c_lor]; 
          #pragma omp atomic update
          output_views[view_id][t.col] += t.v*value;
        }
      }
    }
    msec_timer_partial.stop();
  }
  
  
  
  composeViews();
  
  #pragma omp parallel for
  for(uint32_t i=0; i<fov->nVoxels(); i++) {
    sensitivity[i] = composed_img[i];
  }
  
  fov->applyRotMask(sensitivity);
  
  msec_timer.stop();
  fov->saveImage(output_dir + "/" + name_tosave + "_MLEM", sensitivity);
  
  Z_model.clear();
}
