/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TASKS_RECONSTRUCTION_SIRTFULLZG_H_
#define TASKS_RECONSTRUCTION_SIRTFULLZG_H_

#include "vReconstruction.h"
#include "CylindricalGeometry.h"

class SirtFullZG : public vReconstruction {
  
  StringVec sens_files;
  std::vector<ImageType> sensitivity;
  
  FloatType lambda;
  
public:
  SirtFullZG(vGeometry* s, FieldOfView* f, vProjector* p,
                 const cxxopts::ParseResult& cmd_line)
  : vReconstruction(s, f, p, false, false, "aLORs", cmd_line),
  lambda(1)
  {    
    if ( !cmd_line.count("compute-sensitivity") ) {
      sens_files = cmd_line["sensitivity"].as< StringVec >();
    }
    
    if ( cmd_line.count("save-all-subsets") )
      save_all_subsets = true;
    
    prepareSensitivity();
  }
  
  ~SirtFullZG()
  { }
  
  void performRecon() override;
  
private:
  void prepareSensitivity()
  {
    if ( sens_files.size() ) {
      for (const std::string& str : sens_files) 
        sensitivity.push_back( fov->loadImage(str) );
    } else {
      computeSensitivity();
      if (n_subsets > 1) {
        ImageType full_sens = fov->createInitImage(0);
        for (ImageType subset : sensitivity) {
          for (uint32_t vx=0; vx < subset.size(); vx++)
            full_sens[vx] += subset[vx];
        }
        std::string psf_str = "";
        if (psf_sigma) {
          psf_str += "_psf-" + std::to_string(psf_sigma);
          psf_str += "-" + std::to_string(psf_width);
        }
        if (projector)
          fov->saveImage(output_dir + "/full_sens_" + projector->type() + psf_str, full_sens);
        else
          fov->saveImage(output_dir + "/full_sens" + psf_str , full_sens);
      }
    }
    
    //INVERTING Sensitivity
    for (uint16_t s_sub=0; s_sub<n_subsets; s_sub++) {
      #pragma omp parallel for
      for(uint32_t i=0; i<fov->nVoxels(); i++) {
        sensitivity[s_sub][i] = 1/sensitivity[s_sub][i];
        if (! std::isnormal(sensitivity[s_sub][i]) )
          sensitivity[s_sub][i] = 0;
      }
    }
  }
  
  void computeSensitivity();
};


#endif // TASKS_RECONSTRUCTION_SIRTFULLZG_H_


