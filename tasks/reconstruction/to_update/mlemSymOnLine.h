/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TASKS_RECONSTRUCTION_MLEMSYMONLINE_H_
#define TASKS_RECONSTRUCTION_MLEMSYMONLINE_H_

#include "vReconstruction.h"

class MlemSymOnLine : public vReconstruction {
  
  uint16_t n_iterations;
  uint16_t save_every;
  
  ImageType sensitivity;

public:
  MlemSymOnLine(vGeometry* s, FieldOfView* f, vProjector* p,
                const cxxopts::ParseResult& cmd_line)
  : vReconstruction(s, f, p, false, false, "aLORs", cmd_line),
    n_iterations(1),
    save_every(1)
  {
    if (cmd_line.count("number"))
      n_iterations = cmd_line["number"].as<uint16_t>();
    
    if (cmd_line.count("every"))
      save_every = cmd_line["every"].as<uint16_t>();
    
    createAnglesAndInitViews();
    
    sensitivity.resize( fov->nVoxels() );
    if ( cmd_line.count("compute-sensitivity") )
      computeSensitivity();
    else {
      std::string fname = cmd_line["sensitivity"].as<std::string>();
      
      std::ifstream sensitivityFile;
      sensitivityFile.open(fname);
      if ( !sensitivityFile.is_open() )
        throw std::string ( "Unable to open file " + fname + " for reading");
      
      uint32_t n_voxels = fov->nVoxels();
      char* pointer = reinterpret_cast<char *>(sensitivity.data());
      sensitivityFile.read(pointer, n_voxels*sizeof(float) );
      if (!sensitivityFile) {
        std::string error("Error reading sensitivity: expected ");
        error += std::to_string(n_voxels*sizeof(float));
        error += " bytes but read only ";
        error += std::to_string(sensitivityFile.gcount());
        throw error;
      }
      sensitivityFile.close();
    }
    //INVERTING Sensitivity
    #pragma omp parallel for
    for(uint32_t i=0; i<fov->nVoxels(); i++) {
      sensitivity[i] = 1/sensitivity[i];
      if (! std::isnormal(sensitivity[i]) )
        sensitivity[i] = 0;
    }
  }
  
  ~MlemSymOnLine()
  { }
  
  
  void performRecon() override;
  
  
private:
  void computeSensitivity();
  
};

#endif // TASKS_RECONSTRUCTION_MLEMSYMONLINE_H_

