/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "sirtSymOffLine.h"
#include "CylindricalGeometry.h"


void SirtSymOffLine::performRecon()
{
  /*
  timer msec_timer("msec"), msec_timer_partial("msec");
  
  image = fov->createInitImage(0);
    
  const GroupedCS& grouped_cs = scanner->getGroupedCS();
  
  for (uint16_t iter=1; iter <= n_iterations; iter++) {
    msec_timer.start("Iteration: " + std::to_string(iter), "", false);
    
    createViews();
    for (uint16_t cur_view=0; cur_view<updates_views.size(); cur_view++) {
      std::fill(std::execution::par_unseq,
                updates_views[cur_view], updates_views[cur_view] + fov->nVoxels(),
                0);
    }

    for (uint16_t f_pair=0; f_pair<grouped_cs.size(); f_pair++) {
      detID_t f_id1 = grouped_cs[f_pair][0].id1();
      uint16_t n_cry1 = scanner->getDetector(f_id1).nCrystals();
      detID_t f_id2 = grouped_cs[f_pair][0].id2();
      uint16_t n_cry2 = scanner->getDetector(f_id2).nCrystals();
      lorIDX_t n_lors = n_cry1 * n_cry2;
      
      uint16_t n_views = grouped_cs[f_pair].size();
      detPair_t model_id = grouped_cs[f_pair][0].pairID();
      
//       std::vector<uint16_t> active_views(n_views);
//       for (uint16_t v=0; v<n_views; v++)
//         active_views[v] = grouped_cs[f_pair][v].uniqueView();
      
      msec_timer_partial.start("projection G", "\t", true);
      std::vector< FloatType* > projections;
      projections.resize(n_views);
      for (uint16_t v=0; v<n_views; v++) {
        projections[v] = new FloatType[n_lors];
        std::fill(projections[v], projections[v] + n_lors, 0);
      }
      #pragma omp parallel for
      for (lorIDX_t c_lor=0; c_lor<n_lors; c_lor++) {
        MatrixEntry *bp, *ep;
        bp = G_models.at(model_id).rowPointer(c_lor);
        ep = bp + G_models.at(model_id).rowElementsN(c_lor);
        std::vector<FloatType> temp_proj(n_views, 0);
        while(bp < ep) {
          uint32_t vx = (*bp).col;
          FloatType tor_value = (*bp).v;
          for (uint16_t v=0; v<n_views; v++) {
            const uint16_t& view_id = grouped_cs[f_pair][v].uniqueView();
            temp_proj[v] += estim_views[ view_id ][vx] * tor_value;
          }
          bp++;
        }

        for (uint16_t v=0; v<n_views; v++) {
          detPair_t p_id = grouped_cs[f_pair][v].pairID();
          FloatType counts = in_hist.data[p_id][c_lor];
          projections[v][c_lor] = (counts - temp_proj[v]) * sensitivity.data[model_id][c_lor];
        }
        
        
      }
      msec_timer_partial.stop();
      
      msec_timer_partial.start("backprojection G", "\t", true);
      #pragma omp parallel for
      for (lorIDX_t c_lor=0; c_lor<n_lors; c_lor++) {
        MatrixEntry *bp, *ep;
        bp = G_models.at(model_id).rowPointer(c_lor);
        ep = bp + G_models.at(model_id).rowElementsN(c_lor);
        while(bp < ep) {
          uint32_t vx = (*bp).col;
          FloatType tor_value = (*bp).v;
          for (uint16_t v=0; v<n_views; v++) {
            const uint16_t& view_id = grouped_cs[f_pair][v].uniqueView();
//             #pragma omp atomic update
//             output_views[view_id][vx] += tor_value*projections[v][c_lor];
//             #pragma omp atomic update
//             updates_views[view_id][vx] ++;
            #pragma omp critical
            {
              output_views[view_id][vx] += tor_value*projections[v][c_lor];
              updates_views[view_id][vx] ++;
            }
          }
          bp++;
        }
        
      }
      msec_timer_partial.stop();
      
      for (uint16_t v=0; v<n_views; v++)
        delete[] projections[v];
    }
    
    composeViews();
    composeUpdates();
    
    // updating
    msec_timer_partial.start("updating", "\t", true);
    #pragma omp parallel for
    for (uint32_t c_vx=0; c_vx<image.size(); c_vx++) {
      FloatType update = composed_img[c_vx]/composed_updates[c_vx];
      if ( !std::isinf(update) && !std::isnan(update) )
        image[c_vx] += update;
    }
    msec_timer_partial.stop();
    
    if (scanner->type() == "CYLINDRICAL")
      fov->fixCorners(image);
    
    if (iter % save_every == 0 || iter == 1) 
      fov->saveImage(out_filename + "_" + std::to_string(iter), image);
    
    
    msec_timer.stop();
  }
  */
}


void SirtSymOffLine::computeSensitivity()
{
  /*
  timer msec_timer("msec");
  timer msec_timer_partial("msec");
  
  
  // sensitivty will have only the fundamental pairs as the normalization factors
  // are dumped to the projections counts as the matrix is invertible (diagonal)
  // and there it doesn't matter if we change the distribution of the data 
  // as we are not modelling a particular statistical behaviour
  
  msec_timer.start("Computing sensitivity", "", false);
  
  const GroupedCS& grouped_cs = scanner->getGroupedCS();
  
  for (uint16_t f_pair=0; f_pair<grouped_cs.size(); f_pair++) {
    std::cout << "f_pair " << f_pair << std::endl;
    detID_t f_id1 = grouped_cs[f_pair][0].id1();
    uint16_t n_cry1 = scanner->getDetector(f_id1).nCrystals();
    detID_t f_id2 = grouped_cs[f_pair][0].id2();
    uint16_t n_cry2 = scanner->getDetector(f_id2).nCrystals();
    lorIDX_t n_lors = n_cry1 * n_cry2;
    detPair_t model_id = grouped_cs[f_pair][0].pairID();
    
    sensitivity.data[model_id].resize(n_lors, 0);

    msec_timer_partial.start("f_pair " + std::to_string(f_pair), "\t", true);
    #pragma omp parallel for
    for (lorIDX_t c_lor=0; c_lor<n_lors; c_lor++) {
      MatrixEntry *bp = G_models.at(model_id).rowPointer(c_lor);
      MatrixEntry *ep = bp + G_models.at(model_id).rowElementsN(c_lor);
      
      while( bp<ep ) {
        FloatType tor_value = bp->v;
        sensitivity.data[model_id][c_lor] += tor_value * tor_value;
        bp++;
      }
      
      //inverting and fixing zeros
      const FloatType& temp = sensitivity.data[model_id][c_lor];
      if (temp)
        sensitivity.data[model_id][c_lor] = 1/temp;
      else
        sensitivity.data[model_id][c_lor] = 0;
    }
    msec_timer_partial.stop();
  }
  msec_timer.stop();
  */
}
