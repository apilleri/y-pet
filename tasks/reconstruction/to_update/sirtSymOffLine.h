/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TASKS_RECONSTRUCTION_SIRTSYMOFFLINE_H_
#define TASKS_RECONSTRUCTION_SIRTSYMOFFLINE_H_

#include "vReconstruction.h"

class SirtSymOffLine : public vReconstruction {
  
  uint16_t n_iterations;
  uint16_t n_subsets;
  uint16_t save_every;
  
  ImageType c_matrix;
  HistogramsMap r_matrix;;
  
//   std::vector<FloatType*> updates_views;
  ImageType composed_updates;
  

public:
  SirtSymOffLine(vGeometry* s, FieldOfView* f, vProjector* p,
                 const cxxopts::ParseResult& cmd_line)
  : vReconstruction(s, f, p, true, false, "aLORs", cmd_line),
  n_iterations(1), n_subsets(1),
  save_every(1)
  {
    if ( cmd_line.count("subsets") )
      n_subsets = cmd_line["subsets"].as<uint16_t>();
    
    if (cmd_line.count("number"))
      n_iterations = cmd_line["number"].as<uint16_t>();
    
    if (cmd_line.count("every"))
      save_every = cmd_line["every"].as<uint16_t>();
    
    computeSensitivity();

  }
  
  ~SirtSymOffLine()
  { }
  
  void performRecon() override;
  
private:
  void computeSensitivity();
  
//   void createAndInitUpdateViews()
//   {
//     uint16_t n_uv = scanner->getUniqueViews().size();
//     uint32_t n_vx = fov->nVoxels();
//     
//     updates_views.resize(n_uv);
//     for (uint16_t v=0; v<n_uv; v++) {
//       updates_views[v] = new FloatType[ n_vx ];
//       std::fill(updates_views[v], updates_views[v]+n_vx, 0);
//     }
//     
//     composed_updates.resize(n_vx, 0);
//   }
  
//   void composeUpdates()
//   {
//     timer msec_timer("msec");
//     msec_timer.start("composing updates", "\t", true);
//     for (const AngleType& zA : unique_angles)
//       std::fill(angle_views[zA].begin(), angle_views[zA].end(), 0);
//     
//     uint32_t n_voxels = fov->nVoxels();
//     ImageType temp_image;
//     temp_image.resize(n_voxels, 0);
//     const ViewsList& uv = scanner->getUniqueViews();
//     for (uint16_t cur_view=0; cur_view<uv.size(); cur_view++) {
//       //     fov->saveImage("view_"+std::to_string(cur_view), output_views[cur_view]);
//       
//       AngleType zA = uv[cur_view].zAngle();
//       ThreeVector shift(0, uv[cur_view].yShift(), uv[cur_view].zShift() );
//       bool xR = uv[cur_view].xReflect();
//       bool yR = uv[cur_view].yReflect();
//       bool zR = uv[cur_view].zReflect();
//       
//       fov->flipImage(updates_views[cur_view], temp_image.data(), xR, yR, zR, false);
//       fov->shiftImage(temp_image.data(), angle_views[zA].data(), shift, true);
//     }
//     
//     std::fill(std::execution::par_unseq, composed_updates.begin(), composed_updates.end(), 0);
//     
//     for (const AngleType& zA : unique_angles) {
//       //     fov->saveImage("angle_"+std::to_string(zA), angle_views[zA]);
//       fov->rotateImage2D(angle_views[zA].data(), composed_updates.data(), -zA, true);
//     }
//     
//     msec_timer.stop();
//   }
//   

  
};


#endif // TASKS_RECONSTRUCTION_SIRTSYMOFFLINE_H_

