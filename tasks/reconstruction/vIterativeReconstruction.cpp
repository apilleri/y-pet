/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 * 2021 Luigi MASTURZO
 */


#include "vIterativeReconstruction.h"


void vIterativeReconstruction::computeWeight(uint32_t vx, uint32_t vy, uint32_t vz,
                                             std::vector<float>& weights,
                                             uint32_t idx)
{
  //pesi geometrici
  float h1 = 0.146446; // sui pixel 1,3,5,7
  float h2 = 0.103553; // sui pixel 0,2,6,8
  
  //per facilitarmi con la sommatoria creo un array dei pesi. In posizione del centale metto 0
  float geometric_weights[9] = {h2, h1, h2, h1, 0, h1, h2, h1, h2};
  
  uint32_t len_kernel = weights.size();
  uint32_t len_k = std::sqrt(len_kernel);
  
  std::vector<uint32_t> voxels(len_kernel, 0);
  uint32_t c = 0;
  for (uint32_t x = 0; x < len_k; x++)
  {
    for (uint32_t y = 0; y < len_k; y++)
    {
      uint32_t x_idx = vx - (len_k - 1) / 2 - 1 + x;
      uint32_t y_idx = vy - (len_k - 1) / 2 - 1 + y;
      voxels[c + y] = fov->computeLinearIndex(x_idx, y_idx, vz);
    }
    c += len_k;
  }
  
  std::vector<float> w_pixel(len_kernel, 0.);
  
  for (uint32_t i = 0; i < voxels.size(); i++) {
    uint16_t x_patch = fov->getX(voxels[i]);
    uint16_t y_patch = fov->getY(voxels[i]);
    

    //patch_weight_pixel_0 contiene i pesi di una data patch.
    std::vector<float> patch_weight_pixel(len_kernel, 0.);
    uint32_t c = 0;
    
    for (uint32_t x = 0; x < len_k; x++) {
      for (uint32_t y = 0; y < len_k; y++) {
        uint32_t x_idx = x_patch - (len_k - 1) / 2 - 1 + x;
        uint32_t y_idx = y_patch - (len_k - 1) / 2 - 1 + y;
        uint32_t id_1 = fov->computeLinearIndex(x_idx, y_idx, vz);
        float a = geometric_weights[c + y] * pow((image[id_1] - image[voxels[i]]), 2);
        patch_weight_pixel[c + y] = 1 / (abs(a) + wang_delta);
        //patch_weight_pixel_0[c + y] = 1 / (delta);
        //patch_weight_pixel_0[c + y] = 1 / (sqrt(a*a + delta*delta));;
      }
      c = c + len_k;
    }
    
    //devo riempire wpixel1
    
    for (uint32_t j = 0; j < len_kernel; j++)
    {
      w_pixel[i] += geometric_weights[j] * patch_weight_pixel[j];
    }
    
  }
  //sommo su tutti i pesi.
  for (uint32_t i = 0; i < len_kernel; i++)
  {
    weights[idx] += geometric_weights[i] * w_pixel[i];
  }
  
}

void vIterativeReconstruction::WangPatchRegularization(const ImageType& cur_sensitivity)
{
  // creo immagine di zeri e definisco vari parametri
  // questa versione va bene per un kernel 3x3 (come da articolo)
  ImageType temp_image = fov->createInitImage(0);
  SpaceType len_x = fov->vxN(0);
  SpaceType len_y = fov->vxN(1);
  SpaceType len_z = fov->vxN(2);
  uint32_t len_k = 3; //kernel size
  uint32_t len_kernel = len_k * len_k;
  //pesi geometrici
  float h1 = 0.146446; // sui pixel 1,3,5,7
  float h2 = 0.103553; // sui pixel 0,2,6,8
  
  //per facilitarmi con la sommatoria creo un array dei pesi. In posizione del centale metto 0
  float geometric_weights[9] = {h2, h1, h2, h1, 0, h1, h2, h1, h2};
  
  //looppo su z
  for (uint32_t vz = 0; vz < len_z; vz++) {
    //looppo sulle y
    for (uint32_t vy = 0; vy < len_y; vy++) {
      // escludo le y ai bordi per una lunghezza pari a 3 * len(k). Doverebbe essere solo len_k
      
      if (vy > (2 * len_k) && vy < len_y - (2 * len_k)) {
        //looppo sulle x
        for (uint32_t vx = 0; vx < len_x; vx++) {
          // escludo le x ai bordi per una lunghezza pari a 3 * len(k). Doverebbe essere solo len_k
          if (vx > (2 * len_k) && vx < len_x - (2 * len_k)) {
            uint32_t id0 = fov->computeLinearIndex(vx, vy, vz); //id del pixel centrale
            

            
            //per ogni pixel della patch devo avere un fattore di peso.
            //i pixel interessati sono dentro voxels. ognuno di questi diventa il pixel centrale
            
            //creo array dei pesi totali(della patch iniziale)
            std::vector<float> weights(len_kernel, 0.);
            
            computeWeight(vx - 1, vy - 1, vz, weights, 0);
            computeWeight(vx, vy - 1, vz, weights, 1);
            computeWeight(vx + 1, vy - 1, vz, weights, 2);
            computeWeight(vx - 1, vy, vz, weights, 3);
            computeWeight(vx, vy, vz, weights, 4);
            computeWeight(vx + 1, vy, vz, weights, 5);
            computeWeight(vx - 1, vy + 1, vz, weights, 6);
            computeWeight(vx, vy + 1, vz, weights, 7);
            computeWeight(vx + 1, vy + 1, vz, weights, 8);
            
            
            //sommo su tutti i pesi della patch iniziale
            //float norm_w = 0;
            float final_weight = 0;
            for (uint32_t i = 0; i < len_kernel; i++) {
              final_weight += geometric_weights[i] * weights[i];
              //norm_w += weights[i];
            }
            
            //sembra che non siano normalizzati, provo a farlo a mano con norm_w
            //final_weight = final_weight;
            
            //sono gli indici dei pixel di ogni patch. Ho una patch da 9 pixel.
            std::vector<uint32_t> voxels_4(len_kernel,0);
            
            uint32_t c = 0;
            for (uint32_t x = 0; x < len_k; x++) {
              for (uint32_t y = 0; y < len_k; y++) {
                uint32_t x_idx = vx - (len_k - 1) / 2 - 1 + x;
                uint32_t y_idx = vy - (len_k - 1) / 2 - 1 + y;
                voxels_4[c + y] = fov->computeLinearIndex(x_idx, y_idx, vz);
              }
              c += len_k;
            }
            
            float teta_SM = 0;
            float f = 0;
            for (uint32_t i = 0; i < len_kernel; i++) {
              if (i != (len_kernel - 1) / 2) {
                //f += weights[i] * (image[id0] + image[voxels_4[i]]);
                //f += weights[i] * (image[id0] + image[voxels_4[i]]);
                
                f += weights[i] * image[voxels_4[i]];
                //f += weights[i] * image[voxels_4[i]] + final_weight * image[id0];
                
                //f += weights[i] * 10 * abs((image[id0] - image[voxels_4[i]]));
              }
            }
            
            f = f + final_weight * image[id0];
            
            teta_SM = (1 / (2 * final_weight)) * f;
            
            //teta_SM = teta_SM/(1.5);
            
            //definisco nu
            
            //provo a smootare la sensitivity
            //fov->gaussFilter(sensitivity[s_sub], psf_sigma, psf_width);
            
            float nu = final_weight * cur_sensitivity[id0];
            //float nu = final_weight;
            
            
            temp_image[id0] = (2 * image[id0]) / ((1 - wang_beta * nu * teta_SM) + (sqrt((pow((1 - wang_beta * nu * teta_SM), 2)) + (4 * wang_beta * nu * image[id0]))));

          }
          else
          {
            uint32_t id0 = fov->computeLinearIndex(vx, vy, vz);
            temp_image[id0] = image[id0];
          }
        }
      }
      else {
        //l'immagine rimane uguale, ma devo comunque looppare sulle x
        for (uint32_t vxx = 0; vxx < len_x; vxx++) {
          uint32_t id0 = fov->computeLinearIndex(vxx, vy, vz);
          temp_image[id0] = image[id0];
        }
      }
    }
  }
  
  for (uint32_t i = 0; i < temp_image.size(); i++) 
    image[i] = temp_image[i];
  
  // fine regolarizzazione //
}
