/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TASKS_RECONSTRUCTION_VMLEM_H_
#define TASKS_RECONSTRUCTION_VMLEM_H_

#include "vIterativeReconstruction.h"


class vMLEM : public vIterativeReconstruction {
  
protected:
  ImageType sensitivity;
  
public:
  vMLEM(const std::string& config,
        bool use_tr_models,
        const cxxopts::ParseResult& cmd_line)
  : vIterativeReconstruction(config, cmd_line)
  { 
    // MODELS
    if (cmd_line.count("zmodel")) {
      std::string str = cmd_line["zmodel"].as<std::string>();
      loadNormalizationModel(str, input_hist_type);
    }
    
    if (cmd_line.count("gmodel")) {
      std::string str = cmd_line["gmodel"].as<std::string>();
      loadGeometricModels(str, use_tr_models);
    }
    
    if (cmd_line.count("dmodel")) {
      std::string str = cmd_line["dmodel"].as<std::string>();
      loadDetectorModels(str, true/*use_tr_models*/);
    }
    
    createAnglesAndInitViews();
  }
  
protected:
  
  void prepareSensitivity(const cxxopts::ParseResult& cmd_line)
  {
    sensitivity.resize( fov->nVoxels() );
    if ( cmd_line.count("compute-sensitivity") )
      computeSensitivity();
    else {
      std::string fname = cmd_line["sensitivity"].as<std::string>();
      
      std::ifstream sensitivityFile;
      sensitivityFile.open(fname);
      if ( !sensitivityFile.is_open() )
        throw std::string ( "Unable to open file " + fname + " for reading");
      
      uint32_t n_voxels = fov->nVoxels();
      char* pointer = reinterpret_cast<char *>(sensitivity.data());
      sensitivityFile.read(pointer, n_voxels*sizeof(float) );
      if (!sensitivityFile) {
        std::string error("Error reading sensitivity: expected ");
        error += std::to_string(n_voxels*sizeof(float));
        error += " bytes but read only ";
        error += std::to_string(sensitivityFile.gcount());
        throw error;
      }
      sensitivityFile.close();
    }
    //INVERTING Sensitivity
    #pragma omp parallel for
    for(uint32_t i=0; i<fov->nVoxels(); i++) {
      sensitivity[i] = 1/sensitivity[i];
      if (! std::isnormal(sensitivity[i]) )
        sensitivity[i] = 0;
    }
  }
  
  void saveToDistk(uint16_t iter) 
  {
    if (iter % save_every == 0 || iter == 1) {
      std::string fname = out_filename + "_" + std::to_string(iter+start_iter);
      if (bg_value) {
        ImageType tmp_image(image.size(), -bg_value);
        for (uint32_t vx=0; vx<image.size(); vx++) {
          tmp_image[vx] += image[vx];
          if ( tmp_image[vx] < 0 )
            tmp_image[vx] = 0;
        }
        fov->saveImage(fname, tmp_image, true);
        
      } else
        fov->saveImage(fname, image, true);
    }
  }

  
  virtual void computeSensitivity() = 0;
};



class mlemSymDG : public vMLEM {
  
public:
  mlemSymDG(const std::string& config, const cxxopts::ParseResult& cmd_line)
  : vMLEM(config, true, cmd_line)
  {
    createAnglesAndInitViews();
    prepareSensitivity(cmd_line);
  }
  
protected:
  void performRecon() override;
  void computeSensitivity() override;
  
};

class mlemSymDGnoT : public vMLEM {
  
public:
  mlemSymDGnoT(const std::string& config, const cxxopts::ParseResult& cmd_line)
  : vMLEM(config, false, cmd_line)
  {
    createAnglesAndInitViews();
    prepareSensitivity(cmd_line);
  }
  
protected:
  void performRecon() override;
  void computeSensitivity() override;
  
};

#endif // TASKS_RECONSTRUCTION_VMLEM_H_
