/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vMLEM.h"

void mlemSymDG::performRecon()
{ 
  timer msec_timer("msec"), msec_timer_partial("msec");
  
  scanner->reflectHistograms(in_hist, "LORs");
  
  if (start_image != "")
    image = fov->loadImage(start_image);
  else
    image = fov->createInitImage(1);
  
  ImageType backup_image = fov->createInitImage(0);
  const GroupedCS& grouped_cs = scanner->getGroupedCS();
  for (uint16_t iter=1; iter <= n_iterations; iter++) {
    msec_timer.start("Iteration: " + std::to_string(iter), "", false);
    
    if (psf_sigma && (psf_type == 0 || psf_type == 1)) {
      msec_timer_partial.start("convolution ", "\t", true);
      for (uint32_t vx=0; vx<image.size(); vx++)
        backup_image[vx] = image[vx];
      fov->gaussFilter(image, psf_sigma, psf_width);
      msec_timer_partial.stop();
    }
    
    createViews();
    
    if (psf_sigma && (psf_type == 0 || psf_type == 1)) {
      for (uint32_t vx=0; vx<image.size(); vx++)
        image[vx] = backup_image[vx];
    }
    
    
    // computation starts here
    for (uint16_t f_pair=0; f_pair<grouped_cs.size(); f_pair++) {
      detID_t f_id1 = grouped_cs[f_pair][0].id1();
      detID_t f_id2 = grouped_cs[f_pair][0].id2();
      
      uint32_t n_rc1 = scanner->getDetector(f_id1).rCryN();
      uint32_t n_rc2 = scanner->getDetector(f_id2).rCryN();
      lorIDX_t n_LORs = n_rc1 * n_rc2;
      
      uint32_t n_vc1 = scanner->getDetector(f_id1).vCryN();
      uint32_t n_vc2 = scanner->getDetector(f_id2).vCryN();
      lorIDX_t n_LOFs = n_vc1 * n_vc2;
      
      uint16_t n_views = grouped_cs[f_pair].size();
      detPair_t model_id = grouped_cs[f_pair][0].pairID();
      
      std::vector<uint16_t> active_views(n_views);
      for (uint16_t v=0; v<n_views; v++)
        active_views[v] = grouped_cs[f_pair][v].uniqueView();
      
//       msec_timer_partial.start("projection G", "\t", true);
      std::vector< FloatType* > projections;
      projections.resize(n_views);
      for (uint16_t v=0; v<n_views; v++) {
        projections[v] = new FloatType[n_LOFs];
        std::fill(projections[v], projections[v] + n_LOFs, 0);
      }
      #pragma omp parallel for
      for (lorIDX_t c_lof=0; c_lof<n_LOFs; c_lof++) {
        MatrixEntry *bp, *ep;
        bp = G_models.at(model_id).rowPointer(c_lof);
        ep = bp + G_models.at(model_id).rowElementsN(c_lof);
        while(bp < ep) {
          uint32_t vx = (*bp).col;
          FloatType tor_value = (*bp).v;
          for (uint16_t v=0; v<n_views; v++) {
            projections[v][c_lof] += estim_views[ active_views[v] ][vx] * tor_value;
          }
          bp++;
        }
      }
//       msec_timer_partial.stop();
      
      std::vector< FloatType* > ratios;
      ratios.resize(n_views);
      for (uint16_t v=0; v<n_views; v++) {
        ratios[v] = new FloatType[n_LORs];
        std::fill(ratios[v], ratios[v] + n_LORs, 0);
      }
      
//       msec_timer_partial.start("projection D", "\t", true);
      #pragma omp parallel for
      for (lorIDX_t c_lor=0; c_lor<n_LORs; c_lor++) {
        FloatType *temp_proj = new FloatType[n_views];
        std::fill(temp_proj, temp_proj + n_views, 0);
        MatrixEntry *bp, *ep;
        bp = D_models.at(model_id).rowPointer(c_lor);
        ep = bp + D_models.at(model_id).rowElementsN(c_lor);
        while(bp < ep) {
          uint32_t lof = (*bp).col;
          FloatType tor_value = (*bp).v;
          for (uint16_t v=0; v<n_views; v++) 
            temp_proj[v] += projections[v][lof] * tor_value;
          bp++;
        }
        for (uint16_t v=0; v<n_views; v++) {
          if (temp_proj[v]) {
            detPair_t p_id = grouped_cs[f_pair][v].pairID();
            FloatType counts = in_hist.data[p_id][c_lor]; 
            ratios[v][c_lor] = counts/temp_proj[v];
          }
        }
        delete[] temp_proj;
      }
//       msec_timer_partial.stop();
    
      //erasing projections
      for (uint16_t v=0; v<n_views; v++)
        std::fill(projections[v], projections[v] + n_LOFs, 0);
      
//       msec_timer_partial.start("backproj D", "\t", true);
      #pragma omp parallel for
      for (lorIDX_t c_lor=0; c_lor<n_LOFs; c_lor++) {
        MatrixEntry *bp, *ep;
        bp = D_models_tr.at(model_id).rowPointer(c_lor);
        ep = bp + D_models_tr.at(model_id).rowElementsN(c_lor);
        while(bp < ep) {
          uint32_t col = (*bp).col;
          FloatType tor_value = (*bp).v;
          for (uint16_t v=0; v<n_views; v++) {
            if (std::isnormal( ratios[v][col] ) )
              projections[v][c_lor] += ratios[v][col] * tor_value;
          }
          bp++;
        }
      }
//       msec_timer_partial.stop();
      
      
//       msec_timer_partial.start("backproj G", "\t", true);
      #pragma omp parallel for
      for (uint32_t c_vx=0; c_vx<fov->nVoxels(); c_vx++) {
        MatrixEntry *bp = G_models_tr.at(model_id).rowPointer(c_vx);
        MatrixEntry *ep = bp + G_models_tr.at(model_id).rowElementsN(c_vx);
        
        while( bp<ep ) {
          uint32_t lof = (*bp).col;
          FloatType tor_value = (*bp).v;
          for (uint16_t v=0; v<n_views; v++) 
            output_views[ active_views[v] ][c_vx] += projections[v][lof] * tor_value;
          bp++;
        }

      }
//       msec_timer_partial.stop();
      
      for (uint16_t v=0; v<n_views; v++) {
        delete ratios[v];
        delete projections[v];
      }
      
    }
    // computation stops here
    
    composeViews();
    clearOutputViews();
    
    if (psf_sigma && (psf_type == 0 || psf_type == 2)) {
      msec_timer_partial.start("convolution composed_img", "\t", true);
      fov->gaussFilter(composed_img, psf_sigma, psf_width);
      msec_timer_partial.stop();
    }
    
    // updating 
    msec_timer_partial.start("updating", "\t", true);
    #pragma omp parallel for 
    for (uint32_t c_vx=0; c_vx<image.size(); c_vx++) 
      image[c_vx] *= composed_img[c_vx]*sensitivity[c_vx];
    msec_timer_partial.stop();
        
    // REGULARIZATION
    if (smooth_sigma) {
      msec_timer_partial.start("filtering", "\t", true);
      fov->gaussFilter(image, smooth_sigma, smooth_width);
      msec_timer_partial.stop();
    }
    
    if (wang_beta) {
      msec_timer_partial.start("applying wang patch-based regularization", "\t", true);
      WangPatchRegularization(sensitivity);
      msec_timer_partial.stop();
    }
    
    if (scanner->type() == "CYLINDRICAL")
      fov->fixCorners(image);
    
    msec_timer.stop();
    
    if (iter % save_every == 0 || iter == 1) {
      std::string fname = out_filename + "_" + std::to_string(iter+start_iter);
      if (bg_value) {
        ImageType tmp_image(image.size(), -bg_value);
        for (uint32_t vx=0; vx<image.size(); vx++) {
          tmp_image[vx] += image[vx];
          if ( tmp_image[vx] < 0 )
            tmp_image[vx] = 0;
        }
        fov->saveImage(fname, tmp_image, true);
        
      } else
        fov->saveImage(fname, image, true);
    }
  }
  
}

void mlemSymDG::computeSensitivity()
{
  std::string name_tosave = scanner->name() + "_Sensitivity_Sym";

  
  timer msec_timer("msec");
  timer msec_timer_partial("msec");
  
  if (!use_z_model) {
    name_tosave += "_";
    std::cout << "Allocating normalization histogram" << std::endl;
    Z_model.allocate(scanner->getDetectorsList(),
                     scanner->getCoincidenceSchema(),
                     input_hist_type, 1);
  }
  else {
    scanner->reflectHistograms(Z_model, input_hist_type);
    name_tosave += "_Z";
  }
  name_tosave += "DG";
  
  msec_timer.start("Computing sensitivity", "", false);
  
  data_type& nd = Z_model.data;
  const GroupedCS& grouped_cs = scanner->getGroupedCS();
  
  for (uint16_t f_pair=0; f_pair<grouped_cs.size(); f_pair++) {
//     std::cout << "f_pair " << f_pair << std::endl;
    detID_t f_id1 = grouped_cs[f_pair][0].id1();
    detID_t f_id2 = grouped_cs[f_pair][0].id2();
      
    uint32_t n_vc1 = scanner->getDetector(f_id1).vCryN();
    uint32_t n_vc2 = scanner->getDetector(f_id2).vCryN();
    lorIDX_t n_LOFs = n_vc1 * n_vc2;
    
    uint16_t n_views = grouped_cs[f_pair].size();
    detPair_t model_id = grouped_cs[f_pair][0].pairID();
    
    std::vector< FloatType* > projections;
    projections.resize(n_views);
    for (uint16_t v=0; v<n_views; v++) {
      projections[v] = new FloatType[n_LOFs];
      std::fill(projections[v], projections[v] + n_LOFs, 0);
    }
//     msec_timer_partial.start("backproj D", "\t", true);
    #pragma omp parallel for
    for (lorIDX_t c_lor=0; c_lor<n_LOFs; c_lor++) {
      MatrixEntry *bp, *ep;
//       std::cout << c_lor << " " << model_id << std::endl;
      bp = D_models_tr.at(model_id).rowPointer(c_lor);
      ep = bp + D_models_tr.at(model_id).rowElementsN(c_lor);
      while(bp < ep) {
        uint32_t col = (*bp).col;
        FloatType tor_value = (*bp).v;
        
        for (uint16_t v=0; v<n_views; v++) {
          const detPair_t& pair_id = grouped_cs[f_pair][v].pairID();
          projections[ v ][c_lor] += tor_value*nd[pair_id][col];
        }
        bp++;
      }
    }
//     msec_timer_partial.stop();
    
//     msec_timer_partial.start("backproj G", "\t", true);
    #pragma omp parallel for
    for (lorIDX_t c_vx=0; c_vx<fov->nVoxels(); c_vx++) {
      MatrixEntry *bp = G_models_tr.at(model_id).rowPointer(c_vx);
      MatrixEntry *ep = bp + G_models_tr.at(model_id).rowElementsN(c_vx);
      
      while( bp<ep ) {
        uint32_t lof = bp->col;
        FloatType tor_value = bp->v;
        for (uint16_t v=0; v<n_views; v++) {
          const uint16_t& view_id = grouped_cs[f_pair][v].uniqueView();
          output_views[ view_id ][c_vx] += tor_value*projections[ v ][lof];
        }
        bp++;
      }
    }
//     msec_timer_partial.stop();
    
    for (uint16_t v=0; v<n_views; v++) {
      delete projections[v];
    }
  }
  
  composeViews();
  
  std::string psf_str = "";
  if (psf_sigma && (psf_type == 0 || psf_type == 2) ) {
    fov->gaussFilter(composed_img, psf_sigma, psf_width);
    psf_str += "_psf-" + std::to_string(psf_sigma);
    psf_str += "-" + std::to_string(psf_width);
  }
  
  #pragma omp parallel for
  for(uint32_t i=0; i<fov->nVoxels(); i++) {
    sensitivity[i] = composed_img[i];
  }
  
  fov->applyRotMask(sensitivity);
  
  msec_timer.stop();
  fov->saveImage(output_dir + "/" + name_tosave + psf_str, sensitivity, false);
  
  Z_model.clear();
}

