#include "taskBase.h"

#include "timer.hpp"

TaskBase::TaskBase(const std::string& config, 
                   const cxxopts::ParseResult& cmd_line)
{
  xml_config = config;
  
  output_dir = cmd_line["output-dir"].as<std::string>();
  createDirectory(output_dir);
  
  // init scanner
  bool activate_vg = cmd_line.count("activate-vg");
  scanner = vGeometry::initScanner(xml_config, activate_vg);
  scanner->printGeometryInfo(output_dir);
  scanner->printSymInfo(output_dir);
  
  // init fov
  if (cmd_line.count("fov")) {
    auto& fov_name = cmd_line["fov"].as<std::string>();
    fov = new FieldOfView(xml_config, fov_name);
    fov->printInfo(output_dir + "/" + scanner->name() + "_" + fov_name + ".txt");
  } else
    fov = nullptr;
  
  // init projector
  if (cmd_line.count("projector"))
    initProjector(cmd_line["projector"].as<std::string>());
  else
    projector = nullptr;
  
  if (cmd_line.count("threads")) 
    n_threads = cmd_line["threads"].as<uint16_t>();
  else
    n_threads = omp_get_max_threads();
  
  omp_set_dynamic(0);
  omp_set_num_threads(n_threads);
  std::cout << "Using n threads: " << n_threads << std::endl;

  use_z_model = false;
  use_g_model = false;
  use_d_model = false;

}

void TaskBase::initProjector(const std::string& proj_tag_p)
{
  std::string fname("TaskBase::initProjector()");
  if (!fov)
    throw fname + " Fov not initialized";
  
  projector = vProjector::initProjector(xml_config, fov, proj_tag_p);
}


void TaskBase::loadNormalizationModel(std::string& z_model_str,
                                      std::string& hist_type)
{
  Z_model.allocate(scanner->getDetectorsList(),
                   scanner->getCoincidenceSchema(),
                   hist_type, 0);
  Z_model.load(z_model_str);

  use_z_model = true;
}


void TaskBase::loadGeometricModels(std::string& g_dir, bool use_tr_models)
{
  timer msec_timer("msec");
  
  using PairType = std::pair<detPair_t, CoincidencePairSym>;
  std::cout << "loading G model from directory " << g_dir << std::endl;
  for (const PairType& entry : scanner->getCoincidenceSchema() ) {
    const CoincidencePairSym& pair = entry.second;
    if ( pair.fundamental() ) {
      detID_t id1 = pair.id1();
      detID_t id2 = pair.id2();
      std::string id_pair = std::to_string(id1) + "-" + std::to_string(id2);
      std::string filename =  g_dir + "/" + id_pair;
      
      uint32_t n_ac1 = scanner->getDetector(id1).aCryN();
      uint32_t n_ac2 = scanner->getDetector(id2).aCryN();
      uint32_t n_LORs =  n_ac1 * n_ac2;
      
      msec_timer.start("loading " + id_pair, "", true); 
      SparseModel model(n_LORs, fov->nVoxels() );
      detPair_t sz_id = szudzikPair(id1, id2);
      G_models.insert( {sz_id, model} );
      G_models.at(sz_id).set(filename);
      msec_timer.stop();
      
      if (use_tr_models) {
        msec_timer.start("loading tr " + id_pair, "", true); 
        SparseModel model_tr(fov->nVoxels(), n_LORs);
        G_models_tr.insert( {sz_id, model_tr} );
        G_models_tr.at(sz_id).set(filename + "_tr");
        msec_timer.stop();
      }
    }
  }
  use_g_model = true;
}


void TaskBase::loadDetectorModels(std::string& d_dir, bool use_tr_models)
{
  timer msec_timer("msec");
  
  using PairType = std::pair<detPair_t, CoincidencePairSym>;
  std::cout << "loading D model from directory " << d_dir << std::endl;
  for (const PairType& entry : scanner->getCoincidenceSchema() ) {
    const CoincidencePairSym& pair = entry.second;
    if ( pair.fundamental() ) {
      detID_t id1 = pair.id1();
      detID_t id2 = pair.id2();
      std::string id_pair = std::to_string(id1) + "-" + std::to_string(id2);
      std::string filename =  d_dir + "/" + id_pair;
      
      
      uint32_t n_rc1 = scanner->getDetector(id1).rCryN();
      uint32_t n_rc2 = scanner->getDetector(id2).rCryN();
      
      uint32_t n_vc1 = scanner->getDetector(id1).vCryN();
      uint32_t n_vc2 = scanner->getDetector(id2).vCryN();
      
      lorIDX_t n_LOFs = n_vc1*n_vc2;
      lorIDX_t n_LORs = n_rc1*n_rc2;
      
      msec_timer.start("loading " + id_pair, "", true);
      SparseModel model(n_LORs, n_LOFs );
      detPair_t sz_id = szudzikPair(id1, id2);
      D_models.insert( {sz_id, model} );
      D_models.at(sz_id).set(filename /*+ "_tr"*/);
      msec_timer.stop();
      
      //dump statistics
//       std::ofstream out_file;
//       std::string def_name(id_pair + ".txt");
//       
//       out_file.open(def_name);
//       for (uint32_t lor=0; lor<n_lors; lor++) {
//         MatrixEntry* bp = D_models.at(sz_id).rowPointer(lor);
//         MatrixEntry* ep = bp + D_models.at(sz_id).rowElementsN(lor);
//         float sum=0;
//         while(bp < ep) {
//           uint32_t vx = (*bp).col;
//           FloatType tor_value = (*bp).v;
//           sum += tor_value;
//           out_file << vx << " " << tor_value << " - ";
//           bp++;
//         }
//         out_file << "sum:" << sum << std::endl;
//       }
//       out_file.close();
//       
      if (use_tr_models) {
        msec_timer.start("loading tr " + id_pair, "", true);
        SparseModel model_tr(n_LOFs, n_LORs);
        D_models_tr.insert( {sz_id, model_tr} );
        D_models_tr.at(sz_id).set(filename + "_tr");
        msec_timer.stop();
      }
    }
  }
  use_d_model = true;
}

FloatType TaskBase::projectModelRow(const FloatType* vec,
                                    const SparseModel& model,
                                    lorIDX_t lor_id)
{
  FloatType projection = 0;
  
  uint32_t n_el = model.rowElementsN(lor_id);
  MatrixEntry *row = model.rowPointer(lor_id);
  for (uint32_t i=0; i<n_el; i++)
    projection += vec[ row[i].col ] * row[i].v;
  
  //   MatrixEntry *start_pointer, *stop_pointer;
  //   start_pointer = model.rowPointer(lor_id);
  //   stop_pointer = start_pointer + model.rowElementsN(lor_id);
  //   while(start_pointer < stop_pointer) {
  //     uint32_t vx = (*start_pointer).col;
  //     FloatType tor_value = (*start_pointer).v;
  //     FloatType image_value = vec[vx];
  //     projection += image_value * tor_value;
  //     start_pointer++;
  //   }
  
  return projection;
}

void TaskBase::createAnglesAndInitViews()
{
  unique_angles = scanner->computeUniqueAngles();
  
  //creating the angle views;
  for (const AngleType& angle : unique_angles) {
    angle_views[angle] = fov->createInitImage(0);
  }
  
  // creating the images associated to the unique_views
  uint16_t n_uv = scanner->getUniqueViews().size();
  uint32_t n_vx = fov->nVoxels();
  estim_views.resize(n_uv);
  for (uint16_t v=0; v<n_uv; v++) {
    estim_views[v] = new FloatType[ n_vx ];
    std::fill(estim_views[v], estim_views[v]+n_vx, 0);
  }
  
  output_views.resize(n_uv);
  for (uint16_t v=0; v<n_uv; v++) {
    output_views[v] = new FloatType[ n_vx ];
    std::fill(output_views[v], output_views[v]+n_vx, 0);
  }
  
  composed_img.resize(n_vx, 0);
}


void TaskBase::clearOutputViews()
{
  const ViewsList& uv = scanner->getUniqueViews();
  uint32_t n_voxels = fov->nVoxels();

  for (uint32_t cur_view=0; cur_view<uv.size(); cur_view++) 
    std::fill(std::execution::par_unseq, output_views[cur_view], output_views[cur_view] + n_voxels, 0);
}


void TaskBase::createViews()
{
  timer msec_timer("msec");

  msec_timer.start("views creations", "\t", true);
  
  for (const AngleType& zA : unique_angles) {
    fov->rotateImage2D(image.data(), angle_views[zA].data(), zA, false);
//     fov->rotateImage(image.data(), angle_views[zA].data(), zA, false);
    fov->applyRotMask(angle_views[zA].data());
  }
  
  const ViewsList& uv = scanner->getUniqueViews();
  uint32_t n_voxels = fov->nVoxels();

  clearOutputViews();
  

  #pragma omp parallel for
  for (uint32_t cur_view=0; cur_view<uv.size(); cur_view++) {
    AngleType zA = uv[cur_view].zAngle();
    ThreeVector shift(0, uv[cur_view].yShift(), uv[cur_view].zShift() );
    FloatType shift_v = shift.r2();
    bool xR = uv[cur_view].xReflect();
    bool yR = uv[cur_view].yReflect();
    bool zR = uv[cur_view].zReflect();
    
    
    FloatType* cur_angle_image = angle_views[zA].data();
    if (shift_v) {
      if (xR || yR || zR) {
        ImageType temp_image(n_voxels, 0);
        fov->shiftImage(cur_angle_image, temp_image.data(), -shift, false);
        fov->flipImage(temp_image.data(), estim_views[cur_view], xR, yR, zR, false);
      }
      else
        fov->shiftImage(cur_angle_image, estim_views[cur_view], -shift, false);
    } else {
      if (xR || yR || zR)
        fov->flipImage(cur_angle_image, estim_views[cur_view], xR, yR, zR, false);
      else
        std::copy(cur_angle_image, cur_angle_image+fov->nVoxels(), estim_views[cur_view]);
    }
    
    fov->applyRotMask(estim_views[cur_view]);
  }
  
  msec_timer.stop();
  
  // checking views
//   int i=0;
//   for (auto view : estim_views) {
//     fov->saveImage(std::to_string(i++), view);
//     uint32_t n_less_zero = 0;
//     uint32_t n_infinite = 0;
//     uint32_t n_nan = 0;
//     uint32_t n_zero = 0;
//     uint32_t n_subnormal = 0;
//     uint32_t n_normal = 0;
//     
//     for (uint32_t vx=0; vx < fov->nVoxels(); vx++) {
//       int res = std::fpclassify(view[vx]);
//       switch (res) {
//         case FP_INFINITE: n_infinite++; break;
//         case FP_NAN: n_nan++; break;
//         case FP_ZERO: n_zero++; break;
//         case FP_SUBNORMAL: n_subnormal++; break;
//         case FP_NORMAL: n_normal++;
//       }
//       if (view[vx] < 0 )
//         n_less_zero ++;
//     }
//     
//     std::cout << "less_zero: " << n_less_zero << std::endl;
//     std::cout << "FP_INFINITE: " << n_infinite << std::endl;
//     std::cout << "FP_NAN: " << n_nan << std::endl;
//     std::cout << "FP_ZERO: " << n_zero << std::endl;
//     std::cout << "FP_SUBNORMAL: " << n_subnormal << std::endl;
//     std::cout << "FP_NORMAL: " << n_normal << std::endl << std::endl;
//   }
  
  
}

void TaskBase::composeViews()
{
  timer msec_timer("msec");
  msec_timer.start("composing views", "\t", true);
  
  #pragma omp parallel for
  for (uint32_t c_ang=0; c_ang<unique_angles.size(); c_ang++) {
    const AngleType& zA = unique_angles[c_ang];
    std::fill(angle_views[zA].begin(), angle_views[zA].end(), 0);
  }

  const ViewsList& uv = scanner->getUniqueViews();
  for (uint16_t cur_view=0; cur_view<uv.size(); cur_view++) {
    FloatType* cur_view_image = output_views[cur_view];
    for (uint32_t vx=0; vx<fov->nVoxels(); vx++)
      if (std::isnan(cur_view_image[vx]) || std::isinf(cur_view_image[vx])) 
        cur_view_image[vx] = 0;

    AngleType zA = uv[cur_view].zAngle();
    ThreeVector shift(0, uv[cur_view].yShift(), uv[cur_view].zShift() );
    FloatType shift_v = shift.r2();
    bool xR = uv[cur_view].xReflect();
    bool yR = uv[cur_view].yReflect();
    bool zR = uv[cur_view].zReflect();

//     fov->saveImage("view" + std::to_string(cur_view)
//     + "_" + std::to_string(zA)
//     + "_" + std::to_string(xR)
//     + "_" + std::to_string(yR)
//     + "_" + std::to_string(zR), output_views[cur_view]);
    
    if (xR || yR || zR) {
      if (shift_v) {
        ImageType temp_image(fov->nVoxels(), 0);
        fov->flipImage(cur_view_image, temp_image.data(), xR, yR, zR, false);
        fov->shiftImage(temp_image.data(), angle_views[zA].data(), shift, true);
      }
      else
        fov->flipImage(cur_view_image, angle_views[zA].data(), xR, yR, zR, true);
    } else {
      if (shift_v) {
        fov->shiftImage(cur_view_image, angle_views[zA].data(), shift, true);
      }
      else
        std::copy(cur_view_image, cur_view_image+fov->nVoxels(), angle_views[zA].data());
    }
  }
  
  std::fill(std::execution::par_unseq, composed_img.begin(), composed_img.end(), 0);
  
  for (const AngleType& zA : unique_angles) {
    fov->rotateImage2D(angle_views[zA].data(), composed_img.data(), 360-zA, true);
//     fov->rotateImage(angle_views[zA].data(), composed_img.data(), 360-zA, true);
  }
  
  msec_timer.stop();
}

