#include "detectorSensitivity.h"


void DetectorSensitivity::apply()
{
  timer msec_timer("msec");
  const GroupedCS& grouped_cs = scanner->getGroupedCS();
  
  msec_timer.start("Computing Sensitivities", "", true);
  
  
  for (uint16_t f_pair=0; f_pair<grouped_cs.size(); f_pair++) {
    
    uint16_t n_views = grouped_cs[f_pair].size();
    detPair_t model_id = grouped_cs[f_pair][0].pairID();
    detID_t id1, id2;
    std::tie(id1, id2) = szudzikUnPair(model_id);
    
    const PlanarDetectorType& det1 = scanner->getDetector(id1);
    const PlanarDetectorType& det2 = scanner->getDetector(id2);
    std::string str_id = std::to_string(det1.ID()) + "-" + std::to_string(det2.ID());
    
    lorIDX_t n_LORs = det1.rCryN() * det2.rCryN();
    lorIDX_t n_LOFs = det1.vCryN() * det2.vCryN();
    
    std::vector<FloatType> lof_sens(n_LOFs);
    std::ifstream file_lof;
    file_lof.open(str_id+".lof_sensitivity");
    if ( !file_lof.is_open() )
      throw std::string( " unable to open file: " + str_id+".lof_sensitivity");
    file_lof.read(reinterpret_cast<char *>(lof_sens.data()), n_LOFs*sizeof(FloatType));
    if (!file_lof) // || file.gcount() != _rows*sizeof(uint16_t))
      throw std::string("error reading");
    file_lof.close();
    
    #pragma omp parallel for
    for (uint32_t c_lof=0; c_lof<n_LOFs; c_lof++) {
      for (uint16_t v=0; v<n_views; v++) {
        uint16_t view = grouped_cs[f_pair][v].pairID();
        lof_sens_hist.data[view][c_lof] = lof_sens[c_lof];
      }
    }
    
    #pragma omp parallel for
    for (uint32_t c_lor=0; c_lor<n_LORs; c_lor++) {
      MatrixEntry *bp, *ep;
      bp = D_models.at(model_id).rowPointer(c_lor);
      ep = bp + D_models.at(model_id).rowElementsN(c_lor);
      FloatType projection = 0;
      while(bp < ep) {
        uint32_t lof = (*bp).col;
        FloatType value = (*bp).v;
        projection += value*lof_sens[lof];
        bp++;
      }
      
      for (uint16_t v=0; v<n_views; v++) {
        uint16_t view = grouped_cs[f_pair][v].pairID();
        lof_sens_hist.data[view][c_lor] = projection;
      }
      
    }
    
  }
  msec_timer.stop();
  
  std::string bname = scanner->name() + "_" + out_filename;
  lof_sens_hist.save(bname + "_lof_sens.hg");
  lor_sens_hist.save(bname + "_lor_sens.hg");
  
}


