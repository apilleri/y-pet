/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TASKS_DETECTORSENSITIVITY_DETECTORSENSITIVITY_H_
#define TASKS_DETECTORSENSITIVITY_DETECTORSENSITIVITY_H_


#include "taskBase.h"

class DetectorSensitivity : public TaskBase {
  
protected:
  std::string out_filename;

  HistogramsMap lof_sens_hist;
  HistogramsMap lor_sens_hist;
  
public:
  DetectorSensitivity(vGeometry* s, const cxxopts::ParseResult& cmd_line)
  : TaskBase(s, nullptr, nullptr, false, false, "", cmd_line),
    out_filename()
  {
    
    out_filename = cmd_line["output"].as<std::string>();
    
    lof_sens_hist.allocate(scanner->getDetectorsList(),
                     scanner->getCoincidenceSchema(),
                     "LOFs", 0);
    lor_sens_hist.allocate(scanner->getDetectorsList(),
                           scanner->getCoincidenceSchema(),
                           "LORs", 0);
    
  }
  
  ~DetectorSensitivity() { }
  
  void apply() override;
  
  
};





#endif // TASKS_DETECTORSENSITIVITY_DETECTORSENSITIVITY_H_
