/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TASKS_PROJECTIMAGE_VPROJECTIMAGE_H_
#define TASKS_PROJECTIMAGE_VPROJECTIMAGE_H_

#include "taskBase.h"

#include "timer.hpp"

class vProjectImage : public TaskBase {
  
protected:
  std::string input_img;
  std::string out_filename;
  
  
  
  FloatType psf_sigma;
  uint16_t psf_width;
  uint16_t psf_type;
  
  std::string hist_type;
  HistogramsMap out_hist;
  
  bool internal_init;
  
public:
  vProjectImage(const std::string& config,
                const cxxopts::ParseResult& cmd_line)
  : TaskBase(config, cmd_line), 
    psf_sigma(0), psf_width(0), psf_type(0), internal_init(false)
  { 
    input_img = cmd_line["input"].as<std::string>();
    out_filename = cmd_line["output"].as<std::string>();
    
    if ( cmd_line.count("activate-vg") ) 
      hist_type = "LOFs";
    else
      hist_type = "LORs";
    
    // MODELS
    if (cmd_line.count("zmodel")) {
      std::string str = cmd_line["zmodel"].as<std::string>();
      loadNormalizationModel(str, hist_type);
    }
    
    if (cmd_line.count("gmodel")) {
      std::string str = cmd_line["gmodel"].as<std::string>();
      loadGeometricModels(str, false);
    }
    
    if (cmd_line.count("dmodel")) {
      std::string str = cmd_line["dmodel"].as<std::string>();
      loadDetectorModels(str, false);
    }
    
    
    out_hist.allocate(scanner->getDetectorsList(),
                      scanner->getCoincidenceSchema(),
                      hist_type, 0);
    
    if (cmd_line.count("psf") ) {
      std::vector<FloatType > v = cmd_line["psf"].as< std::vector<FloatType> >();
      if (v.size() != 3)
        throw std::string("psf option: you must provide three values");
      
      psf_sigma = v[0];
      psf_width = static_cast<uint16_t>(v[1]);
      psf_type = v[2];
    }
    
  }
  
  
  ~vProjectImage()
  { }

  
  void setImage(const ImageType& img)
  {
    image = img;
    internal_init = true;
  }
  
  
  const HistogramsMap& getOutputHistogram() const
  {
    return out_hist;
  }
  
  
  void projectGeometricModels(HistogramsMap& out_hist)
  {
    timer msec_timer_partial("msec");
    const GroupedCS& grouped_cs = scanner->getGroupedCS();
    
    for (uint16_t f_pair=0; f_pair<grouped_cs.size(); f_pair++) {
      
      detPair_t f_pair_id = grouped_cs[f_pair][0].pairID();
      detID_t f_id1 = grouped_cs[f_pair][0].id1();
      detID_t f_id2 = grouped_cs[f_pair][0].id2();
      
      uint16_t n_ac1 = scanner->getDetector(f_id1).aCryN();
      uint16_t n_ac2 = scanner->getDetector(f_id2).aCryN();
      lorIDX_t n_LOFs = n_ac1 * n_ac2;

      const SparseModel& model = G_models.at(f_pair_id);
      
      msec_timer_partial.start("f_pair: " + std::to_string(f_pair), "\t", true);
      #pragma omp parallel for
      for (lorIDX_t c_lof=0; c_lof<n_LOFs; c_lof++) {

        for (uint16_t ip=0; ip<grouped_cs[f_pair].size(); ip++) {
          const detPair_t& pair_id = grouped_cs[f_pair][ip].pairID();
          FloatType* view = estim_views[ grouped_cs[f_pair][ip].uniqueView() ];
          out_hist.data[pair_id][c_lof] = projectModelRow(view, model, c_lof);
        }
      }
      msec_timer_partial.stop();
    }
  }
  
  void projectDetectorModels(const HistogramsMap& in_hist,
                             HistogramsMap& out_hist)
  {
    timer msec_timer_partial("msec");
    const GroupedCS& grouped_cs = scanner->getGroupedCS();
    
    for (uint16_t f_pair=0; f_pair<grouped_cs.size(); f_pair++) {
      
      detPair_t f_pair_id = grouped_cs[f_pair][0].pairID();
      lorIDX_t n_LORs = out_hist.data.at(f_pair_id).size();
      
      const SparseModel& model = D_models.at(f_pair_id);
      
      msec_timer_partial.start("f_pair: " + std::to_string(f_pair), "\t", true);
      #pragma omp parallel for
      for (lorIDX_t c_lor=0; c_lor<n_LORs; c_lor++) {
        for (uint16_t ip=0; ip<grouped_cs[f_pair].size(); ip++) {
          const detPair_t& pair_id = grouped_cs[f_pair][ip].pairID();
          const FloatType* view = in_hist.data.at(pair_id).data();
          out_hist.data[pair_id][c_lor] = projectModelRow(view, model, c_lor);
        }
      }
      msec_timer_partial.stop();
    }
  }
  
};


class ProjectImageFullOnLine : public vProjectImage {
  
public:
  ProjectImageFullOnLine(const std::string& config,
                         const cxxopts::ParseResult& cmd_line)
    : vProjectImage(config, cmd_line)
  { }
  
  void apply() override;
};


class ProjectImageSymOffLine : public vProjectImage {
  
public:
  ProjectImageSymOffLine(const std::string& config,
                         const cxxopts::ParseResult& cmd_line)
    : vProjectImage(config, cmd_line)
  { }
  
  void apply() override;
};


class ProjectImageSymOnLine : public vProjectImage {
  
public:
  ProjectImageSymOnLine(const std::string& config,
                        const cxxopts::ParseResult& cmd_line)
    : vProjectImage(config, cmd_line)
  { }
  
  void apply() override;
};

#endif // TASKS_PROJECTIMAGE_VPROJECTIMAGE_H_
