/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vProjectImage.h"

void ProjectImageSymOnLine::apply()
{
  timer msec_timer("msec"), msec_timer_partial("msec");
  
  if (!internal_init)
    image = fov->loadImage(input_img);
  
  if (psf_sigma)
    fov->gaussFilter(image, psf_sigma, psf_width);
  
  HistogramsMap temp_hist;
  temp_hist.allocate(scanner->getDetectorsList(),
                     scanner->getCoincidenceSchema(),
                     hist_type, 0);
  
  createAnglesAndInitViews();
  createViews();
  const GroupedCS& grouped_cs = scanner->getGroupedCS();
  
  msec_timer.start("projection G", "", false);
  for (uint16_t f_pair=0; f_pair<grouped_cs.size(); f_pair++) {
    
    detPair_t f_pair_id = grouped_cs[f_pair][0].pairID();
    lorIDX_t n_lors = out_hist.data.at(f_pair_id).size();
    
    detID_t id1 = grouped_cs[f_pair][0].id1();
    detID_t id2 = grouped_cs[f_pair][0].id2();
    uint16_t n_ac2 = scanner->getDetector(id2).aCryN();
    
    msec_timer_partial.start("f_pair: " + std::to_string(f_pair), "\t", true);
    #pragma omp parallel for
    for (lorIDX_t c_lor=0; c_lor<n_lors; c_lor++) {
      uint16_t c1 = c_lor / n_ac2;
      uint16_t c2 = c_lor % n_ac2;
      // computing TOR
      std::vector<MatrixEntry> row;
      const PlanarDetectorType& det1 = scanner->getDetector(id1);
      const PlanarDetectorType& det2 = scanner->getDetector(id2);
      projector->project(det1, c1, det2, c2, row); 
      for (uint16_t ip=0; ip<grouped_cs[f_pair].size(); ip++) {
        const detPair_t& pair_id = grouped_cs[f_pair][ip].pairID();
        const uint16_t& view_id = grouped_cs[f_pair][ip].uniqueView();
        for (const MatrixEntry& r : row) {
          FloatType image_value = estim_views[view_id][r.col];
          temp_hist.data[pair_id][c_lor] += image_value * r.v;
        }
      }
    }
    msec_timer_partial.stop();
  }
  msec_timer.stop();
  
  if (use_d_model) {
    scanner->reflectHistograms(temp_hist, hist_type);
    msec_timer.start("projection D", "", false);
    projectDetectorModels(temp_hist, out_hist);
    msec_timer.stop();
  } else {
    for (auto hist_pair : temp_hist.data)
      out_hist.data[hist_pair.first] = hist_pair.second;
  }
  
  scanner->reflectHistograms(out_hist, hist_type);
  
  if (use_z_model)
    out_hist *= Z_model;
  
 
  if (!internal_init)
    out_hist.save(output_dir + "/" + out_filename);
}
