/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vProjectImage.h"

void ProjectImageSymOffLine::apply()
{
  timer msec_timer("msec"), msec_timer_partial("msec");
  
  if (!internal_init)
    image = fov->loadImage(input_img);
  
  if (psf_sigma)
    fov->gaussFilter(image, psf_sigma, psf_width);
  
  HistogramsMap temp_hist;
  temp_hist.allocate(scanner->getDetectorsList(),
                     scanner->getCoincidenceSchema(),
                     hist_type, 0);
  
  createAnglesAndInitViews();
  createViews();
  
  msec_timer.start("projection G", "", false);
  projectGeometricModels(temp_hist);
  msec_timer.stop();
  
  if (use_d_model) {
    msec_timer.start("projection D", "", false);
    projectDetectorModels(temp_hist, out_hist);
    msec_timer.stop();
  } else {
    for (auto hist_pair : temp_hist.data)
      out_hist.data[hist_pair.first] = hist_pair.second;
  }
  
  scanner->reflectHistograms(out_hist, hist_type);

  if (use_z_model)
    out_hist *= Z_model;

  if (!internal_init)
    out_hist.save(output_dir + "/" + out_filename);
}
