/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vProjectImage.h"

void ProjectImageFullOnLine::apply() 
{
  if (!internal_init)
    image = fov->loadImage(input_img);
  
  if (psf_sigma && (psf_type == 0 || psf_type == 1))
    fov->gaussFilter(image, psf_sigma, psf_width);
  
  // creating a fake histogram full of ones to extract the complete LOR list
  HistogramsMap fake_hist;
  fake_hist.allocate(scanner->getDetectorsList(),
                     scanner->getCoincidenceSchema(),
                     hist_type, 1);
  SubsetsList subsets = scanner->generateLorList(fake_hist, 1, 0);
  fake_hist.clear();
  
  timer msec_timer("msec");
  msec_timer.start("projecting image", "", true);
  
  #pragma omp parallel for
  for (lorIDX_t c_lof=0; c_lof<subsets[0].size(); c_lof++) {
    const CountsType& entry = subsets[0][c_lof];
    // computing TOR
    std::vector<MatrixEntry> row;
    detID_t id1 = entry.det1();
    detID_t id2 = entry.det2();
    const PlanarDetectorType& det1 = scanner->getDetector(id1);
    const PlanarDetectorType& det2 = scanner->getDetector(id2);
    
    projector->project(det1, entry.cry1(), det2, entry.cry2(), row);
    
    //computing projection
    FloatType projection = 0;
    for (const MatrixEntry& entry : row) 
      projection += image[entry.col] * entry.v;
    
    uint16_t n_ac2 = scanner->getDetector(entry.det2()).aCryN();
    lorIDX_t rel_lor_idx = entry.cry1()*n_ac2 + entry.cry2();
    out_hist.data[ szudzikPair(id1, id2) ][rel_lor_idx] = projection;
  }
  msec_timer.stop();
  
  if (use_z_model)
    out_hist *= Z_model;
  
//   scanner->reflectHistograms(out_hist, "LORs");
  
  if (!internal_init)
    out_hist.save(output_dir + "/" + out_filename);
}
