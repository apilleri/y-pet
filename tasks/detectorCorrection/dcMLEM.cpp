/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vDetectorCorrection.h"

void dcMLEM::apply()
{
  timer msec_timer("msec");
  timer msec_timer_partial("msec");
  
  msec_timer.start("Solving", "", false);
  
  const GroupedCS& grouped_cs = scanner->getGroupedCS();
  
  for (uint16_t iter=1; iter <= n_iterations; iter++) {
    msec_timer_partial.start("Iteration " + std::to_string(iter), "\t", true);
    for (uint16_t f_pair=0; f_pair<grouped_cs.size(); f_pair++) {
      
      uint16_t n_views = grouped_cs[f_pair].size();
      detPair_t model_id = grouped_cs[f_pair][0].pairID();
      const SparseModel& model = D_models.at(model_id);
//       lorIDX_t n_lors = in_hist.data[model_id].size();
      lorIDX_t n_lors = model.rows();
      lorIDX_t n_lofs = model.cols();
      
      //projection
      std::vector< FloatType* > ratios(n_views);
      for (uint16_t v=0; v<n_views; v++) {
        ratios[v] = new FloatType[n_lors];
        std::fill(ratios[v], ratios[v] + n_lors, 0);
      }
      #pragma omp parallel for
      for (lorIDX_t c_lor=0; c_lor<n_lors; c_lor++) {
        MatrixEntry *bp, *ep;
        bp = model.rowPointer(c_lor);
        ep = bp + model.rowElementsN(c_lor);
        while(bp < ep) {
          uint32_t lof = (*bp).col;
          FloatType value = (*bp).v;
          for (uint16_t v=0; v<n_views; v++) {
            uint16_t view = grouped_cs[f_pair][v].pairID();
            ratios[v][c_lor] += out_hist.data[view][lof] * value;
          }
          bp++;
        }
      }
      
      // computing ratio
      for (lorIDX_t c_lor=0; c_lor<n_lors; c_lor++) {
        for (uint16_t v=0; v<n_views; v++) {
          uint16_t view = grouped_cs[f_pair][v].pairID();
          if (ratios[v][c_lor])
            ratios[v][c_lor] = in_hist.data[view][c_lor] / ratios[v][c_lor];
        }
      }

//       if (f_dir!="") {
//         // applying filtering
//         for (lorIDX_t c_lof=0; c_lof<n_lofs; c_lof++) {
//           MatrixEntry *bp, *ep;
//           bp = f_models.at(model_id).rowPointer(c_lof);
//           ep = bp + f_models.at(model_id).rowElementsN(c_lof);
// //           std::cout << c_lof << std::endl;
//           while(bp < ep) {
//             uint32_t lof= (*bp).col;
//             FloatType value = (*bp).v;
// //             std::cout << "\t" << lof << " " << value << std::endl;
//             for (uint16_t v=0; v<n_views; v++) {
//               uint16_t view = grouped_cs[f_pair][v].pairID();
//               smooth_hist.data[view][c_lof] += out_hist.data[view][lof]*value;
//             }
//             bp++;
//           }
//         }
// 
//       }
      
      //backprojection
      #pragma omp parallel for
      for (lorIDX_t c_lof=0; c_lof<n_lofs; c_lof++) {
//         std::vector<FloatType> back_proj(n_views, 0);
        MatrixEntry *bp, *ep;
        bp = D_models_tr.at(model_id).rowPointer(c_lof);
        ep = bp + D_models_tr.at(model_id).rowElementsN(c_lof);
        while(bp < ep) {
          uint32_t lor = (*bp).col;
          FloatType value = (*bp).v;
          for (uint16_t v=0; v<n_views; v++) {
            if (std::isnormal( ratios[v][lor] ) ) {
              uint16_t view = grouped_cs[f_pair][v].pairID();
//               back_proj[v] += ratios[v][lor] * value;
              update.data[view][c_lof] += ratios[v][lor] * value;
            }
          }
          bp++;
        }
        
//         for (uint16_t v=0; v<n_views; v++) {
//           uint16_t view = grouped_cs[f_pair][v].pairID();
//           out_hist.data[view][c_lof] *= back_proj[v];
//         }
        
      }
      
      
      // computing penalties
      if (f_dir!="") {
        std::cout << "applying penalties" << std::endl;
        // applying filtering
        #pragma omp parallel for
        for (lorIDX_t c_lof=0; c_lof<n_lofs; c_lof++) {
          MatrixEntry *bp, *ep;
          bp = f_models.at(model_id).rowPointer(c_lof);
          ep = bp + f_models.at(model_id).rowElementsN(c_lof);
          
          //computing median penalty
          for (uint16_t v=0; v<n_views; v++) {
            uint16_t view = grouped_cs[f_pair][v].pairID();
            std::vector<FloatType> values(f_models.at(model_id).rowElementsN(c_lof),0);
            uint32_t i=0;
            while(bp < ep) {
              uint32_t lof= (*bp).col;
              values[i] = out_hist.data[view][lof];
              bp++;
              i++;
            }
            FloatType median_v = median(values);
            penalty_hist.data[view][c_lof] = beta*(out_hist.data[view][c_lof] - median_v)/median_v;
          }
        }
      }
          

      
      //dividing by the sensitivity and penal term.
      #pragma omp parallel for
      for (lorIDX_t c_lof=0; c_lof<n_lofs; c_lof++) {
        for (uint16_t v=0; v<n_views; v++) {
          uint16_t view = grouped_cs[f_pair][v].pairID();
          update.data[view][c_lof] /= sensitivity.data[view][c_lof]+penalty_hist.data[view][c_lof];
          out_hist.data[view][c_lof] *= update.data[view][c_lof];
          update.data[view][c_lof]=0;
        }
      }
      
      
      
    }
    
    msec_timer_partial.stop();

    
    
    
    if (iter % save_every == 0 || iter == 1) {
      scanner->reflectHistograms(out_hist, "LOFs");
      std::string basen = output_dir + "/" + out_filename + "_";
      out_hist.save( basen + std::to_string(iter) + ".hg");
      scanner->reflectHistograms(out_hist, "LOFs");
    } 
    
  }
  
  
  msec_timer.stop();
  
}

void dcMLEM::computeSensitivity()
{
  std::string name_tosave = scanner->name() + "_LOR_Sensitivity";
  
  timer msec_timer("msec");
  timer msec_timer_partial("msec");
   
  msec_timer.start("Computing sensitivity", "", false);
  
  if (!use_z_model) {
    name_tosave += "_";
//     std::cout << "Allocating normalization histogram" << std::endl;
//     Z_model.allocate(scanner->getDetectorsList(),
//                       scanner->getCoincidenceSchema(),
//                       "LORs", 1);
  }
  else
    name_tosave += "_Z";
  
//   data_type& nd = Z_model.data;
//   
  const GroupedCS& grouped_cs = scanner->getGroupedCS();
   
  // before adding the normalization
  for (uint16_t f_pair=0; f_pair<grouped_cs.size(); f_pair++) {
    std::cout << "f_pair " << f_pair << std::endl;
    detPair_t f_pair_ID = grouped_cs[f_pair][0].pairID();
    lorIDX_t n_lofs = out_hist.data[f_pair_ID].size();
    
    msec_timer_partial.start("backproj D", "\t", true);
    #pragma omp parallel for
    for (lorIDX_t c_lof=0; c_lof<n_lofs; c_lof++) {
      MatrixEntry *bp, *ep;
      bp = D_models_tr.at(f_pair_ID).rowPointer(c_lof);
      ep = bp + D_models_tr.at(f_pair_ID).rowElementsN(c_lof);
      while(bp < ep) {
        sensitivity.data[f_pair_ID][c_lof] += (*bp).v;
        bp++;
      }
    }
    msec_timer_partial.stop();
    
    for (uint16_t v=1; v<grouped_cs[f_pair].size(); v++) {
      detPair_t pair_ID = grouped_cs[f_pair][v].pairID();
      #pragma omp parallel for
      for (lorIDX_t c_lof=0; c_lof<n_lofs; c_lof++)
        sensitivity.data[pair_ID][c_lof] = sensitivity.data[f_pair_ID][c_lof];
    }
  }
  /*
  
  for (uint16_t f_pair=0; f_pair<grouped_cs.size(); f_pair++) {
    std::cout << "f_pair " << f_pair << std::endl;
    detPair_t model_id = grouped_cs[f_pair][0].pairID();
    const SparseModel& model = D_models_tr.at(model_id);
    //       lorIDX_t n_lors = in_hist.data[model_id].size();
    lorIDX_t n_lofs = model.rows();
//     lorIDX_t n_lofs = model.cols();
    
    msec_timer_partial.start("backproj D", "\t", true);
    #pragma omp parallel for
    for (lorIDX_t c_lof=0; c_lof<n_lofs; c_lof++) {
      for (uint16_t v=0; v<grouped_cs[f_pair].size(); v++) {
        MatrixEntry *bp, *ep;
        bp = model.rowPointer(c_lof);
        ep = bp + model.rowElementsN(c_lof);
        while(bp < ep) {
          uint32_t lor = (*bp).col;
          FloatType value = (*bp).v;
          detPair_t pair_ID = grouped_cs[f_pair][v].pairID();
          sensitivity.data[pair_ID][c_lof] += value * nd[pair_ID][lor];
          bp++;
        }
      }
    }
    msec_timer_partial.stop();
  }*/
  
  msec_timer.stop();
  
  sensitivity.save(name_tosave + ".hg");
}


void dcMLEM::loadFilterModels(std::string f_dir_p)
{
  
  timer msec_timer("msec");
  
  using PairType = std::pair<detPair_t, CoincidencePairSym>;
  std::cout << "loading F model from directory " << f_dir << std::endl;
  for (const PairType& entry : scanner->getCoincidenceSchema() ) {
    const CoincidencePairSym& pair = entry.second;
    if ( pair.fundamental() ) {
      detID_t id1 = pair.id1();
      detID_t id2 = pair.id2();
      std::string id_pair = std::to_string(id1) + "-" + std::to_string(id2);
      std::string filename =  f_dir_p + "/" + id_pair + ".filter";
      
      uint32_t n_vc1 = scanner->getDetector(id1).vCryN();
      uint32_t n_vc2 = scanner->getDetector(id2).vCryN();
      
      lorIDX_t n_LOFs = n_vc1*n_vc2;
      
      msec_timer.start(filename, "", true);
      SparseModel model(n_LOFs, n_LOFs );
      detPair_t sz_id = szudzikPair(id1, id2);
      f_models.insert( {sz_id, model} );
      f_models.at(sz_id).set(filename /*+ "_tr"*/);
      msec_timer.stop();
    }
  }
}
