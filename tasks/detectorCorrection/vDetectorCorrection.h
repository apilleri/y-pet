/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TASKS_DETECTORCORECTION_VDETECTORCORRECTION_H_
#define TASKS_DETECTORCORECTION_VDETECTORCORRECTION_H_

#include "taskBase.h"

class vDetectorCorrection : public TaskBase {
  
protected:
  std::string in_hist_filename;
  std::string out_filename;

  HistogramsMap in_hist;
  HistogramsMap out_hist;

public:
  vDetectorCorrection(const std::string& config, const cxxopts::ParseResult& cmd_line)
  : TaskBase(config, cmd_line),
    in_hist_filename(),
    out_filename()
  {
    if (cmd_line.count("dmodel")) {
      std::string str = cmd_line["dmodel"].as<std::string>();
      loadDetectorModels(str, true);
    }
    
    in_hist_filename = cmd_line["input"].as<std::string>();
     
    out_filename = cmd_line["output"].as<std::string>();
    
    in_hist.allocate(scanner->getDetectorsList(),
                     scanner->getCoincidenceSchema(),
                     "LORs", 0);
    in_hist.load(in_hist_filename);
    
//     if (use_normalization_model) {
//       for (auto k : Z_model.keys()) {
//         for (uint32_t idx=0; idx<Z_model.data[k].size(); idx++) {
//           if (Z_model.data[k][idx] == 0)
//             Z_model.data[k][idx] = 1;
//           in_hist.data[k][idx] /= Z_model.data[k][idx];
//         }
//       }
//     }
    
    
    scanner->reflectHistograms(in_hist, "LORs");
    out_hist.allocate(scanner->getDetectorsList(),
                      scanner->getCoincidenceSchema(),
                      "LOFs", 1);
    
    std::cout << "Input Histogram: number of lors: " << in_hist.nLors();
    std::cout << std::endl;
    std::cout << "Input Histogram: number of not null Lors: ";
    std::cout << in_hist.notNull() << " (" ;
    std::cout << ( (float)in_hist.notNull() )/in_hist.nLors() * 100;
    std::cout << "%)" << std::endl;
  }
  
  virtual ~vDetectorCorrection()
  { }
  
};


using entryT =  std::pair<FloatType, uint32_t>;

class dcMLEM : public vDetectorCorrection {

  std::string f_dir;

  uint16_t n_iterations;
  uint16_t save_every;
  
  HistogramsMap sensitivity;
  HistogramsMap update;
  
  HistogramsMap penalty_hist;
  std::map<detPair_t, SparseModel> f_models;
  FloatType beta;
  
public:
  dcMLEM(const std::string& config, const cxxopts::ParseResult& cmd_line)
    : vDetectorCorrection(config, cmd_line),
      n_iterations(1),
      save_every(1),
      beta(0.5)
  {
    
    if (cmd_line.count("fmodel")) {
      f_dir = cmd_line["fmodel"].as<std::string>();
      loadFilterModels(f_dir);
    }
    else
      f_dir="";
    
    if (cmd_line.count("number"))
      n_iterations = cmd_line["number"].as<uint16_t>();
    
    if ( cmd_line.count("every") )
      save_every = cmd_line["every"].as<uint16_t>();
    
    sensitivity.allocate(scanner->getDetectorsList(),
                         scanner->getCoincidenceSchema(),
                         "LOFs", 0);
    
    update.allocate(scanner->getDetectorsList(),
                          scanner->getCoincidenceSchema(),
                          "LOFs", 0);
    
    penalty_hist.allocate(scanner->getDetectorsList(),
                          scanner->getCoincidenceSchema(),
                          "LOFs", 0);
    
    
    computeSensitivity();
  }
  
  void apply() override;
  
  ~dcMLEM()
  { }
  
private:
  void computeSensitivity();
  
  void loadFilterModels(std::string d_dir);
  
};

#endif // TASKS_DETECTORCORECTION_VDETECTORCORRECTION_H_

