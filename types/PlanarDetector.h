/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TYPES_PLANARDETECTOR_H_
#define TYPES_PLANARDETECTOR_H_

#include <cstdint>
#include <utility>
#include <fstream>
#include <tuple>

#include <vector>
#include <map>

#include "euclideanSpace.h"
#include "dataTypes.h"
#include "f_utils.h"

/*! \class PlanarLayerType
 *  \brief Contains the description of a layer of crystals extracted from the XML file
 * 
 *  It is used by the CylindricalGeometry class as a container for all the 
 *  information about one single layer of a detector and by the PlanarDetectorType class
 */
class PlanarLayerType {
  uint16_t n_crystals_Y, n_crystals_Z;
  SpaceType crystal_len_X, crystal_len_Y, crystal_len_Z;
  SpaceType crystal_pitch_Y, crystal_pitch_Z;

public:
  PlanarLayerType(uint16_t n_Y, uint16_t n_Z,
                  SpaceType c_X, SpaceType c_Y, SpaceType c_Z,
                  SpaceType p_Y, SpaceType p_Z)
    :
      n_crystals_Y(n_Y), n_crystals_Z(n_Z),
      crystal_len_X(c_X), crystal_len_Y(c_Y), crystal_len_Z(c_Z),
      crystal_pitch_Y(p_Y), crystal_pitch_Z(p_Z)
  { }
  
  uint16_t index(uint16_t position_Y, uint16_t position_Z) const
  {
    return position_Y + position_Z*n_crystals_Y;
  }

  // return the coordinates row and col of the crystal, relative to the layer
  // (starting from 0 to nCrystals in the layer
  std::tuple<uint16_t, uint16_t> coordinates(uint16_t crystal_ID) const
  {
    uint16_t row, col;
    
    row = crystal_ID%n_crystals_Y;
    col = (crystal_ID - row)/n_crystals_Y;
    
    return std::make_tuple(row, col);
  }

  uint16_t nCrystals() const { return n_crystals_Y*n_crystals_Z; }
  uint16_t nCrystalsY() const { return n_crystals_Y; }
  uint16_t nCrystalsZ() const { return n_crystals_Z; }
  
  SpaceType crystalLengthX() const { return crystal_len_X; }
  SpaceType crystalLengthY() const { return crystal_len_Y; }
  SpaceType crystalLengthZ() const { return crystal_len_Z; }
  
  SpaceType crystalPitchY() const { return crystal_pitch_Y; }
  SpaceType crystalPitchZ() const { return crystal_pitch_Z; }
};

class VirtualLayerType {
  uint16_t n_crystals_Y, n_crystals_Z;
  SpaceType crystal_pitch_Y, crystal_pitch_Z; //no gaps between crystals

public:
  VirtualLayerType()
    :
      n_crystals_Y(0), n_crystals_Z(0),
      crystal_pitch_Y(0), crystal_pitch_Z(0)
  { }
  
  VirtualLayerType(uint16_t n_Y, uint16_t n_Z, SpaceType p_Y, SpaceType p_Z)
    :
      n_crystals_Y(n_Y), n_crystals_Z(n_Z),
      crystal_pitch_Y(p_Y), crystal_pitch_Z(p_Z)
  { }

  uint16_t index(uint16_t position_Y, uint16_t position_Z) const
  {
    return position_Y + position_Z*n_crystals_Y;
  }
  
  uint16_t computePositionY(uint16_t crystal_ID) const
  {
    return crystal_ID%n_crystals_Y;
  }
  
  uint16_t computePositionZ(uint16_t crystal_ID) const
  {
    return crystal_ID/n_crystals_Y;
  }
  
  uint16_t nCrystals() const { return n_crystals_Y*n_crystals_Z; }
  uint16_t nCrystalsY() const { return n_crystals_Y; }
  uint16_t nCrystalsZ() const { return n_crystals_Z; }
  
  SpaceType crystalPitchY() const { return crystal_pitch_Y; }
  SpaceType crystalPitchZ() const { return crystal_pitch_Z; }
};

/*! \class PlanarDetectorType
 *  \brief Single detector block of the scanner.
 *
 *  This class is the object that allows to abstract from a specific geometry
 *  in order to perform model computation and image reconstruction in a generic 
 *  way.
 *  It contains the information about a single detector block contained in a 
 *  detector pair. A detector block represents a planar detector with a generic
 *  orientation in the space. The class contains also the 
 *  orientation of the detector in the space and the list of the crystals types.
 *  It is convenient to consider a detector block as made of all the crystals 
 *  that are read together by a single output electronic.
 *  For example, in a Cylindrical Geometry it is represented by a BLOCK with 
 *  all its layers.
 *  Every time a Detector is needed, it should be created using the 
 *  appropriate methods provided by the vGeometry derived classes.
 */
class PlanarDetectorType {
  //! unique Index
  detID_t _ID;
  
  //! orientation in the space
  /*! Vector perpendcular to the inner surface of the detector (toward lower x
   * for the detector with zero angle)
   */
  
  ThreeVector detector_center;
  ThreeVector _orientation;
  ThreeVector _size;
  
  ThreeVector detector_surface_center;
  ThreeMatrix rotation_matrix;
  
//   ThreeVector min_corner;
//   ThreeVector max_corner;
  
  //! absolute center of the crystals
  /*! absolute position of the center of the crystals of the Detector
   *  Block. In a Cylindrical Geometry it contains the position of every crystal 
   *  of every possible layer of the Block.
   */
  std::vector<ThreeVector> centers;
  std::vector<ThreeVector> best_points;
  
  //! absolute surface center of the crystals
  /*! absolute position of the surface center of the crystals of the Detector
   *  Block. The surface is the one closers to the Z-axis. In a Cylindrical Geometry
   *  it contains the position of every crystal of every possible layer of the Block.
   */
  std::vector<ThreeVector> surface_centers;
  
  //! layers information
  std::vector<PlanarLayerType> layers;
  
  //--- Virtual Layer section
  bool activate_vl;
  bool vl_present;
  //! virtual layer
  VirtualLayerType v_layer;
  
  //! virtual layer centers which are also virtual layer surface centers
  std::vector<ThreeVector> v_centers;
  
  
public:
  explicit PlanarDetectorType(detID_t id_p, const ThreeVector& center_p,
    const ThreeVector& orientation_p, const ThreeVector size_p)
  : _ID(id_p), detector_center(center_p), 
    _orientation(orientation_p),
    _size(size_p),
    detector_surface_center(ThreeVector()),
    activate_vl(false),
    vl_present(false)
  { 
    
    //compute min and max corner
    
  }
  
  PlanarDetectorType() {}
  
  
//   PlanarDetectorType(const PlanarDetectorType& p)
//   {
//     _ID = p._ID;
//     detector_center = p.detector_center;
//     _orientation = p._orientation;
//     _size = p._size;
//     detector_surface_center = p.detector_surface_center;
//     centers = p.centers;
//     best_points = p.best_points;
//     surface_centers = p.surface_centers;
//     layers = p.layers;
//     activate_vl = p.activate_vl;
//     vl_present = p.vl_present;
//     v_layer = p.v_layer;
//     v_centers = p.v_centers;
//     
//   }
  
  // get Methods
  //! return the detector ID
  detID_t ID() const
  {
    return  _ID;
  }
  
  const ThreeVector& orientation() const 
  {
    return _orientation;
  }
  
  const ThreeMatrix& rotationMatrix() const
  {
    return rotation_matrix;
  }
  
  const ThreeVector& size() const
  {
    return _size;
  }
  
  AngleType zAngle() const
  {
    AngleType angle = std::atan2(_orientation[1], _orientation[0]);
    if (angle < 0 )
      angle += 2*M_PI;
    
    return angle;
  }

  bool virtualLayerPresent() const
  {
    return vl_present;
  }
  
  const ThreeVector& detectorCenter() const
  {
    return detector_center;
  }
  
  const ThreeVector& detectorSurfaceCenter() const
  {
    return detector_surface_center;
  }

  //! returns the center position of the crystal pointed by index
  const ThreeVector& rCryCenter(uint16_t index) const
  {
    return centers[index];
  }
  
  const std::vector<ThreeVector>& rCryCenters() const
  {
    return centers;
  }
  
  //! returns the surface position of the crystal pointed by index
  const ThreeVector& rCrySurfaceCenter(uint16_t index) const
  {
    return surface_centers[index];
  }

  const std::vector<ThreeVector>& rCrySurfaceCenters() const
  {
    return surface_centers;
  }

  
  //! returns the center position of the virtual crystal pointed by index
  const ThreeVector& vCryCenter(uint16_t index) const
  {
    if (vl_present)
      return v_centers[index];
    else
      return surface_centers[index];
  }
  
  const std::vector<ThreeVector>& vCryCenters() const
  {
    if (vl_present)
      return v_centers;
    else
      return surface_centers;
  }

  const std::vector<ThreeVector>& aCrySurfaceCenters() const
  {
    if (activate_vl) 
      return v_centers;
    else
      return surface_centers;
  }
  
  const ThreeVector& aCrySurfaceCenter(uint16_t index) const
  {
    if (activate_vl) {
      if (index >= vCryN() ) {
        std::cout << "index " + std::to_string(index) ;
        throw std::string("aCrySurfaceCenter: virtual layer out of bounds");
      }
      return v_centers[index];
    }
    else {
      if (index >= rCryN() ) {
        std::cout << "index " + std::to_string(index) ;
        throw std::string("aCrySurfaceCenter: real layer out of bounds");
      }
      return surface_centers[index];
    }
  }
  
  ThreeVector aCryBestPoint(uint16_t index) const
  {
    if (activate_vl) {
      if (index >= vCryN() ) {
        std::cout << "index " + std::to_string(index) ;
        throw std::string("aCryBestPoint: virtual layer out of bounds");
      }
      return v_centers[index];
    }
    else {
      if (index >= rCryN() ) {
        std::cout << "index " + std::to_string(index) ;
        throw std::string("aCryBestPoint: real layer out of bounds");
      }
      //TODO sistemare questa questione per bene funziona solo con lyso
      FloatType length = -std::log(0.5 + 0.5*std::exp(-0.0833*xLen(index)))/0.0833;
      ThreeVector shift(length, 0, 0);
      shift.rotateZ (zAngle() );
      return surface_centers[index]+ shift;
    }
  }
  
  //! length of the crystal along the X-axis
  SpaceType xLen(uint16_t index) const
  {
    uint16_t c_layer = 0; // computing the layer which the crystals belongs to
    uint16_t n_scanned = 0;
    while(c_layer < layers.size() && index > (n_scanned + layers[c_layer].nCrystals()) ){
      n_scanned += layers[c_layer].nCrystals();
      c_layer++;
    }
    return layers[c_layer].crystalLengthX();
  }

  //! length of the crystal along the Y-axis
  SpaceType yLen(uint16_t index) const
  {
    uint16_t c_layer = 0; // computing the layer which the crystals belongs to
    uint16_t n_scanned = 0;
    while(c_layer < layers.size() && index > (n_scanned + layers[c_layer].nCrystals()) ){
      n_scanned += layers[c_layer].nCrystals();
      c_layer++;
    }
    return layers[c_layer].crystalLengthY();
  }
  
  //! length of the crystal along the Z-axis
  SpaceType zLen(uint16_t index) const
  {
    uint16_t c_layer = 0; // computing the layer which the crystals belongs to
    uint16_t n_scanned = 0;
    while(c_layer < layers.size() && index > (n_scanned + layers[c_layer].nCrystals()) ){
      n_scanned += layers[c_layer].nCrystals();
      c_layer++;
    }
    return layers[c_layer].crystalLengthZ();
  }
  
  SpaceType activeyLen(uint16_t index) const
  {
    if (activate_vl && vl_present) {
      return v_layer.crystalPitchY();
    }
    else
      return yLen(index);
  }
  
  SpaceType activezLen(uint16_t index) const
  {
    
    if (activate_vl  && vl_present) {
      return v_layer.crystalPitchZ();
    }
    else
      return zLen(index);
  }
  
  std::tuple<uint16_t, uint16_t, uint16_t> crystalInfo(uint16_t index) const
  {
    uint16_t layer=0, row=0, col=0;
    
    while (layer < layers.size() && index >= layers[layer].nCrystals() ) {
      index -= layers[layer].nCrystals();
      layer++;
    }
    
    std::tie(row, col) = layers[layer].coordinates(index);
    
    return std::make_tuple(layer, row, col);
  }
  
  uint16_t computeCrystalIndex(uint16_t layer, uint16_t pos_y, uint16_t pos_z) const
  {
    uint16_t start = 0;
    uint16_t c_layer = 0;
    while ( c_layer < layer ) {
      start += layers[c_layer].nCrystals();
      c_layer++;
    }
    
    start += layers[c_layer].index(pos_y, pos_z);
    return start;
  }
  
  //! total number of crystals in the Detector Block
  uint16_t rCryN() const
  {
    return surface_centers.size();
  }
  
  uint16_t vCryN() const
  {
    if (vl_present)
      return v_centers.size();
    else
      return surface_centers.size();
  }
  
  uint16_t vCryNy() const
  {
    if (vl_present)
      return v_layer.nCrystalsY();
    else
      return layers[0].nCrystalsY();
  }
  
  uint16_t vCryNz() const
  {
    if (vl_present)
      return v_layer.nCrystalsZ();
    else
      return layers[0].nCrystalsZ();
  }
  
  uint16_t aCryN() const
  {
    if (activate_vl)
      return vCryN();
    else
      return rCryN();
  }
  
  uint16_t nLayers() const
  {
    return layers.size();
  }
  
  const PlanarLayerType& layer(uint16_t idx) const
  {  
    return layers[idx];
  }
  
  const VirtualLayerType& virtualLayer() const
  {  
    return v_layer;
  }
  // set methods
  
  void setID(detID_t val)
  {
    _ID = val;
  }
  
  void setOrientation(const ThreeVector& orient)
  {
    _orientation = orient;
  }
  
  void setRotationMatrix(const ThreeMatrix& rot)
  {
    rotation_matrix = rot;
  }
  
  void setSize(const ThreeVector& size_p)
  {
    _size = size_p;
  }
  
  void setVirtualLayerPresent()
  {
    vl_present = true;
  }
  
  void setActivateVirtualLayer()
  {
    activate_vl = true;
  }
  
  void setDetectorCenter(const ThreeVector& vec)
  {
    detector_center = vec;
  }
  
  void setDetectorSurfaceCenter(const ThreeVector& vec)
  {
    detector_surface_center = vec;
  }
  
  /*! \brief set the crystal centers vector
   * 
   *  \param abs_centers given by the Geometry class
   */
  void setCrystalsCenters(std::vector<ThreeVector> abs_centers)
  {
    centers.swap(abs_centers);
    
//     //TODO QUI
//     for (auto c : centers) {
//       FloatType length = -std::log(0.5 + 0.5*std::exp(-0.0833*xLen(index)))/0.0833;
//       ThreeVector shift(length, 0, 0);
//       shift.rotateZ (zAngle() );
//     }
//       best_points
//     
//     return centers[index]+ shift;
  }
  
  /*! \brief set the crystal surface centers vector
   * 
   *  \param abs_surface_centers given by the Geometry class
   */
  void setCrystalsSurfaceCenters(std::vector<ThreeVector> abs_surface_centers)
  {
    surface_centers.swap(abs_surface_centers);
  }
  
  /*! \brief add information about a layer
   * 
   *  \param layer layer to add
   */
  void addLayer(PlanarLayerType l)
  {
    layers.push_back(l);
  }
  
  void setVirtualLayer(VirtualLayerType vl)
  {
    v_layer = vl;
  }
  
  void setVirtualCrystalsCenters(std::vector<ThreeVector> abs_centers)
  {
    v_centers.swap(abs_centers);
  }
  
  
  void saveToDisk(const std::string& filename) const
  {
    std::ofstream out_file;
    out_file.open(filename);
    if (!out_file.is_open())
      throw std::string("PlanarDetectorType::saveToDisk: unable to open" + filename + " for writing");
    
    out_file.write( reinterpret_cast<const char *>(&_ID), sizeof(detID_t) );
    out_file.write( reinterpret_cast<const char *>(&_orientation), 3*sizeof(SpaceType) );
    out_file.write( reinterpret_cast<const char *>(&detector_center), 3*sizeof(SpaceType) );
    out_file.write( reinterpret_cast<const char *>(&detector_surface_center), 3*sizeof(SpaceType) );
    
    uint16_t n_cry = centers.size();
    out_file.write( reinterpret_cast<const char *>(&n_cry), sizeof(uint16_t) );
    out_file.write( reinterpret_cast<const char *>(centers.data()), 3*n_cry*sizeof(SpaceType) );
    out_file.write( reinterpret_cast<const char *>(surface_centers.data()), 3*n_cry*sizeof(SpaceType) );
    
    uint16_t n_lay = layers.size();
    out_file.write( reinterpret_cast<const char *>(&n_lay), sizeof(uint16_t) );
    for (const PlanarLayerType& l : layers) {
      uint16_t n = l.nCrystalsY();
      out_file.write( reinterpret_cast<const char *>(&n), sizeof(uint16_t) );
      n = l.nCrystalsZ();
      out_file.write( reinterpret_cast<const char *>(&n), sizeof(uint16_t) );
      
      SpaceType v = l.crystalLengthX();
      out_file.write( reinterpret_cast<const char *>(&v), sizeof(SpaceType) );
      v = l.crystalLengthY();
      out_file.write( reinterpret_cast<const char *>(&v), sizeof(SpaceType) );
      v = l.crystalLengthZ();
      out_file.write( reinterpret_cast<const char *>(&v), sizeof(SpaceType) );
      v = l.crystalPitchY();
      out_file.write( reinterpret_cast<const char *>(&v), sizeof(SpaceType) );
      v = l.crystalPitchZ();
      out_file.write( reinterpret_cast<const char *>(&v), sizeof(SpaceType) );
    }
    
    //virtual layer status
    out_file.write( reinterpret_cast<const char *>(&activate_vl), sizeof(bool) );
    out_file.write( reinterpret_cast<const char *>(&vl_present), sizeof(bool) );
    
    //if there is a virtual layer, the information is saved too
    if (vl_present) {
      uint16_t n = v_layer.nCrystalsY();
      out_file.write( reinterpret_cast<const char *>(&n), sizeof(uint16_t) );
      n = v_layer.nCrystalsZ();
      out_file.write( reinterpret_cast<const char *>(&n), sizeof(uint16_t) );
      SpaceType v = v_layer.crystalPitchY();
      out_file.write( reinterpret_cast<const char *>(&v), sizeof(SpaceType) );
      v = v_layer.crystalPitchZ();
      out_file.write( reinterpret_cast<const char *>(&v), sizeof(SpaceType) );
      
      uint16_t n_cry_l = v_centers.size();
      out_file.write( reinterpret_cast<const char *>(&n_cry_l), sizeof(uint16_t) );
      out_file.write( reinterpret_cast<const char *>(v_centers.data()), 3*n_cry_l*sizeof(SpaceType) );
    }
    
  }
  
  void loadFromDisk(std::string filename)
  {
    std::ifstream in_file;
    in_file.open(filename);
    if (!in_file.is_open())
      throw std::string("PlanarDetectorType::saveToDisk: unable to open" + filename + " for reading");
    
    readFromFile<detID_t>(in_file, &_ID);
    readFromFile<ThreeVector>(in_file, &_orientation);
    readFromFile<ThreeVector>(in_file, &detector_center);
    readFromFile<ThreeVector>(in_file, &detector_surface_center);
    
    uint16_t n_cry;
    readFromFile<uint16_t>(in_file, &n_cry);
    centers.resize(n_cry);
    readFromFile<ThreeVector>(in_file, centers.data(), n_cry);
    surface_centers.resize(n_cry);
    readFromFile<ThreeVector>(in_file, surface_centers.data(), n_cry);
    
    uint16_t n_lay;
    readFromFile<uint16_t>(in_file, &n_lay);
    for (uint16_t i=0; i<n_lay; i++) {
      uint16_t ny, nz;
      SpaceType cx, cy, cz, py, pz;
      readFromFile<uint16_t>(in_file, &ny);
      readFromFile<uint16_t>(in_file, &nz);
      readFromFile<SpaceType>(in_file, &cx);
      readFromFile<SpaceType>(in_file, &cy);
      readFromFile<SpaceType>(in_file, &cz);
      readFromFile<SpaceType>(in_file, &py);
      readFromFile<SpaceType>(in_file, &pz);
      PlanarLayerType temp(ny, nz, cx, cy, cz, py, pz);
      
      layers.push_back(temp);
    }
    
    readFromFile<bool>(in_file, &activate_vl);
    readFromFile<bool>(in_file, &vl_present);
    
    if (activate_vl && !vl_present)
      throw std::string("activate_bl but vl_present = false");

    if (vl_present) {
      uint16_t ny, nz;
      readFromFile<uint16_t>(in_file, &ny);
      readFromFile<uint16_t>(in_file, &nz);
      SpaceType py, pz;
      readFromFile<SpaceType>(in_file, &py);
      readFromFile<SpaceType>(in_file, &pz);
      v_layer = VirtualLayerType(ny, nz, py, pz);
      
      uint16_t n_cry_l;
      readFromFile<uint16_t>(in_file, &n_cry_l);
      v_centers.resize(n_cry_l);
      readFromFile<ThreeVector>(in_file, v_centers.data(), n_cry_l);
    }
    
  }
  
  ThreeVector absoluteMaxCoordinates()
  {
    ThreeVector max(0,0,0);
    
    for (ThreeVector vec : centers) {
      if (std::abs(vec.x())  > max.x() )
        max.setX(std::abs(vec.x()));
      if (std::abs(vec.y())  > max.y() )
        max.setY(std::abs(vec.y()));
      if (std::abs(vec.z())  > max.z() )
        max.setZ(std::abs(vec.z()));
    }
    
    return max;
  }
  
  void printInfo() const
  {
    std::cout << "Detector ID: " << _ID << std::endl;
    std::cout << "Number of real crystals: " << rCryN() << std::endl;
    std::cout << "Number of layers: " << nLayers() << std::endl;
    if (vl_present)
      std::cout << "Number of virtual crystals: " << vCryN() << std::endl;
    std::cout << "Vitual layer activated: " << activate_vl << std::endl;
  }

  std::tuple<std::vector<ThreeVector>, std::vector<double>>
  computeIntegrationPoints(uint16_t cry, const ThreeVector& ray_dir,
                           uint16_t nyz, uint16_t nx) const;
};

/*! \typedef DetectorPair
 *  \brief This type is the base object used to build the model.
 */

using DetectorsList = std::map<detID_t, PlanarDetectorType>;


bool detectortIntersections(const PlanarDetectorType& det,
                            uint16_t cry,
                            const ThreeVector& ray_start,
                            const ThreeVector& ray_dir,
                            SpaceType& d1, SpaceType& d2, SpaceType& d3);
                            
bool detectortInteraction(const PlanarDetectorType& det,
                          const ThreeVector& ray_start,
                          const ThreeVector& ray_dir,
                          SpaceType& d);

#endif // TYPES_PLANARDETECTOR_H_
