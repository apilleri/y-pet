/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TYPES_SINOGRAMS_H_
#define TYPES_SINOGRAMS_H_

#include "dataTypes.h"
#include "HistogramsMap.h"
#include "vGeometry.h"

struct SinogramPoint {
  
  FloatType s;
  FloatType phi;
  FloatType count;
  
  SinogramPoint(FloatType s_p, FloatType phi_p, FloatType count_p)
    : s(s_p), phi(phi_p), count(count_p)
  { }
  
};

class SinogramPlaneInfo {
  
public:
  FloatType z;
  FloatType delta;
  
  SinogramPlaneInfo(FloatType z_p, FloatType delta_p) 
    : z(z_p), delta(delta_p)
  { }
  
};


class SinogramPlane {
  std::vector<SinogramPoint> points;
  SinogramPlaneInfo ring_diff;
  
  bool isDirect() 
  {
    return ring_diff.delta == 0 ? true : false;
  }
  
};


class SinogramData {
  
  std::vector<SinogramPlane> planes;
  
public:
  SinogramData()
  {}
  
  void buildFromHistogram(const vGeometry* scanner, const HistogramsMap& hist)
  {
    
    for (detPair_t k : hist.keys() ) {
      for (lorIDX_t rel_lor : hist.data.at(k) ) {
        detID_t d1, d2;
        detCryID_t c1, c2;
        std::tie(d1, c1, d2, c2) = scanner->getLORFullInfo(k, rel_lor);
        
        const PlanarDetectorType& det1 = scanner->getDetector(d1);
        const PlanarDetectorType& det2 = scanner->getDetector(d2);
        
        const ThreeVector& pos1 = det1.rCrySurfaceCenter(c1);
        const ThreeVector& pos2 = det1.rCrySurfaceCenter(c2);
        
        
      }
    }
  }
  
  void addPoint()
  {
    
  }
};


#endif // TYPES_SINOGRAMS_H_
