/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TYPES_DATATYPES_H_
#define TYPES_DATATYPES_H_

#include <iostream>
#include <cstdint>
#include <cmath>
#include <map>
#include <vector>
#include <string>
#include <numeric>


#include <chrono>
using SClock = std::chrono::system_clock;
using Clock = std::chrono::high_resolution_clock;

#include <execution>

#include <filesystem>
namespace fs = std::filesystem;
// simple types

using SpaceType = float;
using AngleType = double;
using FloatType = float;


using cryID_t = uint32_t;
using detCryID_t = uint16_t;
using detID_t = uint16_t;
using detPair_t = uint32_t;

using lorIDX_t = uint64_t;

using ImageType = std::vector<FloatType>;

using StringVec = std::vector< std::string > ;

#endif // TYPES_DATATYPES_H_

