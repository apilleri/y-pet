/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <random>

#include "HistogramsMap.h"
#include "f_utils.h"
#include "timer.hpp"

// --- Creation methods

void HistogramsMap::allocate(const DetectorsList& det_list,
                             const CoincidenceSchema& cs,
                             const std::string& type,
                             FloatType value)
{
  if (type != "LOFs" && type != "LORs")// && type != "aLORs")
    throw std::string("HistogramsMap::allocate unknown type");
  
  for (const std::pair<detPair_t, CoincidencePairSym>& entry : cs) {
    
    _keys.push_back( entry.first );
    
    const CoincidencePairSym& pair = entry.second;
    detID_t id1 = pair.id1();
    detID_t id2 = pair.id2();
    
    const PlanarDetectorType& det1 = det_list.at(id1);
    const PlanarDetectorType& det2 = det_list.at(id2);
    
    uint32_t n_crystals1=0, n_crystals2=0;
    
    if (type == "LOFs") {
      n_crystals1 = det1.vCryN();
      n_crystals2 = det2.vCryN();
    } else if (type == "LORs") {
      n_crystals1 = det1.rCryN();
      n_crystals2 = det2.rCryN();
    }
//     else {
//       n_crystals1 = det1.aCryN();
//       n_crystals2 = det2.aCryN();
//     }
    
    lorIDX_t cur_n_el = n_crystals1*n_crystals2;
    _sizes.push_back(cur_n_el);
    n_total_lors += cur_n_el;
    
    data[ entry.first ].resize(cur_n_el, value);
  }
  
  if (value)
    not_null = n_total_lors;
  
}


void HistogramsMap::load(std::string filename)
{
  if ( data.empty() )
    throw std::string("HistogramsMap::load histogram not allocated.");
  
  timer msec_timer("msec");
  msec_timer.start("Loading Histograms " + filename, "", true);
  
  std::ifstream inputHistogramFile;
  inputHistogramFile.open(filename);
  if ( !inputHistogramFile.is_open() )
    throw std::string ( "Unable to open file " + filename + " for reading");
  
//   //reading LOFs flag
//   readFromFile<bool>(inputHistogramFile, &using_LOFs);
  uint64_t not_found=0;
  
  uint32_t szudzik_id;
  while( inputHistogramFile.peek()!=EOF && !inputHistogramFile.eof() ) {
    readFromFile<detPair_t>(inputHistogramFile, &szudzik_id);
    if ( data.find(szudzik_id) == data.end() ) {
      not_found++;
      std::cout << "\nError: szudzik_id: " << szudzik_id << std::endl;
//       throw std::string("HistogramsMap::load Unable to find detector pair: ");
      //skipping
      uint32_t read_n_lors;
      readFromFile<uint32_t>(inputHistogramFile, &read_n_lors);
      inputHistogramFile.ignore(read_n_lors*sizeof(uint32_t));
    } else {
      uint32_t n_lors = data[szudzik_id].size();
      uint32_t read_n_lors;
      readFromFile<uint32_t>(inputHistogramFile, &read_n_lors);
      if (read_n_lors != n_lors) {
        std::cout << "\nERROR" << std::endl,
        std::cout << "read_n_lors: " << read_n_lors << std::endl;
        std::cout << "n_lors: " << n_lors << std::endl;
        throw std::string("HistogramsMap::load n_lors != read_n_lors");
      }
      readFromFile<FloatType>(inputHistogramFile, data[szudzik_id].data(), n_lors);
    }
  }
  
  msec_timer.stop();
  
  for (data_type::iterator it = data.begin(); it != data.end(); ++it) {
    for(FloatType value : it->second) {
      if (value)
        not_null++;
    }
  }
  
}


void HistogramsMap::fill(FloatType value)
{
  for (data_type::iterator it = data.begin(); it != data.end(); ++it) {
    std::fill(it->second.begin(), it->second.end(), value);
  }
}


void HistogramsMap::clear()
{
  data.clear();
  _keys.clear();
  n_total_lors = 0;
  not_null = 0;
}

void HistogramsMap::disableDetector(detID_t detector)
{
  for (detPair_t key : _keys ) {
    detID_t id1, id2;
    std::tie(id1, id2) = szudzikUnPair(key);
    if (id1 == detector || id2 == detector)
      std::fill(data[key].begin(), data[key].end(), 0);      
  }
}

void HistogramsMap::save(std::string output_file) const
{
  if ( data.empty() )
    throw std::string("HistogramsMap::save, histogram not allocated");
  
  //     printStats();
  
  std::ofstream outputHistogramFile;
  outputHistogramFile.open(output_file);
  if ( !outputHistogramFile.is_open() )
    throw std::string ( "Unable to open file " + output_file + " for writing");
  
  for (data_type::const_iterator it = data.begin(); it != data.end(); ++it) {
    uint32_t id = it->first;
    const char* pointer = reinterpret_cast<const char *>(&id);
    outputHistogramFile.write(pointer, sizeof(uint32_t));
    uint32_t size = it->second.size();
    pointer = reinterpret_cast<const char *>(&size);
    outputHistogramFile.write(pointer, sizeof(uint32_t));
    pointer = reinterpret_cast<const char *>(it->second.data());
    outputHistogramFile.write(pointer, size*sizeof(float));
  }
  outputHistogramFile.close();
}


// --- Reconstruction related methods

std::tuple<detPair_t, lorIDX_t> HistogramsMap::identifyLOR(lorIDX_t lor)
{
  detPair_t d_pair;
  lorIDX_t rel_lor;
  
  lorIDX_t cur_count = 0;
  lorIDX_t last_count = 0;
  uint16_t last_pair = 0;
  uint16_t cur_pair = 0;
  
  while (lor >= cur_count) {
    last_count = cur_count;
    last_pair = cur_pair;
    cur_count += _sizes[cur_pair];
    cur_pair++ ;
  } 
  
  d_pair = _keys[last_pair];
  rel_lor = lor - last_count;
  
  return std::make_tuple(d_pair, rel_lor);
}


void HistogramsMap::generateSubsets(SubsestList& subsets, uint16_t n_subsets, uint32_t seed)
{
  subsets.resize(n_subsets);
  #pragma omp parallel for
  for (uint16_t cur_sub=0; cur_sub < n_subsets; cur_sub++) {
    subsets[cur_sub].clear();
    subsets[cur_sub].reserve(n_total_lors/n_subsets + 1);
  }
  
  SubsetType all_LORs(n_total_lors);
  lorIDX_t cur_lor = 0;
  
  //TODO questo è lento
  for (detPair_t k : _keys) {
    for (lorIDX_t i=0; i<data[k].size(); i++) {
      all_LORs[cur_lor] = std::make_pair(cur_lor, data[k][i]);
      cur_lor++;
    }
  }
  
  //TODO questo è lento
  std::shuffle (all_LORs.begin(), all_LORs.end(),
                std::default_random_engine(seed));
  
  
  uint16_t cur_sub = 0;
  for (lorIDX_t i=0; i<n_total_lors; i++) {
    if (  all_LORs[i].second != 0) {
      subsets[cur_sub].push_back( all_LORs[i] );
      cur_sub++;
      cur_sub = cur_sub%n_subsets;
    }
  }
  
  #pragma omp parallel for
  for (uint16_t cur_sub=0; cur_sub < n_subsets; cur_sub++) {
    
    std::sort(subsets[cur_sub].begin(), subsets[cur_sub].end(),
              [&](const std::pair<lorIDX_t, FloatType>& a, const std::pair<lorIDX_t, FloatType>& b)
              {
                return a.first < b.first;
              }
    );
    
  }
  
  
//   clear();
}

void HistogramsMap::generateDetSubsets(SubsestList& subsets)
{
  subsets.resize( _keys.size() );
  lorIDX_t cur_lor = 0;
  
  for (uint16_t j=0; j<_keys.size(); j++) {
    detPair_t k = _keys[j];
    for (lorIDX_t i=0; i<data[k].size(); i++) {
      subsets[j].push_back( std::make_pair(cur_lor, data[k][i]) );
      cur_lor++;
    }
  }
  
  clear();
}

// --- Debug

void HistogramsMap::dump2txt(std::string fname)
{
  if ( data.empty() )
    throw std::string("HistogramsMap::save histogram not allocated");
  
  
  std::ofstream outputHistogramFile;
  
  for (data_type::iterator it = data.begin(); it != data.end(); ++it) {
    uint32_t id = it->first;
    uint16_t d1, d2;
    std::tie(d1, d2) = szudzikUnPair(id);
    std::string def_name = fname + "_" + std::to_string(id);
    def_name += + "_" + std::to_string(d1) + "_" + std::to_string(d2) + ".txt";
    outputHistogramFile.open(def_name);
    if ( !outputHistogramFile.is_open() )
      throw std::string ( "Unable to open file " + def_name + " for writing");
    
    FloatType sum = 0;
    for (auto e : it->second) {
//       if ( e == 0 )
      outputHistogramFile << e << std::endl;
      sum += e;
    }
    outputHistogramFile.close();
    
    std::cout << std::setw(5) << id << std::setw(5) << d1;
    std::cout << std::setw(5) << d2 << std::setw(15) << sum << std::endl;
  }
}

void HistogramsMap::printHeader() const
{
  std::cout << std::setw(10) << "p_id";
  std::cout << std::setw(6)  << "id1";
  std::cout << std::setw(6)  << "id2";
  std::cout << std::setw(12) << "size";
  std::cout << std::setw(14)  << "sum";
  std::cout << std::setw(14)  << "max";
  std::cout << std::setw(14)  << "min";
  std::cout << std::setw(14)  << "avg";
  std::cout << std::setw(8)   << "null" << std::endl;
}

void HistogramsMap::printPair(detPair_t id) const
{
  detID_t id1, id2;
  std::tie(id1, id2) = szudzikUnPair(id);
  std::cout << std::setw(10) << id;
  std::cout << std::setw(6) << id1;
  std::cout << std::setw(6) << id2;
  std::cout << std::setw(12) << data.at(id).size();
  FloatType sum = std::accumulate(data.at(id).begin(), data.at(id).end(), 0.);
  std::cout << std::setw(14) << sum;
  std::cout << std::setw(14) << *std::max_element(data.at(id).begin(), data.at(id).end() );
  std::cout << std::setw(14) << *std::min_element(data.at(id).begin(), data.at(id).end() );
  std::cout << std::setw(14) << std::reduce(data.at(id).begin(), data.at(id).end()) / data.at(id).size();
  uint32_t null_elements = std::count_if(data.at(id).begin(), data.at(id).end(),
                                         [&](FloatType v) { return (v==0); } 
                                        ) ;
  std::cout << std::setw(8) << null_elements << std::endl;

}

void HistogramsMap::printStats() const
{
  std::cout << "Histograms statistics" << std::endl;
  float tot_sum=0;

  printHeader();
  
  for (data_type::const_iterator it = data.begin(); it != data.end(); ++it) {
    detPair_t id = it->first;
    printPair(id);
    tot_sum += std::accumulate(data.at(id).begin(), data.at(id).end(), 0.);
  }
  
  std::cout << "# detector pairs: " << data.size() << std::endl;
  std::cout << "tot_sum: " << tot_sum << std::endl;
}


// --- Operators

HistogramsMap& HistogramsMap::operator = (const HistogramsMap& h)
{
  n_total_lors = h.n_total_lors;
  not_null = h.not_null; 
  data = h.data;
  _keys = h._keys;
  _sizes = h._sizes;
  return *this;
}


HistogramsMap& HistogramsMap::operator += (const HistogramsMap& h)
{
  std::vector<detPair_t> keys1 = this->keys();
  std::vector<detPair_t> keys2 = h.keys();
  
  if ( keys1.size() != keys2.size() ) {
    std::cout << "Different number of keys: " << keys1.size() << " " << keys2.size();
    throw std::string("HistogramsMap operator +=");
    
  }
  
  std::sort( keys1.begin(), keys1.end() );
  std::sort( keys2.begin(), keys2.end() );
  
  for (uint16_t k=0; k<keys1.size(); k++) {
    if ( keys1[k] != keys2[k] )
      throw std::string("HistogramsMap operator += : different keys are present");
    detPair_t key = keys1[k];
    if ( this->data.at(key).size() != h.data.at(key).size() )
      throw std::string("HistogramsMap operator += : different size for key: " + std::to_string(key));
  }
  
  for (detPair_t k : keys1) {
    for (uint32_t i=0; i < data[k].size(); i++) {
      data[k][i] += h.data.at(k)[i];
    }
  }
  
  return *this;
}


HistogramsMap& HistogramsMap::operator -= (const HistogramsMap& h)
{
  std::vector<detPair_t> keys1 = this->keys();
  std::vector<detPair_t> keys2 = h.keys();
  
  if ( keys1.size() != keys2.size() ) {
    std::cout << "Different number of keys: " << keys1.size() << " " << keys2.size();
    throw std::string("HistogramsMap operator -=");
    
  }
  
  std::sort( keys1.begin(), keys1.end() );
  std::sort( keys2.begin(), keys2.end() );
  
  for (uint16_t k=0; k<keys1.size(); k++) {
    if ( keys1[k] != keys2[k] )
      throw std::string("HistogramsMap operator -= : different keys are present");
    detPair_t key = keys1[k];
    if ( this->data.at(key).size() != h.data.at(key).size() )
      throw std::string("HistogramsMap operator -= : different size for key: " + std::to_string(key));
  }
  
  for (detPair_t k : keys1) {
    for (uint32_t i=0; i < data[k].size(); i++) {
      data[k][i] -= h.data.at(k)[i];
      if ( data[k][i] < 0)
        data[k][i] = 0;
    }
  }
  
  return *this;
}

HistogramsMap& HistogramsMap::operator *= (const HistogramsMap & h)
{
  std::vector<detPair_t> keys1 = this->keys();
  std::vector<detPair_t> keys2 = h.keys();
  
  if ( keys1.size() != keys2.size() )
    throw std::string("HistogramsMap operator *= : different number of keys");
  
  std::sort( keys1.begin(), keys1.end() );
  std::sort( keys2.begin(), keys2.end() );
  
  for (uint16_t k=0; k<keys1.size(); k++) {
    if ( keys1[k] != keys2[k] )
      throw std::string("HistogramsMap operator *= : different keys are present");
    detPair_t key = keys1[k];
    if ( this->data.at(key).size() != h.data.at(key).size() )
      throw std::string("HistogramsMap operator *= : different size for key: " + std::to_string(key));
  }
  
  for (detPair_t k : keys1) {
    for (uint32_t i=0; i < data[k].size(); i++) {
      data[k][i] *= h.data.at(k)[i];
    }
  }
  
  return *this;
}

HistogramsMap& HistogramsMap::operator /= (const HistogramsMap & h)
{
  std::vector<detPair_t> keys1 = this->keys();
  std::vector<detPair_t> keys2 = h.keys();
  
  if ( keys1.size() != keys2.size() )
    throw std::string("HistogramsMap operator /= : different number of keys");
  
  std::sort( keys1.begin(), keys1.end() );
  std::sort( keys2.begin(), keys2.end() );
  
  for (uint16_t k=0; k<keys1.size(); k++) {
    if ( keys1[k] != keys2[k] )
      throw std::string("HistogramsMap operator /= : different keys are present");
    detPair_t key = keys1[k];
    if ( this->data.at(key).size() != h.data.at(key).size() )
      throw std::string("HistogramsMap operator /= : different size for key: " + std::to_string(key));
  }
  
  for (detPair_t k : keys1) {
    for (uint32_t i=0; i < data[k].size(); i++) {
      if (h.data.at(k)[i])
        data[k][i] /= h.data.at(k)[i];
    }
  }
  
  return *this;
}
