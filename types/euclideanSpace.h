/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef UTILS_EUCLIDEANSPACE_H_
#define UTILS_EUCLIDEANSPACE_H_

#include <iostream>
#include <cmath>
#include <cstdint>
#include <tuple>

#include "dataTypes.h"

class ThreeVector {
  SpaceType _x;
  SpaceType _y;
  SpaceType _z;
  // The components.
  
public:
  ThreeVector() : _x(0), _y(0), _z(0)
  { }
  
  ThreeVector(SpaceType x, SpaceType y, SpaceType z) : _x(x), _y(y), _z(z)
  { }
  
  // Copy constructor
  ThreeVector(const ThreeVector & v)
    : _x(v.x()), _y(v.y()), _z(v.z())
  { }
  
  ~ThreeVector()
  { }  
  
  SpaceType operator [] (uint16_t i) const
  {
    switch(i) {
      case 0:
        return _x;
      case 1:
        return _y;
      case 2:
        return _z;
      default:
        return 0.;
    }
  }
  
  SpaceType& operator [] (uint16_t i) 
  {
    switch(i) {
      case 0:
        return _x;
      case 1:
        return _y;
      case 2:
        return _z;
      default:
        throw std::string("accessing ThreeVector operator[] out of index");
    }
  }
  
  SpaceType x() const { return _x; }
  SpaceType y() const { return _y; }
  SpaceType z() const { return _z; }
  
  
  void setX(SpaceType x) { _x = x; }
  void setY(SpaceType y) { _y = y; }
  void setZ(SpaceType z) { _z = z; }
  
  void set(SpaceType x, SpaceType y, SpaceType z)
  {
    _x = x;
    _y = y;
    _z = z;
  }
  
  SpaceType r2() const { return _x*_x + _y*_y + _z*_z; }
  SpaceType r()  const { return std::sqrt( r2() ); }
  
  // Assignment.
  ThreeVector& operator = (const ThreeVector & p) 
  {
    _x = p._x;
    _y = p._y;
    _z = p._z;
    return *this;
  }
  
  bool operator == (const ThreeVector& v) const 
  {
    SpaceType precision = 1e-4;
    if (((v.x() - precision) < _x && (v.x() + precision) > _x) &&
        ((v.y() - precision) < _y && (v.y() + precision) > _y) &&
        ((v.z() - precision) < _z && (v.z() + precision) > _z) )
      return true;
    else
      return false;
  }
  
  bool operator != (const ThreeVector& v) const 
  {
    return (v.x()!=_x || v.y()!=_y || v.z()!=_z) ? true : false;
  }
  
  // Addition.
  ThreeVector& operator += (const ThreeVector & p)
  {
    _x += p.x();
    _y += p.y();
    _z += p.z();
    return *this;
  }
  
  // Subtraction.
  ThreeVector& operator -= (const ThreeVector & p)
  {
    _x -= p.x();
    _y -= p.y();
    _z -= p.z();
    return *this;
  }
  
  // Unary minus.
  ThreeVector operator - () const 
  {
    return ThreeVector(-_x, -_y, -_z);
  }
  
  // Scaling with real numbers.
  ThreeVector& operator *= (SpaceType a)
  {
    _x *= a;
    _y *= a;
    _z *= a;
    return *this;
  }
  
  // Division by (non-zero) real number.
  ThreeVector& operator /= (SpaceType a)
  {
    if (a == 0) 
      throw std::string ("Attempt to do vector /= 0 -- ");
    
    _x /= a;
    _y /= a;
    _z /= a;
    return *this;
  }
  
  ThreeVector unit() const
  {
    SpaceType tot = r2();
    ThreeVector p(x(),y(),z());
    return tot > 0.0 ? p *= (1.0/std::sqrt(tot)) : p;
  }
  // Vector parallel to this, but of length 1.
  
//   uint16_t minAxisValue() const
//   {
//     if (_x <= _y && _x <= _z)
//       return 0;
//     if (_y <= _x && _y <= _z)
//       return 1;
//     if (_z <= _x && _z <= _y)
//       return 2;
//   }
  
  SpaceType dot(const ThreeVector & p) const
  {
    return _x*p.x() + _y*p.y() + _z*p.z();
  }
  
  void invert() 
  {
    _x *= -1;
    _y *= -1;
    _z *= -1;
  }
  
  ThreeVector componentDot(const ThreeVector & p) const
  {
    return ThreeVector(_x*p.x(), _y*p.y(), _z*p.z() );
  }
  
  void round(SpaceType prec)
  {
    _x = std::round(_x/prec)*prec;
    _y = std::round(_y/prec)*prec;
    _z = std::round(_z/prec)*prec;
  }
  
  ThreeVector cross(const ThreeVector & p) const
  {
    return ThreeVector(_y*p.z()-p.y()*_z, _z*p.x()-p.z()*_x, _x*p.y()-p.x()*_y);
  }
  
  
  
  
  //   ThreeVector & rotateX(SpaceType);
  //   // Rotates the ThreeVector around the x-axis.
  //   
  //   ThreeVector & rotateY(SpaceType);
  //   // Rotates the ThreeVector around the y-axis.
  
  // Rotates the ThreeVector around the z-axis.
  void rotateZ(AngleType angle_rad)
  {
    AngleType cos_angle = std::cos(angle_rad);
    AngleType sin_angle = std::sin(angle_rad);
    
    SpaceType nx;
    nx = _x*cos_angle - _y*sin_angle;
    _y = _x*sin_angle + _y*cos_angle;
    _x = nx;
  }
  
  ThreeVector rotateZnotInPlace(float angle_rad) const
  {
    AngleType cos_angle = std::cos(angle_rad);
    AngleType sin_angle = std::sin(angle_rad);
    SpaceType nx, ny;
    nx = _x * cos_angle - _y * sin_angle;
    ny = _y * cos_angle + _x * sin_angle;
    
    return ThreeVector(nx, ny, _z);
  } /* rotateZ */
  
  //   ThreeVector & rotateZ (SpaceType phi1) 
//   {
//     SpaceType sinphi = std::sin(phi1);
//     SpaceType cosphi = std::cos(phi1);
//     SpaceType tx;
//     tx = _x * cosphi - _y * sinphi;
//     _y = _y * cosphi + _x * sinphi;
//     _x = tx;
//     return *this;
//   } /* rotateZ */

  friend std::ostream& operator<< (std::ostream & os, const ThreeVector & v) 
  {
    os << "(";
    os << std::setw(13) << v.x() << ",";
    os << std::setw(13) << v.y() << "," ;
    os << std::setw(13) << v.z();
    os << ")";
    return os;
  }

};

// Global Methods ThreeVector

static inline ThreeVector operator + (const ThreeVector & a, const ThreeVector & b) 
{
  return ThreeVector(a.x() + b.x(), a.y() + b.y(), a.z() + b.z());
}

static inline ThreeVector operator - (const ThreeVector & a, const ThreeVector & b) 
{
  return ThreeVector(a.x() - b.x(), a.y() - b.y(), a.z() - b.z());
}

static inline ThreeVector operator * (const ThreeVector & p, const SpaceType a) 
{
  return ThreeVector(a*p.x(), a*p.y(), a*p.z());
}

static inline ThreeVector operator / (const ThreeVector & p, const SpaceType a)
{
  if (a == 0)
    std::cerr << "warning, division by zero: ThreeVector operator / (const ThreeVector & p, const SpaceType a)";
  return ThreeVector(p.x()/a, p.y()/a, p.z()/a);
}

static inline ThreeVector operator * (const SpaceType a, const ThreeVector & p)
{
  return ThreeVector(a*p.x(), a*p.y(), a*p.z());
}

static inline SpaceType operator * (const ThreeVector & a, const ThreeVector & b)
{
  return a.dot(b);
}

// std::ostream & operator<< (std::ostream & os, const ThreeVector & v);





class ThreeMatrix {
  SpaceType matrix[3][3];
  
 public:
   ThreeMatrix()
   {
     for (uint8_t i=0; i<3; i++)
       for (uint8_t j=0; j<3; j++)
         matrix[i][j] = 0;
   }
  
  explicit ThreeMatrix(SpaceType diag)
  {
    for (uint8_t i=0; i<3; i++)
      for (uint8_t j=0; j<3; j++)
        matrix[i][j] = 0;
    
    for (uint8_t i=0; i<3; i++)
      matrix[i][i] = diag;
  }
  
  ThreeMatrix(const ThreeMatrix & mat)
  {
    for (uint8_t i=0; i<3; i++)
      for (uint8_t j=0; j<3; j++)
        matrix[i][j] = mat[i][j];
  }
  
  void setRow(const ThreeVector& vec, uint16_t row)
  {
    for (uint16_t i=0; i<3; i++)
      matrix[row][i] = vec[i];
  }
  
  void setCol(const ThreeVector& vec, uint16_t col)
  {
    for (uint16_t i=0; i<3; i++)
      matrix[i][col] = vec[i];
  }
  
  ThreeVector row(uint16_t index) const
  {
    return ThreeVector(matrix[index][0], matrix[index][1], matrix[index][2]);
  }
  
  ThreeVector col(uint16_t index) const
  {
    return ThreeVector(matrix[0][index], matrix[1][index], matrix[2][index]);
  }
  
  void setColumn(const ThreeVector& vec, uint16_t col)
  {
    for (uint16_t i=0; i<3; i++)
      matrix[i][col] = vec[i];
  }
  
  const SpaceType* operator [](const int &index) const  // overloading operator []
  {
    return  matrix[index];
  }

  SpaceType* operator [](const int &index)   // overloading operator []
  {
    return  matrix[index];
  }
  
  ThreeMatrix& operator=(const ThreeMatrix &mat)
  {
    for (uint8_t i=0; i<3; i++)
      for (uint8_t j=0; j<3; j++)
        matrix[i][j] = mat[i][j];
    
    return *this;
  }
  
  // Unary minus.
  ThreeMatrix operator - () const 
  {
    ThreeMatrix tmp;
    for (uint8_t i=0; i<3; i++)
      for (uint8_t j=0; j<3; j++)
        tmp[i][j] = matrix[i][j] * -1;
    
    return tmp;
  }
  
  
  
//   ThreeMatrix & operator+=(const ThreeMatrix &m)
//   {
//     CHK_DIM_2(num_row(),hm2.num_row(),num_col(),hm2.num_col(),+=);
//     SIMPLE_BOP(+=)
//     return (*this);
//   }

  friend std::ostream& operator<<(std::ostream &os, const ThreeMatrix &q)
  {
    os << "\n";
    /* Fixed format needs 3 extra characters for field, while scientific needs 7 */
    int width;
    if(os.flags() & std::ios::fixed)
      width = os.precision()+3;
    else
      width = os.precision()+7;
    
    for (uint8_t i=0; i<3; i++) {
      for (uint8_t j=0; j<3; j++) {
        os.width(width);
        os << q[i][j] << " ";
      }
      os << std::endl;
    }
    return os;
  }

};

// Global Methods ThreeMatrix
static inline ThreeMatrix operator+(const ThreeMatrix &mat1, const ThreeMatrix &mat2)
{
  ThreeMatrix tmp;
  for (uint8_t i=0; i<3; i++)
    for (uint8_t j=0; j<3; j++)
      tmp[i][j] = mat1[i][j] + mat2[i][j];
    
  return tmp;
}

static inline ThreeMatrix operator+(const ThreeMatrix &mat, SpaceType val)
{
  ThreeMatrix tmp;
  for (uint8_t i=0; i<3; i++)
    for (uint8_t j=0; j<3; j++)
      tmp[i][j] = mat[i][j] + val;
    
  return tmp;
}

static inline ThreeMatrix operator+(SpaceType val, const ThreeMatrix &mat)
{
  ThreeMatrix tmp;
  for (uint8_t i=0; i<3; i++)
    for (uint8_t j=0; j<3; j++)
      tmp[i][j] = mat[i][j] + val;
    
  return tmp;
}

static inline ThreeMatrix operator-(const ThreeMatrix &mat1, const ThreeMatrix &mat2)
{
  ThreeMatrix tmp;
  for (uint8_t i=0; i<3; i++)
    for (uint8_t j=0; j<3; j++)
      tmp[i][j] = mat1[i][j] - mat2[i][j];
    
  return tmp;
}

static inline ThreeMatrix operator-(const ThreeMatrix &mat, SpaceType val)
{
  ThreeMatrix tmp;
  for (uint8_t i=0; i<3; i++)
    for (uint8_t j=0; j<3; j++)
      tmp[i][j] = mat[i][j] - val;
    
  return tmp;
}

static inline ThreeMatrix operator-(SpaceType val, const ThreeMatrix &mat)
{
  ThreeMatrix tmp;
  for (uint8_t i=0; i<3; i++)
    for (uint8_t j=0; j<3; j++)
      tmp[i][j] = mat[i][j] - val;
    
  return tmp;
}

static inline ThreeMatrix operator*(const ThreeMatrix &mat, SpaceType t)
{
  ThreeMatrix tmp;
  for (uint8_t i=0; i<3; i++)
    for (uint8_t j=0; j<3; j++)
      tmp[i][j] = mat[i][j]*t;
    
  return tmp;
}

static inline ThreeMatrix operator*( SpaceType t, const ThreeMatrix &mat)
{
  ThreeMatrix tmp;
  for (uint8_t i=0; i<3; i++)
    for (uint8_t j=0; j<3; j++)
      tmp[i][j] = mat[i][j]*t;
    
  return tmp;
}

static inline ThreeMatrix operator/(const ThreeMatrix &mat, SpaceType t)
{
  ThreeMatrix tmp;
  for (uint8_t i=0; i<3; i++)
    for (uint8_t j=0; j<3; j++)
      tmp[i][j] = mat[i][j]/t;
    
  return tmp;
}

static inline ThreeMatrix operator*(const ThreeMatrix &mat1,const ThreeMatrix &mat2)
{
  ThreeMatrix tmp;
  for (uint8_t i=0; i<3; i++) {
    for (uint8_t j=0; j<3; j++) {
      for (uint8_t k=0; k<3; k++) {
        tmp[i][j] += mat1[i][k] * mat2[k][j];
      }
    }
  }
  
  return tmp;
}

static inline ThreeVector operator*(const ThreeMatrix &mat,const ThreeVector &vec)
{
  ThreeVector tmp(0,0,0);
  for (uint8_t i=0; i<3; i++)
    for (uint8_t j=0; j<3; j++)
      tmp[i] += mat[i][j]*vec[j];

  return tmp;
}


#endif // UTILS_EUCLIDEANSPACE_H_
