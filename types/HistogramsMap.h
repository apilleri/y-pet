/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TYPES_HYSTOGRAMSMAP_H_
#define TYPES_HYSTOGRAMSMAP_H_

#include <map>
#include <vector>
#include <tuple>
#include <utility>
#include <iomanip>
#include <algorithm>

#include "dataTypes.h"
#include "PlanarDetector.h"
#include "symmetriesT.h"

using SubsetType = std::vector< std::pair<lorIDX_t, FloatType> >;
using SubsestList = std::vector<SubsetType>;

using data_type = std::map< detPair_t, std::vector<FloatType> >;
/* The data is stored in an map, the key is the unique szudzik pair
 * associated to the TWO ids of the detector which contitute the detector pair.
 * For every key, a linear vector of float counts is present. The lenght of the
 * vector is equal to nCrystals_Detector1 * nCrystas_Detector2. Each index of the 
 * vector corresponds to a LOR. The index LOR is assumed to be:
 * lor_id = crystal_index1 * nCrystals_Detector2 + crystal_index2;
 * */


class HistogramsMap {

  lorIDX_t n_total_lors;
  
  uint32_t not_null;
  
  std::vector<detPair_t> _keys;
  std::vector<uint32_t> _sizes;
  
  
public:
  //data is public, easier to retrieve, 
  data_type data;
  
  HistogramsMap()
  : n_total_lors(0), not_null(0)
  { }
  
  HistogramsMap(const HistogramsMap& h)
  {
    n_total_lors = h.n_total_lors;
    not_null = h.not_null; 
    data = h.data;
    _keys = h._keys;
    _sizes = h._sizes;
  }
  
  lorIDX_t nLors() const 
  {
    return n_total_lors;
  }
  
  uint32_t notNull() const
  {
    return not_null;
  }
  
  const std::vector<detPair_t>& keys() const
  {
    return _keys;
  }
  
  // --- Creation methods
  
  void allocate(const DetectorsList& det_list, const CoincidenceSchema& cs,
                const std::string& type, FloatType value);
  
  void load(std::string filename);
  
  void fill(FloatType value);
  
  void clear();
  
  void disableDetector(detID_t detector);
  
  void save(std::string output_file) const;
  
  // --- Reconstruction related methods
  
  std::tuple<detPair_t, lorIDX_t> identifyLOR(lorIDX_t lor);
  
  void generateSubsets(SubsestList& subsets, uint16_t n_subsets, uint32_t seed);
  void generateDetSubsets(SubsestList& subsets);
  
  // --- Debug
  void dump2txt(std::string fname);
  
  void printHeader() const;
  void printPair(detPair_t id) const;
  void printStats() const ;
  
  // --- Operators
  
  HistogramsMap& operator = (const HistogramsMap& h);
  
  // Addition.of two histograms belonging to the same detector (keys must be equal)
  HistogramsMap& operator += (const HistogramsMap& h);
  HistogramsMap& operator -= (const HistogramsMap& h);
  HistogramsMap& operator *= (const HistogramsMap& h);
  HistogramsMap& operator /= (const HistogramsMap& h);
  
};


class CountsType {
  detID_t _det1;
  detCryID_t _cry1;
  detID_t _det2;
  detCryID_t _cry2;
  FloatType _counts;
  
public:
  CountsType(detID_t d1, detCryID_t c1, detID_t d2, detCryID_t c2, FloatType c)
    : _det1(d1), _cry1(c1), _det2(d2), _cry2(c2), _counts(c)
  { }
  
  detID_t det1() const { return _det1; }
  detCryID_t cry1() const { return _cry1; }
  detID_t det2() const { return _det2; }
  detCryID_t cry2() const { return _cry2; }
  FloatType counts() const { return _counts; }
  detPair_t pairID() const { return szudzikPair(_det1, _det2); }
  
};

using CountsVector = std::vector<CountsType>;

using SubsetsList = std::vector<CountsVector>;

#endif // TYPES_HYSTOGRAMSMAP_H_
