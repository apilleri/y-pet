/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TYPES_SYMMETRIES_H_
#define TYPES_SYMMETRIES_H_

#include "dataTypes.h"
#include "f_utils.h"


class SymmetryType {
  
  AngleType z_angle; // cylindrical symmetry
  SpaceType y_shift; // planar symmetry
  SpaceType z_shift; // cylindrical and planar symmetry
  
  bool x_reflect; // cylindrical symmetry
  bool y_reflect; // planar symmetry
  bool z_reflect; // cylindrical and planar symmetry
  
public:
  SymmetryType() : z_angle(0), y_shift(0), z_shift(0), x_reflect(false),
    y_reflect(false), z_reflect(false)
  { }
  
  SymmetryType(const SymmetryType & v)
  {
    z_angle = v.zAngle();
    y_shift = v.yShift();
    z_shift = v.zShift();
    x_reflect = v.xReflect(); 
    y_reflect = v.yReflect();
    z_reflect = v.zReflect(); 
  }
  
  void zAngle(AngleType v)  { z_angle = v; }
  void yShift(SpaceType v)  { y_shift = v; }
  void zShift(SpaceType v)  { z_shift = v; }
  void xReflect(bool v)     { x_reflect = v; }
  void yReflect(bool v)     { y_reflect = v; }
  void zReflect(bool v)     { z_reflect = v; }

  
  AngleType zAngle() const  { return z_angle; }
  SpaceType yShift() const  { return y_shift; }
  SpaceType zShift() const  { return z_shift; }
  bool xReflect() const     { return x_reflect; }
  bool yReflect() const     { return y_reflect; }
  bool zReflect() const     { return z_reflect; }
  
  bool operator == (const SymmetryType& v) const
  {
    return ( v.zAngle() == z_angle && 
             v.yShift() == y_shift &&
             v.zShift() == z_shift &&
             v.xReflect() == x_reflect &&
             v.yReflect() == y_reflect &&
             v.zReflect() == z_reflect
           ) ? true : false;
  }
  
  SymmetryType& operator = (const SymmetryType& v) 
  {
    z_angle = v.zAngle();
    y_shift = v.yShift();
    z_shift = v.zShift();
    x_reflect = v.xReflect(); 
    y_reflect = v.yReflect();
    z_reflect = v.zReflect(); 
    return *this;
  }
  
  friend std::ostream& operator<< (std::ostream & os, const SymmetryType & s)
  {
    os << std::setw(6) << s.zAngle();
    os << std::setw(7) << s.yShift();
    os << std::setw(7) << s.zShift();
    os << std::setw(3) << s.xReflect();
    os << std::setw(3) << s.yReflect();
    os << std::setw(3) << s.zReflect();
    return os;
  }
  
};

class CoincidencePair {
protected:
  //! first ID
  detID_t _id1;
  //! second ID
  detID_t _id2;
  
public:
  CoincidencePair(detID_t s1, detID_t s2) : _id1(s1), _id2(s2)
  { }
  
  detID_t id1() const { return _id1; }
  detID_t id2() const { return _id2; }
  detPair_t pairID() const { return szudzikPair(_id1, _id2); }
  
  bool operator == (const CoincidencePair& v) const
  {
    return ( (_id1 == v.id1() && _id2 == v.id2()) ||
              (_id1 == v.id2() && _id2 == v.id1())   ) ? true : false;
  }
};

class CoincidencePairSym : public CoincidencePair {
  // data members used for symmetries
  bool _fundamental;
  SymmetryType* _sym;
  
  // szudzik id of the fundamental pair  
  detPair_t sym_id;
  // id of the view related to the symmetry
  uint16_t unique_view;
  
public:
  CoincidencePairSym(detID_t s1, detID_t s2) : CoincidencePair(s1, s2),
    _fundamental(true), _sym(nullptr), sym_id(0), unique_view(0)
  { }
  
  void setFundamental() 
  {
    _fundamental = true;
    sym_id = szudzikPair(_id1, _id2);
    _sym = new SymmetryType(); //an epty symmetry is usefull

  }
  
  bool fundamental() const { return _fundamental; }
  
  detPair_t symID() const { return sym_id; }
  
  uint16_t uniqueView() const { return unique_view; }
  
  const SymmetryType& sym() const {return *_sym; }
  
  void setSymmetry(detPair_t s, const SymmetryType& ssym) 
  {
    _fundamental = false;
    sym_id = s;
    _sym = new SymmetryType(ssym);
  }
  
  void setUniqueView(uint16_t v) { unique_view = v; }
  
  friend std::ostream & operator<< (std::ostream & os, const CoincidencePairSym & c)
  {
    os << std::setw(7) << c.id1();
    os << std::setw(7) << c.id2();
    os << std::setw(7) << c.pairID();
    os << std::setw(3) << c.fundamental();
    os << std::setw(7) << c.symID();
    os << std::setw(5) << c.uniqueView();
    os << c.sym();
    return os;
  }
  
};

using CoincidenceSchema = std::map<detPair_t, CoincidencePairSym>;

using GroupedCS = std::vector< std::vector<CoincidencePairSym> >;

using ViewsList = std::vector<SymmetryType>;



#endif // TYPES_SYMMETRIES_H_
