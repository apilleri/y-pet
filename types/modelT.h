/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TYPES_MODELT_H_
#define TYPES_MODELT_H_

#include <fstream>
#include <cstdint>
#include <vector>
#include <algorithm>
#include <iostream>


#include "dataTypes.h"


// --- MODEL TYPES
/** \class MatrixEntry
 *  \ingroup matricesGroup
 *  \brief Representation of one entry of a generic sparse matrix
 * 
 *  It represents an element of a generic sparse matrix stored in a row-major 
 *  order. It contains the index of the column and the value of that particular
 *  position of the matrix. 
 *  In the case of a Geometric Matrix the column index represents a cooordinate
 *  of a voxel. It can be decomposed into the three FOV components using the 
 *  appropriate methods FieldOfView::getX(), FieldOfView::getY(), FieldOfView::getZ().
 *  In the case of a Detector Matrix the column index represents a LOF index.
 *  TODO definire come calcolare LOF index.
 */
class MatrixEntry {
  
public:
  uint32_t col;
  FloatType v;
  

  MatrixEntry() : col(0), v(0)
  { }
  
  MatrixEntry(uint32_t column, FloatType val) : col(column), v(val)
  { }
  
//   //! Get the column value
//   uint32_t column() const { return _column; }
//   //! Get the entry value
//   FloatType    v() const { return _value; }
//   
//   //! Set the column value
//   void column(uint32_t col) {_column = col; }
//   //! Set the entry value
//   void v(FloatType val) { _value = val; }
};


class SparseModel {
  std::vector<uint64_t>  _cum_counts;
  MatrixEntry *_model;
  
  uint32_t _rows;
  uint32_t _cols;
  
//   bool counts_loaded;
  
public:
  
  SparseModel(uint32_t rows, uint32_t cols)
    :  _model(nullptr), _rows(rows), _cols(cols) //, counts_loaded(false)
  {
    _cum_counts.resize(rows+1, 0);
  }
  
  ~SparseModel()
  {
    delete[] _model;
    _cum_counts.resize(0);
  }
  
  uint32_t rows() const
  {
    return _rows;
  }
  
  uint32_t cols() const
  {
    return _cols;
  }
  
  uint32_t rowElementsN(uint32_t row) const
  {
    return static_cast<uint32_t>(_cum_counts[row+1] - _cum_counts[row]);
  }
  
  void set(std::string fname)
  {
    std::ifstream file;
    file.open(fname + ".counts");
    if ( !file.is_open() )
      throw std::string( "SparseModel::set -> unable to open file: " + fname + ".counts");
    std::vector<uint32_t> counts(_rows);
    file.read(reinterpret_cast<char *>(counts.data()), _rows*sizeof(uint32_t));
    if (!file) // || file.gcount() != _rows*sizeof(uint16_t))
      throw std::string("SparseModel::set -> " + fname + " counts only " + std::to_string(file.gcount()) + " read.");
    file.close();
    _cum_counts[0] = 0;
    for (uint32_t i=0; i<_rows; i++)
      _cum_counts[i+1] = _cum_counts[i] + (uint32_t)counts[i];
    _model = new MatrixEntry[totCounts()];
    file.open(fname);
    if ( !file.is_open() )
      throw std::string( "SparseModel::set -> unable to open file: " + fname);
    file.read(reinterpret_cast<char *>(_model), totCounts()*sizeof(MatrixEntry));
    if (!file) // || file.gcount() != totCounts()*sizeof(MatrixEntry))
      throw std::string("SparseModel::set -> " + fname + " only " + std::to_string(file.gcount()) + " read.");
    file.close();
    
  }

  MatrixEntry* rowPointer(uint32_t row) const
  {
//     return _model + static_cast<uint32_t>(_cum_counts[row]);
    return _model + _cum_counts[row];
  }
  
  
  std::vector<FloatType> operator * (const std::vector<FloatType>& vec)
  {
    if (this->cols() != vec.size())
      throw std::string("Unable to multiply SparseModel x vec: sizes don't match.");
    std::vector<FloatType> results(this->rows(), 0.);
    
    #pragma omp parallel for
    for(uint32_t cur_LOF=0; cur_LOF!=rows(); ++cur_LOF) {
      MatrixEntry *start_pointer, *stop_pointer;
      start_pointer = this->rowPointer(cur_LOF);
      stop_pointer = start_pointer + this->rowElementsN(cur_LOF);
      
      while(start_pointer < stop_pointer) {
        uint32_t column = (*start_pointer).col;
        results[cur_LOF] += (*start_pointer).v * vec[column];
        start_pointer++;
      }
    }
    
    return results;
  }

  friend std::vector<MatrixEntry> operator*(const std::vector<FloatType>& v,
                                            const SparseModel& m)
  {
    // the matrix m is transposed, so we have a product between the dense vector
    // v and the rows of the matrix m
    std::vector<MatrixEntry> s_row;
      
    for (uint32_t row=0; row<m.rows(); row++) {
      FloatType vx_value = 0;
      MatrixEntry* pointer = m.rowPointer(row);
      uint32_t n_el = m.rowElementsN(row);
      for (uint32_t i=0; i<n_el; i++) 
        vx_value += pointer[i].v * v[ pointer[i].col ];
      
      if (vx_value)
        s_row.push_back( MatrixEntry(row,vx_value) );
    }
  
    return s_row;
  }
  
  friend std::vector<MatrixEntry>
  sparseProductTr(const MatrixEntry* vec, uint32_t n_v1, const SparseModel& m)
  {    
    // the matrix m is transposed, so we have a product between the dense vector
    // v and the rows of the matrix m
    std::vector<MatrixEntry> s_row;
    
    
    for (uint32_t row=0; row<m.rows(); row++) {
      
      MatrixEntry* pointer = m.rowPointer(row);
      uint32_t n_v2 = m.rowElementsN(row);
      
      uint32_t i=0, j=0;
      FloatType vx_value = 0;
      
      while(i<n_v1 && j<n_v2) {
       
        if (vec[i].col < pointer[j].col)
          i++;
        else if (vec[i].col > pointer[j].col)
          j++;
        else {
          //must be equal
          vx_value += vec[i].v * pointer[j].v;
          i++;
          j++;
        }
      }
      
      if (vx_value)
        s_row.push_back( MatrixEntry(row,vx_value) );
      
    }
    
    return s_row;
  }
  
  
private:
  uint64_t totCounts()
  {
    return _cum_counts.back();
  }
  
};





#endif // TYPES_MODELT_H_
