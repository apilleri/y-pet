/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TYPES_LMTYPE_H_
#define TYPES_LMTYPE_H_

#include <cstdint>

class LmType {
  
  float detector1;
  float detector2;
  float crystal1;
  float crystal2;
  float count;
  
public:
  LmType()
  : detector1(0), detector2(0), crystal1(0), crystal2(0), count(0)
  { }
  
  LmType (uint32_t d1, uint32_t d2, uint32_t c1, uint32_t c2, float c) 
  : detector1(d1), detector2(d2), crystal1(c1), crystal2(c2), count(c)
  { }
  
  uint16_t D1() const
  {
    return static_cast<uint16_t>(detector1);
  }
  
  uint16_t D2() const
  {
    return static_cast<uint16_t>(detector2);
  }
  
  uint16_t C1() const
  {
    return static_cast<uint16_t>(crystal1);;
  }
  
  uint16_t C2() const
  {
    return static_cast<uint16_t>(crystal2);;
  }
  
  float Count() const
  {
    return count;
  }
  
};

#endif //TYPES_LMTYPE_H_
