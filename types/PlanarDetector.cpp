#include "PlanarDetector.h"
#include "numericalIntegration.h"

#include <tuple>
#include <cmath>

bool detectortIntersections(const PlanarDetectorType& det,
                            uint16_t cry,
                            const ThreeVector& ray_start,
                            const ThreeVector& ray_dir,
                            SpaceType& d1, SpaceType& d2, SpaceType& d3) 
{
  #define  EPS 0.001
  
  std::vector<SpaceType> inside_points;
  
  
  ThreeMatrix transposed;
  transposed.setRow(det.rotationMatrix().col(0), 0);
  transposed.setRow(det.rotationMatrix().col(1), 1);
  transposed.setRow(det.rotationMatrix().col(2), 2);
  
  ThreeVector c_hlength(det.xLen(cry)/2, det.yLen(cry)/2, det.zLen(cry)/2);
  for (uint16_t i=0; i<3; i++) {
    ThreeVector shift;
    shift[i] = c_hlength[i];
    ThreeVector rot_shift = det.rotationMatrix()*shift;
    ThreeVector orientation = det.rotationMatrix().col(i);
    
    ThreeVector surface_point = det.rCryCenter(cry) + rot_shift;
    SpaceType d = ((surface_point - ray_start)*orientation) / (ray_dir*orientation);
    ThreeVector point = ray_start + d*ray_dir;
    ThreeVector ref_distance = transposed*(point-det.rCryCenter(cry));
    if ( (std::abs(ref_distance[0])-c_hlength[0]<=EPS) && (std::abs(ref_distance[1])-c_hlength[1]<=EPS) && (std::abs(ref_distance[2])-c_hlength[2]<=EPS) ) {
      inside_points.push_back(d);
    } 
    
    surface_point = det.rCryCenter(cry) - rot_shift;
    d = ((surface_point - ray_start)*orientation) / (ray_dir*orientation);
    point = ray_start + d*ray_dir;
    ref_distance = transposed*(point-det.rCryCenter(cry));
    //     std::cout << "ref_distance " << ref_distance << std::endl;
    if ( (std::abs(ref_distance[0])-c_hlength[0]<=EPS) && (std::abs(ref_distance[1])-c_hlength[1]<=EPS) && (std::abs(ref_distance[2])-c_hlength[2]<=EPS) ) {
      inside_points.push_back(d);
    }
    
  }
  
  
  if (inside_points[0] > inside_points[1])
    std::swap(inside_points[0], inside_points[1]);
  
  SpaceType d = ((det.detectorSurfaceCenter() - ray_start)*det.orientation() ) / (ray_dir*det.orientation());
  ThreeVector point = ray_start + d*ray_dir;
  ThreeVector ref_distance = transposed *(point-det.detectorSurfaceCenter());
  if ( (std::abs(ref_distance.y()) < det.size().y()/2) && (std::abs(ref_distance.z()) < det.size().z()/2) ) 
    d1 = d;
  else
    d1 = inside_points[0];
  
  d2 = inside_points[0];
  d3 = inside_points[1];
  
  if (inside_points.size() < 2)
    return false;
  else
    return true;
}

bool detectortInteraction(const PlanarDetectorType& det,
                          const ThreeVector& ray_start,
                          const ThreeVector& ray_dir,
                          SpaceType& d)
{
  #define  EPS 0.001
   
  // SpaceType distance = ((plane_point - ray_start)*plane_orientation) / (ray_dir*plane_orientation);
  
  
  ThreeMatrix transposed;
  transposed.setRow(det.rotationMatrix().col(0), 0);
  transposed.setRow(det.rotationMatrix().col(1), 1);
  transposed.setRow(det.rotationMatrix().col(2), 2);
  
  //testing the front plane, most likely to be hit
  
  const ThreeVector& front_point = det.detectorSurfaceCenter();
  ThreeVector orientation = det.rotationMatrix().col(0);
  FloatType p1 = ray_start*orientation;
  FloatType p2 = ray_dir*orientation;
  d = (front_point*orientation - p1 )/ p2;
  
  ThreeVector point = ray_start + d*ray_dir;
  ThreeVector ref_distance = transposed*(point-front_point);
  
  if ( (std::abs(ref_distance[0])-det.size()[0]/2<=EPS) && (std::abs(ref_distance[1])-det.size()[1]/2<=EPS) && (std::abs(ref_distance[2])-det.size()[2]/2<=EPS) ) 
    return true;
  else {
    
    ThreeVector plane_point = front_point + det.rotationMatrix()*ThreeVector(det.size()[0], 0, 0);
    SpaceType d_back = (plane_point*orientation - p1 )/ p2;
    
    // checking y planes
    orientation = det.rotationMatrix().col(1);
    p1 = ray_start*orientation;
    p2 = ray_dir*orientation;
    
    plane_point = det.detectorSurfaceCenter() + det.rotationMatrix()*ThreeVector(0,det.size()[1]/2,0);
    d = (plane_point*orientation - p1 )/ p2;
    if (d <= d_back)
      return true;
    
    plane_point = det.detectorSurfaceCenter() + det.rotationMatrix()*ThreeVector(0,-det.size()[1]/2,0);
    d = (plane_point*orientation - p1 )/ p2;
    if (d <= d_back)
      return true;
      
    // checking z planes
    orientation = det.rotationMatrix().col(2);
    p1 = ray_start*orientation;
    p2 = ray_dir*orientation;
    
    plane_point = det.detectorSurfaceCenter() + det.rotationMatrix()*ThreeVector(0,0,det.size()[2]/2);
    d = (plane_point*orientation - p1 )/ p2;
    if (d <= d_back)
      return true;
    
    plane_point = det.detectorSurfaceCenter() + det.rotationMatrix()*ThreeVector(0,0,-det.size()[2]/2);
    d = (plane_point*orientation - p1 )/ p2;
    if (d <= d_back)
      return true;
    
    return false;
  }
}


/* float cosA=fabs(RayDirection.ScalarProd(NormalA));
	float cosB=fabs(RayDirection.ScalarProd(NormalB));
	A->GetIntegrationPointsOfCrystal(IdA,IntPoints1,FOVCenter,cosA);
	B->GetIntegrationPointsOfCrystal(IdB,IntPoints2,FOVCenter,cosB); */

std::tuple<std::vector<ThreeVector>, std::vector<double>>
PlanarDetectorType::computeIntegrationPoints(uint16_t cry, const ThreeVector &ray_dir, uint16_t nyz, uint16_t nx) const
{
  // ThreeVector center = rCryCenter(cry);
  ThreeVector center = rCrySurfaceCenter(cry);
  // computing the points in the surface of the crystal
  std::vector<IntegrationPoint> int_points = computeGLPoints(nyz);
  SpaceType cryYHalf = yLen(cry) / 2;
  SpaceType cryZHalf = zLen(cry) / 2;

  std::vector<ThreeVector> points;
  std::vector<double> weights;

  for (uint16_t i=0; i<int_points.size(); i++) {
    ThreeVector tmp(0, cryYHalf * int_points[i].getDelta1(),
                    cryZHalf * int_points[i].getDelta2());
    points.push_back( center + tmp.rotateZnotInPlace(zAngle()) );
    weights.push_back(int_points[i].getWeight()*cryYHalf*cryZHalf);
  }

  // evaluate the depth integration points
  if (nx > 1) {
    std::vector<ThreeVector> points_n;
    std::vector<double> weights_n;

    // float cosA = fabs(RayDirection.ScalarProd(NormalA));
    // float cosB = fabs(RayDirection.ScalarProd(NormalB));
    // A->GetIntegrationPointsOfCrystal(IdA, IntPoints1, FOVCenter, cosA);
    // B->GetIntegrationPointsOfCrystal(IdB, IntPoints2, FOVCenter, cosB);

    ThreeVector Normal;
    std::vector<double> p_depth;
    std::vector<double> w_depth;
    // get the integration points
    double att_len = 0.0833; //TODO COMPUTE
    // std::cout << "inclination: ";
    // std::cout << std::fabs(std::cos(zAngle())) << " ";
    // std::cout << std::fabs(ray_dir.componentDot(orientation()).r() );
    // std::cout << std::endl;
    // double inclination = std::fabs(
    // ray_dir.componentDot(orientation()).r() );
    // double inclination = std::fabs(std::cos(zAngle()));
    double inclination = std::fabs(ray_dir.componentDot(orientation()).r());
    // inclination = 1;
    std::tie(p_depth, w_depth) =
    computeExponentialWeights(att_len / inclination, xLen(cry), nx);

    // apply depth to the 2d points
    for (uint16_t i = 0; i < p_depth.size(); i++) {
      for (uint16_t p_plane=0; p_plane<points.size(); p_plane++) {
        ThreeVector tmp(0,0,0);
        for (int k = 0; k < 3; k++)
          tmp[k] = points[p_plane][k] + orientation()[k] * (p_depth[i]);
        points_n.push_back(tmp);
        weights_n.push_back( weights[p_plane]*w_depth[i]);
      }
    }

    points = points_n;
    weights = weights_n;
  }

  return std::make_tuple(points, weights);
}

/*
void
DepthMultirayProjector::computeIntegrationPoints(
		CrystalIdType Id,
		IntegrationPointsAndWeightsVector &IntPositionAndWeights,
		PhysicalPoint &FOVCenter,
		float Inclination)
{
	// PhysicalPoint PixelCenter=thiifndefs->GetPixelCoordinatesById(Id);
	// getting the offset of the integration points
	// IntegrationDeltaAndWeightsVector Points;
	// this->RetrieveIntegrationPoints(Points);
	// LengthType HalfPixelSizeAlongPrincipalAxis=this->PhysicalPixelDimensionAlongPrincipalAxis/2.0;
	// LengthType HalfPixelSizeAlongSecondaryAxis=this->PhysicalPixelDimensionAlongSecondaryAxis/2.0;

	IntegrationDeltaAndWeightsVector::iterator it;
	IntPositionAndWeights.clear();
	// Evaluate the absolute coordinate of all the integration points
	for (it=Points.begin();it!=Points.end();++it)
	{
		// evaluating integration points
		PhysicalPoint tmp;
		tmp=EvaluateSegmentExtrema(PixelCenter,this->PrincipalAxisDirection,
				it->GetDelta1()*HalfPixelSizeAlongPrincipalAxis);
		tmp=EvaluateSegmentExtrema(tmp,this->SecondaryAxisDirection,
				it->GetDelta2()*HalfPixelSizeAlongSecondaryAxis);
		// adding integration points to the list
		IntegrationPointsAndWeights CurrentPoint(tmp,it->GetWeigth()*
												 HalfPixelSizeAlongPrincipalAxis*
												 HalfPixelSizeAlongSecondaryAxis );
		IntPositionAndWeights.push_back(CurrentPoint);
	}
	// evaluate the depth integration points
	if (this->DepthIntegrationPointsNumber!=0)
	{
		PhysicalDirection Normal;
		std::vector<double> Points1;
		std::vector<double> Weigths1;
		// get the integration points
		GetExponentialWeights(
						Points1,
						Weigths1,
						this->CrystalAttenuationLenght/Inclination,
						this->CrystalDepth,
						this->DepthIntegrationPointsNumber);
		this->GetDetectorNormal( Normal,FOVCenter);
		std::vector<double>::iterator itW=Weigths1.begin();
		IntegrationPointsAndWeightsVector out;
		// apply depth to the 2d points
		for (std::vector<double>::iterator itP=Points1.begin();itP!=Points1.end();++itP)
		{
			for (IntegrationPointsAndWeightsVector::iterator it=IntPositionAndWeights.begin();it!=IntPositionAndWeights.end();++it)
			{
				PhysicalPoint tmp=EvaluateSegmentExtrema(it->first,
						Normal,
						-*itP);
				IntegrationPointsAndWeights CurrentPoint(tmp,it->second*(*itW));
				out.push_back(CurrentPoint);
			}
			++itW;
		}
		IntPositionAndWeights=out;
	}
	//std::cout << "Detector " << this->GetDetectorID() <<std::endl;
	//std::cout << "Crystal "  << Id <<std::endl;
	//for (IntegrationPointsAndWeightsVector::iterator it=IntPositionAndWeights.begin();it!=IntPositionAndWeights.end();++it)
	//{
	//	it->first.Dump();
	// std::cout << it->second <<std::endl;
	//}
}

*/