#include "numericalIntegration.h"
#include "FFTpack.h"

std::vector<IntegrationPoint> GLtransformToIntegrationWeigth(
    const std::vector<double> &x, const std::vector<double> &w)
{
  std::vector<IntegrationPoint> output;
  if (x.size() != w.size()) {
    std::cout << "FATAL ERROR: in integration points " << std::endl;
    throw -1;
  }

  // generating all pairs of +-x,w
  std::vector<IntegrationWeigth> VectorOfIntWeights;
  for (uint16_t i = 0; i < x.size(); i++) {
    VectorOfIntWeights.push_back(IntegrationWeigth(x[i], w[i]));
    VectorOfIntWeights.push_back(IntegrationWeigth(-x[i], w[i]));
  }

  // build the structure containing the IntegrationPoint
  for (uint16_t i = 0; i < VectorOfIntWeights.size(); i++)
    for (uint16_t j = 0; j < VectorOfIntWeights.size(); j++)
      output.push_back(
          IntegrationPoint(VectorOfIntWeights[i], VectorOfIntWeights[j]));

  return output;
}


std::vector<IntegrationPoint> computeGLPoints(uint16_t points_n)
{
  std::vector<IntegrationPoint> computed_points;
  std::vector<double> x, w;

  switch (points_n) {
    case 1: {
      IntegrationWeigth w1(0, 1);
      IntegrationPoint p(w1, w1);
      computed_points.push_back(p);
    } break;

    case 2:
      x.resize(1);
      w.resize(1);
      // this coefficients were taken from the Sascha's thesis
      x[0] = (1.0 / 3.0) * sqrt(3);
      w[0] = 1;

      computed_points = GLtransformToIntegrationWeigth(x, w);
      // check if the number of points fits the expectations
      if (computed_points.size() != 4) {
        std::cout << "FATAL ERROR: Number of integration points "
                  << computed_points.size() << " is different from 4"
                  << std::endl;
        throw -1;
      }
      break;

    case 4:
      x.resize(2);
      w.resize(2);

      // this coefficients were taken from the Sascha's thesis
      x[0] = (1.0 / 35.0) * sqrt(525 - 70 * sqrt(30));
      w[0] = (1.0 / 36.0) * (18 + sqrt(30));
      x[1] = (1.0 / 35.0) * sqrt(525 + 70 * sqrt(30));
      w[1] = (1.0 / 36.0) * (18 - sqrt(30));

      computed_points = GLtransformToIntegrationWeigth(x, w);

      // check if the number of points fits the expectations
      if (computed_points.size() != 16) {
        std::cout << "FATAL ERROR: Number of integration points "
                  << computed_points.size() << " is different from 16"
                  << std::endl;
        throw -1;
      }

      break;

    case 8:
      x.resize(4);
      w.resize(4);

      // this coefficients were taken from the Sascha's thesis
      x[0] = 0.960289856497536231683560868569;
      w[0] = 0.101228536290376259152531354310;
      x[1] = 0.796666477413626739591553936476;
      w[1] = 0.222381034453374470544355994426;
      x[2] = 0.525532409916328985817739049189;
      w[2] = 0.313706645877887287337962201987;
      x[3] = 0.183434642495649804939476142360;
      w[3] = 0.362683783378361982965150449277;

      computed_points = GLtransformToIntegrationWeigth(x, w);

      // check if the number of points fits the expectations
      if (computed_points.size() != 64) {
        std::cout << "FATAL ERROR: Number of integration points "
                  << computed_points.size() << " is different from 64"
                  << std::endl;
        throw -1;
      }
      break;

    case 16:
      x.resize(8);
      w.resize(8);

      // this coefficients were taken from the Sascha's thesis
      x[0] = 0.989400934991649932596154173450;
      w[0] = 0.0271524594117540948517805724560;
      x[1] = 0.944575023073232576077988415535;
      w[1] = 0.0622535239386478928628438369944;
      x[2] = 0.865631202387831743880467897712;
      w[2] = 0.0951585116824927848099251076022;
      x[3] = 0.755404408355003033895101194847;
      w[3] = 0.124628971255533872052476282192;
      x[4] = 0.617876244402643748446671764049;
      w[4] = 0.149595988816576732081501730547;
      x[5] = 0.458016777657227386342419442984;
      w[5] = 0.169156519395002538189312079030;
      x[6] = 0.281603550779258913230460501460;
      w[6] = 0.182603415044923588866763667969;
      x[7] = 0.0950125098376374401853193354250;
      w[7] = 0.189450610455068496285396723208;

      computed_points = GLtransformToIntegrationWeigth(x, w);

      // check if the number of points fits the expectations
      if (computed_points.size() != 256) {
        std::cout << "FATAL ERROR: Number of integration points "
                  << computed_points.size() << " is different from 256"
                  << std::endl;
        throw -1;
      }
      break;

    default:
      throw std::string("Error: number of integration points not supported " +
                        std::to_string(points_n));
  }

  return computed_points;
}



std::tuple<std::vector<double>, std::vector<double>> 
computeExponentialWeights(FloatType att_len, FloatType cry_len, uint16_t points_n)
{
  std::vector<double> points;
  std::vector<double> weights;
    
  switch (points_n) {
    case 2:
    {
      points.resize(2, 0);
      weights.resize(2, 0);
      double mid_point = -std::log(0.5 * (1.0 + exp(-1.0 * att_len * cry_len))) / att_len;
      points[0] = mid_point * 0.5;
      points[1] = mid_point + 0.5 * (cry_len - mid_point);
      weights[0] = mid_point * weight_function(points[0], att_len, cry_len);
      weights[1] = (cry_len - mid_point) * weight_function(points[1], att_len, cry_len);
      break;
    }
    default:
    {
      // number of points >2
      points.resize(points_n, 0);
      weights.resize(points_n, 0);
      const double left = (double)0.0;
      const double right = cry_len; /* length */
      int n_rcoefs = (int)points_n;
      const int n = 1023;
      double z[n], q[n], x[n], dx[n], f[n], ww[n], a0[n], b0[n];
      fejer2_abscissae(n, z, q);
      /* map [-1,1] to [left,right] */
      for (int k = 0; k < n; k++) {
        const double bpa = right + left;
        const double bma = right - left;
        x[k] = 0.5 * (z[k] * bma + bpa);
        dx[k] = 0.5 * bma;
      }
      for (int k = 0; k < n; k++) {
        f[k] = exp(-x[k] * att_len);
        ww[k] = f[k] * q[k] * dx[k];
      }
      /* calculate the recursion coefficients a and b from the quadrature scheme
       */
      lanczos_tridiagonalize(n, x, ww, a0, b0);
      /* calculate abscissa and weights */
      gaussqr_from_rcoeffs(n_rcoefs, a0, b0, x, ww);
      /* abscissa and weights */
      for (int k = 0; k < std::min(n, n_rcoefs); k++) {
        points[k] = x[k];
        weights[k] = ww[k] * att_len;
      }
      break;
    }
  }

  return std::tie(points, weights);
}

double weight_function(double x, double att_len, double cry_len) {
  if ((x < (double)0.0) || (x > cry_len)) {
    std::cout
        << "error in DepthIntegration::function_value() -> x out of range: "
        << x << std::endl;
    throw -1;
  }
  return (att_len * std::exp((double)-1.0 * att_len * x));
}

/*
        [x,w] = gaussqr_from_rcoeffs(a,b)

        Calculates the sorted eigenvlues and normalized eigenvectors
        of a (modified) symmetric tri-diagonal matrix, via the implicit-
        shift QL algorithm. The 'modified' comes from the fact that the
        off-diagonals are mapped to their square-roots before decomposition.

        Then, the eigendecomposition is used to compute the gaussian quadrature
        rule for the given recursion coefficients. The first element of 'b' is
        used to normalize the quadrature scheme, so set the first element
        of 'b' to be equal to the desired $\int w(x) \, dx$ on input.

        The method is based on the SLATEC subroutine IMTQLV.

        n := length of a, b, x, and w

        On input:
           a := a vector representing the matrix diagonal
           b := a vector whose elements are the square of the matrix
                        sub- & super-diagonal; it has the same length as a,
                        and the first element is not part of the matrix

        On output:
            x := gaussian quadrature abscissae
            w := gaussian quadrature weights
*/
void gaussqr_from_rcoeffs(const int n, const double *a, const double *b, double *x,
                          double *w) {
  if (n < 3 || a == 0 || b == 0 || x == 0 || w == 0) {
    fprintf(stderr,
            "\nerror in GaussExpPointsWeights::gaussqr_from_rcoeffs() -> non "
            "admissable parameters\n");
    exit(EXIT_FAILURE);
  }

  std::vector<double> d;
  Assign(d, n, a);
  std::vector<double> e(n, (double)0.0);

  // note that this routine will work perfectly well
  // for arbitrary off-diagonals if we omit the sqrt
  for (int i = 0; i < n - 1; i++) {
    e[i] = std::sqrt(b[i + 1]);
  }

  const int TQLI_MAX_ITER = 32;

  double bb, c, dd, f, g, p, r, s, cs;

  std::vector<std::vector<double>> z;
  AssignIdentity(z, n);

  for (int l = 0; l < n; l++) {
    int m = 0;
    int iter = 0;
    do {
      for (m = l; m < n - 1; m++) {
        dd = std::fabs(d[m]) + std::fabs(d[m + 1]);
        if (std::fabs(e[m]) + dd == dd) break;
      }
      if (m != l) {
        if (iter++ >= TQLI_MAX_ITER) {
          fprintf(stderr,
                  "\nerror in GaussExpPointsWeights::gaussqr_from_rcoeffs()\n");
          exit(EXIT_FAILURE);
        }
        g = (d[l + 1] - d[l]) / ((double)2.0 * e[l]);
        r = std::sqrt((g * g) + (double)1.0);

        if ((g >= ((double)0.0)) && (r >= ((double)0.0))) {
          cs = r;
        } else {
          if ((g >= ((double)0.0)) && (r < ((double)0.0))) {
            cs = r * ((double)-1.0);
          } else {
            if ((g < ((double)0.0)) && (r < ((double)0.0))) {
              cs = r;
            } else {
              cs = r * ((double)-1.0);
            }
          }
        }

        /* g = d[m] - d[l] + e[l] / ( g + copysign(r,g) ); */
        g = d[m] - d[l] + e[l] / (g + cs);
        s = c = (double)1.0;
        p = (double)0.0;
        for (int i = m - 1; i >= l; i--) {
          f = s * e[i];
          bb = c * e[i];
          if (std::fabs(f) >= std::fabs(g)) {
            c = g / f;
            r = std::sqrt((c * c) + (double)1.0);
            e[i + 1] = f * r;
            s = (double)1.0 / r;
            c *= s;
          } else {
            s = f / g;
            r = std::sqrt((s * s) + (double)1.0);
            e[i + 1] = g * r;
            c = (double)1.0 / r;
            s *= c;
          }
          g = d[i + 1] - p;
          r = (d[i] - g) * s + (double)2.0 * c * bb;
          p = s * r;
          d[i + 1] = g + p;
          g = c * r - bb;
          for (int k = 0; k < n; k++) {
            f = z[k][i + 1];
            z[k][i + 1] = s * z[k][i] + c * f;
            z[k][i] = c * z[k][i] - s * f;
          }
        }
        d[l] = d[l] - p;
        e[l] = g;
        e[m] = (double)0.0;
      }
    } while (m != l);
  }

  heapsort_eig(d, z); /* sort the eigenvalues */

  for (int i = 0; i < n; i++) {
    x[i] = d[i];  // the abscissae are just the eigenvalues
    w[i] =
        b[0] * (z[0][i] * z[0][i]);  // first element of each column eigenvector
  }
}

// given a vector of eigenvalues and a matrix of
// corresponding eigenvector columns, sort the
// eigenvalues (and vectors) in ascending order
// using the heapsort algorithm

// ASSUMES: length(v) == rows(m) == columns(m)
//          and that this size is > 0,
//          and the first index of the matrix
//          denotes the row
void heapsort_eig(std::vector<double> &v, std::vector<std::vector<double>> &m) {
  const int N = v.size();

  int n = N;
  int i = n / 2;
  while (true) {
    double t;
    std::vector<double> tv(N, (double)0.0);

    if (i > 0) {
      i--;
      t = v[i];
      for (int k = 0; k < N; k++) {
        tv[k] = m[k][i];
      }
    } else {
      n--;
      if (n == 0) {
        return;
      }
      t = v[n];
      for (int k = 0; k < N; k++) {
        tv[k] = m[k][n];
      }
      v[n] = v[0];
      for (int k = 0; k < N; k++) {
        m[k][n] = m[k][0];
      }
    }

    int j = i;
    int w = i * 2 + 1;

    while (w < n) {
      if (w + 1 < n && v[w + 1] > v[w]) {
        w++;
      }
      if (v[w] > t) {
        v[j] = v[w];
        for (int k = 0; k < N; k++) {
          m[k][j] = m[k][w];
        }
        j = w;
        w = j * 2 + 1;
      } else {
        break;
      }
    }
    v[j] = t;
    for (int k = 0; k < N; k++) {
      m[k][j] = tv[k];
    }
  }
}

/*
        [a,b] = lanczos_tridiagonalize(x,w)

        The lanczos method for tridiagonalization.

        n := length of a, b, x, and w
        x,w := vectors of absciscae and weights
        a,b := the computed polynomial recursion coefficients

        Note that the first element of 'b' will be equal
        to sum(w) and can be used as a normalization constant
        for generated quadrature rules.
*/
void lanczos_tridiagonalize(const int n, const double *x_in, const double *w_in,
                            double *a, double *b) {
  if (n < 3 || x_in == 0 || w_in == 0 || a == 0 || b == 0) {
    fprintf(stderr,
            "\nerror in GaussExpPointsWeights::lanczos_tridiagonaliza() -> non "
            "admissable parameters\n");
    exit(EXIT_FAILURE);
  }

  std::vector<double> x;
  Assign(x, n, x_in);

  std::vector<double> w;
  Assign(w, n, w_in);

  std::vector<double> p0(x);

  std::vector<double> p1(n, (double)0.0);

  p1[0] = w[0];

  for (int i = 0; i < (n - 1); i++) {
    double pi = w[i + 1];
    double gamma = (double)1.0;
    double sigma = (double)0.0;
    double t = (double)0.0;
    for (int k = 0; k <= (i + 1); k++) {
      double rho = p1[k] + pi;
      double zeta = gamma * rho;
      double old_sigma = sigma;
      if (rho <= (double)0.0) {
        gamma = (double)1.0;
        sigma = (double)0.0;
      } else {
        gamma = p1[k] / rho;
        sigma = pi / rho;
      }
      double tk = sigma * (p0[k] - x[i + 1]) - gamma * t;
      p0[k] -= (tk - t);
      t = tk;
      if (sigma <= (double)0.0) {
        pi = old_sigma * p1[k];
      } else {
        pi = t * t / sigma;
      }
      p1[k] = zeta;
    }
  }
  Assign(a, p0);
  Assign(b, p1);
}

/*
        function [z,q] = fejer2(n)

        Weights q and nodes z for the n-point Fejer type-2 quadrature rules,
        calculated via the inverse (fast) double discrete fourier transform.

        The knots are the same knots as the Clenshaw-Curtis knots, implying that
        by doubling the number of evalulation points, you can re-use the
   function evaluations at the old points, since the new set of knots will
   contain the old set of knots. In other words, the knots for an n-point fejer2
   rule will be contained in the (2*n+1)-point fejer2 rule.

        Although capable of computing knots and weights for arbitrary 'n', the
        computation will be much faster and efficient if (n+1) has factors only
        from the set from the int set {2,3,4,5}.

        Therefore one recommended sequence of n is
   {3,7,15,31,63,127,255,511,1023,...}. Three (or five) times this set would
   also be acceptable.
*/

void fejer2_abscissae(const int n, double *z, double *q) {
  if (n < 3 || z == 0 || q == 0) {
    fprintf(stderr,
            "\nerror in GaussExpPointsWeights::fejer2_abscissae() -> non "
            "admissable parameters\n");
    exit(EXIT_FAILURE);
  }

  int n_fft = n + 1;

  /* allocate memory */
  double *v = new (std::nothrow) double[n_fft];
  double *work = new (std::nothrow) double[2 * n_fft];
  int *ifac = new (std::nothrow) int[sizeof(int) * 8];

  if (v == 0 || work == 0 || ifac == 0) {
    fprintf(stderr,
            "\nerror in GaussExpPointsWeights::fejer2_abscissae() -> "
            "impossible to allocate memory\n");
    exit(EXIT_FAILURE);
  }

  /* calculate the knots */
  for (int k = 1; k < n_fft; k++) {
    z[k - 1] = -cos(((double)k * M_PI) / (double)n_fft);
  }

  /* calculate the transformed weights */
  v[0] = (double)2.0;
  for (int k = 1; k < n_fft; k++) {
    v[k] = (double)0.0;
  }

  for (int k = 1; k < n_fft / 2; k++) {
    v[2 * k - 1] = ((double)2.0) / ((double)1.0 - (double)4.0 * (k * k));
  }

  v[n_fft - 1 - (n_fft & 1)] =
      ((double)n_fft - (double)3.0) / ((double)2.0 * (n_fft / 2) - (double)1.0) -
      (double)1.0;

  /* take the fourier transform, and scale */
  rffti(&n_fft, work, ifac);
  rfftb(&n_fft, v, work, ifac);
  for (int i = 0; i < n_fft; i++) {
    v[i] /= (double)n_fft;
  }

  /* transfer u to q, removing first (zero) element */
  for (int i = 1; i < n_fft; i++) {
    q[i - 1] = v[i];
  }

  /* clean up */
  delete[](ifac);
  delete[](work);
  delete[](v);
}

void AssignIdentity(std::vector<std::vector<double>> &m, const int n) {
  m.resize(n);
  for (int i = 0; i < n; i++) {
    m[i].resize(n);
    for (int j = 0; j < n; j++) {
      m[i][j] = 0.0;
    }
    m[i][i] = 1.0;
  }
}

void Assign(std::vector<double> &v, const int n, const double *a) {
  v.resize(n);
  for (int i = 0; i < n; i++) {
    v[i] = a[i];
  }
}

void Assign(double *a, const std::vector<double> &v) {
  for (unsigned i = 0; i < v.size(); i++) {
    a[i] = v[i];
  }
}
