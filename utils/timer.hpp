/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef TIMER_HPP_INCLUDED
#define TIMER_HPP_INCLUDED

#include <iostream>
#include <string>
#include <chrono>

/*
 * to initialize the timer use timer timerName(" unit of measure ");
 * the unit of measure can be sec, msec, musec, nsec
 * to start the timer: timerName.start("optional message");
 * to stop the timer: timerName.stop()
 */

class timer {
  std::chrono::high_resolution_clock::time_point startTime, stopTime;
  std::string unit_of_measure;
  std::string start_string;
  bool in_line;
  
public:
    
  timer(const std::string& uom = "sec") : unit_of_measure(uom), in_line(false)
  { }

  void
  start(const std::string& message = "",
        const std::string& start_str = "",
        bool in_line_p = false)
  {
    start_string = start_str;
    in_line = in_line_p;
    
    std::cout << start_string << "-- start ";
    if (message != "")
      std::cout << message << std::flush;
    if (!in_line)
      std::cout << std::endl;
    else
      std::cout << std::flush;
    startTime = std::chrono::high_resolution_clock::now();  
  }

  void
  stop()
  {
    if (in_line)
      std::cout << " -> Time: ";
    else
      std::cout << start_string << "-- Time: ";
    stopTime = std::chrono::high_resolution_clock::now();
    
    if (unit_of_measure == "sec")
      std::cout << std::chrono::duration_cast<std::chrono::seconds>(stopTime - startTime).count();
    if (unit_of_measure == "msec")
      std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(stopTime - startTime).count();
    if (unit_of_measure == "musec")
      std::cout << std::chrono::duration_cast<std::chrono::microseconds>(stopTime - startTime).count();
    if (unit_of_measure == "nsec")
      std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(stopTime - startTime).count();
    std::cout << " " << unit_of_measure << std::endl;
  }

};

#endif //TIMER_HPP_INCLUDED
