/* sam */

#ifndef FFT_PACK_HPP
#define FFT_PACK_HPP

// #include "Definitions.h"
// #include "GaussExpMath.h"

#define double_CONSTANT(x) x

void cfftb(int *n, double *c, double *wsave, int *ifac);
void cfftf(int *n, double *c, double *wsave, int *ifac);
void cffti(int *n, double *wsave, int *ifac);

void cosqb(int *n, double *x, double *wsave, int *ifac);
void cosqf(int *n, double *x, double *wsave, int *ifac);
void cosqi(int *n, double *wsave, int *ifac);

void cost(int *n, double *x, double *wsave, int *ifac);
void costi(int *n, double *wsave, int *ifac);

void ezfftb(int *n, double *r, double *azero, double *a, double *b, double *wsave,
            int *ifac);
void ezfftf(int *n, double *r, double *azero, double *a, double *b, double *wsave,
            int *ifac);
void ezffti(int *n, double *wsave, int *ifac);

void rfftb(int *n, double *r, double *wsave, int *ifac);
void rfftf(int *n, double *r, double *wsave, int *ifac);
void rffti(int *n, double *wsave, int *ifac);

void sinqb(int *n, double *x, double *wsave, int *ifac);
void sinqf(int *n, double *x, double *wsave, int *ifac);
void sinqi(int *n, double *wsave, int *ifac);

void sint(int *n, double *x, double *wsave, int *ifac);
void sinti(int *n, double *wsave, int *ifac);

#endif

/* ~sam */
