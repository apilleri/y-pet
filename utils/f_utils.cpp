/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <string>
#include <iostream>
#include <fstream>
#include <iomanip> 

#include "f_utils.h"


// --- FILE SECTION

// creates an empty directory 
void createDirectory(std::string dir_name) 
{
//   fs::path basepath(dir_name);
  if ( !fs::exists(dir_name) )
    fs::create_directory(dir_name);
}

void truncateFile(std::string file_name)
{
  std::ofstream fileToOpen;
  fileToOpen.open(file_name);
  if ( !fileToOpen.is_open() ) 
    throw std::string ( "Unable to open file for creation: " + file_name );
  fileToOpen.close();
}

std::vector<std::string> getRootFilenames(const std::string& dir_path)
{
  using dir_entry = fs::directory_entry;
  
  std::vector<std::string> out;
  
  fs::path fs_path(dir_path);
  for (const dir_entry & entry : fs::directory_iterator(fs_path))
    if ( entry.path().extension() == ".root" )
      out.push_back( entry.path() );
  
  return out;
}
// ---

// --- Comand Line utils

void checkCommandLine(const cxxopts::ParseResult& cmd_line,
                      const std::string& tag,
                      const cxxopts::Options& options)
{
  if ( !cmd_line.count(tag) ) {
    std::string err_str = "Missing command line option: " + tag + "\n\n";
    throw (err_str + options.help());
  }
}

// ---

// --- HASH SECTION

detPair_t szudzikPair(detID_t x, detID_t y)
{
  return (x >= y) ? x*x + x + y : x + y*y;
}

std::tuple<detID_t, detID_t> szudzikUnPair(detPair_t z)
{
  detPair_t sq = std::sqrt(z);
  detPair_t sq2 = sq*sq;
  
  return (z - sq2 >= sq) ? std::make_tuple(sq, z - sq2 - sq) : std::make_tuple(z - sq2, sq);
}

// ---

// --- XML SECTION

XMLDocument* xmlLoad(const std::string& file)
{
  XMLDocument* xml_doc = new XMLDocument;
  
  XMLError eResult = xml_doc->LoadFile( file.c_str() );
  if (eResult != XML_SUCCESS) {
    std::string error;
    error = "Error loading XML file: " + file;
    error +=  " with error: " + std::to_string(eResult);
    throw error;
  }
  
  return xml_doc;
}

const XMLElement* xmlGetElement(const XMLNode* xml_node, const char *tag)
{
  const XMLElement *element;
  // --- Getting pointer to the tag child of the XML tree
  element = xml_node->FirstChildElement(tag);
  if (element == nullptr) {
    std::string error;
    error = "XML_ERROR_PARSING_ELEMENT: " + std::string(tag) + " not found.";
    throw std::string(error);
  }
  
  return element;
}

std::string xmlGetText(const XMLElement* xml_el, const char *tag)
{
  const XMLElement *element;
  // --- Getting pointer to the tag child of the XML tree
  element = xml_el->FirstChildElement(tag);
  if (element == nullptr) {
    std::string error;
    error = "XML_ERROR_PARSING_ELEMENT: " + std::string(tag) + " not found.";
    throw std::string(error);
  }
  
  return element->GetText();
}

std::string extractScannerName(std::string xml_config)
{
  XMLDocument* xml_doc = xmlLoad(xml_config);
  const XMLElement *scanner_el = xmlGetElement(xml_doc, "SCANNER");
  
  return xmlGetText(scanner_el, "NAME");
}

std::string extractScannerType(std::string xml_config)
{
  XMLDocument* xml_doc = xmlLoad(xml_config);
  const XMLElement *scanner_el = xmlGetElement(xml_doc, "SCANNER");
  
  return xmlGetText(scanner_el, "TYPE");
}

// ---

std::string vec2str( std::vector<uint16_t> v)
{
  std::string str("");
  if ( ! v.empty() ) {
    str += std::to_string( v[0] );
    
    for (uint16_t i=1; i<v.size(); i++)
      str += "-" + std::to_string( v[i] );
  }
  
  return str;
}

FloatType minNotZero(FloatType *vec, uint32_t size)
{
  FloatType min;
  
  //finding the first not null element
  uint32_t i=0;
  while(i<size && vec[i]==0)
    i++;
  
  if (i == size)
    return 0; //empty vector
    else
      min = vec[i];
    
    while(i<size) {
      if (vec[i] != 0 && vec[i]<min)
        min = vec[i];
      i++;
    }
    
    return min;
}

ThreeVector intersectionPoint(const ThreeVector& f1, const ThreeVector& f2,
                              const ThreeVector& s1, const ThreeVector& s2)
{
  // Line AB represented as a1x + b1y = c1
  SpaceType a1 = f2.y() - f1.y();
  SpaceType b1 = f1.x() - f2.x();
  SpaceType c1 = a1*(f1.x()) + b1*(f1.y());
  
  // Line CD represented as a2x + b2y = c2
  SpaceType a2 = s2.y() - s1.y();
  SpaceType b2 = s1.x() - s2.x();
  SpaceType c2 = a2*(s1.x())+ b2*(s1.y());
  
  SpaceType determinant = a1*b2 - a2*b1;
  
  if (determinant == 0)  {
    std::string error = "CylindricalGeometry::computeFieldOfViewHalfWidth()";
    error += "parallel lines";
    
    std::cout << "Warning: " + error << std::endl;
    return ThreeVector(0,0,0);
//     throw std::string();
  }
  else {
    SpaceType x = (b2*c1 - b1*c2)/determinant;
    SpaceType y = (a1*c2 - a2*c1)/determinant;
    return ThreeVector(x, y, 0);
  }
}


ThreeVector computeRotationAngles(const ThreeVector& v1, const ThreeVector& v2)
{
  ThreeVector u1 = v1.unit();
  ThreeVector u2 = v2.unit();
  ThreeVector v = u1.cross(u2);
  AngleType cos_theta = u1.dot(u2);
  
  AngleType E1=0, E2=0, E3=0;
  
  // using precision 1e-4 to compare float
  if (((cos_theta - 1e-4) < -1) && ((cos_theta + 1e-4) > -1)) {
    E3 = M_PI;
  }
  else {
    ThreeMatrix R(1);
    ThreeMatrix Vx;
    //Vx is the skew-symmetric cross-product matrix of vector v
    Vx[0][1] = -v.z();
    Vx[1][0] = +v.z();
    Vx[0][2] = +v.y();
    Vx[2][0] = -v.y();
    Vx[1][2] = -v.x();
    Vx[2][1] = +v.x();
    
    R = R + Vx + Vx*Vx/(1+cos_theta);
    
    //computing Euler angles
    
    if (R[0][2] == 1 || R[0][2] == -1 ) {
      //special case
      E3 = 0; //set arbitrarily
      AngleType dlta = std::atan2(R[0][1],R[0][2]);
      if (R[1][3] == -1) {
        E2 = M_PI/2;
        E1 = E3 + dlta;
      }
      else {
        E2 = -M_PI/2;
        E1 = -E3 + dlta;
      }
    }
    else {
      E2 = - std::asin(R[0][2]);
      E1 = std::atan2(R[1][2]/std::cos(E2), R[2][2]/std::cos(E2));
      E3 = - std::atan2(R[0][1]/std::cos(E2), R[0][0]/std::cos(E2));
    } 
  }
  
  return ThreeVector(E1, E2, E3);
}




FloatType median(std::vector<FloatType> vec)
{
  size_t size = vec.size();
  
  if (size == 0)   {
    return 0;
  }
  else {
    std::sort(vec.begin(), vec.end());
    if (size % 2 == 0)
      return (vec[size / 2 - 1] + vec[size / 2]) / 2;
    else
      return vec[size / 2];
  }
}

std::vector<ThreeVector> crystalCorners(SpaceType halfx, SpaceType halfy, SpaceType halfz, AngleType angle)
{
  std::vector<ThreeVector> corners;
  ThreeVector tmp;
  
  tmp = ThreeVector(-halfx, -halfy, -halfz);
  tmp.rotateZ(angle);
  corners.push_back(tmp);
  
  tmp = ThreeVector(+halfx, -halfy, -halfz);
  tmp.rotateZ(angle);
  corners.push_back(tmp);
  
  tmp = ThreeVector(+halfx, +halfy, -halfz);
  tmp.rotateZ(angle);
  corners.push_back(tmp);
  
  tmp = ThreeVector(-halfx, +halfy, -halfz);
  tmp.rotateZ(angle);
  corners.push_back(tmp);
  
  tmp = ThreeVector(-halfx, -halfy, +halfz);
  tmp.rotateZ(angle);
  corners.push_back(tmp);
  
  tmp = ThreeVector(+halfx, -halfy, +halfz);
  tmp.rotateZ(angle);
  corners.push_back(tmp);
  
  tmp = ThreeVector(+halfx, +halfy, +halfz);
  tmp.rotateZ(angle);
  corners.push_back(tmp);
  
  tmp = ThreeVector(-halfx, +halfy, +halfz);
  tmp.rotateZ(angle);
  corners.push_back(tmp);
  
  return corners;
}

#include <random>

std::vector< std::vector<lorIDX_t> >
generateSubsets(lorIDX_t n_lors,  uint16_t n_subsets, uint32_t seed)
{
  /* Every time we need to work with subsets, it is necessary that the 
   * distribution of the LORs in the subsets remains constant. So if we
   * want to compute the sensitivity or the reconstruction, the LORs must
   * be distributed in the same way. This function generates the subsets based 
   * on a unique lor_index. >From the lor_index is possible to retrieve the 
   * inrfmation about the detectors, crystals and counts, using the specific
   * method of the HistogramType class
   */
  
  std::vector< std::vector<lorIDX_t> > subsets(n_subsets);
  for (uint16_t cur_sub=0; cur_sub < n_subsets; cur_sub++)
    subsets[cur_sub].reserve(n_lors/n_subsets);
  
  std::vector<lorIDX_t> all_LORs(n_lors);
  for (lorIDX_t i=0; i<n_lors; i++)
    all_LORs[i] = i;
  std::shuffle (all_LORs.begin(), all_LORs.end(),
                std::default_random_engine(seed));
  
  for (lorIDX_t i=0; i<n_lors; i++) 
    subsets[i%n_subsets].push_back( all_LORs[i] );

  return subsets;
}




std::tuple<std::vector<float>, uint16_t, uint16_t>
makeKernel(float fwhm_x, float fwhm_y, uint16_t nsx, uint16_t nsy, bool print)
{
  float k = 2.*std::sqrt(2.*std::log(2.));
  
  float sigma_x = fwhm_x/k;
  float sigma_y = fwhm_y/k;
  
  //   float h_wf = sigma*5;
  uint16_t h_wx = std::round(sigma_x*nsx);
  uint16_t h_wy = std::round(sigma_y*nsy);
  uint16_t k_widthx = h_wx*2+1;
  uint16_t k_widthy = h_wy*2+1;
  uint16_t k_size = k_widthx*k_widthy;
  std::vector<float> kernel;
  kernel.resize(k_size);
  
  float sum_kernel = 0.;
  for (int x=0, index=0; x<k_widthx; x++) {
    float v_x = 0.5* ( std::erf(x-h_wx+0.5)/(std::sqrt(2.)*sigma_x) - std::erf(x-h_wx-0.5)/(std::sqrt(2.)*sigma_x) );
    for (int y=0; y<k_widthy; y++, index++) {
      kernel[index] = 0.5* ( std::erf(y-h_wy+0.5)/(std::sqrt(2.)*sigma_y) - std::erf(y-h_wy-0.5)/(std::sqrt(2.)*sigma_y) )*v_x;
      sum_kernel += kernel[index];
    }
  }
  
  for (int i = 0; i < k_size; i++)
    kernel[i] /= sum_kernel;
  
  if (print) {
    for (uint16_t i=0;i<k_widthx; i++) {
      for (uint16_t j=0; j<k_widthy; j++)
        std::cout << kernel[i*k_widthy+j] << " ";
      std::cout << std::endl;
    }
  }
  return std::make_tuple(kernel, k_widthx, k_widthy);
}


void shiftKernel(std::vector<float>& map, uint16_t ny, uint16_t nz, uint16_t c,
                 const std::vector<float>& kernel, uint16_t wx, uint16_t wy, bool print)
{
  map.resize(ny*nz,0);
  
  float added = 0;
  
  int16_t py = c%ny;
  int16_t pz = c/ny;
  
  for (uint16_t x=0; x<wx; x++) {
    for (uint16_t y=0; y<wy; y++) {
      
      int16_t d_y = py-wx/2+x;
      int16_t d_z = pz-wy/2+y;
      if (d_z>=0 && d_z<nz && d_y>=0 && d_y<ny) {
        added += kernel[x*wy+y];
        map[d_z*ny + d_y] = kernel[x*wy+y];
      }
      
    }
  }
  
  for (uint16_t i=0; i<map.size(); i++)
    map[i] /= added;
  
  if (print) {
    for (uint16_t i=0; i<ny; i++) {
      for (uint16_t j=0; j<nz; j++)
        std::cout << map[i*ny + j] << " ";
      std::cout << std::endl;
    }
  }
  
}


std::vector<ThreeVector>
createPointsGrid(SpaceType x_len, SpaceType y_len, SpaceType z_len,
                 uint16_t npx, uint16_t npy, uint16_t npz)
{
  std::vector<ThreeVector> points;
  //regular grid of points
  SpaceType x_step = x_len/npx;
  SpaceType y_step = y_len/npy;
  SpaceType z_step = z_len/npz;

  for (uint16_t i=0; i<npx; i++) 
    for (uint16_t j=0; j<npy; j++) 
      for (uint16_t k=0; k<npz; k++) {
        SpaceType x = -x_len/2 + x_step/2 + i*x_step;
        SpaceType y = -y_len/2 + y_step/2 + j*y_step;
        SpaceType z = -z_len/2 + z_step/2 + k*z_step;
        points.push_back( ThreeVector(x, y, z) );
      }

  return points;
}