#ifndef UTILS_NUMERICALINTEGRATION_H
#define UTILS_NUMERICALINTEGRATION_H

#include "dataTypes.h"
#include "euclideanSpace.h"

// --- GAUSS_LEGENDRE INTEGRATION

typedef	 double WeightType;
typedef  float DeltaType;

// Implements a 1d integration point with a weight
class IntegrationWeigth {
	DeltaType delta;
	WeightType weight;
public:
	IntegrationWeigth(DeltaType _delta,WeightType _weigth):
        delta(_delta),weight(_weigth) {};

	DeltaType getDelta()    { return delta; }
	WeightType getWeight()  { return weight; }
};

// Implements a 2d integration point with
class IntegrationPoint {
    DeltaType delta[2];
    WeightType weight;
    WeightType weightN;
    DeltaType deltaN;

public:
    IntegrationPoint(){};

    IntegrationPoint(IntegrationWeigth point1, IntegrationWeigth point2) : 
        weightN(1) , deltaN(0)
    {
        delta[0] = point1.getDelta();
        delta[1] = point2.getDelta();
        weight = point1.getWeight() * point2.getWeight();
    };

    IntegrationPoint(IntegrationWeigth point1,
                     IntegrationWeigth point2,
                     FloatType crystalLenght,
                     FloatType attenuationLenght,
                     uint16_t numberOfIntegrationPoints,
                     uint16_t pointNumber)
    {
        delta[0] = point1.getDelta();
        delta[1] = point2.getDelta();
        weight = point1.getWeight() * crystalLenght /
                 (numberOfIntegrationPoints - 1) * attenuationLenght *
                 point2.getWeight();
        if (pointNumber == 1 || pointNumber == numberOfIntegrationPoints)
          weight = weight / 2;
        deltaN =
            (pointNumber - 1) * crystalLenght / (numberOfIntegrationPoints - 1);
        weightN = std::exp(-attenuationLenght * deltaN);
    }

    DeltaType getDelta1() { return delta[0]; }
    DeltaType getDelta2() { return delta[1]; }
    WeightType getWeight() { return weight; }
    DeltaType getDeltaN() { return deltaN; }
    DeltaType getWeightN() { return weightN; }
};

std::vector<IntegrationPoint> GLtransformToIntegrationWeigths
    (const std::vector<double> &x, const std::vector<double> &w);

std::vector<IntegrationPoint> computeGLPoints(uint16_t points_n);

std::tuple<std::vector<double>, std::vector<double>>
computeExponentialWeights(FloatType attenuation_length, FloatType cry_length,
                          uint16_t points_n);

double weight_function(double x, double att_len, double cry_len);

void Assign(std::vector<double> &v, const int n, const double *a);

void Assign(double *a, const std::vector<double> &v);

void AssignIdentity(std::vector<std::vector<double>> &m, const int n);

void heapsort_eig(std::vector<double> &v,
                         std::vector<std::vector<double>> &m);

void fejer2_abscissae(const int n, double *z, double *q);

void lanczos_tridiagonalize(const int n, const double *x, const double *w, double *a,
                            double *b);

void gaussqr_from_rcoeffs(const int n, const double *a, const double *b, double *x,
                          double *w);


#endif