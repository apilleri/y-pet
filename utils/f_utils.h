/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef UTILS_F_UTILS_H_
#define UTILS_F_UTILS_H_

#include <cmath>
#include <string>
#include <tuple>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>

#include <filesystem>
namespace fs = std::filesystem;

#include "tinyxml2.h"
using namespace tinyxml2;

#include "euclideanSpace.h"
#include "dataTypes.h"
#include "cxxopts.hpp"

// --- FILE SECTION

void createDirectory(std::string dir_name);
void truncateFile(std::string file_name);
std::vector<std::string> getRootFilenames(const std::string& dir_path);

template <class T>
void readFromFile(std::ifstream& file, T* buffer, uint32_t n_entries=1)
{
  file.read( reinterpret_cast<char *>(buffer), n_entries*sizeof(T) );
  if (!file) {
    std::string error("Error reading from file: expected ");
    error += std::to_string(n_entries*sizeof(T));
    error += " bytes but read only ";
    error += std::to_string(file.gcount());
    throw error;
  }
}

// ---


// --- Comand Line utils

void checkCommandLine(const cxxopts::ParseResult& cmd_line,
                      const std::string& tag,
                      const  cxxopts::Options& options);

// ---

// --- HASH SECTION

detPair_t szudzikPair(detID_t x, detID_t y);
std::tuple<detID_t, detID_t> szudzikUnPair(detPair_t z);

// ---

// ---XML SECTION

XMLDocument* xmlLoad(const std::string& file);
const XMLElement* xmlGetElement(const XMLNode* xml_node, const char *tag);
std::string xmlGetText(const XMLElement* xml_el, const char *tag);
std::string extractScannerName(std::string xml_config);
std::string extractScannerType(std::string xml_config);

template <class T>
T xmlGetNumber(const XMLElement* xml_el, const char *tag)
{
  const XMLElement *element;
  // --- Getting pointer to the tag child of the XML tree
  element = xml_el->FirstChildElement(tag);
  if (element == nullptr) {
    std::string error;
    error = "XML_ERROR_PARSING_ELEMENT: " + std::string(tag) + " not found.";
    throw std::string(error);
  }
  
  float temp;
  XMLError eResult = element->QueryFloatText(&temp);
  if (eResult != XML_SUCCESS)
    throw std::string("Error QueryFloatText in: " + std::string(tag) );
  
  return static_cast<T>(temp);
}

// --- 


//these two functions are used only in the visualizer. TODO move there
std::string vec2str(std::vector<uint16_t> v);
std::vector<ThreeVector> crystalCorners(SpaceType halfx, SpaceType halfy, SpaceType halfz, AngleType angle);


FloatType minNotZero(FloatType *vec, uint32_t size);


//these two functions are used only in the cylindrical geometry. TODO move there
ThreeVector intersectionPoint(const ThreeVector& f1, const ThreeVector& f2,
                              const ThreeVector& s1, const ThreeVector& s2);

ThreeVector computeRotationAngles(const ThreeVector& v1, const ThreeVector& v2);


FloatType median(std::vector<FloatType> vec);








template <class T>
std::vector<T> splitString(const std::string& str, char delim)
{
  std::vector<T> cont;
  std::stringstream ss(str);
  std::string token;
  while (std::getline(ss, token, delim)) {
    cont.push_back(std::stoi(token));
  }
  return cont;
}


template <class T>
bool fpAreEqual(T f1, T f2, T precision)
{
  if (((f1 - precision) < f2) && ((f1 + precision) > f2))
    return true;
  else
    return false;
}

std::vector< std::vector<lorIDX_t> >
generateSubsets(lorIDX_t n_lors,  uint16_t n_subsets, uint32_t seed);


std::tuple<std::vector<float>, uint16_t, uint16_t>
makeKernel(float fwhm_x, float fwhm_y, uint16_t width_x, uint16_t width_y, bool print);

void shiftKernel(std::vector<float>& map, uint16_t ny, uint16_t nz, uint16_t c,
                 const std::vector<float>& kernel, uint16_t wx, uint16_t wy, bool print);


std::vector<ThreeVector>
createPointsGrid(SpaceType x_len, SpaceType y_len, SpaceType z_len,
                 uint16_t npx, uint16_t npy, uint16_t npz);


#endif // UTILS_F_UTILS_H_
