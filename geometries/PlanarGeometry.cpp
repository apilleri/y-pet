/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <PlanarGeometry.h>

PlanarGeometry::PlanarGeometry(std::string config,
                               bool activate_vg)
: vGeometry(activate_vg)
{
  XMLDocument* xml_doc = xmlLoad(config);
  
  // --- Parsing SCANNER elements
  const XMLElement *scanner_el = xmlGetElement(xml_doc, "SCANNER");
  _type = xmlGetText(scanner_el, "TYPE");
  _name = xmlGetText(scanner_el, "NAME");
  desc.half_heads_distance =
    xmlGetNumber<SpaceType>(scanner_el, "HALF_HEADS_DISTANCE");
    desc.module_grid_Y = xmlGetNumber<uint16_t>(scanner_el, "MODULE_GRID_Y");
  desc.module_grid_Z = xmlGetNumber<uint16_t>(scanner_el, "MODULE_GRID_Z");
  desc.module_offset_Y = xmlGetNumber<SpaceType>(scanner_el, "MODULE_OFFSET_Y");
  desc.module_offset_Z = xmlGetNumber<SpaceType>(scanner_el, "MODULE_OFFSET_Z");
  desc.block_grid_Y = xmlGetNumber<uint16_t>(scanner_el, "BLOCK_GRID_Y");
  desc.block_grid_Z = xmlGetNumber<uint16_t>(scanner_el, "BLOCK_GRID_Z");
  desc.block_offset_Y = xmlGetNumber<SpaceType>(scanner_el, "BLOCK_OFFSET_Y");
  desc.block_offset_Z = xmlGetNumber<SpaceType>(scanner_el, "BLOCK_OFFSET_Z");
  desc.n_layers = xmlGetNumber<uint16_t>(scanner_el, "LAYERS_PER_BLOCK");
  
  // --- Parsing LAYERs childs of the XML tree
  desc.head_len_X = 0;
  desc.n_crystals_block = 0;
  desc.block_len_Y = 0;
  desc.block_len_Z = 0;
  for (uint16_t cur_layer=0; cur_layer<desc.n_layers; cur_layer++) {
    uint16_t n_Y, n_Z;
    SpaceType c_X, c_Y, c_Z;
    SpaceType p_Y, p_Z;
    
    std::string layer_str = "LAYER" + std::to_string(cur_layer);
    const XMLElement* layer_el = xmlGetElement(scanner_el, layer_str.c_str() );
    n_Y = xmlGetNumber<uint16_t>(layer_el, "CRYSTALS_NUMBER_Y");
    n_Z = xmlGetNumber<uint16_t>(layer_el, "CRYSTALS_NUMBER_Z");
    c_X = xmlGetNumber<SpaceType>(layer_el, "CRYSTAL_LENGTH_X");
    c_Y = xmlGetNumber<SpaceType>(layer_el, "CRYSTAL_LENGTH_Y");
    c_Z = xmlGetNumber<SpaceType>(layer_el, "CRYSTAL_LENGTH_Z");
    p_Y = xmlGetNumber<SpaceType>(layer_el, "CRYSTAL_PITCH_Y");
    p_Z = xmlGetNumber<SpaceType>(layer_el, "CRYSTAL_PITCH_Z");
    
    desc.n_crystals_block += n_Y*n_Z;
    desc.head_len_X += c_X;
    
    PlanarLayerType temp_layer(n_Y, n_Z, c_X, c_Y, c_Z, p_Y, p_Z);
    desc.layers.push_back(temp_layer);
    
    //maximum block length, useful where there are more than on layers
    SpaceType len;
    len = n_Y*p_Y; // + (p_Y-c_Y);
    if (len > desc.block_len_Y)
      desc.block_len_Y = len;
    len = n_Z*p_Z; // + (p_Z-c_Z);
    if (len > desc.block_len_Z)
      desc.block_len_Z = len;
  }
  
  // --- Computing some useful parameter
  uint16_t& mnY = desc.module_grid_Y;
  uint16_t& mnZ = desc.module_grid_Z;
  uint16_t& bnY = desc.block_grid_Y;
  uint16_t& bnZ = desc.block_grid_Z;
  desc.n_modules = mnY * mnZ;
  desc.n_blocks = bnY * bnZ;
  //changed the offsets from distance between borders to distance between centers
  desc.module_len_Y = (bnY - 1)*desc.block_offset_Y + desc.block_len_Y; // bnY*desc.block_len_Y + (bnY-1)*desc.block_offset_Y;
  desc.module_len_Z = (bnZ - 1)*desc.block_offset_Z + desc.block_len_Z; // bnZ*desc.block_len_Z + (bnZ-1)*desc.block_offset_Z;
  desc.head_len_Y = (mnY - 1)*desc.module_offset_Y + desc.module_len_Y; // mnY*desc.module_len_Y + (mnY - 1)*desc.module_offset_Y;
  desc.head_len_Z = (mnZ - 1)*desc.module_offset_Z + desc.module_len_Z; // mnZ*desc.module_len_Z + (mnZ - 1)*desc.module_offset_Z;
  
  // --- Computing positions of the elements of the scanner:
  // --- Every position is computed with respect to the parent object
  // Setting center of the RingSector, computing the rotation angles of the
  // ring_sectors.
  
  // x,y,z are used, everywhere, to set the three components of a ThreeVector
  // in order to simplify reading the code
  SpaceType x, y, z;
  
  // Setting ModulesCenters
  // startCenter represents the center of the first module, 
  // starting from lower y and lower z
  x = desc.half_heads_distance + desc.head_len_X/2;
  y = (-desc.head_len_Y + desc.module_len_Y)/2;
  z = (-desc.head_len_Z + desc.module_len_Z)/2;
  ThreeVector startCenter = ThreeVector(x, y, z);
  for (uint16_t i=0; i<mnZ; i++) {
//     z = i*(desc.module_len_Z+desc.module_offset_Z);
    z = i*desc.module_offset_Z;
    for (uint16_t j=0; j<mnY; j++) {
//       y = j*(desc.module_len_Y+desc.module_offset_Y);
      y = j*desc.module_offset_Y;
      ThreeVector shift = ThreeVector(0, y, z);
      modules_centers.push_back(startCenter + shift);
    }
  }
  
  // Setting BlockCenters
  // startCenter represents the center of the first block inside the module,
  y = (-desc.module_len_Y + desc.block_len_Y)/2;
  z = (-desc.module_len_Z + desc.block_len_Z)/2;
  startCenter = ThreeVector(0, y, z);
  for (uint16_t i=0; i<bnZ; i++) {
    // y should be in the inner loop because it changes first
//     z = i*(desc.block_len_Z+desc.block_offset_Z);
    z = i*desc.block_offset_Z;
    for (uint16_t j=0; j<bnY; j++) {
//       y = j*(desc.block_len_Y+desc.block_offset_Y);
      y = j*desc.block_offset_Y;
      ThreeVector shift = ThreeVector(0, y, z);
      blocks_centers.push_back(startCenter + shift);
    }
  }
  
  // Setting crystalCenter
  for (uint16_t curLayer=0; curLayer<desc.n_layers; curLayer++) {
    PlanarLayerType& layer = desc.layers[curLayer];
    uint16_t ncY = layer.nCrystalsY();
    uint16_t ncZ = layer.nCrystalsZ();
    
    // Setting x position of the layers with respect to the block
    x = +desc.head_len_X/2;
    for (uint16_t scannedLayer=0; scannedLayer<curLayer; scannedLayer++) 
      //iterating alog already computed layers
      x -= desc.layers[scannedLayer].crystalLengthX();
    x -= desc.layers[curLayer].crystalLengthX()/2;
    
    y = -(ncY-1)*layer.crystalPitchY() /2;
    z = -(ncZ-1)*layer.crystalPitchZ() /2;
    
    startCenter = ThreeVector(x, y, z);
    std::vector<ThreeVector> tempCenters;
    
    for (uint16_t i=0; i<ncZ; i++) { // y should be in the inner loop 
      z = i*layer.crystalPitchZ();
      for (uint16_t j=0; j<ncY; j++) {
        y = j*layer.crystalPitchY();
        ThreeVector shift = ThreeVector(0, y, z);
        tempCenters.push_back( startCenter + shift );
      }
    }
    crystals_centers.push_back(tempCenters);
    tempCenters.clear();
  }
  
  // --- Parsing Virtual Layer if present  
  const XMLElement* vlayer_el = xml_doc->FirstChildElement("VIRTUAL_LAYER");
  
  if (vlayer_el  != nullptr) {
    if (virtual_geometry) {
      throw std::string("Activate Virtual Geometry but no virual layer in config file");
    }
    desc.vl_present = true;
    std::string vl_position = xmlGetText(vlayer_el, "POSITION");
    uint16_t vl_ny =  xmlGetNumber<uint16_t>(vlayer_el, "CRYSTALS_NUMBER_Y");
    uint16_t vl_nz =  xmlGetNumber<uint16_t>(vlayer_el, "CRYSTALS_NUMBER_Z");
    
    SpaceType vlayer_y_size, vlayer_z_size;
    if (vl_position == "FRONT") {
      vlayer_y_size = desc.block_len_Y;
      vlayer_z_size = desc.block_len_Z;
      x = -desc.head_len_X/2;
    } else if (vl_position == "MIDDLE") {
      //       AngleType angle = M_PI/desc.n_sectors;
      //       vlayer_y_size = std::round(2*std::tan(angle)*first_sector_center.x()*100)/100;
      vlayer_y_size = desc.block_len_Y;
      vlayer_z_size = desc.block_len_Z;
      x = 0;
    }
    else
      throw std::string("VL position unrecognized: " + vl_position);
    
    SpaceType cy_size = vlayer_y_size/vl_ny;
    SpaceType cz_size = vlayer_z_size/vl_nz;
    
    desc.v_layer = VirtualLayerType(vl_ny, vl_nz, cy_size, cz_size);
    
    y = -vlayer_y_size/2 + cy_size/2;
    z = -vlayer_z_size/2 + cz_size/2;
    
    startCenter = ThreeVector(0, y, z);
    for (uint16_t i=0; i<vl_nz; i++) { // y should be in the inner loop 
      z = i*cz_size;
      for (uint16_t j=0; j<vl_ny; j++) {
        y = j*cy_size;
        ThreeVector shift = ThreeVector(x, y, z);
        v_crystals_centers.push_back( startCenter + shift );
      }
    }
  }
  else 
    desc.vl_present = false;  
  
  // mandatory order of functions calls
  generateDetectors();
  
  generateCoincidenceSchema();
  genereateUniqueViews();
  
  generateGroupedCS();
}

std::vector<ThreeVector> PlanarGeometry::CoincidenceSchemaMaxFOVPoints() const
{
  ThreeVector point(0,0,0);
  std::vector<ThreeVector> fov_points;
  
  point[0] = desc.half_heads_distance;
  point[1] = -desc.head_len_Y/2 - 3;
  fov_points.push_back(point);
  
  point[0] = desc.half_heads_distance;
  point[1] = desc.head_len_Y/2 + 3;
  fov_points.push_back(point);
  
  point[0] = -desc.half_heads_distance;
  point[1] = desc.head_len_Y/2 + 3;
  fov_points.push_back(point);
  
  point[0] = -desc.half_heads_distance;
  point[1] = -desc.head_len_Y/2 - 3;
  fov_points.push_back(point);
  
  return fov_points;
}


cryID_t
PlanarGeometry::computeCrystalID(const std::vector<uint16_t>& hierarchy) const
{
  if (hierarchy.size() != 4)
    throw std::string("PlanarGeometry::computeCrystalID expecting a vector of lentht 4, " 
    + std::to_string(hierarchy.size()) + " instead.");
  
  const uint16_t& head = hierarchy[0];
  const uint16_t& module = hierarchy[1];
  const uint16_t& block = hierarchy[2];
  const uint16_t& crystal = hierarchy[3];
  
  if (head > 1)
    throw std::string("Error in PlanarGeometry::computeCrystalID: head" +
    std::to_string(head) + " out of bounds, only two heads");
  
  if (module > desc.n_modules - 1)
    throw std::string("Error in PlanarGeometry::computeCrystalID: module" +
    std::to_string(module) + " out of bounds. Nmodules: " +
    std::to_string( desc.n_modules) );
  
  const uint16_t& nb = desc.n_blocks;
  if (block > nb - 1)
    throw std::string("Error in PlanarGeometry::computeCrystalID: block" +
    std::to_string(block) + " out of bounds. Nblocks: " +
    std::to_string( desc.n_blocks) );
  
  const uint16_t& ncb = desc.n_crystals_block;
  if (crystal > ncb - 1)
    throw std::string("Error in PlanarGeometry::computeCrystalID: crystal" +
    std::to_string(crystal) + " out of bounds. Ncrystals: " +
    std::to_string( desc.n_crystals_block) );
  
  return
  (cryID_t) crystal + (cryID_t) block*ncb + (cryID_t) module*ncb*nb +
  (cryID_t) head*ncb*nb*desc.n_modules;
}

detID_t
PlanarGeometry::computeDetectorID(const std::vector<uint16_t>& hierarchy) const
{
  if (hierarchy.size() != 3)
    throw std::string("PlanarGeometry::computeCrystalID expecting a vector of lentht 3, " 
    + std::to_string(hierarchy.size()) + " instead.");
  
  const uint16_t& head = hierarchy[0];
  const uint16_t& module = hierarchy[1];
  const uint16_t& block = hierarchy[2];
  
  
  if (head > 1)
    throw std::string("Error in PlanarGeometry::computeCrystalID: head" +
    std::to_string(head) + " out of bounds, only two heads");
  
  if (module > desc.n_modules - 1)
    throw std::string("Error in PlanarGeometry::computeCrystalID: module" +
    std::to_string(module) + " out of bounds. Nmodules: " +
    std::to_string( desc.n_modules) );
  
  const uint16_t& nb = desc.n_blocks;
  if (block > nb - 1)
    throw std::string("Error in PlanarGeometry::computeCrystalID: block" +
    std::to_string(block) + " out of bounds. Nblocks: " + std::to_string(nb) );
  
  return
  (detID_t) block + (detID_t) module*nb +
  (detID_t) head*nb*desc.n_modules;
  
}


std::vector<uint16_t>
PlanarGeometry::explodeCrystalID(cryID_t unique_ID) const
{
  const uint16_t& ncb = desc.n_crystals_block;
  const uint16_t& nb = desc.n_blocks;
  const uint16_t& nm = desc.n_modules;
  
  uint16_t crystal = unique_ID % ncb;;
  unique_ID = (unique_ID - crystal) / ncb;
  
  uint16_t block = unique_ID%nb;
  unique_ID = (unique_ID - block) / nb;
  
  uint16_t module = unique_ID % nm;
  uint16_t head = ( unique_ID - module ) / nm;

  return {head, module, block, crystal};
}


std::vector<uint16_t>
PlanarGeometry::explodeDetectorID(detID_t unique_ID) const
{
  const uint16_t& nb = desc.n_blocks;
  const uint16_t& nm = desc.n_modules;
  
  uint16_t block = unique_ID%nb;
  unique_ID = (unique_ID - block) / nb;
  uint16_t module = unique_ID % nm;
  uint16_t head = ( unique_ID - module ) / nm;
  
  return {head, module, block};
}


void PlanarGeometry::printGeometryInfo(std::string output_dir) const
{
  std::ofstream txt_file;
  std::string fname = output_dir + "/" + _name + "_GeometryInfo.txt";
  
  txt_file.open(fname);
  
  if (!txt_file.is_open())
    throw std::string("Unable to open " + fname);
  
  txt_file << "PLANAR INFO" << std::endl;
  txt_file << "Half heads distance: " << desc.half_heads_distance << std::endl;
  
  txt_file << "\nHEAD INFO" << std::endl;
  txt_file << "Head X length: " << desc.head_len_X << std::endl;
  txt_file << "Head Y length: " << desc.head_len_Y << std::endl;
  txt_file << "Head Z length: " << desc.head_len_Z << std::endl;
  txt_file << "Module grid Y: " << desc.module_grid_Y << std::endl;
  txt_file << "Module grid Z: " << desc.module_grid_Z << std::endl;
  txt_file << "Module offset Y: " << desc.module_offset_Y << std::endl;
  txt_file << "Module offset Z: " << desc.module_offset_Z << std::endl;
  
  txt_file << "\nMODULE INFO" << std::endl;
  txt_file << "Module Y length: " << desc.module_len_Y << std::endl;
  txt_file << "Module Z length: " << desc.module_len_Z << std::endl;
  txt_file << "Block grid Y: " << desc.block_grid_Y << std::endl;
  txt_file << "Block grid Z: " << desc.block_grid_Z << std::endl;
  txt_file << "Number of blocks: " << desc.n_blocks << std::endl;
  txt_file << "Block Y offset: " << desc.block_offset_Y << std::endl;
  txt_file << "Block Z offset: " << desc.block_offset_Z << std::endl;
  
  txt_file << "\nBLOCK INFO" << std::endl;
  txt_file << "Block length Y: " << desc.block_len_Y << std::endl;
  txt_file << "Block length Z: " << desc.block_len_Z << std::endl;
  txt_file << "Number of layers: " << desc.n_layers << std::endl;
  txt_file << "Number of crystals per block: " << desc.n_crystals_block << std::endl;
  
  for (uint16_t i=0; i<desc.n_layers; i++) {
    txt_file << "\nLAYER " << i << " INFO" << std::endl;
    txt_file << "Number of crystals Y: " << desc.layers[i].nCrystalsY() << std::endl;
    txt_file << "Number of crystals Z: " << desc.layers[i].nCrystalsZ() << std::endl;
    txt_file << "Crystal length X: " << desc.layers[i].crystalLengthX() << std::endl;
    txt_file << "Crystal length Y: " << desc.layers[i].crystalLengthY() << std::endl;
    txt_file << "Crystal length Z: " << desc.layers[i].crystalLengthZ() << std::endl;
    txt_file << "Crystal pitch Y: " << desc.layers[i].crystalPitchY() << std::endl;
    txt_file << "Crystal pitch Z: " << desc.layers[i].crystalPitchZ() << std::endl;
  }
  
  if ( v_crystals_centers.size() ) {
    txt_file << "\nVIRTUAL LAYER INFO" << std::endl;
    txt_file << "Number of crystals Y: " << desc.v_layer.nCrystalsY() << std::endl;
    txt_file << "Number of crystals Z: " << desc.v_layer.nCrystalsZ() << std::endl;
    txt_file << "Crystal pitch Y: " << desc.v_layer.crystalPitchY() << std::endl;
    txt_file << "Crystal pitch Z: " << desc.v_layer.crystalPitchZ() << std::endl;
  }
  
  txt_file << "\nDETECTORS INFO" << std::endl;
  for (uint16_t sector = 0; sector<2; sector++) {
    txt_file << "sector: " << std::setw(8) << sector << std::endl;
    for (uint16_t module = 0; module<desc.n_modules; module++) {
      txt_file << "\tmodule: " << std::setw(8) << module << std::endl;
      for (uint16_t block =0; block<desc.n_blocks; block++) {
        std::vector<uint16_t> hierarchy = {sector, module, block};
        uint16_t det_id = computeDetectorID(hierarchy);
        txt_file << "\t\tblock: " << std::setw(8) << block << std::endl;
        txt_file << "\t\t->det_id: " << std::setw(8) << det_id << std::endl;
        txt_file << "\t\t->surface_center: " << getDetector(det_id).detectorSurfaceCenter() << std::endl;
        txt_file << "\t\t->center: " << getDetector(det_id).detectorCenter() << std::endl;
        txt_file << "\t\t->orientation: " << getDetector(det_id).orientation() << std::endl;
      }
    }
    txt_file << std::endl;
  }
  
  txt_file << "\nFOV points:\n";
  std::vector<ThreeVector> fov_points = CoincidenceSchemaMaxFOVPoints();
  for (const ThreeVector& vec : fov_points)
    txt_file << vec << " " << vec.r() << std::endl;
  
  
  ThreeVector v_max = *(std::max_element(fov_points.begin(),
                                         fov_points.end(),
                                         [](ThreeVector a, ThreeVector b)
                                         {return a.x() < b.x();}) );
  
  txt_file << "\nFOV maximum half width" << std::setw(10) << v_max.x() << std::endl;
  
  txt_file.close();
}

