/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "CylindricalGeometry.h"


ThreeVector
CylindricalGeometry::computeDetectorSurfaceCenter(uint16_t sector, uint16_t module, uint16_t block) const
{
  ThreeVector block_abs_center =  first_sector_center + modules_centers[module]
  + blocks_centers[block] - ThreeVector(desc.sector_len_X/2, 0, 0);
  block_abs_center.rotateZ( sectors_rotations[sector] );
  
  return block_abs_center;
}


ThreeVector
CylindricalGeometry::computeDetectorCenter(uint16_t sector, uint16_t module, uint16_t block) const
{
  ThreeVector block_abs_center =  first_sector_center + modules_centers[module]
  + blocks_centers[block];
  block_abs_center.rotateZ( sectors_rotations[sector] );
  
  return block_abs_center;
}


std::vector<ThreeVector>
CylindricalGeometry::computeCrystalsCenters(uint16_t sector,
                                                    uint16_t module,
                                                    uint16_t block
                                                   ) const
{
  std::vector<ThreeVector> centers;
  centers.reserve( desc.n_crystals_block );
  ThreeVector block_abs_center =  first_sector_center
  + modules_centers[module]
  + blocks_centers[block];
  for (uint16_t layer=0; layer < desc.n_layers; layer++) {
    uint16_t n_crystals = crystals_centers[layer].size();
    for (uint16_t crystal=0; crystal < n_crystals; crystal++) {
      ThreeVector temp =  block_abs_center + crystals_centers[layer][crystal];
      temp.rotateZ( sectors_rotations[sector] );
      centers.push_back(temp);
    }
    
  }
  
  return centers;
}


std::vector<ThreeVector>
CylindricalGeometry::computeCrystalsSurfaceCenters(uint16_t sector,
                                                   uint16_t module,
                                                   uint16_t block ) const
{
  std::vector<ThreeVector> centers;
  centers.reserve( desc.n_crystals_block );
  ThreeVector block_abs_center =  first_sector_center
  + modules_centers[module]
  + blocks_centers[block];
  for (uint16_t layer=0; layer < desc.n_layers; layer++) {
    SpaceType surface_x = -desc.layers[layer].crystalLengthX()/2;
    uint16_t n_crystals = crystals_centers[layer].size();
    for (uint16_t crystal=0; crystal < n_crystals; crystal++) {
      ThreeVector crystal_abs_center =  block_abs_center
      + crystals_centers[layer][crystal]
      + ThreeVector(surface_x,0,0) ;
      
      crystal_abs_center.rotateZ( sectors_rotations[sector] );
      centers.push_back(crystal_abs_center);
    }
  }
  
  return centers;
}

std::vector<ThreeVector>
CylindricalGeometry::computeVirtualCrystalsCenters(uint16_t sector,
                                                   uint16_t module,
                                                   uint16_t block) const
{
  uint16_t n_crystals = desc.v_layer.nCrystalsY() * desc.v_layer.nCrystalsZ();
  
  std::vector<ThreeVector> v_centers;
  ThreeVector block_abs_center =  first_sector_center
  + modules_centers[module]
  + blocks_centers[block];
  for (uint16_t crystal=0; crystal < n_crystals; crystal++) {
    ThreeVector crystal_abs_center =  block_abs_center + v_crystals_centers[crystal];
    crystal_abs_center.rotateZ( sectors_rotations[sector] );
    v_centers.push_back(crystal_abs_center);
  }
  
  return v_centers;
}

ThreeVector
CylindricalGeometry::computeDetectorOrientation(uint16_t sector,
                                                uint16_t module,
                                                uint16_t block) const
{
  const SpaceType& z_length = desc.block_len_Z;
  const SpaceType& y_length = desc.block_len_Y;
  
  //center
  ThreeVector block_abs_surf_center =  first_sector_center + modules_centers[module]
  + blocks_centers[block] - ThreeVector(desc.sector_len_X/2, 0, 0);
  block_abs_surf_center.rotateZ( sectors_rotations[sector] );
  
  //upper left corner
  ThreeVector block_ul_corner =  first_sector_center + modules_centers[module]
  + blocks_centers[block] + ThreeVector(-desc.sector_len_X/2, y_length/2, z_length/2);
  block_ul_corner.rotateZ( sectors_rotations[sector] );
  
  //down left corner
  ThreeVector block_dl_corner =  first_sector_center + modules_centers[module]
  + blocks_centers[block] + ThreeVector(-desc.sector_len_X/2, -y_length/2, z_length/2);
  block_dl_corner.rotateZ( sectors_rotations[sector] );
  
  //computing two vectors on the plane of the detector surface.
  ThreeVector vec1 = block_ul_corner - block_abs_surf_center;
  ThreeVector vec2 = block_dl_corner - block_abs_surf_center;
  
  ThreeVector normal_vector = vec1.cross(vec2);
  return normal_vector.unit();
}

ThreeMatrix
CylindricalGeometry::computeDetectorRotationMatrix(uint16_t sector) const
{
  ThreeMatrix matrix;
  matrix.setCol( ThreeVector(1,0,0).rotateZnotInPlace(sectors_rotations[sector]), 0);
  matrix.setCol( ThreeVector(0,1,0).rotateZnotInPlace(sectors_rotations[sector]), 1);
  matrix.setCol( ThreeVector(0,0,1), 2);

  return matrix;
}


void CylindricalGeometry::generateDetectors()
{
  const uint16_t& n_sectors = desc.n_sectors;
  const uint16_t& n_modules = desc.n_modules;
  const uint16_t& n_blocks = desc.n_blocks;
  
  for (uint16_t sec=0; sec<n_sectors; sec++) {
    for (uint16_t mod=0; mod<n_modules; mod++) {
      for (uint16_t blk=0; blk<n_blocks; blk++) {
        
        std::vector<uint16_t> hierarchy = {sec, mod, blk};
        detID_t id = computeDetectorID(hierarchy);
        ThreeVector size(desc.sector_len_X, desc.block_len_Y, desc.block_len_Z);
        PlanarDetectorType detector( id,
                                     computeDetectorCenter(sec, mod, blk),
                                     computeDetectorOrientation(sec, mod, blk),
                                     size);
        detector.setRotationMatrix( computeDetectorRotationMatrix(sec) );
        detector.setDetectorSurfaceCenter( computeDetectorSurfaceCenter(sec, mod, blk) );
        detector.setCrystalsCenters( computeCrystalsCenters(sec, mod, blk) );
        detector.setCrystalsSurfaceCenters( computeCrystalsSurfaceCenters(sec, mod, blk) );
        for (const PlanarLayerType& layer : desc.layers)
          detector.addLayer(layer);
        
        if (virtual_geometry)
          detector.setActivateVirtualLayer();
        
        if (desc.vl_present) {
          detector.setVirtualLayerPresent();
          detector.setVirtualLayer(desc.v_layer);
          detector.setVirtualCrystalsCenters( computeVirtualCrystalsCenters(sec, mod, blk) );
        }
               
        detectors.insert( {id, detector} );
      }
    }
  }
}



