/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "PlanarGeometry.h"

std::vector<ThreeVector>
PlanarGeometry::computeCrystalsCenters(uint16_t head, uint16_t module, uint16_t block) const
{
  std::vector<ThreeVector> centers;
  centers.reserve( desc.n_crystals_block );
  ThreeVector block_abs_center = modules_centers[module] + blocks_centers[block];
  for (uint16_t layer=0; layer < desc.n_layers; layer++) {
    uint16_t n_crystals = crystals_centers[layer].size();
    for (uint16_t crystal=0; crystal < n_crystals; crystal++) {
      ThreeVector temp =  block_abs_center + crystals_centers[layer][crystal];
      temp.rotateZ( M_PI*head );
      centers.push_back(temp);
    }
    
  }
  
  return centers;
}

std::vector<ThreeVector>
PlanarGeometry::computeCrystalsSurfaceCenters(uint16_t head, uint16_t module, uint16_t block) const
{
  std::vector<ThreeVector> centers;
  centers.reserve( desc.n_crystals_block );
  ThreeVector block_abs_center = modules_centers[module] + blocks_centers[block];
  for (uint16_t layer=0; layer < desc.n_layers; layer++) {
    SpaceType surface_x = -desc.layers[layer].crystalLengthX()/2;
    uint16_t n_crystals = crystals_centers[layer].size();
    for (uint16_t crystal=0; crystal < n_crystals; crystal++) {
      ThreeVector crystal_abs_center =  block_abs_center
      + crystals_centers[layer][crystal]
      + ThreeVector(surface_x,0,0) ;
      
      crystal_abs_center.rotateZ( M_PI*head );
      centers.push_back(crystal_abs_center);
    }
  }
  
  return centers;
}

std::vector<ThreeVector>
PlanarGeometry::computeVirtualCrystalsCenters(uint16_t head, uint16_t module, uint16_t block) const
{
  uint16_t n_crystals = desc.v_layer.nCrystalsY() * desc.v_layer.nCrystalsZ();
  
  std::vector<ThreeVector> v_centers;
  ThreeVector block_abs_center = modules_centers[module] + blocks_centers[block];
  for (uint16_t crystal=0; crystal < n_crystals; crystal++) {
    ThreeVector crystal_abs_center =  block_abs_center + v_crystals_centers[crystal];
    crystal_abs_center.rotateZ( M_PI*head );
    v_centers.push_back(crystal_abs_center);
  }
  
  return v_centers;
}

ThreeMatrix
PlanarGeometry::computeDetectorRotationMatrix(uint16_t sector) const
{
  ThreeMatrix matrix;
  matrix.setCol( ThreeVector(1,0,0).rotateZnotInPlace(sector*M_PI), 0);
  matrix.setCol( ThreeVector(0,1,0).rotateZnotInPlace(sector*M_PI), 1);
  matrix.setCol( ThreeVector(0,0,1), 2);
  
  return matrix;
}

void PlanarGeometry::generateDetectors()
{
  const uint16_t& n_modules = desc.n_modules;
  const uint16_t& n_blocks = desc.n_blocks;
    
  for (uint16_t head=0; head<2; head++) {
    for (uint16_t mod=0; mod<n_modules; mod++) {
      for (uint16_t blk=0; blk<n_blocks; blk++) {
        std::vector<uint16_t> hierarchy = {head, mod, blk};
        detID_t id = computeDetectorID(hierarchy);
        ThreeVector orientation = ThreeVector(1,0,0);
        orientation.rotateZ(M_PI*head);
        ThreeVector center = modules_centers[mod] + blocks_centers[blk];
        ThreeVector s_center = center - ThreeVector(desc.head_len_X/2,0,0);
        center.rotateZ(M_PI*head);
        s_center.rotateZ(M_PI*head);
        
        ThreeVector size(desc.head_len_X, desc.block_len_Y, desc.block_len_Z);
        PlanarDetectorType detector(id,
                                    center,
                                    orientation,
                                    size);
        detector.setRotationMatrix( computeDetectorRotationMatrix(head) );
        detector.setDetectorSurfaceCenter(s_center);
        detector.setCrystalsCenters( computeCrystalsCenters(head, mod, blk) );
        detector.setCrystalsSurfaceCenters( computeCrystalsSurfaceCenters(head, mod, blk) );
        for (const PlanarLayerType& layer : desc.layers)
          detector.addLayer(layer);
        
        if (virtual_geometry)
          detector.setActivateVirtualLayer();
        
        if (desc.vl_present) {
          detector.setVirtualLayerPresent();
          detector.setVirtualLayer(desc.v_layer);
          detector.setVirtualCrystalsCenters( computeVirtualCrystalsCenters(head, mod, blk) );
        }
        
        detectors.insert( {id, detector} );
      }
    }
  }
}

