/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "PlanarGeometry.h"
#include "symmetriesT.h"

void PlanarGeometry::generateCoincidenceSchema()
{
  const uint16_t& n_modules = desc.n_modules;
  const uint16_t& n_blocks = desc.block_grid_Y * desc.block_grid_Z;
  for (uint16_t mod1=0; mod1<n_modules; mod1++) {
    for (uint16_t blk1=0; blk1<n_blocks; blk1++) {
      for (uint16_t mod2=0; mod2<n_modules; mod2++) {
        for (uint16_t blk2=0; blk2<n_blocks; blk2++) {
          
          uint16_t d1 = computeDetectorID( {0, mod1, blk1} );
          uint16_t d2 = computeDetectorID( {1, mod2, blk2} );
          
          const ThreeVector& p1 = detectors.at(d1).detectorCenter();
          const ThreeVector& p2 = detectors.at(d2).detectorCenter();
          SpaceType distance = (p2-p1).r();
          
          bool found_symmetry = false;
          uint16_t sym_idx;
          SymmetryType sym;
          using CoincPairType = std::pair<detPair_t, CoincidencePairSym>;
          for (const CoincPairType& entry : sym_coinc_schema) {
            const CoincidencePairSym& pair = entry.second;
            
            if (pair.fundamental() && !found_symmetry) {
              
              ThreeVector c_p1 = detectors.at(pair.id1()).detectorCenter();
              ThreeVector c_p2 = detectors.at(pair.id2()).detectorCenter();
              
              SpaceType class_distance = (c_p2-c_p1).r();
              
              if ( fpAreEqual<SpaceType>(distance, class_distance, 1e-4)) {
                
                // equal distance, there should be a symmetry transformation
                ThreeVector shift1 = p1-c_p1;
                shift1.round(1e-4);
                ThreeVector shift2 = p2-c_p2;
                shift2.round(1e-4);
                
                if (shift1 == shift2) {
                  if (!shift1.x() ) { //avoid y or x traslation
                    // translation found, added to the class
                    found_symmetry = true;
                    sym_idx = entry.first;
                    sym.zShift( shift1.z() );
                    sym.yShift( shift1.y() );
                  }
                }
                else {
                  // look for a possible  reflections
                  //reflect both y and z coordinate
                  ThreeVector r_p1 = p1.componentDot(ThreeVector(1,-1,-1));
                  ThreeVector r_p2 = p2.componentDot(ThreeVector(1,-1,-1));
                  shift1 = r_p1-c_p1;
                  shift1.round(1e-4);
                  shift2 = r_p2-c_p2;
                  shift2.round(1e-4);
                  if (shift1 == shift2) {
                    if ( !shift1.x() ) {//avoid  x traslation
                      // reflection found, added to the class
                      found_symmetry = true;
                      sym_idx = entry.first;
                      if ( !fpAreEqual<SpaceType>(shift1.z(), 0, 1e-1) ) 
                        sym.zShift(-shift1.z());
                      sym.zReflect(true);
                      if ( !fpAreEqual<SpaceType>(shift1.y(), 0, 1e-1) ) 
                        sym.yShift(-shift1.y());
                      sym.yReflect(true);
                      break;
                    }
                  }
                  
                  
                  //TODO cercare di ottimizzare, ora i tre controlli son brutti
                  //reflect z coordinate
                  r_p1 = p1.componentDot(ThreeVector(1,1,-1));
                  r_p2 = p2.componentDot(ThreeVector(1,1,-1));
                  shift1 = r_p1-c_p1;
                  shift1.round(1e-4);
                  shift2 = r_p2-c_p2;
                  shift2.round(1e-4);
                  if (shift1 == shift2) {
                    if ( !shift1.x() ) {//avoid  x traslation
                      // reflection found, added to the class
                      found_symmetry = true;
                      sym_idx = entry.first;
                      if ( !fpAreEqual<SpaceType>(shift1.z(), 0, 1e-1) ) 
                        sym.zShift(-shift1.z());
                      if ( !fpAreEqual<SpaceType>(shift1.y(), 0, 1e-1) ) 
                        sym.yShift(shift1.y()); //only refl in z, no minus
                      sym.zReflect(true);
                      break;
                    }
                  }
                  
                  //reflect y coordinate
                  r_p1 = p1.componentDot(ThreeVector(1,-1,1));
                  r_p2 = p2.componentDot(ThreeVector(1,-1,1));
                  shift1 = r_p1-c_p1;
                  shift1.round(1e-4);
                  shift2 = r_p2-c_p2;
                  shift2.round(1e-4);
                  if (shift1 == shift2) {
                    if ( !shift1.x() ) {//avoid  x traslation
                      // reflection found, added to the class
                      found_symmetry = true;
                      sym_idx = entry.first;
                      if ( !fpAreEqual<SpaceType>(shift1.z(), 0, 1e-1) ) 
                        sym.zShift(shift1.z()); //only refl in y, no minus
                      if ( !fpAreEqual<SpaceType>(shift1.y(), 0, 1e-1) ) 
                        sym.yShift(-shift1.y());
                      sym.yReflect(true);
                      break;
                    }
                  }
                  

                }
              }
              //               else different distance
            }
            
          }
          
          CoincidencePairSym pair(d1, d2);
          if (!found_symmetry)
            pair.setFundamental();
          else {
            uint32_t s_pair = szudzikPair(sym_coinc_schema.at(sym_idx).id1(),
                                          sym_coinc_schema.at(sym_idx).id2());
            pair.setSymmetry(s_pair, sym);
          }
          sym_coinc_schema.insert( {pair.pairID(), pair} );
          
        }
      }
    }
  }
  
}
