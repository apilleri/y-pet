/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef GEOMETRIES_PLANARGEOMETRY_H_
#define GEOMETRIES_PLANARGEOMETRY_H_

#include "vGeometry.h"

class PlanarGeometry : public vGeometry {
  
  struct descriptionStruct {
    //! Half distance betwen the two heads of the scanner
    SpaceType half_heads_distance;
    
    // --- HEAD DESCRITPION

    //! Length of the head along the X-axis
    SpaceType head_len_X;
    //! Length of the head along the Y-axis
    SpaceType head_len_Y;
    //! Length of the head along the Z-axis
    SpaceType head_len_Z;
    //! Number of modules along the Y-axis
    uint16_t module_grid_Y;
    //! Number of modules along the Z-axis
    uint16_t module_grid_Z;
    //! Total number of modules in each head (Internal Computation)
    uint16_t n_modules;
    //! Offset between two adjacent modules along the Y-axis
    SpaceType module_offset_Y;
    //! Offset between two adjacent modules along the Z-axis
    SpaceType module_offset_Z;
    
    // --- MODULE DESCRIPTION
    
    //! Length of a module in along the Y-axis
    SpaceType module_len_Y;
    //! Length of a module in along the Y-axis
    SpaceType module_len_Z;
    //! Number of blocks in the Y direction in each module
    uint16_t block_grid_Y;
    //! Number of blocks in the Z direction in each module
    uint16_t block_grid_Z;
    //! Total number of blocks in each module (Internal Computation)
    uint16_t n_blocks;
    //! Offset between two adjacent blocks in the Y direction
    SpaceType block_offset_Y;
    //! Offset between two adjacent blocks in the Z direction
    SpaceType block_offset_Z;
    
    // --- BLOCK DESCRIPTION
    //! Length of a block in along the Y-axis
    SpaceType block_len_Y;
    //! Length of a block in along the Z-axis
    SpaceType block_len_Z;
    //! number of layers in each block
    uint16_t n_layers;
    //! number of crystals in each block
    uint16_t n_crystals_block;
    
    //! Layers description
    std::vector< PlanarLayerType > layers;
    
    // --- VIRTUAL LAYER DESCRIPTION
    bool vl_present;
    VirtualLayerType v_layer;
    
  } desc;
  
  //! Position of the centers of the modules of the rightmost (higher x) head.
  std::vector<ThreeVector> modules_centers;
  
  //! Position of the centers the blocks with respecto to one module.
  std::vector<ThreeVector> blocks_centers;
  
  //! Positions of the centers of the crystal of the layers with respect to one block.
  std::vector< std::vector<ThreeVector> > crystals_centers;
    
  //! Positions of the centers of the crystal of the virtual layer with respect to one block
  std::vector<ThreeVector>  v_crystals_centers;
  
public:
  explicit PlanarGeometry(std::string config, bool activate_vg);
  
  
  //! Estimate of the scanner size (useful for drawing the scanner)
  ThreeVector getScannerSize() const override
  {
    SpaceType x = (desc.half_heads_distance + desc.head_len_X)*2;
    SpaceType y = desc.head_len_Y;
    SpaceType z = desc.head_len_Z;
    return ThreeVector(x, y, z);
  }
  
  cryID_t computeCrystalID(const std::vector<uint16_t>& hierarchy) const override;
  
  detID_t computeDetectorID(const std::vector<uint16_t>& hierarchy) const override;
  
  std::vector<uint16_t> explodeCrystalID(cryID_t unique_ID) const override;
  
  std::vector<uint16_t> explodeDetectorID(detID_t unique_ID) const override;
  

  std::vector<ThreeVector> CoincidenceSchemaMaxFOVPoints() const override;
  
  void printGeometryInfo(std::string output_dir) const override;
  
private:
  // Start DETECTOR SECTION
  std::vector<ThreeVector>
  computeCrystalsCenters(uint16_t head, uint16_t module, uint16_t block) const;
  
  std::vector<ThreeVector>
  computeCrystalsSurfaceCenters(uint16_t head, uint16_t module, uint16_t block) const;
  
  std::vector<ThreeVector>
  computeVirtualCrystalsCenters(uint16_t head, uint16_t module, uint16_t block) const;
  
  ThreeMatrix
  computeDetectorRotationMatrix(uint16_t sector) const;
  
  void generateDetectors();
  // End DETECTOR SECTION
  
  // Start SYM SECTION
  void generateCoincidenceSchema();
  // End SYM SECTION

};

#endif // GEOMETRIES_PLANARGEOMETRY_H_
