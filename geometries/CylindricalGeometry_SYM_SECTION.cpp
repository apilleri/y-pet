/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "CylindricalGeometry.h"

//#define DEBUG_SECTORS

void CylindricalGeometry::generateCoincidenceSchema()
{
  std::vector<CoincidencePairSym> sym_sectors;
  // FIRST PASS: computing all the possible coincidence sectors
  const uint16_t& n_sectors = desc.n_sectors;
  for (uint16_t sec1=0; sec1 < n_sectors; sec1++) {
    uint16_t start_sec2 = (sec1 + n_sectors/2 - desc.coincidence_sector_step);
    uint16_t stop_sec2 = (sec1 + n_sectors/2);// + coincidence_sector_step);
    for (uint16_t sec2=start_sec2; sec2 <= stop_sec2; sec2++) {
      
      uint16_t checked_sec2 = sec2%n_sectors;
      CoincidencePairSym pair(sec1, checked_sec2);
      std::vector<CoincidencePairSym>::iterator it;
      it = find (sym_sectors.begin(), sym_sectors.end(), pair);
      if ( it == sym_sectors.end() ) 
        sym_sectors.push_back(pair);
      
    }
  }

    
  
  // SECOND PASS: computing simmetries between sectors
  // assuming the first sector pair as fundamental
  sym_sectors[0].setFundamental();
  bool found_symmetry;
  
  for (uint16_t i=1; i<sym_sectors.size(); i++) {
    found_symmetry = false;
    SymmetryType sym;
    ThreeVector pos1 = first_sector_center;
    pos1.rotateZ(sectors_rotations[ sym_sectors[i].id1() ]);
    ThreeVector pos2 = first_sector_center;
    pos2.rotateZ(sectors_rotations[ sym_sectors[i].id2() ]);
    SpaceType dist = (pos2-pos1).r();
    
    for (uint16_t j=0; j<i; j++) {
      ThreeVector class_pos1 = first_sector_center;
      class_pos1.rotateZ(sectors_rotations[ sym_sectors[j].id1() ]);
      ThreeVector class_pos2 = first_sector_center;
      class_pos2.rotateZ(sectors_rotations[ sym_sectors[j].id2() ]);
      SpaceType class_dist = (class_pos2-class_pos1).r();
      
      if ( fpAreEqual<SpaceType>(class_dist, dist, 1e-4) ) {
        found_symmetry = true;

        ThreeVector rot_ang = computeRotationAngles(class_pos2-class_pos1, pos2-pos1);
        // in the case of a cylindrical scanner we must have E1=0 and E2=0
        // as the rotation around the x and y axis are forbidden. The E3 angle 
        // is the angle of rotation of the vector connecting the first and 
        // the second sectors (pointing towward the second). This angle is
        // the opposite of the angle of rotation of sector pair using the
        // convention of positive angles for counter clock-wise rotations
        // (this is the reason of the minus in the formula above E3 = -...)
        // we don't care about the signs of E1 and E2 as they should be zero.
        
        if (!fpAreEqual<AngleType>(rot_ang.x(), 0, 1e-3))
          throw std::string("CylindricalGeometry::genSchema rotation around the x axis!");
        if (!fpAreEqual<AngleType>(rot_ang.y(), 0, 1e-3))
          throw std::string("CylindricalGeometry::genSchema rotation around the y axis!");
        
        AngleType rotation = std::round(rot_ang.z()*180/M_PI*100)/100; //2 decimal points
        
        pos2.rotateZ( -rot_ang.z() );
        pos1.rotateZ( -rot_ang.z() );
        SpaceType x2_shift = (pos2 - class_pos2).x();
        SpaceType y2_shift = (pos2 - class_pos2).y();
        SpaceType x1_shift = (pos1 - class_pos1).x();
        SpaceType y1_shift = (pos1 - class_pos1).y();
        
        if ( !fpAreEqual<SpaceType>(x1_shift, x2_shift, 1e-3) || !fpAreEqual<SpaceType>(y1_shift, y2_shift, 1e-3) ) {
          std::cout << x1_shift << " " << x2_shift << " " << y1_shift << " " << y2_shift << std::endl;
          throw std::string("CylindricalGeometry::genSchema not uniform residual translation!");
        }
        
        //the length is the same so it's enough to check the shift of only one coordinate
        if ( !fpAreEqual<SpaceType>(x1_shift, 0, 1e-3) ) 
          rotation += 180;
        
        if (rotation < 0) 
          rotation += 360;
        /* si inseriscono le riflessioni anche rispetto a x e y. Una rilessione rispetto
          * ad x e y corrisponde ad una rotazione di 180 gradi. In questo modo si riducono 
          * gli angoli di rotazione delle viste della metà, in quanto si prendono solo
          * angoli nell'intervallo [0, 180). Questoè utile in quanla rotazione è 
          * molto più onerosa della riflessione
          */
        if (rotation >= 180) {
          rotation -= 180;
          sym.xReflect(true);
          sym.yReflect(true);
        }
        
        sym.zAngle(rotation);
        detPair_t sym_id = szudzikPair(sym_sectors[j].id1(),sym_sectors[j].id2());
        sym_sectors[i].setSymmetry(sym_id, sym);
        break;
      }
      
    }
    if (!found_symmetry) 
      sym_sectors[i].setFundamental();
    
  }
  
#ifdef DEBUG_SECTORS
  std::ofstream txt_file;
  txt_file.open(_name + "_symmetries.txt");
  txt_file << "sym sectors" << std::endl;
  txt_file << std::setw(5) << "ID1";
  txt_file << std::setw(5) << "ID2";
  txt_file << std::setw(3) << "F";
  txt_file << std::setw(8) << "zAng";
  txt_file << std::setw(3) << "xR";
  txt_file << std::setw(3) << "yR";
  txt_file << std::endl;
  for (const CoincidencePairSym& entry : sym_sectors) {
    txt_file << std::setw(5) << entry.id1();
    txt_file << std::setw(5) << entry.id2();
    txt_file << std::setw(3) << entry.fundamental();
    txt_file << std::setw(8) << entry.sym().zAngle();
    txt_file << std::setw(3) << entry.sym().xReflect();
    txt_file << std::setw(3) << entry.sym().yReflect();
    txt_file << std::endl;
  }
#endif
  
  
  // THIRD PASS: computing symmtries between blocks in a single sector (all sectors
  // have the same symmetries between blocks;

  for (auto s : sym_sectors) {
    std::vector<CoincidencePairSym> sym_blocks = detectSymmetricBlocks(s.id1(), s.id2());
    
    for (auto b : sym_blocks) {
    
      if ( s.fundamental() ) 
        sym_coinc_schema.insert( {b.pairID(), b} );
      else {
        CoincidencePairSym pair(b.id1(), b.id2());
        SymmetryType sym;
        
        sym.zAngle( s.sym().zAngle() );
        sym.xReflect( s.sym().xReflect() );
        sym.yReflect( s.sym().yReflect() );
        sym.zShift( b.sym().zShift() );
        sym.zReflect( b.sym().zReflect() );
        
        uint16_t s_s1, s_s2;
        std::tie(s_s1, s_s2) = szudzikUnPair( s.symID() );
        
        detID_t s_d1, s_d2;
        std::tie(s_d1, s_d2) = szudzikUnPair( b.symID() );
        std::vector<uint16_t> es_d1 = explodeDetectorID(s_d1);
        std::vector<uint16_t> es_d2 = explodeDetectorID(s_d2);
        s_d1 = computeDetectorID( {s_s1, es_d1[1], es_d1[2]} );
        s_d2 = computeDetectorID( {s_s2, es_d2[1], es_d2[2]} );
        pair.setSymmetry( szudzikPair(s_d1, s_d2), sym);
        sym_coinc_schema.insert( {pair.pairID(), pair} );
      }
      
    }
  }
  
}


std::vector<CoincidencePairSym>
CylindricalGeometry::detectSymmetricBlocks(uint16_t sec1, uint16_t sec2) const
{
  std::vector<CoincidencePairSym> sym_blocks;
  
  const uint16_t& n_modules = desc.n_modules;
  const uint16_t& n_blocks = desc.block_grid_Y * desc.block_grid_Z;
  for (uint16_t mod1=0; mod1<n_modules; mod1++) {
    for (uint16_t blk1=0; blk1<n_blocks; blk1++) {
      for (uint16_t mod2=0; mod2<n_modules; mod2++) {
        for (uint16_t blk2=0; blk2<n_blocks; blk2++) {
          
          uint16_t d1 = computeDetectorID( {sec1, mod1, blk1} );
          uint16_t d2 = computeDetectorID( {sec2, mod2, blk2} );
          
          const ThreeVector& p1 = detectors.at(d1).detectorCenter();
          const ThreeVector& p2 = detectors.at(d2).detectorCenter();
          SpaceType distance = (p2-p1).r();
          
          bool found_symmetry = false;
          uint16_t sym_idx;
          SymmetryType sym;
          for (uint16_t i=0; i<sym_blocks.size(); i++) {
            if (!found_symmetry) {
              ThreeVector c_p1 = detectors.at(sym_blocks[i].id1()).detectorCenter();
              ThreeVector c_p2 = detectors.at(sym_blocks[i].id2()).detectorCenter();
              
              SpaceType class_distance = (c_p2-c_p1).r();
              
              if ( fpAreEqual<SpaceType>(distance, class_distance, 1e-4)) {
                // equal distance, there should be a symmetry transformation
                ThreeVector shift1 = p1-c_p1;
                shift1.round(1e-4);
                ThreeVector shift2 = p2-c_p2;
                shift2.round(1e-4);
                
                if (shift1 == shift2) {
                  if (!shift1.y() && !shift1.x() ) { //avoid y or x traslation
                    // translation found, added to the class
                    found_symmetry = true;
                    sym_idx = i;
                    sym.zShift(shift1.z());
                  }
                }
                else {
                  // look for a possible Z reflection
                  //reflect z coordinate
                  ThreeVector r_p1 = p1.componentDot(ThreeVector(1,1,-1));
                  ThreeVector r_p2 = p2.componentDot(ThreeVector(1,1,-1));
                  
                  shift1 = r_p1-c_p1;
                  shift1.round(1e-4);
                  shift2 = r_p2-c_p2;
                  shift2.round(1e-4);
                  
                  if (shift1 == shift2) {
                    if (!shift1.y() && !shift1.x() ) {//avoid y or x traslation
                      // reflection found, added to the class
                      found_symmetry = true;
                      sym_idx = i;
                      if ( !fpAreEqual<SpaceType>(shift1.z(), 0, 1e-1) )
                        sym.zShift(-shift1.z());
                      sym.zReflect(true);
                    }
                  }

                }
              }
              //               else different distance
            }
            //             else already added
          }
          
          CoincidencePairSym pair(d1, d2);
          if (!found_symmetry)
            pair.setFundamental();
          else {
            uint32_t s_pair = szudzikPair(sym_blocks[sym_idx].id1(),sym_blocks[sym_idx].id2());
            pair.setSymmetry(s_pair, sym);
          }
          sym_blocks.push_back(pair);
        }
      }
    }
  }

  return sym_blocks;
}
