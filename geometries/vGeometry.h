/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */


#ifndef GEOMETRIES_VGEOMETRY_H_
#define GEOMETRIES_VGEOMETRY_H_

#include <iostream>
#include <iomanip>
#include <cstdint>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <map>
#include <algorithm>
#include <functional>

#include "tinyxml2.h"
using namespace tinyxml2;

#include "dataTypes.h"
#include "f_utils.h"
#include "euclideanSpace.h"
#include "PlanarDetector.h"
#include "symmetriesT.h"
#include "modelT.h"
#include "HistogramsMap.h"

/*! \class vGeometry
 *  \brief Representation of a generic Scanner
 *
 *  The vGeometry class provides the methods used to extract information about
 *  the absolute positions and orientations of the crystals of the scanner.
 *
 *  The fundamental concept of the software is the creations of planar detectors.
 *  Each geometry must provide a list of detector pairs that are used to compute
 *  the model and to reconstruct the image. 
 *  
 *  The derived class must define all the methods needed to correct initialize 
 *  a Detector class, that are:
 *    - position of the surface center of all the crystals of the detector
 *    - types of the crystals
 *    - list of different types.
 * 
 *  The derived class must define two methods which returns two lists of detector
 *  pairs. It is supposed that the scanner has intrinsic immediately exploitable 
 *  symmetries that can be exploited. The detector pairs are then subdived into
 *  "real" and "symmetric" (a symmetry may be "rotational" or "translational" TODO).
 *
 *  The first list of detector pairs, defined by the type DetectorPairList, 
 *  contains the "real" detector pairs as type DetectorPair (so the position of
 *  the crystals are present). These list is used to compute the model.
 *  -> virtual DetectorPairList generateFundamentalDetectorPairsList() = 0;
 *
 *  The second list contains all the possible combinations of detectors in the
 *  form of SymPair. This list is used by the reconstruction. The two lists are parallel.
 *  -> virtual SymSchema computeFullDetectorPairsList() = 0;
 *
 *
 *  Crystals and Detectors are identified by a unique index:
 *    uint32_t unique_crystal_ID
 *    uint16_t unique_detector_ID
 *  The definition of these numbers depends on the actual real geometry that 
 *  inherits from vGeometry. As the geometries may have different levels, these
 *  methods do not share a common interface among all the geometries.
 *  It is assumed that a scanner has less than
 *  -> 2**32 - 1 = 4294967295 crystals
 *  -> 2**16 - 1 = 65535 detectors
 * 
 * TODO sistemare
 */
class vGeometry {
  
  public:
  static vGeometry* initScanner(std::string xml_config, bool activate_vg);
  
  protected:
  //! scanner configuration file
  std::string xml_config;

  //! scanner name
  std::string _name;

  //! scanner type
  std::string _type;
  
  DetectorsList detectors;
  bool virtual_geometry;
  
  CoincidenceSchema sym_coinc_schema;
  
  GroupedCS grouped_cs;
  
  ViewsList unique_views;
  
 public:
   vGeometry(bool activate_vg)
   : xml_config(""), _name(""), _type(""), virtual_geometry(activate_vg)
  { }

  virtual ~vGeometry()
  { };

  std::string name() const
  {
    return _name;
  }

  std::string type() const
  {
    return _type;
  }

  bool virtualGeometry() const
  {
    return virtual_geometry;
  }
  
  lorIDX_t nLORs() const
  {
    lorIDX_t n_lors = 0;
    
    for (const std::pair<const detPair_t, CoincidencePairSym>& pair : sym_coinc_schema) {
      const PlanarDetectorType& det1 = detectors.at( pair.second.id1() );
      const PlanarDetectorType& det2 = detectors.at( pair.second.id2() );
      
      n_lors += det1.aCryN() * det2.aCryN();
    }
    
    return n_lors;
  }
  
  lorIDX_t nLOFs() const
  {
    lorIDX_t n_lofs = 0;
    
    for (const std::pair<const detPair_t, CoincidencePairSym>& pair : sym_coinc_schema) {
      const PlanarDetectorType& det1 = detectors.at( pair.second.id1() );
      const PlanarDetectorType& det2 = detectors.at( pair.second.id2() );
      
      n_lofs += det1.vCryN() * det2.vCryN();
    }
    
    return n_lofs;
  }
  
  const PlanarDetectorType& getDetector(detID_t id) const
  {
    return detectors.at(id);
  }
  
  const DetectorsList& getDetectorsList() const
  {
    return detectors;
  }
  
  const CoincidenceSchema& getCoincidenceSchema() const
  {
    return sym_coinc_schema;
  }
  
  const GroupedCS& getGroupedCS() const
  {
    return grouped_cs;
  }
  
  const ViewsList& getUniqueViews() const
  {
    return unique_views;
  }
  
  virtual std::vector<ThreeVector> CoincidenceSchemaMaxFOVPoints() const = 0;
  
  //! Scanner radius: face-toface opposite detecotr pair distance
  virtual ThreeVector getScannerSize() const = 0;
  
  virtual cryID_t computeCrystalID(const std::vector<uint16_t>& hierarchy) const = 0;
  
  virtual detID_t computeDetectorID(const std::vector<uint16_t>& hierarchy) const = 0;
  
  virtual std::vector<uint16_t> explodeCrystalID(cryID_t unique_ID) const = 0;
  
  virtual std::vector<uint16_t> explodeDetectorID(detID_t unique_ID) const = 0;
    
  virtual void printGeometryInfo(std::string output_dir) const = 0;
  
  void printSymInfo(std::string output_dir) const;
  
  void genereateUniqueViews();

  void generateGroupedCS();
  
  std::vector<AngleType> computeUniqueAngles();
  
  std::tuple<detID_t, detCryID_t, detID_t, detCryID_t>
  getLORFullInfo(detPair_t d_pair, lorIDX_t rel_lor) const;
  
  SubsetsList generateLorList(HistogramsMap& hist,
                              uint16_t n_subsets,
                              uint32_t seed);

  
  void reflectHistograms(HistogramsMap& histogram, std::string h_type) const;


};

#endif  // GEOMETRIES_VGEOMETRY_H_
