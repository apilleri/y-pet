/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef GEOMETRIES_CYLINDRICALGEOMETRY_H_
#define GEOMETRIES_CYLINDRICALGEOMETRY_H_

#include "vGeometry.h"

/*! \class CylindricalGeometry
 *  \brief Representation of a Cylindrical PET Scanner
 *
 *  TODO rifare descrizione
 * 
 */
class CylindricalGeometry : public vGeometry {

  /*! \class descriptionStruct
   *  \brief Contains the description of the scanner extracted from the XML file
   *
   *  It is used by the CylindricalGeometry class as a container for all the
   *  information about the scanner. It is "easier" to implement a method to get
   *  acces only to the descriptionStruct instead of implementing a method for every
   *  variable of the desc.
   */
  struct descriptionStruct {
    //! Internal radius of the cylinder
    SpaceType cylinder_radius;
    //! Number of ring sectors in which the scanner is subdivided
    uint16_t n_sectors;
    
    uint16_t coincidence_sector_step;

    // --- RING SECTOR DESCRITPION
    
    //! Length of a sector along the X-axis
    SpaceType sector_len_X;
    // sector_len_Y is equal to module_len_Y
    //! Length of a sector along the Z-axis
    SpaceType sector_len_Z;
    //! Number of modules in each ring sector
    uint16_t n_modules;
    //! Offset between the centers of  two adjacent modules
    SpaceType modules_offset;
    
    // --- MODULE DESCRIPTION
    
    //! Length of a module in along the Y-axis
    SpaceType module_len_Y;
    //! Length of a module in along the Y-axis
    SpaceType module_len_Z;
    //! Number of blocks in the Y direction in each module
    uint16_t block_grid_Y;
    //! Number of blocks in the Z direction in each module
    uint16_t block_grid_Z;
    //! Total number of blocks in each module (Internal Computation)
    uint16_t n_blocks;
    //! Offset between the centers of two adjacent blocks in the Y direction
    SpaceType block_offset_Y;
    //! Offset between the centers of two adjacent blocks in the Z direction
    SpaceType block_offset_Z;
    
    // --- BLOCK DESCRIPTION
    //! Length of a block in along the Y-axis
    SpaceType block_len_Y;
    //! Length of a block in along the Z-axis
    SpaceType block_len_Z;
    //! number of layers in each block
    uint16_t n_layers;
    //! number of crystals in each block
    uint16_t n_crystals_block;
    
    //! Layers description
    std::vector< PlanarLayerType > layers;
    
    // --- VIRTUAL LAYER DESCRIPTION
    bool vl_present;
    VirtualLayerType v_layer;
    
  } desc;
  
  // --- BASE POSITIONS OF THE SCANNER ELEMENTS
  //  Each object position is relative to the parent object which contains the
  //  object itself. The storing order is relative to the ordering defined by the
  //  parameters:
  //  rotation_direction, principal_axis, secondary_axis
  //  principal_axis_direction, secondary_axis_direction
  // 
  //  Only the positions of the objects relative to the first sector are stored.
  //  The others position are computed on-the-fly when needed, by using the
  //  virtual methods 
  //  computeCrystalSurfaceCenter()
  //  computeCrystalsCenters() ;
  //  computeCrystalsSurfaceCenters() ;
  //  computeDetectorCrystalTypes() ;

  
  //! position of the first sector (angle = 0)
  ThreeVector first_sector_center;
  
  //! rotation angle with respect to the Z-axis in the X-Y plane
  std::vector<AngleType> sectors_rotations;
  
  //! Position of the centers of the modules with respect to the first rSector.
  std::vector<ThreeVector> modules_centers;
  
  //! Position of the centers the blocks with respecto to one module.
  std::vector<ThreeVector> blocks_centers;
  
  //! Positions of the centers of the crystal of the layers with respect to one block.
  //! One vector for each layer
  std::vector< std::vector<ThreeVector> > crystals_centers;
  
  //! Positions of the centers of the crystal of the virtual layer with respect to one block
  std::vector<ThreeVector>  v_crystals_centers;
  
public:
  explicit CylindricalGeometry(const std::string& config, bool activate_vg);

  
  //! Estimate of the scanner size (useful for drawing the axes)
  ThreeVector getScannerSize() const override
  {
    SpaceType x = (desc.cylinder_radius + desc.sector_len_X)*2;
    SpaceType y = x;
    SpaceType z = desc.sector_len_Z;
    return ThreeVector(x, y, z);
  }
  
  uint16_t getCoincidenceSectorStep() const
  {
    return desc.coincidence_sector_step;
  }
  
  
  // --- LOR methods
  uint32_t crystals2LOR(uint16_t cry1, uint16_t cry2) const
  {
    return (cry1 * desc.n_crystals_block + cry2);
  }
  
  std::tuple<uint16_t, uint16_t> LOR2crystals(uint32_t lor) const
  {
    uint16_t cry2 = lor % desc.n_crystals_block;
    uint16_t cry1 = (lor - cry2) / desc.n_crystals_block;
    return std::make_tuple(cry1, cry2);
    
  }
  
  uint16_t getSectorsNumber() const
  {
    return desc.n_sectors;
  }
  
  cryID_t computeCrystalID(const std::vector<uint16_t>& hierarchy) const override;
  
  detID_t computeDetectorID(const std::vector<uint16_t>& hierarchy) const override;
  
  std::vector<uint16_t> explodeCrystalID(cryID_t unique_ID) const override;
  
  std::vector<uint16_t> explodeDetectorID(detID_t unique_ID) const override;


  std::vector<ThreeVector> CoincidenceSchemaMaxFOVPoints() const override;

private:
  // Start DETECTOR SECTION
  ThreeVector
  computeDetectorSurfaceCenter(uint16_t sector, uint16_t module, uint16_t block) const;
  
  ThreeVector
  computeDetectorCenter(uint16_t sector, uint16_t module, uint16_t block) const;
  
  std::vector<ThreeVector>
  computeCrystalsCenters(uint16_t sector, uint16_t module, uint16_t block) const;
  
  std::vector<ThreeVector>
  computeCrystalsSurfaceCenters(uint16_t sector, uint16_t module, uint16_t block) const;
  
  std::vector<ThreeVector>
  computeVirtualCrystalsCenters(uint16_t sector, uint16_t module, uint16_t block) const;
  
  ThreeVector
  computeDetectorOrientation(uint16_t sector, uint16_t module, uint16_t block) const;
  
  ThreeMatrix
  computeDetectorRotationMatrix(uint16_t sector) const;
  
  void generateDetectors();
  // End DETECTOR SECTION
  
  // Start SYM SECTION
  void generateCoincidenceSchema();
  
  std::vector<CoincidencePairSym>
  detectSymmetricBlocks(uint16_t sec1, uint16_t sec2) const;
  // End SYM SECTION

  void printGeometryInfo(std::string output_dir) const override;
  
};


#endif  // GEOMETRIES_CYLINDRICALGEOMETRY_H_

