/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <random>
#include <algorithm>
#include <iterator>

#include "timer.hpp"

#include "vGeometry.h"
#include "CylindricalGeometry.h"
#include "PlanarGeometry.h"


vGeometry* vGeometry::initScanner(std::string xml_config, bool activate_vg)
{

  std::string type = extractScannerType(xml_config);
  
  if (type == "CYLINDRICAL") {
    std::cout << "CYLINDRICAL scanner initialized. " << std::endl;
    return  new CylindricalGeometry(xml_config, activate_vg);
    
  }
  else if (type == "PLANAR") {
    std::cout << "PLANAR scanner initialized. " << std::endl;
    return new PlanarGeometry(xml_config, activate_vg);
    
  }
  else
    throw std::string("vGeometry::initScanner unknown scanner type: " + type);
}

void vGeometry::genereateUniqueViews()
{
  //aliases
  CoincidenceSchema& cs = sym_coinc_schema;
  ViewsList& uv = unique_views;
  
  for (CoincidenceSchema::iterator it=cs.begin(); it!=cs.end(); ++it) {
    const SymmetryType& sym = it->second.sym();
    
    ViewsList::iterator f = std::find(uv.begin(), uv.end(), sym);
    if ( f != uv.end() ) {
      uint16_t pos = std::distance(uv.begin(), f);
      it->second.setUniqueView( pos );
    } else {
      uv.push_back(sym);
      it->second.setUniqueView( uv.size() - 1 );
    }
  }

}

std::vector<AngleType> vGeometry::computeUniqueAngles()
{
  std::vector<AngleType> unique_angles;
  for (const SymmetryType& v : unique_views ) {
    std::vector<AngleType>::iterator it;
    it = std::find(unique_angles.begin(), unique_angles.end(), v.zAngle() );
    if ( it == unique_angles.end() )
      unique_angles.push_back( v.zAngle() );
  }
  
  return unique_angles;
}


void vGeometry::generateGroupedCS()
{
  using csPair = std::pair<detPair_t, CoincidencePairSym>;
  //retrieve fundamental pairs:
  for (const csPair& entry : sym_coinc_schema) {
    const CoincidencePairSym& pair = entry.second;
    if ( pair.fundamental() ) {
      std::vector<CoincidencePairSym> temp_v;
      temp_v.push_back(pair);
      grouped_cs.push_back(temp_v);
    }
  }
  
  //filling vectors
  for (const csPair& entry : sym_coinc_schema) {
    const CoincidencePairSym& pair = entry.second;
    if ( !pair.fundamental() ) {
      for (uint16_t i=0; i<grouped_cs.size(); i++) {
        if ( pair.symID() == grouped_cs[i][0].pairID() ) {
          grouped_cs[i].push_back(pair);
          break;
        }
      }
    }
  }
  
}


std::tuple<detID_t, detCryID_t, detID_t, detCryID_t>
vGeometry::getLORFullInfo(detPair_t d_pair, lorIDX_t rel_lor) const
{
  
  detID_t det1, det2;
  std::tie(det1, det2) = szudzikUnPair(d_pair);
  
  detCryID_t cry2 = rel_lor % detectors.at(det2).aCryN();
  detCryID_t cry1 = (rel_lor - cry2) / detectors.at(det2).aCryN();
  
  return std::make_tuple(det1, cry1, det2, cry2);
}

SubsetsList
vGeometry::generateLorList(HistogramsMap& hist, uint16_t n_subsets, uint32_t seed)
{

  
  
  CountsVector lor_list;
  lor_list.reserve( hist.notNull() );
  for (data_type::iterator it = hist.data.begin(); it != hist.data.end(); ++it) {
    detID_t det1, det2;
    std::tie(det1, det2) = szudzikUnPair(it->first);
    for(lorIDX_t cur_lor=0; cur_lor<it->second.size(); cur_lor++) {
      FloatType& value = it->second[cur_lor];
      if (value) {
        detCryID_t cry2 = cur_lor % detectors[det2].aCryN();
        detCryID_t cry1 = (cur_lor - cry2) / detectors[det2].aCryN();
        lor_list.push_back( CountsType(det1, cry1, det2, cry2, value) );
      }
    }
  }
  
  SubsetsList subsets;
  if (n_subsets > 1) {
    std::shuffle (lor_list.begin(), lor_list.end(), std::default_random_engine(seed));
    // create_subsets;
    lorIDX_t lors_per_subset = hist.notNull() / n_subsets;
    uint16_t s_sub;
    for (s_sub=0; s_sub<n_subsets-1; s_sub++) {
      CountsVector temp = CountsVector(lor_list.begin()+lors_per_subset*s_sub,
                                       lor_list.begin()+lors_per_subset*(s_sub+1) );
      subsets.push_back(temp);
    }
    CountsVector temp = CountsVector(lor_list.begin()+lors_per_subset*s_sub,
                                     lor_list.end() );
    subsets.push_back(temp);
  }
  else
    subsets.push_back(lor_list);
  
  std::cout << "vGeometry::generateLorList" << std::endl;
  for (uint16_t s_sub=0; s_sub<n_subsets; s_sub++) {
    std::cout << "\tSubset: " << s_sub+1 << " lors: " << subsets[s_sub].size();
    std::cout << std::endl;
  }
  
  return subsets;
}


void vGeometry::reflectHistograms(HistogramsMap& histogram, std::string h_type) const
{
  if (h_type != "LOFs" && h_type != "LORs")// && h_type != "aLORs")
    throw std::string("vGeometry::reflectHistograms unknown histogram type");
  
  timer msec_timer("msec");
  msec_timer.start("Reflecting Histograms", "", true);
  
  using PairType = std::pair<detPair_t, CoincidencePairSym>;
  for (const PairType entry : sym_coinc_schema) {
    const CoincidencePairSym& pair = entry.second;
    
    bool xR = pair.sym().xReflect();
    bool yR = pair.sym().yReflect();
    bool zR = pair.sym().zReflect();
    
    SpaceType yS = pair.sym().yShift();
    SpaceType zS = pair.sym().zShift();
    
    if ( (yR && !xR) || zR ) {
      detID_t d1 = pair.id1();
      detID_t d2 = pair.id2();
      detPair_t f_id = pair.symID();
      detID_t f_d1, f_d2;
      std::tie(f_d1, f_d2) = szudzikUnPair(f_id);
      
      uint16_t crystals_n1, crystals_n2;
      std::vector<ThreeVector> cry_list1, cry_list2;
      std::vector<ThreeVector> f_cry_list1, f_cry_list2;
      
      if (h_type == "LOFs") {
        crystals_n1 = getDetector(d1).vCryN();
        crystals_n2 = getDetector(d2).vCryN();
        cry_list1 = getDetector(d1).vCryCenters();
        cry_list2 = getDetector(d2).vCryCenters();
        f_cry_list1 = getDetector(f_d1).vCryCenters();
        f_cry_list2 = getDetector(f_d2).vCryCenters();
      } else if (h_type == "LORs") {
        crystals_n1 = getDetector(d1).rCryN();
        crystals_n2 = getDetector(d2).rCryN();
        cry_list1 = getDetector(d1).rCrySurfaceCenters();
        cry_list2 = getDetector(d2).rCrySurfaceCenters();
        f_cry_list1 = getDetector(f_d1).rCrySurfaceCenters();
        f_cry_list2 = getDetector(f_d2).rCrySurfaceCenters();
      }
//       else {
//         crystals_n1 = getDetector(d1).aCryN();
//         crystals_n2 = getDetector(d2).aCryN();
//         cry_list1 = getDetector(d1).aCrySurfaceCenters();
//         cry_list2 = getDetector(d2).aCrySurfaceCenters();
//         f_cry_list1 = getDetector(f_d1).aCrySurfaceCenters();
//         f_cry_list2 = getDetector(f_d2).aCrySurfaceCenters();
//       }
      
      
//       std::vector<ThreeVector> cry_list1 = getDetector(d1).rCrySurfaceCenters();
//       std::vector<ThreeVector> cry_list2 = getDetector(d2).rCrySurfaceCenters();
//       uint16_t crystals_n1 = getDetector(d1).aCryN();
//       uint16_t crystals_n2 = getDetector(d2).aCryN();
//       
//       const std::vector<ThreeVector>& f_cry_list1 = getDetector(f_d1).rCrySurfaceCenters();
//       const std::vector<ThreeVector>& f_cry_list2 = getDetector(f_d2).rCrySurfaceCenters();
      
      ThreeVector reflection;
      AngleType start_angle = 0;
      if (xR && yR) {
        start_angle = 180;
        reflection = ThreeVector(1, 1, 1-2*zR );
      }
      else {
        reflection = ThreeVector(1, 1-2*yR, 1-2*zR );
      }
      
      AngleType angle = (start_angle + pair.sym().zAngle())*M_PI/180;
      
      #pragma omp parallel for
      for (uint16_t i=0; i<crystals_n1; i++) {
        cry_list1[i] = cry_list1[i].componentDot(reflection);
        cry_list1[i].rotateZ( -angle );
        cry_list1[i] = cry_list1[i] - ThreeVector(0, yS, zS).componentDot(reflection);
      }
      #pragma omp parallel for
      for (uint16_t i=0; i<crystals_n2; i++) {
        cry_list2[i] = cry_list2[i].componentDot(reflection);
        cry_list2[i].rotateZ( -angle );
        cry_list2[i] = cry_list2[i] - ThreeVector(0, yS, zS).componentDot(reflection);
      }

      std::vector<uint16_t> crystals_map1;
      crystals_map1.resize(crystals_n1, 0);
      std::vector<uint16_t> crystals_map2;
      crystals_map2.resize(crystals_n2, 0);
      
      #pragma omp parallel for
      for(uint16_t cur_cr=0; cur_cr<crystals_n1; ++cur_cr) {
        std::vector<ThreeVector>::iterator it;
        it = std::find(f_cry_list1.begin(), f_cry_list1.end(), cry_list1[cur_cr]);
        if ( it == f_cry_list1.end() ) {
          #pragma omp critical
          {
            std::cout << "new_c1 not found: " + std::to_string(cur_cr);
            std::cout << std::endl;
          }
          throw std::string("reflectHistograms");
        }
        crystals_map1[cur_cr] = std::distance(f_cry_list1.begin(), it);
      }
      
      #pragma omp parallel for
      for(uint16_t cur_cr=0; cur_cr<crystals_n2; ++cur_cr) {
        std::vector<ThreeVector>::iterator it;
        it = std::find(f_cry_list2.begin(), f_cry_list2.end(), cry_list2[cur_cr]);
        if ( it == f_cry_list2.end() ) {
          #pragma omp critical
          {
            std::cout << "new_c2 not found: " + std::to_string(cur_cr);
            std::cout << std::endl;
          }
          throw std::string("reflectHistograms");
        }
        crystals_map2[cur_cr] = std::distance(f_cry_list2.begin(), it);
      }
      
      std::vector<FloatType> new_histogram_vector;
      new_histogram_vector.resize(crystals_n1*crystals_n2, 0);
      
      #pragma omp parallel for
      for(lorIDX_t cur_lor=0; cur_lor<crystals_n1*crystals_n2; ++cur_lor) {
        uint16_t c1 = cur_lor/crystals_n2;
        uint16_t c2 = cur_lor%crystals_n2;
        uint16_t new_c1 = crystals_map1[c1];
        uint16_t new_c2 = crystals_map2[c2];
        lorIDX_t n_lor = new_c1*crystals_n2 + new_c2;
        new_histogram_vector[n_lor] = histogram.data[pair.pairID()][cur_lor];
      }
      
      histogram.data[pair.pairID()] = new_histogram_vector;
    }
    
  }
  
  msec_timer.stop();
}


void vGeometry::printSymInfo(std::string output_dir) const
{
  std::ofstream txt_file;
  std::string fname = output_dir + "/" + _name + "_SymInfo.txt";
  
  txt_file.open(fname);
  if (!txt_file.is_open())
    throw std::string ("Unabel to open " + fname );
  
  uint16_t fundamental = 0;
  
  txt_file << "Symmetric pairs:" << std::endl;
  for (const std::vector<CoincidencePairSym>& vec : grouped_cs) {
    txt_file << "- ";
    for (const CoincidencePairSym& s : vec) {
      txt_file << s.pairID() << ",";
    }
    txt_file << std::endl;
  }
  
  
  txt_file << "\nCoincidence Schema:" << std::endl;

  for (const std::vector<CoincidencePairSym>& vec : grouped_cs) {
    txt_file << std::setw(7) << "ID1";
    txt_file << std::setw(7) << "ID2";
    txt_file << std::setw(7) << "p_ID";
    txt_file << std::setw(3) << "F";
    txt_file << std::setw(7) << "S_ID";
    txt_file << std::setw(5) << "UV";
    txt_file << std::setw(6) << "zA";
    txt_file << std::setw(7) << "yS";
    txt_file << std::setw(7) << "zS";
    txt_file << std::setw(3) << "xR";
    txt_file << std::setw(3) << "yR";
    txt_file << std::setw(3) << "zR";
    txt_file << std::endl;
    for (const CoincidencePairSym& s : vec) {
      txt_file << s << std::endl;
      fundamental += s.fundamental();
    }
    txt_file << std::endl;
  }
  txt_file << "Total number of pairs: " << sym_coinc_schema.size() << std::endl;
  txt_file << "\t\tfundamental pairs: " << fundamental << std::endl;
  
  txt_file << "\nUnique Views:" << std::endl;
  txt_file << std::setw(3) << "ID";
  txt_file << std::setw(6) << "zA";
  txt_file << std::setw(7) << "yS";
  txt_file << std::setw(7) << "zS";
  txt_file << std::setw(3) << "xR";
  txt_file << std::setw(3) << "yR";
  txt_file << std::setw(3) << "zR";
  txt_file << std::endl;
  for (uint16_t u=0; u<unique_views.size(); u++)
    txt_file << std::setw(3) << u << unique_views[u] << std::endl;
  txt_file << "\nTotal number of Unique Views: " << unique_views.size() << std::endl;
  
  txt_file.close();
}
