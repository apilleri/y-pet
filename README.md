# Y-PET toolkit
The Y-PET software provides image reconstruction for Positron Emission Tomography  
histogrammed data. 

The software provides the following main features:

*  user-defined cylindrical or planar geometry
*  user-defined Field of View with box voxels
*  graphical visualizer of the scanner geometry
*  image reconstruction using Ordinary Poisson ML-EM and OS-EM algorithms
*  automatic implementation of the matrix factorization approach to apply the  
  resolution modelling

Requirements:  

* c++ 17 compiler
* ITK (version >= 5.0.1)
* VTK (version >= 8.1.2)
* optional - ROOT (version >= 6.18.04) (to automatic convert GATE simulations  
 to the input histogram format.
 
Make sure that everything is compiled using the same c++ standard (check ROOT
configuration)

# General concepts
The environment has a right-handed Cartesian 3D coordinate system. The origin of  
the axes is equal to the geometrical center of the scanner. The Z-axis is equal to  
the axis of the scanner and points outside of the X-Y plane, the X-axis is  
horizontal and the Y-axis is vertical.  

The software relies upon the concept of planar detectors. A planar detector is  
a matrix of scintillation crystals with one or more layer of crystals.

## XML configuration file
The major part of the configuration options must be defined in a XML file with  
three mandatory tags (SCANNER, FOV, PROJECTORS)

### SCANNER TAG
The SCANNER tag defines the scanner geometry, which can be cylindrical or planar.

#### The Cylindrical Scanner Geometry 
A cylindrical scanner is composed of an even number of ring sectors. Each ring  
sector is composed of a series of modules. Each module contains blocks and each  
block contains one or more layers of crystals. The layer is made of a 
matrix of scintillation crystals.
```xml
<SCANNER>
  <TYPE>CYLINDRICAL</TYPE>
  <NAME>Cylindrical_PET</NAME>
  <!-- A sector is set in coincidence with the opposite sector plus 2*N adjacent
       sectors as defined in the COINCIDENCE_SECTOR_STEP tag.-->
  <COINCIDENCE_SECTOR_STEP>1</COINCIDENCE_SECTOR_STEP>
  
  <!-- Half-Distance between two opposite sectors -->
  <CYLINDER_RADIUS>60</CYLINDER_RADIUS> 
  
  <SECTORS_NUMBER>8</SECTORS_NUMBER>
  
  <!-- Number of modules in each ring sector, distributed along the z-axis -->
  <MODULES_NUMBER>1</MODULES_NUMBER>
  <!-- Distance between the centers of two adjacent modules -->
  <MODULES_OFFSET>0</MODULES_OFFSET>
  
  <!-- Number of blocks in each module, along the y-axis -->
  <BLOCK_GRID_Y>1</BLOCK_GRID_Y>
  <!-- Number of blocks in each module, along the z-axis -->
  <BLOCK_GRID_Z>1</BLOCK_GRID_Z>
  <!-- Distance between the centers of two adjacent modules along the y-axis -->
  <BLOCK_OFFSET_Y>0</BLOCK_OFFSET_Y>
  <!-- Distance between the centers of two adjacent modules along the z-axis -->
  <BLOCK_OFFSET_Z>0</BLOCK_OFFSET_Z>
  
  <LAYERS_PER_BLOCK>1</LAYERS_PER_BLOCK>
  
  <LAYER0>
    <CRYSTALS_NUMBER_Y>30</CRYSTALS_NUMBER_Y>
    <CRYSTALS_NUMBER_Z>30</CRYSTALS_NUMBER_Z>
    <CRYSTAL_LENGTH_X>12</CRYSTAL_LENGTH_X>
    <CRYSTAL_LENGTH_Y>1.5</CRYSTAL_LENGTH_Y>
    <CRYSTAL_LENGTH_Z>1.5</CRYSTAL_LENGTH_Z>
    <CRYSTAL_PITCH_Y>1.6</CRYSTAL_PITCH_Y>
    <CRYSTAL_PITCH_Z>1.6</CRYSTAL_PITCH_Z>
  </LAYER0>
  
</SCANNER>
```



#### The Planar Scanner Geometry
A Planar scanner is composed of two heads. Each head is made of a grid of modules.  
Each module is composed of a grid uf sub-modules.
```xml
<SCANNER>
  <TYPE>PLANAR</TYPE>
  <NAME>Planar_PET</NAME>
  
  <!--  Half-Distance between the two heads -->
  <HALF_HEADS_DISTANCE>230</HALF_HEADS_DISTANCE> 
  
  <MODULE_GRID_Y>3</MODULE_GRID_Y>
  <MODULE_GRID_Z>3</MODULE_GRID_Z>
  <MODULE_OFFSET_Y>7.8</MODULE_OFFSET_Y>
  <MODULE_OFFSET_Z>7.8</MODULE_OFFSET_Z>
  
  <BLOCK_GRID_Y>1</BLOCK_GRID_Y>
  <BLOCK_GRID_Z>1</BLOCK_GRID_Z>
  <BLOCK_OFFSET_Y>0</BLOCK_OFFSET_Y>
  <BLOCK_OFFSET_Z>0</BLOCK_OFFSET_Z>
  
  <LAYERS_PER_BLOCK>1</LAYERS_PER_BLOCK>
  
  <LAYER0>
    <CRYSTALS_NUMBER_Y>23</CRYSTALS_NUMBER_Y>
    <CRYSTALS_NUMBER_Z>23</CRYSTALS_NUMBER_Z>
    <CRYSTAL_LENGTH_X>20</CRYSTAL_LENGTH_X>
    <CRYSTAL_LENGTH_Y>1.9</CRYSTAL_LENGTH_Y>
    <CRYSTAL_LENGTH_Z>1.9</CRYSTAL_LENGTH_Z>
    <CRYSTAL_PITCH_Y>2</CRYSTAL_PITCH_Y>
    <CRYSTAL_PITCH_Z>2</CRYSTAL_PITCH_Z>
  </LAYER0>
  
</SCANNER>
```

### FIELD OF VIEW TAG
The Field of View (FOV) is completely described by the number of voxels  
and their lengths along the three cartesian directions. It is possible  
to define multipel FOV with different characteristics by changing the name  
of the main tag. The FOV tag requires six mandatory fields 

```xml
<FOV_NAME>
<VOXEL_NUMBER_X>101</VOXEL_NUMBER_X>
<VOXEL_NUMBER_Y>101</VOXEL_NUMBER_Y>
<VOXEL_NUMBER_Z>120</VOXEL_NUMBER_Z>
<VOXEL_LENGTH_X>0.84</VOXEL_LENGTH_X>
<VOXEL_LENGTH_Y>0.84</VOXEL_LENGTH_Y>
<VOXEL_LENGTH_Z>0.855</VOXEL_LENGTH_Z>
</FOV_NAME>
```

### PROJECTORS TAG
Inside the PROJECTORS tag it's possible to  define the parameters of the  
available projection algorithms provided by the software.  
The Siddon and Jacobs projectors do not require any parameter.  
The Aguiar and Lougovski projectors start from a simple ray-tracer to determine  
all the voxels in the tube-of-response. To speed up the computation the voxels  
are sampled around the ray that connect the start and end point of the projection  
line. The step" parameter defines the maximum departure from the central voxel  
when sampling the voxels in the plane perpendicular to the projection-line.  

The start and end points of the projection line are set inside the crystal at a  
depth in which, assuming perpendicular incidence, 50% of the photons are, on  
average, absorbed by the crystal. (For now, it works only for LYSO crystals).

The name of the tag can be defined by th user. The content of the tag and the  
name of the tag options are instead fixed.

```xml
<PROJECTORS>
  <siddon>SIDDON</siddon>
  <jacobs>JACOBS</jacobs>
  <aguiar-2 step="2" threshold="0.05" fwhm="1.2">AGUIAR</aguiar-2>
  <aguiar-3 step="3" threshold="0.05" fwhm="1.2">AGUIAR</aguiar-3>
  <aguiar-4 step="4" threshold="0.05" fwhm="1.2">AGUIAR</aguiar-4>
  <aguiar-8 step="8" threshold="0.05" fwhm="1.2">AGUIAR</aguiar-8>
  <loug-2 step="2">FASTOD</loug-2>
  <loug-3 step="3">FASTOD</loug-3>
  <loug-4 step="4">FASTOD</loug-4>
  <loug-8 step="8">FASTOD</loug-8>
</PROJECTORS>
```

# VISUALIZER
The software provides a graphical visualizer written using VTK.  
The visualizer is mainly used to verify the correctness of the geometric  
configuration of the scanner and to check the model computation.  
It is capable to provide a three-dimensional navigable visualization of  
current imaging system, showing the scanner geometry and the FOV. It is also  
possible to highlight the voxels that are crossed by a chosen sample projection  
line. The highlight is performed with a coloured gradient associated to the probability  
values of that particular voxel in the TOR. The highlighted voxels correspond  
to the one belonging to the chosen row of the system matrix which can include  
both geometric and detector component.

To only  visualize the scanner and the FOV use:  

<<<<<<< HEAD
 visualizeScanner -c CONFIG.XML --fov FOV_TAG

To visualize an on-the-fly projection line add:  

 --onLine --projector PROJECTOR_TAG --first idx_cry1 --second idx_cry2

To visualize a stored projection line using geometric and detector model add:  

 --onLine -g G_MATRIX_DIR -d D_MATRIX_DIR --first idx_cry1 --second idx_cry2
=======
  visualizeScanner -c CONFIG.XML --fov FOV_TAG

To visualize an on-the-fly projection line add:  

   --onLine --projector PROJECTOR_TAG --first idx_cry1 --second idx_cry2

To visualize a stored projection line using geometric and detector model add:  

   --onLine -g G_MATRIX_DIR -d D_MATRIX_DIR --first idx_cry1 --second idx_cry2
>>>>>>> 8230a0f0a0736912df290d6bc0ab23bef303bc49

#Indexing of detectors and crystals
## The Cylindrical Scanner Geometry
The cylindrical scanner geometry is subdivided into sector, module, block and crystal.  
In the Visualizer tool, when computing a line, the two crystals must be specified using  
the four IDs separated by a comma.

* the ID of the sectors starts from 0 to n_sectors-1 and increases anti-clockwise.  
  The fist sector is the one with positive x and y=0.
* the ID of the modules inside the sector starts from 0 to n_modules-1.  
  The first module is the one with the lowest z coordinate.
* the ID of the blocks inside the module starts from 0 to n_blocks-1.  
  The first block it the one with the lowest y and z coordinate. The coordinate  
  that first increases is the y-coordinate  
* the ID of the crystals inside the block starts from 0 to n_crystals-1.
  The first crystal it the one with the lowest y and z coordinate. The coordinate  
  that first increases is the y-coordinate

The unique ID of a block (which corresponds to a planar detector) is computed as:
detectorID = block_ID + module_ID*n_blocks + sector_ID*n_blocks*n_modules

## The Planar Scanner Geometry
The planar scanner geometry is subdivided into head, module, block and crystal.  
In the Visualizer tool, when computing a line, the two crystals must be specified using  
the four IDs separated by a comma.

* the ID of the heads can be 0 or 1. The first head is the one with positive x and y=0.
* the ID of the modules inside the head starts from 0 to n_modules-1.  
  The first module is the one with the lowest y and z coordinate. The coordinate  
  that first increases is the y-coordinate  
* the ID of the blocks inside the module starts from 0 to n_blocks-1.  
  The first block it the one with the lowest y and z coordinate. The coordinate  
  that first increases is the y-coordinate  
* the ID of the crystals inside the block starts from 0 to n_crystals-1.
  The first crystal it the one with the lowest y and z coordinate. The coordinate  
  that first increases is the y-coordinate

The unique ID of a block (which corresponds to a planar detector) is computed as:
detectorID = block_ID + module_ID*n_blocks + head_ID*n_blocks*n_modules


# Reconstruction using on-the-fly model computation
The simplest way to reconstruct the data is to use the recon-OSEM application,  
which reconstruct using an on-the-fly projection algorithm  with no detector  
component.

<<<<<<< HEAD
 recon-OSEM -c CONFIG.XML --fov FOV_TAG -i input_hg -o out_image  
 --projector PROJECTOR_TAG

It is possible to define an image-space space-invariant Gaussian kernel:  
 --psf sigma,width,type  
 with type = (0 - both, 1 - pre-projection-only, 2 - post-backprojeciton-only)  

Basic reconstruction options:  
-x, --subsets argNumber of subsets  
-n, --number argNumber of iterations  
-e, --every argSave image every N iterations  
--save-all-subsets  Save all subsets  
=======
 recon-OSEM -c CONFIG.XML --fov FOV_TAG -i input_hg -o out_image --projector PROJECTOR_TAG

It is possible to define an image-space space-invariant Gaussian kernel:  
 --psf sigma,width,type  
  with type = (0 - both, 1 - pre-projection-only, 2 - post-backprojeciton-only)  

Basic reconstruction options:  
 -x, --subsets arg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Number of subsets  
 -n, --number arg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Number of iterations  
 -e, --every arg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Save image every N iterations  
 --save-all-subsets  Save all subsets  
>>>>>>> 8230a0f0a0736912df290d6bc0ab23bef303bc49


# Stored Model computation 
To reduce the memory requirements of the stored models, the software  
automatically computes and exploits the symmetries of the particular  
scanner geometry under use. The symmetries are computed between pairs  
of planar detectors and not between LORs. 
 
The software computes every possible coincidence between pairs of planar  
detectors according to the coincidence scheme and creates groups of detector  
pairs belonging to the same class of symmetry. A group of symmetry contains  
a list of detector pairs with a so-called *fundamental pair* and all the other  
symmetrical pairs. 

The software computes the models only for the fundamental pairs and it can create  
the geometric model _G_, the detector model _D_ and the system model _S = D G_ as  
the product of the two previous models. For every model also the transposed matrix  
is computed, as it is used by some algorithms implementation. The models are saved  
in a sparse format, the \textit{straight} model has a row major format whereas the  
transposed model has a column major format. Every entry of the matrix contains the  
index of the column/row and the corresponding value.  
Each model consists of two files: one file contains the elements of the matrix in  
a sequential order and the other file contains the number of elements for every  
row/column of the sparse matrix. The second file is dense with respect to the  
first dimension of the matrix: the file associated to the \textit{straight} model  
contains a number of elements equal to the number of rows of the matrix and the  
file associated to the transposed model contains a number of elements equal to the  
number of columns of the $G$ matrix (as it contains the $G^T$ matrix).

## Geometric component
The geometric model is computed using the projection algorithms. In the simplest  
approach, the ray-driven methods compute the intersection between the straight  
line connecting the centers of the two crystals and the voxels of the FOV (like  
the Siddon and Jacobs projectors).  
More sophisticated approaches are able to model the so-called *Tube of Response  
(TOR)*, by modelling the finite size of the crystal surfaces and the distance  
between the crystals and the voxels (like the Aguiari and Lougovski projectors).  


## Detector component
The detector model is computed using a GEANT4 Monte Carlo simulation which models  
the inter-crystal penetration and the inter-crystal scattering. The model is  
created in an automatic way for all the fundamental pairs. The software exports  
the geometric information of the fundamental detectors that are loaded into a second  
application which recreates the detectors into the GEANT4 environment and performs  
the simulation.


# Reconstruction using stored model

# Data format
## Detector Pair and LOR
A coincidence event is dentified by two IDs: detector_pair_ID and LOR_ID.  
The detector_pair_ID is a unique ID computed from the two planar detectors  
which detected the event.  
A detector_ID is stored in a 16bit unsigned integer and the detector_pair_ID  
is stored in  a 32bit unsigned integer using the Szudzik hash function. 
```C++
detPair_t szudzikPair(detID_t x, detID_t y)
{
  return (x >= y) ? x*x + x + y : x + y*y;
}
std::tuple<detID_t, detID_t> szudzikUnPair(detPair_t z)
{
  detPair_t sq = std::sqrt(z);
  detPair_t sq2 = sq*sq;
  
  return (z - sq2 >= sq) ? std::make_tuple(sq, z - sq2 - sq) : std::make_tuple(z - sq2, sq);
}
```
The LOR_ID is defined starting from the ID of the two crystals which idenfied  
the coincidence event:  
LOR_ID = ID_cry1 * n_crystals_per_block + ID_cry2  
There is a total of n_LORS = n_crystals_per_block1 * n_crystals_per_block2.  

Both the LOR_ID and the detector_pair_ID are not symmetric with respect to an  
exchange of their costituents. 

## Histogram of Data
The coincidence events are stored in a histogram dense format:
det_pair_ID_0 (32 bit unsigned integer)
n_events_in_LOR_0 (32 bit float)
...
n_events__inLOR_(n_LORS-1) (32 bit float).
...
det_pair_ID_(N-1) (32 bit unsigned integer)
n_events_in_LOR_0 (32 bit float)
...
n_events__inLOR_(n_LORS-1) (32 bit float).


# Root to Histogram
The root2hist is useful to convert the results fo the GATE simulation into the  
data format used by the Y-PET software. 

Usage:  
  root2hist -c CONFIG.XML  

 Input/Output options:  
  -f, --file arg    Input root file (to process a single file)  
  -d, --dir arg     Input root files directory (to process all the root files in the directory)  
  -o, --output arg  Output data file name (no extension)  
      --type arg    Type of histograms x,x,x,x (all, true, random, scatter)  
                    (default: 1,0,0,0)  
  
 Parameters options:  
  -l, --lower arg      Lower bound of the energy window (keV) (default: 350)  
  -u, --upper arg      Upper bound of the energy window (keV) (default: 650)  
  -s, --staggered      Toggle staggered flag (For the staggered dual-layer scanner)  
      --print-scatter  Print scatter phantom info  
  -t, --threads arg    Number of parallel threads (useful with more than one   
                       .root file) (default: 1)  

