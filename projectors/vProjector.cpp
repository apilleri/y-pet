/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vProjector.h"

vProjector* 
vProjector::initProjector(const std::string& xml_config,
                          FieldOfView *FOV,
                          const std::string& tag_p)
{
  XMLDocument* xml_doc = xmlLoad(xml_config);
  const XMLElement *projector_head = xmlGetElement(xml_doc, "PROJECTORS");
  const XMLElement *projector_el = xmlGetElement(projector_head, tag_p.c_str());
  std::string projector_name = projector_el->GetText();
  
  if (projector_name == "SIDDON") {
    std::cout << "Siddon projector initialized." << std::endl;
    return new SiddonProjector(FOV);
  }
  
  else if (projector_name == "JACOBS") {
    std::cout << "Jacobs projector initialized." << std::endl;
    return new JacobsProjector(FOV);
  }
  
  else if (projector_name == "JOSEPH") {
    std::cout << "Joseph projector initialized." << std::endl;
    return new JosephProjector(FOV);
  }
  
  else if (projector_name == "MULTIRAY") {
    uint16_t points = std::atoi(projector_el->Attribute("points"));
    FloatType border = std::atof(projector_el->Attribute("border"));
    bool cross = std::atoi(projector_el->Attribute("crossed"));
    std::cout << "Multiray projector initialized: ";
    std::cout << "points " << points << " cross " << cross << std::endl;
    return new MultirayProjector(points, border, cross, FOV);
  }
  
  else if (projector_name == "DEPTH_MULTIRAY") {
    uint16_t pyz = std::atoi(projector_el->Attribute("pyz"));
    uint16_t px = std::atoi(projector_el->Attribute("px"));
    std::cout << "DepthMultiray projector initialized: ";
    std::cout << "points " << px << " " << pyz << " " << pyz << std::endl;
    return new DepthMultirayProjector(pyz, px, FOV);
  }
  
  else if (projector_name == "AGUIAR") {
    uint16_t step = std::atoi(projector_el->Attribute("step"));
    FloatType threshold = std::atof(projector_el->Attribute("threshold"));
    FloatType fwhm = std::atof(projector_el->Attribute("fwhm"));
    std::cout << "Aguiar projector initialized: ";
    std::cout << "step " << step << " threshold " << threshold;
    std::cout << " fwhm " << fwhm << std::endl;
    return new AguiarProjector(step, threshold, fwhm, FOV);
  }
 
  else
    throw std::string("Found no usable PROJECTOR in " + xml_config);
  
}

// #define DEBUG_SIDDON

std::tuple<FloatType, FloatType>
vProjector::computeAlfaMinMax(const ThreeVector& begin, const ThreeVector& ray) const
{
  FloatType alphaMin = 0, alphaMax = 1;
  
  #ifdef DEBUG_SIDDON
  std::cout << std::endl;
  #endif
  
  for (uint16_t i=0; i<3; i++) {
    FloatType diff = std::round(ray[i]*1e4)/1e4;
    if (diff) {
      FloatType alpha1 = ( fov->plane(i,0) - begin[i] )/diff;
      FloatType alphan = ( fov->plane(i,fov->vxN(i)) - begin[i] )/diff;
      #ifdef DEBUG_SIDDON
      std::cout << "alpha1: " << alpha1 << " alphan: " << alphan << std::endl;
      #endif
      FloatType t_min, t_max;
      std::tie(t_min, t_max) = std::minmax(alpha1, alphan);
      alphaMin = std::max(alphaMin, t_min);
      alphaMax = std::min(alphaMax, t_max);
    }
  } 
  #ifdef DEBUG_SIDDON
  std::cout << "\nalphaMin " << alphaMin << " alphaMax " << alphaMax << std::endl;
  #endif
  return std::make_tuple(alphaMin, alphaMax);
}  
