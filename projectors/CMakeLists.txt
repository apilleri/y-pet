file(GLOB PROJECTORS_SOURCE "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

set( projectors_files ${PROJECTORS_SOURCE} )

add_library(
  projectors_lib
  ${projectors_files}
)

target_link_libraries(
  projectors_lib
  utils_lib
  fov_lib
  types_lib
)
