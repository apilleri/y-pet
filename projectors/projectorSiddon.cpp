/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vProjector.h"

#define NORMALIZE

void SiddonProjector::project(const PlanarDetectorType& det1, uint16_t cry1,
                              const PlanarDetectorType& det2, uint16_t cry2,
                              std::vector<MatrixEntry>& row)
{
  #ifdef NORMALIZE
  FloatType total_probability = 0;
  #endif
  
//   const ThreeVector& start = det1.aCrySurfaceCenter(cry1);
//   const ThreeVector& stop = det2.aCrySurfaceCenter(cry2);
  ThreeVector start = det1.aCryBestPoint(cry1);
  ThreeVector stop = det2.aCryBestPoint(cry2);
  
  row.clear();
  
  #ifdef DEBUG_SIDDON
  std::cout << "\nstart: " << start << std::endl;
  std::cout << "stop: " << stop << std::endl << std::endl;
  #endif
  
  ThreeVector ray = stop-start;
  ray.round(1e-5);
  
  FloatType alphaMin = 0, alphaMax = 1;
  
  std::tie(alphaMin, alphaMax) = computeAlfaMinMax(start, ray);
  
  
  //the ray does not intercept the fov
  if (alphaMin >= alphaMax)
    return;
  
  std::vector<FloatType> alphas;
  alphas.reserve( fov->vxN(0)+fov->vxN(1)+fov->vxN(2)+3 );
  alphas.push_back(alphaMin);
  for (uint16_t axis=0; axis<3; axis++) {
    FloatType diff = std::round(ray[axis]*1e4)/1e4;
    #ifdef DEBUG_SIDDON
    std::cout << "\ni " << axis << std::endl;
    std::cout << " diff " << diff << std::endl;
    #endif
    if (diff) {
      
      FloatType first = alphaMin;
      FloatType second = alphaMax;
      if (diff < 0)
        std::swap(first, second);
      
      uint16_t idxMin = fov->vxN(axis)+1 - (fov->halfLength(axis) - first*ray[axis] - start[axis])/fov->vxL(axis);
      uint16_t idxMax = std::floor((start[axis] + second*ray[axis] + fov->halfLength(axis))/fov->vxL(axis));
      
      #ifdef DEBUG_SIDDON
      std::cout << " idxMin " << idxMin << std::endl;
      std::cout << " idxMax " << idxMax << std::endl;
      #endif
      
      if (diff > 0) {
        for(uint16_t j=idxMin; j<=idxMax; j++) {
          FloatType tmp = (fov->plane(axis,j)-start[axis])/diff;
          if (tmp < alphaMax && tmp > alphaMin)
            alphas.push_back(tmp);
        }
      } else {
        for(int16_t j=idxMax; j>=idxMin; j--) {
          FloatType tmp = (fov->plane(axis,j)-start[axis])/diff;
          if (tmp < alphaMax && tmp > alphaMin)
            alphas.push_back(tmp);
        }
      }
    }
  }
  alphas.push_back(alphaMax);
  std::sort(alphas.begin(), alphas.end());
  std::vector<FloatType>::iterator new_end;
  new_end = std::unique(alphas.begin(), alphas.end());
  #ifdef DEBUG_SIDDON
  std::cout << "removed elements: " << alphas.size()-std::distance(alphas.begin(), new_end);
  #endif
  alphas.resize(std::distance(alphas.begin(), new_end));
  
  #ifdef DEBUG_SIDDON
  std::cout << "\nalphas: " << alphas.size() << std::endl;
  for (auto a : alphas)
    std::cout << a << std::endl;
  std::cout << "end alphas" << std::endl;
  #endif
  
  
  FloatType ray_length = ray.r();
  
//   row.resize(alphas.size()-1);
//   uint16_t nvx=0;
  
  for (uint16_t i=1; i<alphas.size(); i++) {
    FloatType tmp1 = (alphas[i] + alphas[i-1])/2;
    uint16_t vX = (start.x() + tmp1*ray[0] - fov->plane(0,0) )/fov->vxL(0);
    uint16_t vY = (start.y() + tmp1*ray[1] - fov->plane(1,0) )/fov->vxL(1);
    uint16_t vZ = (start.z() + tmp1*ray[2] - fov->plane(2,0) )/fov->vxL(2);
    if ( vX >= fov->vxN(0) || vY >= fov->vxN(1) || vZ >= fov->vxN(2) ) {
      #ifdef DEBUG_SIDDON
      std::cout << "skipping" << std::endl;
      continue;
      #endif
    }
    
    uint32_t vx_idx = fov->computeLinearIndex(vX, vY, vZ);
    FloatType probability = ray_length*(alphas[i] - alphas[i-1]);
    #ifdef NORMALIZE
    total_probability += probability;
    #endif
    row.emplace_back( MatrixEntry(vx_idx, probability) );
    
    #ifdef DEBUG_SIDDON
    std::cout << std::setw(4) << vX << std::setw(4) << vY << std::setw(4) << vZ << std::setw(15) << (stop-start).r()*(alphas[i] - alphas[i-1]) << " " << fov->vx2pos(vX, vY, vZ) << std::endl;
    #endif
  }
  
  #ifdef NORMALIZE
  for (MatrixEntry& e : row)
    e.v /= total_probability;
  #endif
}
