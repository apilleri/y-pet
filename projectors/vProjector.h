/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef PROJECTORS_VPROJECTOR_H
#define PROJECTORS_VPROJECTOR_H

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <chrono>

#include <vector>
#include <forward_list>
#include <tuple>


#include "fieldOfView.h"
#include "dataTypes.h"
#include "PlanarDetector.h"
#include "euclideanSpace.h"
#include "modelT.h"
#include "f_utils.h"

#define EPSILON 1e-6

/*! \class vProjector
 *  \brief Representation of a generic Projector
 *
 *  The vProjector class is an abstract class that provides the interface for a 
 *  generic Projector. It provides the methods used to compute a projection
 *  through the FOV using the information contained in a FieldOfView object and in 
 *  a DetectorPair.
 */

using ProjInfo = std::map<std::string, FloatType>;

class vProjector {

protected:
  //! Pointer to the FieldOfView object, used by project()
  const FieldOfView *fov = nullptr;

  std::string _type;
  
  ProjInfo _info;
  
public:
  
  /*! \brief projection methods
   *  
   *  \param start crystal index of the starting point of the LOR taken from detector_pair.first
   *  \param stop crystals index of the starting point of the LOR taken from detector_pair.second
   *  \param row output object used to store the result of the projection
   */
  
  static vProjector* initProjector(const std::string& xml_config,
                                   FieldOfView *FOV,
                                   const std::string& tag_p);
  
  virtual void project(const PlanarDetectorType& det1, uint16_t cry1,
                       const PlanarDetectorType& det2, uint16_t cry2,
                       std::vector<MatrixEntry>& row) = 0;
  
  
  virtual ~vProjector()
  { }
  
//   void siddonSingleRay(const ThreeVector& start, const ThreeVector& stop,
//                        std::vector<MatrixEntry>& row);
  
  std::string type() const
  {
    return _type;
  }
  
  const ProjInfo & info()
  {
    return _info;
  }
  
  std::tuple<FloatType, FloatType>
  computeAlfaMinMax(const ThreeVector& begin, const ThreeVector& ray) const;
  
  std::tuple<ThreeVector, ThreeVector>
  crystalOppositeCorners(const PlanarDetectorType& det, uint16_t cry,
                         bool invert) const;
  
};



class SiddonProjector : public vProjector
{
  /* Based on: 
   * 1985 - Siddon - Fast calculation of the exact radiological 
   * path for a three-dimensional CT array
   */
  
public:
  
  explicit SiddonProjector(const FieldOfView *fov_p)
  {
    fov = fov_p;
    _type = "SiddonProjector";
  }
  
  ~SiddonProjector() {}
  
  void project(const PlanarDetectorType& det1, uint16_t cry1,
               const PlanarDetectorType& det2, uint16_t cry2,
               std::vector<MatrixEntry>& row) override; 
  

};


class JacobsProjector : public vProjector
{
  /* Based on:
   * 1998 - Jacobs et al - A fast algorithm to calculate the exact 
   * radiological path through a pixel or voxel space;
   * 1999 - Christiaens - A fast, cache-aware algorithm for the calculation of 
   * radiological paths exploiting subword parallelism
   */
  
public:
  
  explicit JacobsProjector(const FieldOfView *fov_p)
  {
    fov = fov_p;
    _type = "JacobsProjector";
  }
  
  ~JacobsProjector() {}
  
  void project(const PlanarDetectorType& det1, uint16_t cry1,
               const PlanarDetectorType& det2, uint16_t cry2,
               std::vector<MatrixEntry>& row) override; 
               
};


class JosephProjector : public vProjector
{
  /* Based on:
   * 1982 - Joseph et al - An improved algorithm for reprojecting rays through 
   * pixel images.
   */
  
  
  FloatType tolerance_factor = 1.e-4;
  FloatType toleranceX, toleranceY, toleranceZ;
  FloatType boundX, boundY, boundZ;
  
public:
    
    explicit JosephProjector(const FieldOfView *fov_p)
    {
      fov = fov_p;
      _type = "JosephProjector";
      
      toleranceX = fov->vxL(0) * tolerance_factor;
      toleranceY = fov->vxL(1) * tolerance_factor;
      toleranceZ = fov->vxL(2) * tolerance_factor;
      
      boundX = -fov->halfLength(0) - fov->vxL(0)* 0.5;
      boundY = -fov->halfLength(1) - fov->vxL(1)* 0.5;
      boundZ = -fov->halfLength(2) - fov->vxL(2)* 0.5;
    }
    
    ~JosephProjector() {}
    
    void project(const PlanarDetectorType& det1, uint16_t cry1,
                 const PlanarDetectorType& det2, uint16_t cry2,
                 std::vector<MatrixEntry>& row) override; 
                 
};

class MultirayProjector : public vProjector
{
  uint16_t n_points;
  FloatType border;
  bool cross;
  
  
public:
  
  MultirayProjector(uint16_t np, FloatType border_p,
                    bool cross_p, const FieldOfView *fov_p) 
  : n_points(np), border(border_p), cross(cross_p)
  {
    fov = fov_p;
    _type = "MultirayProjector";
    _type += "-" + std::to_string(n_points);
    _type += "-" + std::to_string(cross);
    _type += "-" + std::to_string(border);

    _info["points"] = n_points;
    _info["cross"] = cross;
    _info["border"] = border;
  }
  
  ~MultirayProjector() {}
  
  void project(const PlanarDetectorType& det1, uint16_t cry1,
               const PlanarDetectorType& det2, uint16_t cry2,
               std::vector<MatrixEntry>& row) override;
};



class AguiarProjector : public vProjector
{
  /* Based on:
   * 2010 - Aguiar et al - Geometrical and Monte Carlo projectors in 3D PET 
   * reconstruction
   */
  
  uint16_t step;
  
  FloatType threshold;
  
  FloatType fwhm;
  
public:
  
  explicit AguiarProjector(uint16_t step_p,
                           FloatType threshold_p,
                           FloatType fwhm_p,
                           const FieldOfView *fov_p)
  {
    fov = fov_p;    
    step = step_p;  
    threshold = threshold_p;
    fwhm = fwhm_p;
    
    _type = "AguiarProjector";
    _type += "-" + std::to_string(step);
    _type += "-" + std::to_string(threshold);
    _type += "-" + std::to_string(fwhm);

    _info["step"] =step;
    _info["threshold"] = threshold;
    _info["fwhm"] = fwhm;
  }
  
  ~AguiarProjector() {}
  
  void project(const PlanarDetectorType& det1, uint16_t cry1,
               const PlanarDetectorType& det2, uint16_t cry2,
               std::vector<MatrixEntry>& row) override;
               
            
};



class DepthMultirayProjector : public vProjector
{
  uint16_t npyz, npx;
  // std::vector<ThreeVector> points;
  
public:
 DepthMultirayProjector(uint16_t pyz, uint16_t px, const FieldOfView* fov_p)
     : npyz(pyz), npx(px) {
   fov = fov_p;
   _type = "DepthMultiray";
   _type += "-" + std::to_string(npx);
   _type += "x" + std::to_string(npyz);
   _type += "x" + std::to_string(npyz);

   _info["npx"] = npx;
   _info["npyz"] = npyz;
 }

  ~DepthMultirayProjector() {}

  void project(const PlanarDetectorType& det1, uint16_t cry1,
               const PlanarDetectorType& det2, uint16_t cry2,
               std::vector<MatrixEntry>& row) override;

  void computeIntegrationPoints();   
};



#endif // PROJECTORS_VPROJECTOR_H

