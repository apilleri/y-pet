/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vProjector.h"
#include <random>
#include <PlanarDetector.h>

// #define NORMALIZE
// #define MASK


void
DepthMultirayProjector::project(const PlanarDetectorType& det1,
                           uint16_t cry1,
                           const PlanarDetectorType& det2,
                           uint16_t cry2,
                           std::vector<MatrixEntry>& row)
{
    
  const ThreeVector& start_position = det1.rCryCenter(cry1);
  const ThreeVector& stop_position = det2.rCryCenter(cry2);
  AngleType start_ang = det1.zAngle();
  AngleType stop_ang = det2.zAngle(); 

  std::vector<ThreeVector> points1, points2;
  std::vector<double> weights1, weights2;

  std::tie(points1, weights1) = det1.computeIntegrationPoints(
      cry1, (stop_position - start_position).unit(), npyz, npx);

  std::tie(points2, weights2) = det2.computeIntegrationPoints(
      cry2, (stop_position - start_position).unit(), npyz, npx);

  std::unordered_map<uint32_t, FloatType> buffer;
  for (uint16_t c_p1 = 0; c_p1 < points1.size(); c_p1++) {
    ThreeVector start = points1[c_p1];
    for (uint16_t c_p2 = 0; c_p2 < points2.size(); c_p2++) {
      ThreeVector stop = points2[c_p2];

      ThreeVector ray = stop-start;
      ThreeVector ray_dir = ray.unit();
      
      FloatType alphas_u[3];
      alphas_u[0] = (ray[0] != 0) ? fov->vxL(0)/std::fabs(ray[0]) : 0;
      alphas_u[1] = (ray[1] != 0) ? fov->vxL(1)/std::fabs(ray[1]) : 0;
      alphas_u[2] = (ray[2] != 0) ? fov->vxL(2)/std::fabs(ray[2]) : 0;
      
      
      FloatType inc[3];
      inc[0] = (ray[0] < 0) ? -1 : 1;
      inc[1] = (ray[1] < 0) ? -1 : 1;
      inc[2] = (ray[2] < 0) ? -1 : 1;
      
      #ifdef DEBUG_JACOBS
      std::cout << "\ninc updates" << std::endl;
      std::cout << "inc[0] " << inc[0] << std::endl;
      std::cout << "inc[1] " << inc[1] << std::endl;
      std::cout << "inc[2] " << inc[2] << std::endl;
      #endif
      
      FloatType alphaMin, alphaMax;
      std::tie(alphaMin, alphaMax) = computeAlfaMinMax(start, ray);
      
      #ifdef DEBUG_JACOBS
      std::cout << "\alphaMin " << alphaMin << std::endl;
      std::cout << "alphaMax " << alphaMax << std::endl;
      #endif
      
      
      //the ray does not intercept the fov
      if (alphaMin >= alphaMax)
        return;
      
      uint16_t idxMin[3] = {1,1,1};
      uint16_t idxMax[3] = {0,0,0};
      FloatType alphas[3] = {1,1,1};
      
      for (uint16_t axis=0; axis<3; axis++) {
        #ifdef DEBUG_JACOBS
        std::cout << "\naxis " << axis << std::endl;
        std::cout << " ray " << ray[axis] << std::endl;
        #endif
        if (ray[axis]) {
          
          if (ray[axis] > 0) {
            idxMin[axis] = std::ceil((start[axis] + alphaMin*ray[axis] + fov->halfLength(axis))/fov->vxL(axis) );
            //fov->vxN(axis)+1 - (fov->halfLength(axis) - alphaMin*ray[axis] - start[axis])/fov->vxL(axis);
            idxMax[axis] = (start[axis] + alphaMax*ray[axis] + fov->halfLength(axis))/fov->vxL(axis);
            alphas[axis] = (fov->plane(axis,idxMin[axis])-start[axis])/ray[axis];
          } else {
            idxMin[axis] = std::ceil( (start[axis] + alphaMax*ray[axis] + fov->halfLength(axis))/fov->vxL(axis) );
            //fov->vxN(axis)+1 - (fov->halfLength(axis) - alphaMax*ray[axis] - start[axis])/fov->vxL(axis);
            idxMax[axis] = (start[axis] + alphaMin*ray[axis] + fov->halfLength(axis))/fov->vxL(axis);
            alphas[axis] = (fov->plane(axis,idxMax[axis])-start[axis])/ray[axis];
          }
          #ifdef DEBUG_JACOBS
          std::cout << " idxMin " << idxMin[axis] << std::endl;
          std::cout << " idxMax " << idxMax[axis] << std::endl;
          std::cout << " alphas "  << alphas[axis] << std::endl;
          #endif
        } //no else, already fixed on declaration
        
        
      }
      
      uint16_t n_planes = (idxMax[0]-idxMin[0]+1) + (idxMax[1]-idxMin[1]+1) + (idxMax[2]-idxMin[2]+1);
      #ifdef DEBUG_JACOBS
      std::cout << "\nn_planes " << n_planes << std::endl;
      #endif
      
      
      
      // Computing the first alphas
      for (uint16_t axis=0; axis<3; axis++) {
        if ( std::fabs(alphas[axis] - alphaMin) < EPSILON) {
          #ifdef DEBUG_JACOBS
          std::cout << "first inc " << axis << std::endl;
          #endif
          alphas[axis] = alphaMin + alphas_u[axis]; 
        }
      }
        
      FloatType min_alpha = std::min(alphas[0], std::min(alphas[1], alphas[2]) );
      #ifdef DEBUG_JACOBS
      std::cout << "\nmin_alpha " << min_alpha << std::endl;
      #endif
      
      //computing first voxel
      FloatType tmp = (min_alpha + alphaMin)/2;
      uint16_t coord[3];
      coord[0] = (start.x() + tmp*ray[0] - fov->plane(0,0) )/fov->vxL(0);
      coord[1] = (start.y() + tmp*ray[1] - fov->plane(1,0) )/fov->vxL(1);
      coord[2] = (start.z() + tmp*ray[2] - fov->plane(2,0) )/fov->vxL(2);
      
      FloatType alpha_c = alphaMin;
      FloatType ray_length = ray.r();
      uint16_t intercepts = 0;
      while ( intercepts<n_planes-1) {
        
        // when the ray intercepts the FOV in a corner there could be more than 
        // one increment at the same time. So, it is necessary to take into 
        // account this possibility when incrementing the variable of the for, to 
        // avoid that the ray-tracer keep on tracing after alfamax is reached. 
        uint16_t n_incs = 0;
        
        #ifdef DEBUG_JACOBS
        std::cout << "\nintercepts " << intercepts << std::endl;
        std::cout << "\tcoord[0] " << coord[0];
        std::cout << " coord[1] " << coord[1];
        std::cout << " coord[2] " << coord[2] << std::endl;
        #endif
        
        // It is possible to modify the inc variables to directly compute the 
        // linear index without using vX, vY and vZ and the call to the function
        // fov->computeLinearIndex(). The problem is that we cannot have a boundary
        // check on the voxels values. I Must investigate why the voxel sometimes 
        // goes out of the fixed interval 
        
        
        
        if ( coord[0] < fov->vxN(0) && coord[1] < fov->vxN(1) && coord[2] < fov->vxN(2) ) {
          uint32_t idx = fov->computeLinearIndex(coord[0],coord[1],coord[2]);
          
//           ThreeVector voxel = fov->vx2pos(coord[0],coord[1],coord[2]);
//           FloatType od = ( (-ray).cross(stop - voxel) ).r() / ray_length;
//           FloatType fwhm = 1.2;
//           FloatType prob = 1 - od/fwhm;
//           
//           buffer[idx] += prob*prob1 * prob2;
          
//           row.push_back( MatrixEntry( idx, prob) );
          
          FloatType prob = ( min_alpha - alpha_c ) * ray_length * weights1[c_p1] * weights2[c_p2]; // / norm_prob;
          buffer[idx] += prob;
          // max_prob = std::max(max_prob, buffer[idx]);
//           row.push_back( MatrixEntry( idx, prob) );
          
//           #ifdef NORMALIZE
//           total_probability += prob;
//           #endif

        }
        #ifdef DEBUG_JACOBS
        else
          std::cout << "\tskipping" << std::endl;
        #endif
        
        for (uint16_t axis=0; axis<3; axis++) {
          if (std::fabs(min_alpha - alphas[axis]) < EPSILON) {
            // interception found
            n_incs++;
            coord[axis] += inc[axis];
            alpha_c = alphas[axis];
            alphas[axis] = min_alpha + alphas_u[axis];
            #ifdef DEBUG_JACOBS
            std::cout << "\tinc axis " << axis << std::endl;
            #endif
          }
          #ifdef DEBUG_JACOBS
          std::cout << "\talphas[" << axis << "] " << alphas[axis] << std::endl;
          #endif
        }
        min_alpha = std::min(alphas[0], std::min(alphas[1], alphas[2]) );
        
        intercepts += n_incs;
      }
      
    }

  }


  
  row.clear();
  
  #ifdef NORMALIZE
  FloatType total_probability = 0;
  #endif
  
  row.reserve( buffer.size() );
  for (const std::pair<uint32_t, FloatType> entry : buffer) {
    #ifdef MASK
    if ( fov->mask(entry.first) ) {
    #endif
      row.push_back( MatrixEntry(entry.first, entry.second) );
      #ifdef NORMALIZE
      total_probability += entry.second;
      #endif
    #ifdef MASK
    }
    #endif
  }
  
  
  #ifdef NORMALIZE
  for (MatrixEntry& e : row)
    e.v /= total_probability;
  #endif
  
}
