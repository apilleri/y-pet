/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vProjector.h"

// #define DEBUG_JOSEPH

#define NORMALIZE

void JosephProjector::project(const PlanarDetectorType& det1, uint16_t cry1,
                              const PlanarDetectorType& det2, uint16_t cry2,
                              std::vector<MatrixEntry>& row)
{  
  //   ThreeVector start = det1.aCrySurfaceCenter(cry1);
  //   ThreeVector stop = det2.aCrySurfaceCenter(cry2);
  ThreeVector start = det1.aCryBestPoint(cry1);
  ThreeVector stop = det2.aCryBestPoint(cry2);
  
  #ifdef NORMALIZE
  FloatType total_probability = 0;
  #endif
  
  
  ThreeVector ray = stop-start;
  ray.round(1e-5);
  ThreeVector ray2 = ray.componentDot(ray);
  
  // Variables for Joseph
  ThreeVector pos; // Position of point in image space
  FloatType wfl[2] = { 0.0, 0.0 }; // Weight floor
  FloatType wcl[2] = { 0.0, 0.0 }; // Weight ceil
  FloatType w[4] = { 0.0, 0.0, 0.0, 0.0 }; // Interpolation weight
  int16_t index[2] = { 0, 0 }; // index in the image space
  
  // Condition on the largest component of r
  FloatType alphaMin=0, alphaMax=1;
  
  if( std::fabs( ray[0] ) > std::fabs( ray[1] ) )
  {
    std::tie(alphaMin, alphaMax) = computeAlfaMinMax(start, ray);
    
    //the ray does not intercept the fov
    if (alphaMin >= alphaMax) 
      return;    
    if( ray[1] == 0.0 ) 
      if( start[1] < -fov->halfLength(1) || start[1] > fov->halfLength(1) )
        return;
    if( ray[2] == 0.0 ) 
      if( start[2] < -fov->halfLength(2) || start[2] > fov->halfLength(2) )
        return;
    
    row.clear();
    
    // Computing weight of normalization
    FloatType const factor = std::fabs( ray[0] ) / ray.r2();
    FloatType const weight = fov->vxL(0) / factor ;
    
    // Computing the increment
    FloatType const ri[3] = {1.0, ray[1] / ray[0], ray[2] / ray[0]};
    
    // Computing the first and the last plane
    int iMin = 0, iMax = 0;
    if( ray[0] > 0.0 )
    {
      iMin = ::ceil( fov->vxN(0) - toleranceX - ( fov->halfLength(0) - alphaMin * ray[0] - start[0] ) / fov->vxL(0) );
      iMax = ::floor( toleranceX + ( start[0] + alphaMax * ray[0] - (-fov->halfLength(0)) ) / fov->vxL(0) );
    }
    else if( ray[0] < 0.0 )
    {
      iMin = ::ceil( fov->vxN(0) - toleranceX - ( fov->halfLength(0) - alphaMax * ray[0] - start[0] ) / fov->vxL(0) );
      iMax = ::floor( toleranceX + ( start[0] + alphaMin * ray[0] - (-fov->halfLength(0)) ) / fov->vxL(0) );
    }
    // Computing an offset to get the correct position in plane
    FloatType const offset = -fov->halfLength(0) + ( fov->vxL(0) * 0.5 ) - start[0];
    // Loop over all the crossing planes
    for( int i = iMin; i < iMax; ++i ) {
      // Computing position on crossed plane in term of grid spacing
      FloatType const step = offset + i * fov->vxL(0);
      pos[1] = start[1] + step * ri[ 1];
      pos[2] = start[2] + step * ri[ 2];
      // Find the index in the image
      index[0] = ::floor( toleranceY + ( pos[ 1] - boundY ) / fov->vxL(1) );
      index[1] = ::floor( toleranceZ + ( pos[ 2] - boundZ ) / fov->vxL(2) );
      
      if (index[0]-1 >=0 && index[0]-1<fov->vxN(1) && index[1]-1 >=0 && index[1]-1<fov->vxN(2) ) {
        uint32_t nvx_XY = fov->vxN(0)*fov->vxN(1);
        uint32_t idx = i + ( index[0] - 1 ) * fov->vxN(0) + ( index[1] - 1 ) * nvx_XY;
        
        // Bilinear interpolation component (using floor)
        wfl[0] = pos[ 1] - ( boundY + index[0] * fov->vxL(1) );
        wfl[1] = pos[ 2] - ( boundZ + index[1] * fov->vxL(2) );
        wfl[0] /= fov->vxL(1);
        wfl[1] /= fov->vxL(2);
        
        // Bilinear interpolation component (using ceil)
        wcl[0] = 1.0 - wfl[0];
        wcl[1] = 1.0 - wfl[1];
        
        // Final coefficients
        w[0] = wcl[0] * wcl[1];
        w[1] = wcl[0] * wfl[1];
        w[2] = wfl[0] * wfl[1];
        w[3] = wfl[0] * wcl[1];
        
        
        if (idx < fov->nVoxels() /*&& fov->mask(idx)*/) {
          FloatType prob = w[0] * weight;
          row.push_back( MatrixEntry(idx, prob) );
          #ifdef NORMALIZE
          total_probability += prob;
          #endif
        }
        uint32_t cur_idx = idx + nvx_XY;
        if (cur_idx < fov->nVoxels() /*&& fov->mask(cur_idx)*/) {
          FloatType prob = w[1] * weight;
          row.push_back( MatrixEntry(cur_idx, prob) );
          #ifdef NORMALIZE
          total_probability += prob;
          #endif
        }
        cur_idx = idx + fov->vxN(0) + nvx_XY;
        if (cur_idx < fov->nVoxels() /*&& fov->mask(cur_idx)*/) {
          FloatType prob = w[2] * weight;
          row.push_back( MatrixEntry(cur_idx, prob) );
          #ifdef NORMALIZE
          total_probability += prob;
          #endif
        }
        cur_idx = idx + fov->vxN(0);
        if (cur_idx < fov->nVoxels() /*&& fov->mask(cur_idx)*/) {
          FloatType prob = w[3] * weight;
          row.push_back( MatrixEntry(cur_idx, prob) );
          #ifdef NORMALIZE
          total_probability += prob;
          #endif
        }
        
      }
    }
  }
  else
  {
    FloatType alphaMin, alphaMax;
    std::tie(alphaMin, alphaMax) = computeAlfaMinMax(start, ray);
    
    //the ray does not intercept the fov
    if (alphaMin >= alphaMax)
      return;    
    if( ray[0] == 0.0 ) if( start[0] < -fov->halfLength(0) || start[0] > fov->halfLength(0) )
      return;
    if( ray[2] == 0.0 ) if( start[2] < -fov->halfLength(2) || start[2] > fov->halfLength(2) )
      return;
    
    row.clear();
    
    // Computing weight of normalization
    FloatType const factor = std::fabs( ray[1] ) / ray.r2();
    FloatType const weight = fov->vxL(1) / factor;
    
    // Computing the increment
    FloatType const ri[3] = {ray[0] / ray[1], 1.0, ray[2] / ray[1] };
    
    // Computing the first and the last plane
    int jMin = 0, jMax = 0;
    if( ray[1] > 0.0 ) {
      jMin = ::ceil( fov->vxN(1) - toleranceY - ( fov->halfLength(1) - alphaMin * ray[1] - start[1] ) / fov->vxL(1) );
      jMax = ::floor( toleranceY + ( start[1] + alphaMax * ray[1] - (-fov->halfLength(1)) ) / fov->vxL(1) );
    }
    else if( ray[1] < 0.0 ) {
      jMin = ::ceil( fov->vxN(1) - toleranceY - ( fov->halfLength(1) - alphaMax * ray[1] - start[1] ) / fov->vxL(1) );
      jMax = ::floor( toleranceY + ( start[1] + alphaMin * ray[1] - (-fov->halfLength(1)) ) / fov->vxL(1) );
    }
    
    // Computing an offset to get the correct position in plane
    FloatType const offset = -fov->halfLength(1) + ( fov->vxL(1) * 0.5 ) - start[1];
    
    // Loop over all the crossing planes
    for( int j = jMin; j < jMax; ++j ) {
      // Computing position on crossed plane in term of grid spacing
      FloatType const step = offset + j * fov->vxL(1);
      pos[0] = start[0] + step * ri[ 0];
      pos[2] = start[2] + step * ri[ 2];
      
      // Find the index in the image
      index[0] = ::floor( toleranceX + ( pos[ 0] - boundX ) / fov->vxL(0) );
      index[1] = ::floor( toleranceZ + ( pos[ 2] - boundZ ) / fov->vxL(2) );
      
      
      if (index[0]-1 >=0 && index[0]-1<fov->vxN(0) && index[1]-1 >=0 && index[1]-1<fov->vxN(2) ) {
        uint32_t nvx_XY = fov->vxN(0)*fov->vxN(1);
        uint32_t idx = ( index[0] - 1 ) + j * fov->vxN(0) + ( index[1] - 1 ) * nvx_XY;
        
        // Bilinear interpolation component (using floor)
        wfl[0] = pos[ 0] - ( boundX + index[0] * fov->vxL(0) );
        wfl[1] = pos[ 2] - ( boundZ + index[1] * fov->vxL(2) );
        wfl[0] /= fov->vxL(0);
        wfl[1] /= fov->vxL(2);
        
        // Bilinear interpolation component (using ceil)
        wcl[0] = 1.0 - wfl[0];
        wcl[1] = 1.0 - wfl[1];
        
        // Final coefficients
        w[0] = wcl[0] * wcl[1];
        w[1] = wcl[0] * wfl[1];
        w[2] = wfl[0] * wfl[1];
        w[3 ] = wfl[0] * wcl[1];
        
        
        
        if (idx < fov->nVoxels() && fov->mask(idx)) {
          FloatType prob = w[0] * weight;
          row.push_back( MatrixEntry(idx, prob) );
          #ifdef NORMALIZE
          total_probability += prob;
          #endif
        }
        uint32_t cur_idx = idx + nvx_XY;
        if (cur_idx < fov->nVoxels() && fov->mask(cur_idx)) {
          FloatType prob = w[1] * weight;
          row.push_back( MatrixEntry(cur_idx, prob) );
          #ifdef NORMALIZE
          total_probability += prob;
          #endif
        }
        cur_idx = idx + 1 + nvx_XY;
        if (cur_idx < fov->nVoxels() && fov->mask(cur_idx)) {
          FloatType prob = w[2] * weight;
          row.push_back( MatrixEntry(cur_idx, prob) );
          #ifdef NORMALIZE
          total_probability += prob;
          #endif
        }
        cur_idx = idx + 1;
        if (cur_idx < fov->nVoxels() && fov->mask(cur_idx)) {
          FloatType prob = w[3] * weight;
          row.push_back( MatrixEntry(cur_idx, prob) );
          #ifdef NORMALIZE
          total_probability += prob;
          #endif
        }
        
      }
    
    }
    
    
  }
  
  
  #ifdef NORMALIZE
  for (MatrixEntry& e : row)
    e.v /= total_probability;
  #endif
  
}


