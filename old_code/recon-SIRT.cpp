/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>

#include "cxxopts.hpp"

#include "dataTypes.h"
#include "f_utils.h"

#include "imaging_system.h"

int main(int argc, char **argv) 
{
  try {
    cxxopts::Options options(argv[0], "Image Reconstruction OSEM");
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file",
     cxxopts::value<std::string>() )
    ("fov", "Field of View", cxxopts::value<std::string>() )
    ("t, threads", "Number of threads", cxxopts::value<uint16_t>() )
    ("h, help", "Print help");
    
    options.add_options("Input/Output")
    ("i, input", "Input histogram(S)", cxxopts::value< StringVec >() )
    ("o, output", "Output image basename(s)", cxxopts::value< StringVec >() )
    ("output-dir", "Output directory (default \"recon\"",
     cxxopts::value<std::string>()->default_value("recon") )
    ("r, random", "Random histogram(s)", cxxopts::value< StringVec >());
    
    options.add_options("Models")
    ("z, zmodel", "Normalization histogram", cxxopts::value<std::string>() )
    ("projector", "Projector for on-the-fly computation", cxxopts::value<std::string>() )
    ("is-psf", "Apply image space resolution modelling with a "
    "space invariant 3D Gauss kernel: sigma,width",
     cxxopts::value< std::vector<FloatType> >() )
    ("bpconv", "Smooth backprojection with a space invariant 3D Gauss "
    "kernel: sigma,width", cxxopts::value< std::vector<FloatType> >() );
    
    //     ("b, background", "Background histogram", cxxopts::value<std::string>() )
    //     ("bgv", "Background value", cxxopts::value<FloatType>() )
    
    options.add_options("Sensitivity")
    ("compute-sensitivity", "Compute sensitivty in place")
    ("s, sensitivity", "Sensitivity raw file", cxxopts::value< StringVec >() );
    
    options.add_options("Reconstruction")
    ("start-image", "Start image", cxxopts::value<std::string>() )
    ("start-iter", "Start iteration number", cxxopts::value<uint16_t>() )
    ("x, subsets", "Number of subsets", cxxopts::value<uint16_t>() )
    ("n, number", "Number of iterations", cxxopts::value<uint16_t>() )
    ("e, every",  "Save image every N iterations", cxxopts::value<uint16_t>() )
    ("save-all-subsets", "Save all subsets", cxxopts::value<bool>() );
    
    options.add_options("Filtering")
    ("f, filter", "Apply a gaussian filter: sigma,width",
     cxxopts::value< std::vector<FloatType> >() );
    
    cxxopts::ParseResult cl_options = options.parse(argc, argv);
    
    if ( !cl_options.count("config") ) {
      std::string err_str = "Missing Scanner Decription XML config file\n\n";
      throw err_str + options.help();
    }
    
    if (!cl_options.count("fov"))
      throw std::string( "Missing fov\n\n" + options.help() );
  
    if (!cl_options.count("sensitivity") && !cl_options.count("compute-sensitivity")) {
      std::string err_str = "Missing (and not computing) sensitivity image\n\n";
      throw err_str + options.help();
    }
    
    if (cl_options.count("sensitivity") && cl_options.count("compute-sensitivity")) {
      std::string err_str = "Both computing and providing sensitivity\n\n";
      throw err_str + options.help();
    }
    
    if (!cl_options.count("input")) {
      std::string err_str = "Missing Input acquisition histogram(s)\n\n";
      throw err_str + options.help();
    }
    
    if (!cl_options.count("output")) {
      std::string err_str = "Missing output image basename(s)\n\n";
      throw err_str + options.help();
    }
    
    if (cl_options.count("start-image") && !cl_options.count("start-iter")) {
      std::string err_str = "You must specify both start-image and start-iter\n\n";
      throw err_str + options.help();
    }
    
    if (!cl_options.count("start-image") && cl_options.count("start-iter")) {
      std::string err_str = "You must specify both start-image and start-iter\n\n";
      throw err_str + options.help();
    }
    
    size_t o_s = cl_options["output"].as< StringVec >().size();
    size_t i_s = cl_options["input"].as< StringVec >().size();
    size_t r_s = 0;
    if ( cl_options.count("random") )
      r_s = cl_options["random"].as< StringVec >().size();
    
    if ( o_s != i_s ) {
      std::string err_str = "You must provide the same number of input";
      err_str += " and output arguments\n\n" + options.help();
      throw err_str + options.help();
    }
    if (r_s) {
      if ( r_s != i_s) {
        std::string err_str = "You must provide the same number of input";
        err_str += " and random arguments\n\n" + options.help();
        throw err_str + options.help();
      }
    }
    
    if (cl_options.count("sensitivity") && cl_options.count("subsets")) {
      const StringVec& sens_files = cl_options["sensitivity"].as< StringVec >();
      const uint16_t& subsets = cl_options["subsets"].as<uint16_t>();
      if (sens_files.size() != subsets) {
        std::string err_str = "Number of sensitivity files different than";
        err_str += " the number of subsets";
        throw err_str + options.help();
      }
    }
    
    
    const std::string& scanner_xml = cl_options["config"].as<std::string>();
    const std::string& fov_tag = cl_options["fov"].as<std::string>();
    const std::string& output_dir = cl_options["output-dir"].as<std::string>();
    ImagingSystem system(scanner_xml, fov_tag, output_dir);    
    
    std::ofstream txt_file(output_dir + "/parameters.txt");
    if ( !txt_file.is_open() )
      throw std::string("Unable to open " + output_dir + "/parameters.txt");
    txt_file << "Reconstruction parameters: " << std::endl;
    if (cl_options.count("zmodel") ) {
      txt_file << "Using normalization model: ";
      txt_file << cl_options["zmodel"].as<std::string>() << std::endl;
    }
    if (cl_options.count("filter") ) {
      //       const std::vector<FloatType> par ;
      txt_file << "Applying Gaussian filter: ";
      txt_file << "sigma: " << cl_options["filter"].as<std::vector<FloatType>>()[0];
      txt_file << std::endl;
      txt_file << "width: " << cl_options["filter"].as<std::vector<FloatType>>()[1];
      txt_file << std::endl;
    }
    if (cl_options.count("is-psf") ) {
      //       const std::vector<FloatType> par ;
      txt_file << "Applying Image Space PSF convolution: ";
      txt_file << "sigma: " << cl_options["is-psf"].as<std::vector<FloatType>>()[0];
      txt_file << " width: " << cl_options["is-psf"].as<std::vector<FloatType>>()[1];
      txt_file << std::endl;
    }
    txt_file.close();
    
    system.initReconstruction("SIRT_Full_ZG", cl_options);
    system.reconstruction()->apply();
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nError: " << error << std::endl;
    exit(1);
  }
  
  return EXIT_SUCCESS;
}






