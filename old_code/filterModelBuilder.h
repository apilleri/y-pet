/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef MODELS_FILTERMODELBUILDER_H_
#define MODELS_FILTERMODELBUILDER_H_

#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include <cstdint>

#include "dataTypes.h"
#include "vBuilder.h"

class FilterModelBuilder : public vBuilder {
  
  FloatType fwhm_x, fwhm_y;
  uint16_t nsx, nsy;
  
  uint16_t max;
  
  bool normalize;
  
public:
  FilterModelBuilder(const std::string& xmlFile, bool overwrite,
                     uint16_t max_p, bool normalize_p);
  
  void computeFundamentalPair(const PlanarDetectorType& det1,
                              const PlanarDetectorType& det2) override;
                              
};


#endif // MODELS_FILTERMODELBUILDER_H_


