/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vProjector.h"

// #define DEBUG_FASTOD22


void FastOD2Projector::project(const PlanarDetectorType& det1, uint16_t cry1,
                              const PlanarDetectorType& det2, uint16_t cry2,
                              std::vector<MatrixEntry>& row)
{
  //TODO restringere il raggio del cilindro
  FloatType cylinderR = std::sqrt(det1.yLen(cry1)*det1.zLen(cry1)/M_PI);
  FloatType rDiffValue = cylinderR - sphereR;
  FloatType rSumValue = cylinderR + sphereR;
  
  
//   ThreeVector start = det1.aCrySurfaceCenter(cry1);
//   ThreeVector stop = det2.aCrySurfaceCenter(cry2);
  ThreeVector start = det1.aCryBestPoint(cry1);
  ThreeVector stop = det2.aCryBestPoint(cry2);
  
  ThreeVector ray = stop-start;
  ray.round(1e-5);
  
  FloatType alphas_u[3];
  alphas_u[0] = (ray[0] != 0) ? fov->vxL(0)/std::fabs(ray[0]) : 0;
  alphas_u[1] = (ray[1] != 0) ? fov->vxL(1)/std::fabs(ray[1]) : 0;
  alphas_u[2] = (ray[2] != 0) ? fov->vxL(2)/std::fabs(ray[2]) : 0;
  
  #ifdef DEBUG_FASTOD2
  std::cout << "\nalphas updates" << std::endl;
  std::cout << "alphas_u[0] " << alphas_u[0] << std::endl;
  std::cout << "alphas_u[1] " << alphas_u[1] << std::endl;
  std::cout << "alphas_u[2] " << alphas_u[2] << std::endl;
  #endif
  
  FloatType inc[3];
  inc[0] = (ray[0] < 0) ? -1 : 1;
  inc[1] = (ray[1] < 0) ? -1 : 1;
  inc[2] = (ray[2] < 0) ? -1 : 1;
  
  #ifdef DEBUG_FASTOD2
  std::cout << "\ninc updates" << std::endl;
  std::cout << "inc[0] " << inc[0] << std::endl;
  std::cout << "inc[1] " << inc[1] << std::endl;
  std::cout << "inc[2] " << inc[2] << std::endl;
  #endif
  
  FloatType alphaMin, alphaMax;
  std::tie(alphaMin, alphaMax) = computeAlfaMinMax(start, ray);
  
  #ifdef DEBUG_FASTOD2
  std::cout << "\nalphaMin " << alphaMin << std::endl;
  std::cout << "alphaMax " << alphaMax << std::endl;
  #endif
  
  
  //the ray does not intercept the fov
  if (alphaMin >= alphaMax)
    return;
  
  row.clear();
  
  uint16_t mainAxis;
  if ( std::abs(ray.x()) < std::abs(ray.y()) ) {
    #ifdef T_DEBUG
    std::cout << "major axis y" << std::endl;
    #endif
    mainAxis = 1;    
  }
  else {
    #ifdef T_DEBUG
    std::cout << "major axis x" << std::endl;
    #endif
    mainAxis = 0;
  }
  
  #ifdef DEBUG_FASTOD2
  std::cout << "\nmainAxis " << mainAxis << std::endl;
  #endif
  
  
  uint16_t idxMin[3] = {1,1,1};
  uint16_t idxMax[3] = {0,0,0};
  FloatType alphas[3] = {1,1,1};
  
  
  for (uint16_t axis=0; axis<3; axis++) {
    #ifdef DEBUG_FASTOD2
    std::cout << "\naxis " << axis << std::endl;
    std::cout << " ray " << ray[axis] << std::endl;
    #endif
    if (std::fabs(ray[axis]) > EPSILON) {
      
      if (ray[axis] > 0) {
        idxMin[axis] = std::ceil((start[axis] + alphaMin*ray[axis] + fov->halfLength(axis))/fov->vxL(axis) );
        //fov->vxN(axis)+1 - (fov->halfLength(axis) - alphaMin*ray[axis] - start[axis])/fov->vxL(axis);
        idxMax[axis] = (start[axis] + alphaMax*ray[axis] + fov->halfLength(axis))/fov->vxL(axis);
        alphas[axis] = (fov->plane(axis,idxMin[axis])-start[axis])/ray[axis];
      } else {
        idxMin[axis] = std::ceil( (start[axis] + alphaMax*ray[axis] + fov->halfLength(axis))/fov->vxL(axis) );
        //fov->vxN(axis)+1 - (fov->halfLength(axis) - alphaMax*ray[axis] - start[axis])/fov->vxL(axis);
        idxMax[axis] = (start[axis] + alphaMin*ray[axis] + fov->halfLength(axis))/fov->vxL(axis);
        alphas[axis] = (fov->plane(axis,idxMax[axis])-start[axis])/ray[axis];
      }
      #ifdef DEBUG_FASTOD2
      std::cout << " idxMin " << idxMin[axis] << std::endl;
      std::cout << " idxMax " << idxMax[axis] << std::endl;
      std::cout << " alphas "  << alphas[axis] << std::endl;
      #endif
    } //no else, already fixed on declaration
    
    
  }
  
  uint16_t n_planes = (idxMax[0]-idxMin[0]+1) + (idxMax[1]-idxMin[1]+1) + (idxMax[2]-idxMin[2]+1);
  #ifdef DEBUG_FASTOD2
  std::cout << "\nn_planes " << n_planes << std::endl;
  #endif
  
  // Computing the first alphas
  for (uint16_t axis=0; axis<3; axis++) {
    if ( std::fabs(alphas[axis] - alphaMin) < EPSILON) {
      #ifdef DEBUG_FASTOD2
      std::cout << "first inc " << axis << std::endl;
      #endif
      alphas[axis] = alphaMin + alphas_u[axis]; 
    }
  }
  
  FloatType min_alpha = std::min(alphas[0], std::min(alphas[1], alphas[2]) );
  #ifdef DEBUG_FASTOD2
  std::cout << "\nmin_alpha " << min_alpha << std::endl;
  #endif
  
  //computing first voxel
  FloatType tmp = (min_alpha + alphaMin)/2;
  uint16_t coord[3];
  coord[0] = (start.x() + tmp*ray[0] - fov->plane(0,0) )/fov->vxL(0);
  coord[1] = (start.y() + tmp*ray[1] - fov->plane(1,0) )/fov->vxL(1);
  coord[2] = (start.z() + tmp*ray[2] - fov->plane(2,0) )/fov->vxL(2);
  
  
  int16_t iX, iY;
  int16_t *first, *second;
  
  if (mainAxis == 0) {
    first = &iX;
    second = &iY;
  }
  else {
    first = &iY;
    second = &iX;
  }
  
  FloatType ray_length = ray.r();
  uint16_t intercepts = 0;
  int32_t last = -1;
  while ( intercepts<n_planes-1) {
    
    #ifdef DEBUG_FASTOD2
    std::cout << "\nintercepts " << intercepts << std::endl;
    std::cout << "\tcoord[0] " << coord[0];
    std::cout << " coord[1] " << coord[1];
    std::cout << " coord[2] " << coord[2] << std::endl;
    #endif
    
    iX = coord[0];
    iY = coord[1];
    
    bool same_plane=false;
    
    if (*first == last)
      same_plane = true;
    
    if (!same_plane) {
      last = *first;
      int32_t sec_axis_stop = *second+step;
      *second -= step;
      if (*second < 0)
        *second = 0;
      while (*second < fov->vxN(1-mainAxis) && *second <= sec_axis_stop) {
        
        int32_t z_axis_stop = coord[2] + step;
        int32_t iZ = coord[2] - step;
        if (iZ < 0)
          iZ = 0;
        
        while (iZ < fov->vxN(2) && iZ <= z_axis_stop) {
          
          ThreeVector voxel = fov->vx2pos(iX, iY, iZ);
          #ifdef DEBUG_FASTOD2
          std::cout << iX << " " << iY << " " << iZ << " " << voxel;
          #endif
          FloatType probability = 0;
          //orthogonal distance
          
          // probability normalized to the maximum value that is voxel_volume
          FloatType od = ( (-ray).cross(stop - voxel - ThreeVector(0,0,fov->vxL(2)/4) ) ).r() / ray_length;
          #ifdef DEBUG_FASTOD2
          std::cout << " od1 " << od;
          #endif
          if ( od < rSumValue) {
            #ifdef DEBUG_FASTOD2
            std::cout << " in " << std::endl;
            #endif
            if (od <= rDiffValue)
              probability += 1;
            else 
              probability += (od-rSumValue)/(rDiffValue - rSumValue);
          }
          #ifdef DEBUG_FASTOD2
          else std::cout << " out" << std::endl;
          #endif
          
          od = ( (-ray).cross(stop - voxel + ThreeVector(0,0,fov->vxL(2)/4) ) ).r() / ray_length;
          #ifdef DEBUG_FASTOD2
          std::cout << " od2 " << od;
          #endif
          if ( od < rSumValue) {
            #ifdef DEBUG_FASTOD2
            std::cout << " in " << std::endl;
            #endif
            if (od <= rDiffValue)
              probability += 1;
            else 
              probability += (od-rSumValue)/(rDiffValue - rSumValue);
          }
          #ifdef DEBUG_FASTOD2
          else std::cout << " out" << std::endl;
          #endif
          
          
          if ( probability > 0 && iX < fov->vxN(0) ) 
            row.emplace_back( MatrixEntry(fov->computeLinearIndex(iX, iY, iZ), probability) );
          
          iZ++;
        }
        (*second)++;
      }
    }
    
    
    // when the ray intercepts the FOV in a corner there could be more than 
    // one increment at the same time. So, it is necessary to take into 
    // account this possibility when incrementing the variable of the loop.
    for (uint16_t axis=0; axis<3; axis++) {
      if (std::fabs(min_alpha - alphas[axis]) < EPSILON) {
        intercepts++;
        coord[axis] += inc[axis];
        if ( coord[axis] >= fov->vxN(axis) )
          return;
        alphas[axis] = min_alpha + alphas_u[axis];
        #ifdef DEBUG_FASTOD2
        std::cout << "\tinc axis " << axis << std::endl;
        #endif
      }
      #ifdef DEBUG_FASTOD2
      std::cout << "\talphas[" << axis << "] " << alphas[axis] << std::endl;
      #endif
    }
    min_alpha = std::min(alphas[0], std::min(alphas[1], alphas[2]) );
  }
  
  
}


