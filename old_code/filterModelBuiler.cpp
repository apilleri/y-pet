/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */


#include "modelT.h"
#include "filterModelBuilder.h"
#include "vGeometry.h"
#include "CylindricalGeometry.h"
#include "progress_bar.hpp"

FilterModelBuilder::FilterModelBuilder(const std::string& xmlFile,
                                       bool overwrite,
                                       uint16_t max_p,
                                       bool normalize_p)
: vBuilder(xmlFile), max(max_p), normalize(normalize_p)
{ 
  out_dir_name = scanner->name() + "_filterD_model";
  
  if ( !fs::exists(out_dir_name) )
    fs::create_directory(out_dir_name);
  else if (!overwrite)
    throw std::string("Directory " + out_dir_name + " exists, use --overwrite");
  
  scanner->printGeometryInfo(out_dir_name);
  scanner->printSymInfo(out_dir_name);
  
  file_log_name = out_dir_name + "/log";
  
  fwhm_x = 3.0/2;
  fwhm_y = 3.0/2;
  nsx = 3;
  nsy = 3;
  
  std::ofstream file_log(file_log_name);
  file_log << "Computing Detector model for scanner " << scanner->name();
  file_log << "Using Max number of elements per row:  " << max << std::endl;
  file_log << "Normalizing:  " << normalize << std::endl;
  file_log << "---" << std::endl;
  file_log.close();
}

void
FilterModelBuilder::computeFundamentalPair(const PlanarDetectorType& det1,
                                           const PlanarDetectorType& det2)
{

  std::string f_name = "DetectorModelBuilder::computeFundamentalPair";
  
  det1.printInfo();
  det2.printInfo();
  
  uint16_t n_vc1 = det1.vCryN();
  uint16_t n_vc2 = det2.vCryN();
  
  lorIDX_t n_LOFs = static_cast<lorIDX_t>(n_vc1)*n_vc2;

  
  // --- Computing filter Detector Matrix
  std::ofstream file_log(file_log_name, std::ios::app);
  if ( !file_log.is_open() ) 
    throw f_name + " unable to open file: " + file_log_name;
  file_log << "computing pair: " << det1.ID() << " --- " << det2.ID();
  file_log << std::endl;
  
  // --- Model
  ComputedModelType model(n_LOFs); 
  
  SClock::time_point timeI = SClock::now();
  ProgressBar progressBar(n_LOFs, file_log);
  ProgressBar progressBar2(n_LOFs, std::cout);
  std::atomic<uint32_t> notAddedCounter = 0;

  for (uint16_t start_cry = 0; start_cry < n_vc1; start_cry++) {
    std::vector<FloatType> kernel_start;
    uint16_t wx_start, wy_start;
    std::tie(kernel_start, wx_start, wy_start) =
      makeKernel(fwhm_x*det1.zLen(start_cry),
                fwhm_y*det1.yLen(start_cry), nsx, nsy, false);
      
    std::vector<FloatType> map_start;
    shiftKernel(map_start, det1.vCryNy(), det1.vCryNz(), start_cry,
                kernel_start, wx_start, wy_start, false);
    
    #pragma omp parallel for
    for (uint16_t stop_cry = 0; stop_cry < n_vc2; stop_cry++) {
      
      std::vector<FloatType> kernel_stop;
      uint16_t wx_stop, wy_stop;
      std::tie(kernel_stop, wx_stop, wy_stop) =
        makeKernel(fwhm_x*det2.zLen(stop_cry),
                   fwhm_y*det2.yLen(stop_cry), nsx, nsy, false);

      std::vector<FloatType> map_stop;
      shiftKernel(map_stop, det2.vCryNy(), det2.vCryNz(), stop_cry,
                  kernel_stop, wx_stop, wy_stop, false);
      
      std::vector<entryT> tensor_product(n_LOFs);
      
      uint32_t forward_lof = crystals2LOF(start_cry, stop_cry, n_vc2);
      
      FloatType tot_LOF_p = computeTensorProduct(map_start, map_stop, tensor_product, 0);
      
      if ( tensor_product.empty() ) {
        std::cout << "\n" << start_cry << " " << stop_cry << std::endl;
        throw f_name + "tensor_product empty";
      }
      
      
      FloatType norm_saved = 0;
      uint16_t current_N_added = 0;
      // --- Save every lor until the fired lof is found and the Threshold probability is reached
      bool added = false, c1, c2;
      do {
        norm_saved += tensor_product[current_N_added].first;
        if (!added)
          added = (forward_lof == tensor_product[current_N_added].second);
        current_N_added++;
        c1 = current_N_added < max;
        c2 = current_N_added < tensor_product.size();
      } while( /*!added  ||*/ c1 && c2 );
      
      if (!added) notAddedCounter++;
      
//       std::cout << forward_lof << std::endl;
      model[forward_lof].resize(current_N_added);
      for(uint32_t ii = 0; ii < current_N_added; ii++) {
        model[forward_lof][ii].col = tensor_product[ii].second;
        FloatType value = tensor_product[ii].first / norm_saved; //norm 1
        model[forward_lof][ii].v = value;
//         std::cout << "\t" << tensor_product[ii].second << " " << value << std::endl;;
      }
      
      #pragma omp critical 
      {
        ++progressBar;
        ++progressBar2;
      }
      
    }
  }
  
  
  SClock::time_point timeF = SClock::now();
  file_log << "NotAdded: " << notAddedCounter << std::endl;
  file_log << "Time: ~ " << std::chrono::duration_cast<std::chrono::seconds>(timeF - timeI).count() << " s\n" << std::endl;
  
  
  // --- Detector Matrix Check
  file_log << "Detector Matrix Check" << std::endl;
  for (uint32_t c_lof=0; c_lof<n_LOFs; c_lof++) {
    for (const MatrixEntry& entry : model[c_lof] ) {
      if ( entry.v < 0 )
        file_log << "Error p<0 - LOF: " << c_lof << " LOR: " << entry.col << std::endl;
      if ( std::isnan( entry.v ) )
        file_log << "Error nan - LOF: " << c_lof << " LOR: " << entry.col << std::endl;
      if ( std::isinf( entry.v ) )
        file_log << "Error inf - LOF: " << c_lof << " LOR: " << entry.col << std::endl;
      if ( entry.col > n_LOFs )
        file_log << "Error out of bounds - LOF: " << c_lof << " LOR: " << entry.col << std::endl;
    }
  }
  file_log << "Done!" << std::endl;
  
  std::string model_name = out_dir_name + "/" + std::to_string(det1.ID()) + "-" + std::to_string(det2.ID());
  
  saveModel(model_name+".filter", model);
  
}




