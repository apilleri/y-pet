/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <tuple>

#include "cxxopts.hpp"
#include "f_utils.h"
#include "timer.hpp"
#include "CylindricalGeometry.h"
#include "HistogramsMap.h"
#include "phantoms.h"

#include <sys/types.h>
#include <dirent.h>

std::tuple<uint16_t, uint16_t> convert(uint32_t crystal)
{
  uint16_t det, idAx, idTrans;

  idAx = crystal/240;
  det = (crystal%240)/30;
  idTrans = (crystal%240)%30;
  
  uint16_t idCry = idAx*30 + idTrans;
  
  return std::make_tuple(det, idCry);
}


int main(int argc, char *argv[])
{
  try {   
    cxxopts::Options options(argv[0], "Histogram Utils");
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file", cxxopts::value<std::string>() )
    ("hist", "Input histogram for computing the norm", cxxopts::value<std::string>() )
    ("castor", "Castor histogram datafile", cxxopts::value<std::string>() )
    ("o, output", "Output Histogram",cxxopts::value<std::string>())
    ("activate-vg", "Activate virtual geometry")
    
    ("h, help", "Print help");
    
    auto cmd_line = options.parse(argc, argv);
    
    if ( cmd_line.count("help") ){
      std::cout << options.help() << std::endl;
      exit(1);
    }
    
    if (!cmd_line.count("config"))
      throw std::string( "Missing Scanner Decription XML config file\n\n" + options.help() );
    if (!cmd_line.count("hist"))
      throw std::string( "Missing hist file\n\n" + options.help() );
    if (!cmd_line.count("castor"))
      throw std::string( "Missing castor data file\n\n" + options.help() );
    if (!cmd_line.count("output"))
      throw std::string( "Missing output histogram file\n\n" + options.help() );
    
    const std::string& scanner_xml = cmd_line["config"].as<std::string>();
    const std::string& norm_hist_fn = cmd_line["hist"].as<std::string>();
    const std::string& castor_fn = cmd_line["castor"].as<std::string>();
    const std::string& out_file = cmd_line["output"].as<std::string>();
    bool activate_vg = cmd_line.count("activate-vg");
    
    vGeometry *scanner = vGeometry::initScanner(scanner_xml, activate_vg);
    
    HistogramsMap norm_hist;
    norm_hist.allocate(scanner->getDetectorsList(),
                     scanner->getCoincidenceSchema(),
                     "LORs", 0);
    norm_hist.load( norm_hist_fn );
    norm_hist.printStats();
    
    std::ifstream castor_file;
    castor_file.open(castor_fn);
    std::ofstream castor_out;
    castor_out.open(out_file);
    
    uint32_t n_crystals = 900*8;
    
    uint32_t time;
    float amount;
    uint32_t c1, c2;
    uint32_t n_events = 0;
    float tot_events = 0;
    uint32_t n_null = 0;
    while( castor_file.peek()!=EOF && !castor_file.eof() ) {
      readFromFile<uint32_t>(castor_file, &time);
      readFromFile<float>(castor_file, &amount);
      readFromFile<uint32_t>(castor_file, &c1);
      readFromFile<uint32_t>(castor_file, &c2);
      
      
      
      if (c1 < 0 || c1 > n_crystals)
        std::cout << "warning c1: " << c1 << std::endl;
      if (c2 < 0 || c2 > n_crystals)
        std::cout << "warning c2: " << c2 << std::endl;
      
      n_events++;
      if (amount == 0)
        n_null++;
      else {
        tot_events += amount;
        
        uint16_t det1, det2;
        uint16_t nc1, nc2;
        
        std::tie(det1, nc1) = convert(c1);
        std::tie(det2, nc2) = convert(c2);
        
        uint32_t detPair = szudzikPair(det1, det2);
        std::vector<detPair_t> k = norm_hist.keys();
        if (norm_hist.data.find(detPair) == norm_hist.data.end())
          detPair = szudzikPair(det2, det1);
        if (norm_hist.data.find(detPair) == norm_hist.data.end()) {
          std::cout << "detPair not found" << std::endl;
          std::cout << "c1: " << c1 << " c2: " << c2 << std::endl;
          std::cout << "det1: " << det1 << " nc1: " << nc1 << std::endl;
          std::cout << "det2: " << det2 << " nc2: " << nc2 << std::endl;
          std::cout << "detPair: " << detPair << std::endl;
          return 1;
        }
        
        uint32_t lor = nc1*900+nc2;
        
        if (lor >= 900*900) {
          std::cout << "lor: " << lor << std::endl;
          std::cout << "c1: " << c1 << " c2: " << c2 << std::endl;
          std::cout << "det1: " << det1 << " nc1: " << nc1 << std::endl;
          std::cout << "det2: " << det2 << " nc2: " << nc2 << std::endl;
          std::cout << "detPair: " << detPair << std::endl;
        }
      
        float norm_factor = norm_hist.data[detPair].at(lor);
        
        castor_out.write(reinterpret_cast<char*>(&time), sizeof(uint32_t)); 
        castor_out.write(reinterpret_cast<char*>(&norm_factor), sizeof(uint32_t)); 
        castor_out.write(reinterpret_cast<char*>(&amount), sizeof(float)); 
        castor_out.write(reinterpret_cast<char*>(&c1), sizeof(uint32_t)); 
        castor_out.write(reinterpret_cast<char*>(&c2), sizeof(uint32_t)); 
      }
      
//       std::cout << time << " " << amount << " " << c1 << " " << c2 << std::endl;
    }
    
    std::cout << n_events << " " << n_null << " " << tot_events << std::endl;
    
    
    
    castor_file.close();
    castor_out.close();
//     while( inputHistogramFile.peek()!=EOF && !inputHistogramFile.eof() ) {
//       readFromFile<detPair_t>(inputHistogramFile, &szudzik_id);
//       if ( data.find(szudzik_id) == data.end() ) {
//         std::cout << "\nError: szudzik_id: " << szudzik_id << std::endl;
//         throw std::string("HistogramsMap::load Unable to find detector pair: ");
//       }
//       
//       uint32_t n_lors = data[szudzik_id].size();
//       uint32_t read_n_lors;
//       readFromFile<uint32_t>(inputHistogramFile, &read_n_lors);
//       if (read_n_lors != n_lors) {
//         std::cout << "\nERROR" << std::endl,
//         std::cout << "read_n_lors: " << read_n_lors << std::endl;
//         std::cout << "n_lors: " << n_lors << std::endl;
//         throw std::string("HistogramsMap::load n_lors != read_n_lors");
//       }
//       readFromFile<FloatType>(inputHistogramFile, data[szudzik_id].data(), n_lors);
//       
//     }
    
    
//     HistogramsMap out_hg;
//     out_hg.allocate(scanner->getDetectorsList(),
//                     scanner->getCoincidenceSchema(),
//                     "LORs", 0);
//     
//     std::vector<detPair_t> keys = out_hg.keys();
//     
//     
//     // the measured histogram is fixed in case of zero counts: the null LOR counts
//     // are set to the minumum value present in that detector pair
//     uint32_t fixed = 0;
//     for (detPair_t k : keys) {
//       FloatType min = 1e20; //naive technique
//       
//       for (uint32_t i=0; i < meas_hg.data[k].size(); i++) {
//         if ( meas_hg.data[k][i] > 0 && meas_hg.data[k][i] < min )
//           min = meas_hg.data[k][i];
//       }
//       
//       for (uint32_t i=0; i < meas_hg.data[k].size(); i++) {
//         if ( meas_hg.data[k][i] == 0 ) {
//           fixed++;
//           meas_hg.data[k][i] = min;
//         }
//       }
//     }
//     
//     HistogramsMap teo_hg;
//     
//     if (uniform) {
//       teo_hg.allocate(scanner->getDetectorsList(),
//                       scanner->getCoincidenceSchema(),
//                       "LORs", 1);
//     } else {
//       const std::string& teo_file = cmd_line["teoric"].as<std::string>();
//       teo_hg.allocate(scanner->getDetectorsList(),
//                       scanner->getCoincidenceSchema(),
//                       "LORs", 0);
//       teo_hg.load( teo_file );
//       teo_hg.printStats();
//     }
//     
//     uint32_t n_nulls = 0;
//     for (detPair_t k : keys) {
//       for (uint32_t i=0; i < out_hg.data[k].size(); i++) {
//         if ( teo_hg.data.at(k)[i] == 0) {
//           n_nulls++;
//           out_hg.data[k][i] = 1;
//         } else
//           out_hg.data[k][i] = meas_hg.data[k][i] / teo_hg.data[k][i];
//         
//       }
//     }
//     
//     std::cout << "project n_nulls: " << n_nulls << std::endl;
//     std::cout << "fixed: " << fixed << std::endl;
//     
//     const GroupedCS& gcs = scanner->getGroupedCS();
//     
//     std::cout << "Histograms statistics" << std::endl;
//     float tot_sum=0;
//     out_hg.printHeader();
//     for (const std::vector<CoincidencePairSym>& vec : gcs) {
//       FloatType local_sum = 0;
//       for (const CoincidencePairSym& pair : vec) {
//         detPair_t id = pair.pairID();
//         out_hg.printPair(id);
//         local_sum += std::accumulate(out_hg.data[id].begin(),
//                                      out_hg.data[id].end(), 0.);
//       }
//       std::cout << "\t\tMean counts: " << std::setw(8) << local_sum/vec.size();
//       std::cout << std::endl << std::endl;
//       tot_sum += local_sum;
//     }
//     
//     std::cout << "# detector pairs: " << out_hg.data.size() << std::endl;
//     std::cout << "tot_sum: " << tot_sum << std::endl;
//     
//     out_hg.save(out_file);
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nError: " << error << std::endl;
    exit(1);
  }
}
