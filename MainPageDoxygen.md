# recon
\mainpage
##General Aspects
The software provides visualization and reconstruction tools for 
PET scanners. All the information needed by the software are stored in a XML file.
The information consists in the geometry of the scanner, the geometry of the Field of View 
and some parameters needed by the reconstruction process. 

The software provides three binaries:
- visualizeGeometry: it provides an intteractive VTK visualizer to access the scanner
  geometry.
- computeModel: it allows the computation and storage to disk of the geometric model of
  the system
- reconstruct: it allows the reconstruction of the image from a histogram of LOR counts

NB: The environment has a right-handed Cartesian 3D coordinate system. The origin of
the axes is equal to the geometrical center of the scanner. The Z-axis is equal to
the axis of the scanner, the X-axis is horizontal and the Y-axis is vertical.

---

##XML Configuration File
All the information needed to execute the software is extracted from an XML file which must
contains a certain set of tags. The main sections are relative to the scanner, the FOV,
the projector and concidence schema of the detectors.

###Geometry
The geometry is described into the XML configuration file inside the <SCANNER> tag.
Until now only a Cylindrical geometry is implemented.

###The Cylindrical Scanner Geometry

The scanner is supposed to be made of a series of repeated ring sectors.
Each ring sector is composed of a series of modules. Each module contains blocks
and each block contains one or more layers of crystals. The layer is made of a
matrix of scintillation crystals. Inside a ring, the modules area arranged in a 
row along the Z direction. Inside a module, the blocks are arranged as a regular
grid: it is possible to define the number of blocks along the Y and Z directions.
Inside a block, the layers are stacked along the X direction. The layers have a 
linear index starting from 0 from the outermost one (the one with higher positive X).
Inside a layer, the crystals are arranged as a regular grid: it is possible to 
define the number of crystals along the Y and Z directions.

The first part is about the <CYLINDER> where the major characteristics of the 
scanner are defined, like the number of ring sectors is defined.
The first sector, with ID=0, is located with coordinate of the center (x,0,z). 
It is possible to define the <ROTATION_DIRECTION> of the ring sectors starting from 
the one with ID=0.


The placement of the modules and the blocks is defined by choosing a PRINCIPAL and 
a SECONDARY axis and their direction. To understand these parameters, let's consider
the following picutre:

INSERIRE FIGURA CON LE VARIE CONFIGURAZIONI

The PRINCIPAL_AXIS and SECONDARY_AXIS parameters are used for the placement of
- the modules inside the sector
- the blocks inside the module, arranged in Y-Z grid
- the crystals inside the layer. 

If a block contains more than one layer, the outermost layer has the lowest index (i.e. 0)

<code>
<SCANNER>\n
&nbsp;&nbsp;<TYPE>CYLINDRICAL</TYPE>\n\n
&nbsp;&nbsp;<NAME>IRIS</NAME>\n\n
&nbsp;&nbsp;<CYLINDER>\n
&nbsp;&nbsp;&nbsp;&nbsp;<CYLINDER_RADIUS>55.4</CYLINDER_RADIUS><!--  Half-Distance between  two opposed detector-->\n
&nbsp;&nbsp;&nbsp;&nbsp;<SECTORS_NUMBER>8</SECTORS_NUMBER>\n
&nbsp;&nbsp;&nbsp;&nbsp;<ROTATION_DIRECTION>ANTICLOCKWISE</ROTATION_DIRECTION>\n
&nbsp;&nbsp;&nbsp;&nbsp;<PRINCIPAL_AXIS>Y</PRINCIPAL_AXIS>\n
&nbsp;&nbsp;&nbsp;&nbsp;<SECONDARY_AXIS>Z</SECONDARY_AXIS>\n
&nbsp;&nbsp;&nbsp;&nbsp;<PRINCIPAL_AXIS_DIRECTION>POSITIVE</PRINCIPAL_AXIS_DIRECTION>\n
&nbsp;&nbsp;&nbsp;&nbsp;<SECONDARY_AXIS_DIRECTION>NEGATIVE</SECONDARY_AXIS_DIRECTION>\n
&nbsp;&nbsp;</CYLINDER>\n\n

&nbsp;&nbsp;<RING_SECTOR>\n
&nbsp;&nbsp;&nbsp;&nbsp;<SECTOR_LENGTH_X>12</SECTOR_LENGTH_X>\n
&nbsp;&nbsp;&nbsp;&nbsp;<MODULES_NUMBER>2</MODULES_NUMBER>\n
&nbsp;&nbsp;&nbsp;&nbsp;<MODULES_OFFSET>6.84</MODULES_OFFSET>\n
&nbsp;&nbsp;</RING_SECTOR>\n\n

&nbsp;&nbsp;<MODULE>\n
&nbsp;&nbsp;&nbsp;&nbsp;<MODULE_LENGTH_Y>45.36</MODULE_LENGTH_Y><!-- Including crystal reflecting tape--><!--45.28-->\n
&nbsp;&nbsp;&nbsp;&nbsp;<MODULE_LENGTH_Z>44.46</MODULE_LENGTH_Z><!-- Including crystal reflecting tape--><!--44.35-->\n
&nbsp;&nbsp;&nbsp;&nbsp;<BLOCK_GRID_Y>1</BLOCK_GRID_Y>\n
&nbsp;&nbsp;&nbsp;&nbsp;<BLOCK_GRID_Z>1</BLOCK_GRID_Z>\n
&nbsp;&nbsp;&nbsp;&nbsp;<BLOCK_OFFSET_Y>0</BLOCK_OFFSET_Y>\n
&nbsp;&nbsp;&nbsp;&nbsp;<BLOCK_OFFSET_Z>0</BLOCK_OFFSET_Z>\n
&nbsp;&nbsp;</MODULE>\n\n

&nbsp;&nbsp;<BLOCK>\n
&nbsp;&nbsp;&nbsp;&nbsp;<BLOCK_LENGTH_Y>45.36</BLOCK_LENGTH_Y>\n
&nbsp;&nbsp;&nbsp;&nbsp;<BLOCK_LENGTH_Z>44.46</BLOCK_LENGTH_Z>\n
&nbsp;&nbsp;&nbsp;&nbsp;<LAYERS_NUMBER>1</LAYERS_NUMBER>\n
&nbsp;&nbsp;</BLOCK>\n\n

&nbsp;&nbsp;<LAYER0>\n
&nbsp;&nbsp;&nbsp;&nbsp;<CRYSTALS_NUMBER_Y>27</CRYSTALS_NUMBER_Y>\n
&nbsp;&nbsp;&nbsp;&nbsp;<CRYSTALS_NUMBER_Z>26</CRYSTALS_NUMBER_Z>\n
&nbsp;&nbsp;&nbsp;&nbsp;<CRYSTAL_TYPE>0</CRYSTAL_TYPE>\n
&nbsp;&nbsp;&nbsp;&nbsp;<CRYSTAL_PITCH_Y>1.68</CRYSTAL_PITCH_Y>\n
&nbsp;&nbsp;&nbsp;&nbsp;<CRYSTAL_PITCH_Z>1.71</CRYSTAL_PITCH_Z>\n
&nbsp;&nbsp;</LAYER0>\n\n
  
&nbsp;&nbsp;<CRYSTAL_TYPE0>\n
&nbsp;&nbsp;&nbsp;&nbsp;<CRYSTAL_LENGTH_X>12</CRYSTAL_LENGTH_X>\n
&nbsp;&nbsp;&nbsp;&nbsp;<CRYSTAL_LENGTH_Y>1.6</CRYSTAL_LENGTH_Y>\n
&nbsp;&nbsp;&nbsp;&nbsp;<CRYSTAL_LENGTH_Z>1.6</CRYSTAL_LENGTH_Z>\n
&nbsp;&nbsp;</CRYSTAL_TYPE0>\n\n

</SCANNER>\n
</code>


###The Field of View
The Field of View (FOV) is completely described by the number of voxels
and their lengths along the three cartesian directions.

<code>
<FOV>\n
&nbsp;&nbsp;<VOXEL_NUMBER_X>101</VOXEL_NUMBER_X>\n
&nbsp;&nbsp;<VOXEL_NUMBER_Y>101</VOXEL_NUMBER_Y>\n
&nbsp;&nbsp;<VOXEL_NUMBER_Z>120</VOXEL_NUMBER_Z>\n
&nbsp;&nbsp;<VOXEL_LENGTH_X>0.84</VOXEL_LENGTH_X>\n
&nbsp;&nbsp;<VOXEL_LENGTH_Y>0.84</VOXEL_LENGTH_Y>\n
&nbsp;&nbsp;<VOXEL_LENGTH_Z>0.855</VOXEL_LENGTH_Z>\n
</FOV>
</code>

###PROJECTOR
In this section it is necessary to provide the name of the projector that should be used 
during the reconstruction process.

<PROJECTOR sphere="1">MIXEDOD2</PROJECTOR> <!-- sphere 1 or 2 -->
or
<PROJECTOR>SIDDON</PROJECTOR>

The software provides two projectors, a single ray Siddon ray tracer and a more accurate 
projector based on the orthogonal distance between the center of the voxel and the LOR.
The latter one need a parameter "sphere" (1 for cubic voxel or 2 for rectangular voxel)

###COINCIDENCE SCHEMA

<PAIRS_LIST>
  <ITEM type="real">0-4</ITEM>
  <ITEM type="real">0-3</ITEM>
  <ITEM type="virtual">0-2</ITEM>
</PAIRS_LIST>


---
