/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef MODELS_SYSTEMMODELBUILDER_H_
#define MODELS_SYSTEMMODELBUILDER_H_

#include "vBuilder.h"
#include "vGeometry.h"
#include "f_utils.h"

class SystemModelBuilder : public vBuilder {
  
  std::string g_dir;
  std::string d_dir;
  
  uint16_t splits;
  
public:
  
  explicit SystemModelBuilder(const std::string& xmlFile,
                              const std::string& fov_tag_p,
                              const std::string& g_dir_p,
                              const std::string& d_dir_p,
                              bool overwrite
                             );
  
  ~SystemModelBuilder() {}

  void setSplits(uint16_t splits_p) { splits = splits_p; }
  
  void computeFundamentalPair(const PlanarDetectorType& det1,
                              const PlanarDetectorType& det2) override;
};

#endif // MODELS_SYSTEMMODELBUILDER_H_

