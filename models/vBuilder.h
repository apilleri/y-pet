/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef MODELS_VBUILDER_H_
#define MODELS_VBUILDER_H_

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <utility>
#include <cstdint>
#include <chrono>


#include "dataTypes.h"
#include "modelT.h"
#include "PlanarDetector.h"
#include "vGeometry.h"
#include "fieldOfView.h"

using ComputedModelType = std::vector< std::vector<MatrixEntry> > ;
using entryT =  std::pair<FloatType, uint32_t>;

/* Order of methods calling for a derived vBuilder class:
 * - declaration *Builder(scanner_xml)
 * - setOverwrite if needed
 * - init()
 */

class vBuilder {
protected:
  //! scanner configuration file
  std::string xml_config;
  
  std::string out_dir_name;
  
  std::string file_log_name;

  //! flag for computing the transposed model
  bool transposed;
  
  vGeometry *scanner;
  
  //! Pointer to the FOV object of the system
  FieldOfView *FOV;

public:
  explicit vBuilder(const std::string& xmlFile, bool activate_vg)
    : xml_config(xmlFile),
      out_dir_name(""),
      file_log_name(""),
      transposed(false)
  {
    scanner = vGeometry::initScanner(xml_config, activate_vg);
  }
  
  ~vBuilder()
  {
    delete FOV;
    delete scanner;
  }
  
  void setTransposed() { transposed = true; }

  //! compute the model relative to a single DetectorPair
  virtual void computeFundamentalPair(const PlanarDetectorType& det1,
                                      const PlanarDetectorType& det2) = 0;

  //! computes the whole model, one DetectorPair at the time, and stores it to disk.
  void computeAndStoreFullModel();
  

  void transposeModel(ComputedModelType& in, uint32_t current_cols);

  void truncateOutFiles(const std::string& base_path);

  
  void saveModel(const std::string& fname, const ComputedModelType& model);
  
  
  FloatType computeTensorProduct(const std::vector<FloatType> &q1,
                                 const std::vector<FloatType> &q2,
                                 std::vector<entryT> &res,
                                 FloatType th);
  
  
  inline uint32_t crystals2LOF(uint16_t c1, uint16_t c2, uint16_t n_cry)
  {
    return (static_cast<uint32_t>(c1)*n_cry + c2);
  }
  
  
};



#endif // MODELS_VBUILDER_H_
