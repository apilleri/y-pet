/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "geometricModelBuilder.h"

#include "vProjector.h"
#include "tinyxml2.h"

using namespace tinyxml2;


GeometricModelBuilder::GeometricModelBuilder(const std::string& xmlFile,
																						 const std::string& fov_tag_p,
																						 const std::string& proj_tag_p,
																						 bool activate_vg,
																						 bool overwrite
																						)
: vBuilder(xmlFile, activate_vg), projector(nullptr)
{ 
	FOV = new FieldOfView(xmlFile, fov_tag_p);
	projector = vProjector::initProjector(xml_config, FOV, proj_tag_p);
	
	out_dir_name = scanner->name() + "_G_model_" + projector->type() + "_";
	if (scanner->virtualGeometry())
		out_dir_name += "VirtualGeometry_";
	out_dir_name += std::to_string(FOV->vxN(0)) + "x";
	out_dir_name += std::to_string(FOV->vxN(1)) + "x";
	out_dir_name += std::to_string(FOV->vxN(2));
	
	if ( !fs::exists(out_dir_name) )
		fs::create_directory(out_dir_name);
	else if (!overwrite)
		throw std::string("Directory " + out_dir_name + " exists, use --overwrite");
	
	FOV->printInfo(out_dir_name + "/" + scanner->name() + "_" + fov_tag_p + ".txt");
	scanner->printGeometryInfo(out_dir_name);
	scanner->printSymInfo(out_dir_name);
	
	file_log_name = out_dir_name + "/log";
	
	std::ofstream file_log(file_log_name);
	file_log << "Computing Geometric model for scanner " << scanner->name();
	file_log << "\nUsing projector: " << projector->type() << " ";
	for (auto it = projector->info().begin(); it != projector->info().end(); it++)
		file_log << it->first << " " << std::to_string(it->second) << " ";
	file_log << std::endl <<"---" << std::endl;
	file_log.close();
}

void
GeometricModelBuilder::computeFundamentalPair(const PlanarDetectorType& det1,
																							const PlanarDetectorType& det2)
{
	uint16_t id1 = det1.ID();
	uint16_t id2 = det2.ID();
	uint32_t n_ac1 = det1.aCryN();
	uint32_t n_ac2 = det2.aCryN();
	uint32_t n_LORs =  n_ac1 * n_ac2;
	
	std::ofstream file_log(file_log_name, std::ios::app);
	file_log << "computing pair: " << id1;
	file_log << " nC: " << n_ac1;
	file_log << " --- " << id2;
	file_log << " nC: " << n_ac2 << std::endl;
	file_log << "\tallocation time: " << std::flush;
		
	//TODO probabilmente sto discorso non serve 
	/*  The matrix is preallocated to optimize the computation, meaning the 
	 *  insertion of elements. The moduleMatrix vector is resized to the exact 
	 *  number of LOR of the current part of the model. This number most likely 
	 *  is constant during the whole module computation as the scanner is usually 
	 *  uniform in the number of crystals  in each detector. 
	 *  (TODO)For every row of the matrix a block of LOR_SIZE is preallocated. This 
	 *  number should be provided from the gemetry class as an extimate of the maximun 
	 *  number of possibe crossed voxels in the LOR. After the projection the row of
	 *  the matrix is set to the exact number of crossed voxels of the LOR before the 
	 *  actual copy ito the matrix. 
	 * 
	 *  As the std::vector::resize method do not shrink the capacity of the container
	 *  a reallocation is performed at most one time during the whole computation.
	 *  This is particularly useful when the number of sub models to compute is high.
	 *
	 */
	SClock::time_point timeI = SClock::now();
	ComputedModelType model(n_LORs);
	SClock::time_point timeF = SClock::now();
	file_log << std::chrono::duration_cast<std::chrono::milliseconds>(timeF - timeI).count()
					<< " ms" << std::endl;
	
	file_log << "\tcomputation " << std::flush;
	timeI = SClock::now();
	
	#pragma omp parallel for
	for(uint32_t lor=0; lor<n_LORs; lor++) {
		std::vector<MatrixEntry> vxList;
		uint16_t c1 = lor/n_ac2;
		uint16_t c2 = lor%n_ac2;
		projector->project(det1, c1, det2, c2, vxList);
		std::sort(vxList.begin(), vxList.end(),
							[&](const MatrixEntry& a, const MatrixEntry& b)
							{
								return a.col < b.col;
							}
		);
		model[lor].resize( vxList.size() );
		model[lor].assign( vxList.begin(), vxList.end() );
	}
	
	timeF = SClock::now();
	
	file_log << "time: ~ "
					<< std::chrono::duration_cast<std::chrono::milliseconds>(timeF - timeI).count()
					<< " ms" << std::endl;

	// --- Checking values
	file_log << "\tchecking values" << std::flush;
	#pragma omp parallel for
	for (uint32_t lor=0; lor<n_LORs; lor++) {
		for (uint32_t vx=0; vx<model[lor].size(); vx++) {
			FloatType probability = model[lor][vx].v;
			if ( !std::isfinite(probability) || std::isnan(probability)) {
				#pragma omp critical
				{
					file_log << "error: LOR " << std::setw(10) << lor;
					file_log << std::setw(4) << FOV->getX( model[lor][vx].col );
					file_log << std::setw(4) << FOV->getY( model[lor][vx].col );
					file_log << std::setw(4) << FOV->getZ( model[lor][vx].col );
					file_log << std::setw(9) << probability;
					file_log << std::endl;
				}
			}
		}
	}
	file_log << " done!" << std::endl;

	// --- Checking duplicates
	file_log << "\tchecking duplicates" << std::flush;
	#pragma omp parallel for
	for (uint32_t lor=0; lor<n_LORs; lor++) {
		for (auto& entry : model[lor]) {
			auto c = std::count_if(model[lor].begin(), model[lor].end(), 
				[&entry](MatrixEntry& loop_e){ return loop_e.col == entry.col;}
				);
			if (c > 1) {
				file_log << "duplicate: LOR " << std::setw(10) << lor;
				file_log << " cout " << c << std::endl;
			}
		}
	}
	file_log << " done!" << std::endl;
	file_log.close();
	
	// --- Saving
	std::string model_name =  out_dir_name + "/" + std::to_string(id1) + "-" + std::to_string(id2);
	truncateOutFiles(model_name);
	
	saveModel(model_name, model);
	
	if (transposed) {
		file_log.open(file_log_name, std::ios::app);
		file_log << "\tcomputing transposed model" << std::flush;
		timeI = SClock::now();
		
		transposeModel(model, FOV->nVoxels());
		
		timeF = SClock::now();
		file_log << " -> ~ "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(timeF - timeI).count()
		<< " msec" << std::endl;
		file_log.close();
		
		saveModel(model_name + "_tr", model);
	}
	
}
