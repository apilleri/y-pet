/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef MODELS_DETECTORMODELBUILDER_H_
#define MODELS_DETECTORMODELBUILDER_H_

#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include <cstdint>

#include "dataTypes.h"
#include "HistogramsMap.h"
#include "vBuilder.h"


using fired_t = uint32_t;
using statistic_t = uint32_t;
using raw_t = uint16_t;


class DetectorModelBuilder : public vBuilder {

  std::string simulations_dir;
  
  float threshold;
  
  uint16_t max;
  
  bool normalize;
  
  HistogramsMap norm_hist;
  
public:
  DetectorModelBuilder(const std::string& xmlFile,
                       float threshold_p,
                       uint16_t max_p,
                       bool normalize_p,
                       bool overwrite
                      );
  
  void setSimulationDir(const std::string& fname)
  {
    simulations_dir = fname;
  }
  
  ~DetectorModelBuilder()
  {
    const CoincidenceSchema& cs = scanner->getCoincidenceSchema();
    for (const std::pair<detPair_t, CoincidencePairSym> entry : cs) {
      const CoincidencePairSym& pair = entry.second;
      if (! pair.fundamental() ) {
        uint32_t pairID = pair.pairID();
        uint32_t symID = pair.symID();
        
        #pragma omp parallel for
        for (uint32_t idx=0; idx<norm_hist.data[symID].size(); idx++)
          norm_hist.data[pairID][idx] = norm_hist.data[symID][idx];
        
      }
    }
    norm_hist.save(out_dir_name + "/" + scanner->name() + "_nirm_hist.hg");
  }
  
  void computeFundamentalPair(const PlanarDetectorType& det1,
                              const PlanarDetectorType& det2) override;
  

private:
  void copyEvtMap(const std::vector<raw_t>& sim,
                  uint16_t start, uint16_t stop,
                  uint16_t stop_nc, uint16_t stop_nvc,
                  std::vector<raw_t> &dest);
};



template <class T>
void loadFromFile(std::string filename, std::vector<T> & buffer, size_t n)
{
  std::ifstream stream_file;
  stream_file.open(filename);
  if ( !stream_file.is_open() )
    throw std::string("Unable to open file: " + filename + "for reading.");
  
  buffer.resize(n);
  if (! stream_file.read(reinterpret_cast<char *>(buffer.data()), n*sizeof(T)) )
    throw std::string ( "Unable to read from file " + filename + " readed: " + std::to_string(stream_file.tellg()) );
  stream_file.close();
  
  
}


#endif // MODELS_DETECTORMODELBUILDER_H_

