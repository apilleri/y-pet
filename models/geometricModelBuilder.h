/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef MODELS_GEOMETRICMODELBUILDER_H_
#define MODELS_GEOMETRICMODELBUILDER_H_

#include "vBuilder.h"
#include "fieldOfView.h"
#include "vGeometry.h"
#include "vProjector.h"
#include "f_utils.h"

class GeometricModelBuilder : public vBuilder {
  
  //! pointer to the PROJECTOR object
  vProjector* projector;

public:
  
  GeometricModelBuilder(const std::string& xmlFile,
                        const std::string& fov_tag_p,
                        const std::string& proj_tag_p,
                        bool activate_vg,
                        bool overwrite
                       );
  
  ~GeometricModelBuilder()
  {
    delete projector;
  }
  
public:
 
  void computeFundamentalPair(const PlanarDetectorType& det1,
                              const PlanarDetectorType& det2) override;

};

#endif // MODELS_GEOMETRICMODELBUILDER_H_
