/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "vBuilder.h"

void vBuilder::computeAndStoreFullModel()
{
  std::ofstream file_log(file_log_name, std::ios::app);
  if ( !file_log.is_open() )
    throw std::string( "Unable to open file: " + file_log_name);
  
  const CoincidenceSchema& cs = scanner->getCoincidenceSchema();
  for (const std::pair<detPair_t, CoincidencePairSym> entry : cs) {
    const CoincidencePairSym& pair = entry.second;
    if ( pair.fundamental() ) {

      detID_t det1 = pair.id1();
      detID_t det2 = pair.id2();

      std::cout << "Computing pair: " << pair.id1();
      std::cout << " - " << pair.id2() << std::endl;
      computeFundamentalPair(scanner->getDetector(det1),
                             scanner->getDetector(det2) );
    }
  }
  file_log.close();
}


void vBuilder::transposeModel(ComputedModelType& in, uint32_t cols)
{
  uint32_t rows = in.size();
  ComputedModelType out(cols);
  
  #pragma omp parallel for
  for (uint32_t cur_row=0; cur_row<rows; cur_row++) {
    for (uint32_t cur_col=0; cur_col < in[cur_row].size(); cur_col++) {
      uint32_t column = in[cur_row][cur_col].col;
      FloatType prob = in[cur_row][cur_col].v;
      #pragma omp critical
      {
        out[column].push_back(MatrixEntry(cur_row, prob));
      }
    }
    in[cur_row].resize(0); // freeing memory
  }
  
  //sorting
  #pragma omp parallel for
  for (uint32_t cur_row=0; cur_row<out.size(); cur_row++) {
    std::sort(out[cur_row].begin(),
              out[cur_row].end(),
              [](MatrixEntry a, MatrixEntry b) { return a.col < b.col;}
    );
  }
  
  in.resize( out.size() );
  for (uint32_t cur_row=0; cur_row<in.size(); cur_row++) {
    in[cur_row].resize( out[cur_row].size() );
    for (uint32_t col=0; col<out[cur_row].size(); col++)
      in[cur_row][col] = out[cur_row][col];
    out[cur_row].resize(0); // freeing memory
  }
  
}

void vBuilder::truncateOutFiles(const std::string& base_path)
{
  std::ofstream file_model;
  std::ofstream file_counts;
  file_counts.open(base_path + ".counts");
  if ( !file_counts.is_open() )
    throw std::string( "Unable to open file " + base_path + ".counts" );
  file_counts.close();
  file_model.open(base_path, std::fstream::out | std::fstream::trunc);
  if ( !file_model.is_open() )
    throw std::string( "Unable to open file: " + base_path);
  file_model.close();
  if (transposed) {
    file_counts.open(base_path + "_tr.counts");
    if ( !file_counts.is_open() )
      throw std::string( "Unable to open file " + base_path + "_tr.counts" );
    file_counts.close();
    file_model.open(base_path+"_tr", std::fstream::out | std::fstream::trunc);
    if ( !file_model.is_open() )
      throw std::string( "Unable to open file: " + base_path+"_tr");
    file_model.close();
  }
}


void vBuilder::saveModel(const std::string& fname, const ComputedModelType& model)
{
  if ( model.empty() )
    throw std::string("saveModel called but there's nothing to store!");
  
  uint32_t rows = model.size();
  
  SClock::time_point timeI, timeF;
  std::ofstream file_counts, file_model;
  std::ofstream file_log(file_log_name, std::ios::app);
  
  //saving model
  file_log << "\twriting time" << std::flush;
  timeI = SClock::now();
  
    std::vector<uint32_t> counts;
    counts.resize(rows);
    for (uint32_t i=0; i<rows; i++) {
      uint64_t length = model[i].size();
      if (length > UINT32_MAX)
        throw std::string ("saveModel: the number of elements in the row exceeds UINT32_MAX");
      counts[i] = static_cast<uint32_t>(length);
    }
    file_counts.open(fname + ".counts", std::fstream::app);
    if ( !file_counts.is_open() )
      throw std::string( "saveModel: unable to open file " + fname + ".counts" );
    file_counts.write(reinterpret_cast<const char *>(counts.data()),
                      rows*sizeof(uint32_t));
    file_counts.close();
    
    file_model.open(fname, std::fstream::app);
    if ( !file_model.is_open() )
      throw std::string( "saveModel: unable to open file " + fname);
    
    for (uint32_t cur_row=0; cur_row<rows; cur_row++) {
      uint32_t cur_count = counts[cur_row];
      std::vector<MatrixEntry> temp(cur_count);
      std::copy(model[cur_row].begin(), model[cur_row].end(), temp.begin());
      file_model.write(reinterpret_cast<const char *>(temp.data()),
                       cur_count*sizeof(MatrixEntry));
    }
    file_model.close();
  
  timeF = SClock::now();
  file_log << " -> ~ "
  << std::chrono::duration_cast<std::chrono::milliseconds>(timeF - timeI).count()
  << " msecs" << std::endl;
  file_log.close();
  
}


FloatType vBuilder::computeTensorProduct(const std::vector<FloatType> &q1,
                                         const std::vector<FloatType> &q2,
                                         std::vector<entryT> &res,
                                         FloatType th)
{
  
  FloatType total=0;
  
  uint16_t n_rc1 = q2.size(); //backward contains the crystals of the start detector
  uint16_t n_rc2 = q1.size(); //forward contains the crystals of the stop detector
  
  res.clear();
  res.reserve(n_rc1*n_rc2/10); //reserve one tenth of the LORs number
  
  //ii runs over start detector, jj runs over stop detector
  for (uint16_t ii = 0; ii < n_rc1; ii++ ) {
    if (q2[ii]) {
      for(uint16_t jj=0; jj < n_rc2; jj++) {
        if (q1[jj]) {
          FloatType cur_prob = q1[jj]*q2[ii];
          total += cur_prob;
          if (cur_prob > th)
            res.push_back(std::make_pair(cur_prob, crystals2LOF(ii, jj, n_rc2) ) );
        }
      }
    }
  }
  
  std::sort(res.begin(), res.end(),
            [](const entryT& l, const entryT& r) { return l.first > r.first; });
  
  return total;
}
