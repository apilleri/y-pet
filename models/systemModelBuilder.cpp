/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "systemModelBuilder.h"

#include "timer.hpp"
#include <progress_bar.hpp>

SystemModelBuilder::SystemModelBuilder(const std::string& xmlFile,
                                       const std::string& fov_tag_p,
                                       const std::string& g_dir_p,
                                       const std::string& d_dir_p,
                                       bool overwrite
                                      )
: vBuilder(xmlFile, false), splits(0)
{ 
  FOV = new FieldOfView(xml_config, fov_tag_p);
  
  out_dir_name = scanner->name() + "_S_model" + "_";
  out_dir_name += std::to_string(FOV->vxN(0)) + "x";
  out_dir_name += std::to_string(FOV->vxN(1)) + "x";
  out_dir_name += std::to_string(FOV->vxN(2));
  
  if ( !fs::exists(out_dir_name) )
    fs::create_directory(out_dir_name);
  else if (!overwrite)
    throw std::string("Directory " + out_dir_name + " exists, use --overwrite");
  
  file_log_name = out_dir_name + "/log";
  
  g_dir = g_dir_p;
  d_dir = d_dir_p;

  
  std::ofstream file_log(file_log_name);
  file_log << "Computing System model for scanner " << scanner->name();
  file_log << "\nUsing Geometric model " << g_dir << std::endl;
  file_log << "Using Detector model " << d_dir << std::endl;
  file_log << "---" << std::endl;
  file_log.close();
}


void SystemModelBuilder::computeFundamentalPair(const PlanarDetectorType& det1,
                                                const PlanarDetectorType& det2)
{  
  std::string f_name = "SystemModelBuilder::computeFundamentalPair";
  
  detID_t id1 = det1.ID();
  detID_t id2 = det2.ID();
  std::string id_pair = std::to_string(id1) + "-" + std::to_string(id2);
  std::string g_filename =  g_dir + "/" + id_pair;
  std::string d_filename =  d_dir + "/" + id_pair;
  

  lorIDX_t n_LORs =  det1.rCryN() * det2.rCryN();
  lorIDX_t n_LOFs =  det1.vCryN() * det2.vCryN();
  
  uint32_t n_voxels = FOV->nVoxels();
  
  timer msec_timer("msec");
  msec_timer.start("loading " + id_pair, "", true);
  SparseModel *g_model_tr = new SparseModel(n_voxels, n_LOFs);
  g_model_tr->set(g_filename + "_tr");
  SparseModel *d_model = new SparseModel(n_LORs, n_LOFs);
  d_model->set(d_filename);
  msec_timer.stop();
  
  std::ofstream file_log(file_log_name, std::ios::app);
  file_log << "computing pair: " << det1.ID()
    << " --- " << det2.ID()
    << std::endl;
  
 
  if (n_LORs % splits !=0 )
    throw std::string("n_LORs must be a multiple of splits");
  
  std::string model_name = out_dir_name + "/";
  model_name += std::to_string(det1.ID()) + "-" + std::to_string(det2.ID());
  //truncating files
  truncateOutFiles(model_name);
  
  timer sec_timer("sec");
  uint32_t block = n_LORs/splits;
  ComputedModelType s_model(block);
//   std::vector< std::vector<OldStorage> > s_model;
//   s_model.resize(block);
  
  
  timer m_sec_timer("msec");
  
  
  for (uint16_t split=0; split<splits; split++) {
    std::cout << std::endl;
    sec_timer.start("split: " + std::to_string(split), "", false);

    ProgressBar progress(block);
    
//     float nis = 0, njs = 0; // profile
    
    SClock::time_point timeI = SClock::now();
    #pragma omp parallel for
    for (uint32_t c_lor=0; c_lor<block; c_lor++) {
      s_model[c_lor].clear();
      
      uint32_t model_lor = c_lor + block*split;
      
      MatrixEntry* d_pointer = d_model->rowPointer(model_lor);
      uint32_t n_el = d_model->rowElementsN(model_lor);
      
//       uint32_t nis_int= 0, njs_int = 0, not_null = 0; // profile
      
      for (uint32_t row=0; row<g_model_tr->rows(); row++) {
        

        uint32_t n_v2 = g_model_tr->rowElementsN(row);
        
        // a lot of voxels are not crossed by any LOF, as we are considering
        // just the FOV subtended by a single pair.
        
        if (n_v2) {
          
          MatrixEntry* pointer = g_model_tr->rowPointer(row);
          
          // it is useless to evaluate the lor if we are working in another
          // portion of the FOV, this if partially avoid this.
          
          if (d_pointer[0].col <= pointer[n_v2-1].col) {
            
            //using a pivot to reduce the evaluated g row by a binary search
            uint32_t start = 0, pivot = 0;
            while (d_pointer[0].col > pointer[start].col) {
              pivot = start;
              start = (start + n_v2)/2;
            }
            
            
            uint32_t i=0, j=pivot;
            FloatType vx_value = 0;
            
            //G and D rows must be sorted
            while(i<n_el && j<n_v2) {
              
              if (d_pointer[i].col < pointer[j].col)
                i++;
              else if (d_pointer[i].col > pointer[j].col)
                j++;
              else {
                //must be equal
                vx_value += d_pointer[i].v * pointer[j].v;
                i++;
                j++;
              }
            }
            
            if (vx_value) {
//               OldStorage temp(FOV->getX(row), FOV->getY(row), FOV->getZ(row), vx_value);
//               s_model[c_lor].push_back(temp);
              s_model[c_lor].push_back( MatrixEntry(row, vx_value) );
            }
            
//             nis_int += i; // profile
//             njs_int += j;
//             not_null++;
          }
        }

        
      }

//       #pragma omp atomic update // profile
//       nis += (float) nis_int/not_null;
//       #pragma omp atomic update
//       njs += (float) njs_int/not_null;
      
      #pragma omp critical
      {
        ++(progress);
      }
      
    }
    SClock::time_point timeF = SClock::now();
    file_log << "\tcomputation time: ~ "
    << std::chrono::duration_cast<std::chrono::milliseconds>(timeF - timeI).count()
    << " ms" << std::endl;
    saveModel(model_name, s_model);  
    sec_timer.stop();
    
//     std::cout << "nis medio: " << nis/block << std::endl; // profile
//     std::cout << "njs medio: " << njs/block << std::endl;
  }
  
  
  file_log.close();
  
}
