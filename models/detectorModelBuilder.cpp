/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */


#include "modelT.h"
#include "detectorModelBuilder.h"
#include "vGeometry.h"
#include "CylindricalGeometry.h"
#include "progress_bar.hpp"


DetectorModelBuilder::DetectorModelBuilder(const std::string& xmlFile,
                                           float threshold_p,
                                           uint16_t max_p,
                                           bool normalize_p,
                                           bool overwrite
                                          )
: vBuilder(xmlFile, false),
  simulations_dir(""), threshold(threshold_p),
  max(max_p), normalize(normalize_p)
{ 
  out_dir_name = scanner->name() + "_D_model";
  
  if ( !fs::exists(out_dir_name) )
    fs::create_directory(out_dir_name);
  else if (!overwrite)
    throw std::string("Directory " + out_dir_name + " exists, use --overwrite");
  
  scanner->printGeometryInfo(out_dir_name);
  scanner->printSymInfo(out_dir_name);
  
  norm_hist.allocate(scanner->getDetectorsList(),
                     scanner->getCoincidenceSchema(),
                     "LORs", 0);
  
  file_log_name = out_dir_name + "/log";
  
  std::ofstream file_log(file_log_name);
  file_log << "Computing Detector model for scanner " << scanner->name();
  file_log << "\nUsing threshold: " << threshold << std::endl;
  file_log << "Using Max number of LOFs per LOR:  " << max << std::endl;
  file_log << "Normalizing:  " << normalize << std::endl;
  file_log << "---" << std::endl;
  file_log.close();
}

void
DetectorModelBuilder::computeFundamentalPair(const PlanarDetectorType& det1,
                                             const PlanarDetectorType& det2)
{
  std::string f_name = "DetectorModelBuilder::computeFundamentalPair";
  
  std::string f_forward, f_backward;
  uint32_t szudzik_id = szudzikPair(det1.ID(), det2.ID());
  
  det1.printInfo();
  det2.printInfo();
  
  size_t n_rc1 = det1.rCryN();
  size_t n_rc2 = det2.rCryN();
  
  size_t n_vc1 = det1.vCryN();
  size_t n_vc2 = det2.vCryN();
  
  lorIDX_t n_LOFs = static_cast<lorIDX_t>(n_vc1)*n_vc2;
  lorIDX_t n_LORs = static_cast<lorIDX_t>(n_rc1)*n_rc2;
  
  // --- setting base filenames
  f_forward = simulations_dir + "/" + std::to_string(szudzik_id) +
  "_" + std::to_string(det1.ID()) + "_" + std::to_string(det2.ID());
  f_backward = simulations_dir + "/" + std::to_string(szudzik_id) +
  "_" + std::to_string(det2.ID()) + "_" + std::to_string(det1.ID());
  
  std::string t_str;
  // --- Loading information about the number of fired photons
  std::vector<fired_t> fired_forward, fired_backward;
  t_str = ".simulation.fired";
  loadFromFile<fired_t>(f_forward + t_str, fired_forward, n_LOFs);
  loadFromFile<fired_t>(f_backward + t_str, fired_backward, n_LOFs);
  
  // --- Loading simulations
  std::vector<raw_t> sim_forward, sim_backward;
  t_str = ".simulation.raw";
  loadFromFile<raw_t>(f_forward + t_str, sim_forward, n_vc1*n_vc2*n_rc2);
  loadFromFile<raw_t>(f_backward + t_str, sim_backward, n_vc2*n_vc1*n_rc1);

  // --- Loading statistic
  std::vector<statistic_t> stat_forward, stat_backward;
  t_str = ".simulation.statistic";
  loadFromFile<statistic_t>(f_forward + t_str, stat_forward, n_LOFs);
  loadFromFile<statistic_t>(f_backward + t_str, stat_backward, n_LOFs);

  // --- Computing Detector Matrix
  std::ofstream file_log(file_log_name, std::ios::app);
  if ( !file_log.is_open() ) 
    throw f_name + " unable to open file: " + file_log_name;
  file_log << "computing pair: " << det1.ID() << " --- " << det2.ID();
  file_log << std::endl;
  
  std::atomic<uint32_t> notAddedCounter = 0;

  // --- Statistical information
  std::vector<uint16_t> savedLORs(n_LOFs, 0);
  std::vector<FloatType> savedPROBs(n_LOFs, 0.);
  std::vector<FloatType> totalPROBs(n_LOFs, 0.);
  
  // --- Model
  ComputedModelType model(n_LOFs); //we compute first the transposed model
  std::vector<FloatType> lof_sensitivity(n_LOFs);
  std::vector<FloatType> lor_sensitivity(n_LORs);
  
  SClock::time_point timeI = SClock::now();
  ProgressBar progressBar(n_LOFs, file_log);
  ProgressBar progressBar2(n_LOFs, std::cout);

  
//   #
  for (size_t start_cry = 0; start_cry < n_vc1; start_cry++) {
     #pragma omp parallel for
    for (size_t stop_cry = 0; stop_cry < n_vc2; stop_cry++) {
      std::vector<raw_t> evtMap_forward(n_rc2), evtMap_backward(n_rc1);
      std::vector<FloatType> evtMap_forward_f(n_rc2), evtMap_backward_f(n_rc1);
      std::vector<entryT> tensor_product(n_LORs);
      
      uint32_t forward_lof = crystals2LOF(start_cry, stop_cry, n_vc2);
      uint32_t backward_lof = crystals2LOF(stop_cry, start_cry, n_vc1);
      
      if (stat_forward[forward_lof] == 0) {
        std::cout << "forward no good interactions..skipping" << std::endl;
        continue;
      }
      if (stat_backward[backward_lof] == 0) {
        std::cout << "backward no good interactions..skipping" << std::endl;
        continue;
      }

      copyEvtMap(sim_forward, start_cry, stop_cry, n_rc2, n_vc2, evtMap_forward);
      copyEvtMap(sim_backward, stop_cry, start_cry, n_rc1, n_vc1, evtMap_backward);
      

      for (uint16_t i = 0; i<n_rc2; i++)
        evtMap_forward_f[i] = ((FloatType )evtMap_forward[i])/fired_forward[forward_lof];
      for (uint16_t i = 0; i<n_rc1; i++) 
        evtMap_backward_f[i] = ((FloatType )evtMap_backward[i])/fired_backward[backward_lof];
      
      FloatType tot_LOF_p = computeTensorProduct(evtMap_forward_f, evtMap_backward_f, tensor_product, 0.00001);
      
      // DEBUG
      if ( tensor_product.empty() ) {
        std::cout << "\n" << start_cry << " " << stop_cry << std::endl;
        std::cout << "forward_lof: " << forward_lof << std::endl;
        std::cout << "backward_lof: " << backward_lof << std::endl;
        std::cout << "fired_forward[forward_lof]: " << fired_forward[forward_lof] << std::endl;
        std::cout << "fired_backward[backward_lof]: " << fired_backward[backward_lof] << std::endl;
        std::cout << "stat_forward[forward_lof]: " << stat_forward[forward_lof] << std::endl;
        std::cout << "stat_backward[backward_lof]: " << stat_backward[backward_lof] << std::endl;
        
        //some debug in case the product fails
        uint32_t empty_f = 0, empty_b = 0;
        std::cout << "forward: ";
        for (auto e : evtMap_forward) {
          std::cout << e << " ";
          if (e==0)
            empty_f++;
        }
        std::cout << "\nbackward: ";
        std::cout << std::endl;
        for (auto e : evtMap_backward) {
          std::cout << e << " ";
          if (e==0)
            empty_b++;
        }
        std::cout << std::endl;
        std::cout << "empty_f: " + std::to_string(empty_f) << std::endl;
        std::cout << "empty_b: " + std::to_string(empty_b) << std::endl;
        
        empty_f = 0; empty_b = 0;
        std::cout << "forward: ";
        for (auto e : evtMap_forward_f) {
          std::cout << e << " ";
          if (e==0)
            empty_f++;
        }
        std::cout << "\nbackward: ";
        std::cout << std::endl;
        for (auto e : evtMap_backward_f) {
          std::cout << e << " ";
          if (e==0)
            empty_b++;
        }
        std::cout << std::endl;
        std::cout << "empty_f: " + std::to_string(empty_f) << std::endl;
        std::cout << "empty_b: " + std::to_string(empty_b) << std::endl;
        
        throw f_name + "tensor_product empty";
      }
      
      
      FloatType norm_saved = 0;
      uint16_t current_N_added = 0;
      // --- Save every lor until the fired lof is found and the Threshold probability is reached
      bool added = false, c1, c2, c3;
      do {
        norm_saved += tensor_product[current_N_added].first;
        if (!added)
          added = (forward_lof == tensor_product[current_N_added].second);
        current_N_added++;

        c1 = (norm_saved/tot_LOF_p)<threshold;
        c2 = current_N_added<max;
        c3 = current_N_added < tensor_product.size();
      } while( /*!added  ||*/ c1 && c2 && c3);
      
      if (!added) notAddedCounter++;
      
      savedLORs[forward_lof] = current_N_added;
      savedPROBs[forward_lof] = norm_saved/tot_LOF_p; //percentuale di prob salvato rispetto al totale della prob della LOF
      totalPROBs[forward_lof] = tot_LOF_p;
      
      FloatType sens_forward = static_cast<FloatType>(stat_forward[forward_lof])/fired_forward[forward_lof];
      FloatType sens_backward = static_cast<FloatType>(stat_backward[backward_lof])/fired_backward[backward_lof];
//       std::cout << "f: " << stat_forward[forward_lof] << " " << fired_forward[forward_lof] << std::endl;
//       std::cout << "b: " << stat_backward[backward_lof] << " " << fired_backward[backward_lof] << std::endl;
      lof_sensitivity[forward_lof] = sens_forward * sens_backward;
//       std::cout << "tot_lof: " << tot_LOF_p << " lof_sens: " << lof_sensitivity[forward_lof] << std::endl;
      
      model[forward_lof].resize(current_N_added);
      for(uint32_t ii = 0; ii < current_N_added; ii++) {
        model[forward_lof][ii].col = tensor_product[ii].second;
//         FloatType value = tensor_product[ii].first / norm_saved * lof_sensitivity[forward_lof]; //phys norm
        FloatType value = tensor_product[ii].first / norm_saved; //norm 1
        model[forward_lof][ii].v = value;
        
//         std::cout << value << std::endl;
        
      }
      
      #pragma omp critical 
      {
        ++progressBar;
        ++progressBar2;
      }
      
    }
  }
  
  
  SClock::time_point timeF = SClock::now();
  file_log << "NotAdded: " << notAddedCounter << std::endl;
  file_log << "Mean LORs number: " << std::accumulate(savedLORs.begin(), savedLORs.end(), 0.)/n_LOFs << std::endl;
  file_log << "Min LORs per LOF: " << *std::min_element(savedLORs.begin(), savedLORs.end()) << std::endl;
  file_log << "Max LORs per LOF: " << *std::max_element(savedLORs.begin(), savedLORs.end()) << std::endl;
  file_log << "Mean saved PROB: " << std::accumulate(savedPROBs.begin(), savedPROBs.end(), 0.)/n_LOFs << std::endl;
  file_log << "Min saved PROB: " << *std::min_element(savedPROBs.begin(), savedPROBs.end()) << std::endl;
  file_log << "Max saved PROB: " << *std::max_element(savedPROBs.begin(), savedPROBs.end()) << std::endl;
  file_log << "Time: ~ " << std::chrono::duration_cast<std::chrono::seconds>(timeF - timeI).count() << " s\n" << std::endl;
  

  // --- Detector Matrix Check
  file_log << "Detector Matrix Check" << std::endl;
  for (uint32_t c_lof=0; c_lof<n_LOFs; c_lof++) {
    for (const MatrixEntry& entry : model[c_lof] ) {
      if ( entry.v < 0 )
        file_log << "Error p<0 - LOF: " << c_lof << " LOR: " << entry.col << std::endl;
      if ( std::isnan( entry.v ) )
        file_log << "Error nan - LOF: " << c_lof << " LOR: " << entry.col << std::endl;
      if ( std::isinf( entry.v ) )
        file_log << "Error inf - LOF: " << c_lof << " LOR: " << entry.col << std::endl;
      if ( entry.col > n_LORs )
        file_log << "Error out of bounds - LOF: " << c_lof << " LOR: " << entry.col << std::endl;
    }
  }
  file_log << "Done!" << std::endl;
  
  std::string model_name = out_dir_name + "/" + std::to_string(det1.ID()) + "-" + std::to_string(det2.ID());
  
  // --- Saving stats
  std::ofstream stats_file;
  stats_file.open(model_name + ".stats");
  if ( !stats_file.is_open() )
    throw f_name + " unable to open stats_file for writing!";
  stats_file.write((char *) savedLORs.data(), n_LOFs*sizeof(uint16_t) );
  stats_file.write((char *) totalPROBs.data(), n_LOFs*sizeof(FloatType) );
  stats_file.write((char *) savedPROBs.data(), n_LOFs*sizeof(FloatType) );
  stats_file.close();

  
  // --- Saving lof_sensitivity
  std::ofstream sensitivity_file;
  sensitivity_file.open(model_name + ".lof_sensitivity");
  if ( !sensitivity_file.is_open() )
    throw std::string("Unable to open sensitivity_file for writing!");
  sensitivity_file.write((char *) lof_sensitivity.data(), n_LOFs*sizeof(FloatType) );
  sensitivity_file.close();
  
//   // As we are dealing with acquired counts it's safe to assume that every count
//   // on a LOR must come from a LOF, so every row of the Detector Matrix
//   // must have sum equal to 1.
//   // so far we have computed the transposed Detector Model, to normalize
//   // the matrix for every LOR it's necessary
//   // to transpose the model, sum the elements on each ROW to compute the 
//   // normalization and apply it to the matrix.

  if (normalize) {
    // NORMALIZAING ROWS  
    file_log << "\tcomputing transposed model" << std::flush;
    timeI = SClock::now();
    
    transposeModel(model, n_LORs);
    
    timeF = SClock::now();
    file_log << " -> ~ "
    << std::chrono::duration_cast<std::chrono::milliseconds>(timeF - timeI).count()
    << " msec" << std::endl;
    
    file_log << "\tnormalizing matrix" << std::flush;
    timeI = SClock::now();
    
    #pragma omp parallel for
    for (uint32_t cur_row=0; cur_row<n_LORs; cur_row++) {
      std::vector<MatrixEntry>& row = model[cur_row];
      FloatType sum = 0;
      // computing normalization factor
      for (uint32_t cur_col=0; cur_col <row.size(); cur_col++)
        sum += row[cur_col].v;
      // applying normalization
      for (uint32_t cur_col=0; cur_col <row.size(); cur_col++)
        row[cur_col].v /= sum;
    }
    
    timeF = SClock::now();
    file_log << " -> ~ "
    << std::chrono::duration_cast<std::chrono::milliseconds>(timeF - timeI).count()
    << " msec" << std::endl;
    file_log.close();
    
    saveModel(model_name, model);
    
    //computing and saving lor sensitivity
    #pragma omp parallel for
    for(uint32_t cur_row=0; cur_row<n_LORs; cur_row++) {
      lor_sensitivity[cur_row] = 0;
      if ( model[cur_row].size() == 0 )
        std::cout << "warning uguale a zero" << std::endl;
      for(uint32_t ii = 0; ii < model[cur_row].size(); ii++) {
        FloatType prob = model[cur_row][ii].v;
        uint32_t lof = model[cur_row][ii].col;
        lor_sensitivity[cur_row] += prob * lof_sensitivity[lof];
      }
      norm_hist.data[szudzik_id][cur_row] = lor_sensitivity[cur_row];
    }
      
      
    sensitivity_file.open(model_name + ".lor_sensitivity");
    if ( !sensitivity_file.is_open() )
      throw std::string("Unable to open sensitivity_file for writing!");
    sensitivity_file.write((char *) lor_sensitivity.data(), n_LORs*sizeof(FloatType) );
    sensitivity_file.close();
    
    
    file_log.open(file_log_name, std::ios::app);
    file_log << "\ttransposing again" << std::flush;
    timeI = SClock::now();
    
    transposeModel(model, n_LOFs);
    
    timeF = SClock::now();
    file_log << " -> ~ "
    << std::chrono::duration_cast<std::chrono::milliseconds>(timeF - timeI).count()
    << " msec" << std::endl;
    file_log.close();
    saveModel(model_name + "_tr", model);
    // END NORMALIZAING ROWS
  }
  else {
    // NOT NORMALIZAING ROWS
    saveModel(model_name + "_tr", model);
    
    file_log << "\tcomputing transposed model" << std::flush;
    timeI = SClock::now();
    
    transposeModel(model, n_LORs);
    
    timeF = SClock::now();
    file_log << " -> ~ "
    << std::chrono::duration_cast<std::chrono::milliseconds>(timeF - timeI).count()
    << " msec" << std::endl;
    
    saveModel(model_name, model);
    
    //computing and saving lor sensitivity
    #pragma omp parallel for
    for(uint32_t cur_row=0; cur_row<n_LORs; cur_row++) {
      lor_sensitivity[cur_row] = 0;
      for(uint32_t ii = 0; ii < model[cur_row].size(); ii++) {
        FloatType prob = model[cur_row][ii].v;
        uint32_t lof = model[cur_row][ii].col;
        lor_sensitivity[cur_row] += prob * lof_sensitivity[lof];
      }
    }
    
    sensitivity_file.open(model_name + ".lor_sensitivity");
    if ( !sensitivity_file.is_open() )
      throw std::string("Unable to open sensitivity_file for writing!");
    sensitivity_file.write((char *) lor_sensitivity.data(), n_LORs*sizeof(FloatType) );
    sensitivity_file.close();
    
    // END NOT NORMALIZAING ROWS
  }
}


void DetectorModelBuilder::copyEvtMap(const std::vector<raw_t>& sim,
                                      uint16_t start, uint16_t stop,
                                      uint16_t stop_nc, uint16_t stop_nvc,
                                      std::vector<raw_t> &dest)
{
  uint64_t offset = static_cast<uint64_t>(stop_nc)*(stop_nvc*start + stop);
  std::copy(sim.begin() + offset, sim.begin() + offset+stop_nc, dest.begin());
}



