/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <cstdlib>

#include "cxxopts.hpp"
#include "vPhantomCreator.h"

int main(int argc, char *argv[])
{
  
  try {   
    cxxopts::Options options(argv[0], "Phantoms creator");
    
    options.add_options("Configuration")
    ("c, config", "XML configuration file",
      cxxopts::value<std::string>())
    ("t, type", "Phantom type (cylinder, derenzo, planar, rodsgrid)",
      cxxopts::value<std::string>())
    ("tag", "XML TAG to extract from the configuration file",
     cxxopts::value<std::string>())
    ("h, help", "Print help");
    
    options.add_options("GATE")
    ("gate", "Create GATE simulation .mac")
    ("g, geometry", "Geometry macro file", cxxopts::value<std::string>())
    ("p, physics", "Physics and Digitizer macro file",
      cxxopts::value<std::string>())
    ("seed", "Seed of the simulation", cxxopts::value<uint32_t>() )
    ("s, source", "Source type (BackToBack, Positron)",
     cxxopts::value<std::string>()->default_value("Positron"))
    ("energy", "Energy of the particle (default Fluorine18 spectrum)",
     cxxopts::value<FloatType>()->default_value("0"))
    ("unstable", "Set Unstable Flag and HalfLife (implicit Fluorine18)",
     cxxopts::value<FloatType>()->implicit_value("6586"))
    ("l, length", "Acquisition length",
     cxxopts::value<float>()->default_value("1"))
    ("v, vis", "Visualize source on Gate? (no StartDAQ)",
     cxxopts::value<uint32_t>()->default_value("200"))
    ("no-phantom", "Do not build the attenuation phantom")
    ("2D", "Source emits perpendicular to the z-axis");
    
    options.add_options("Image")
    ("fov", "Field of View", cxxopts::value<std::string>() )
    ("i, image", "Create Raw image");
    
    auto parsed_options = options.parse(argc, argv);
    
    if (!parsed_options.count("config"))
      throw std::string( "Missing Scanner Decription XML config file\n\n" + options.help() );
    if (!parsed_options.count("fov"))
      throw std::string( "Missing fov\n\n" + options.help() );
    if (!parsed_options.count("type"))
      throw std::string( "Missing phantom type\n\n" + options.help() );
    
    if (!parsed_options.count("tag"))
      throw std::string( "Missing XML TAG\n\n" + options.help() );
    
    if (!parsed_options.count("gate") && !parsed_options.count("image") ) 
      throw std::string("Provide at least one option between --gate and --image");
      
    const std::string& config = parsed_options["config"].as<std::string>();
    const std::string& phantom_type = parsed_options["type"].as<std::string>();
    const std::string& tag = parsed_options["tag"].as<std::string>();
    const std::string& fov_tag = parsed_options["fov"].as<std::string>();
    
    vPhantomCreator* simulation = vPhantomCreator::init(phantom_type, config, tag, fov_tag);

    std::string scanner_name = extractScannerName(config);
    simulation->setOutFile(scanner_name + "_" + phantom_type + "_" + tag);
    
    if (parsed_options.count("gate")) {
      if (!parsed_options.count("geometry"))
        throw std::string( "Missing Geometry macro file\n\n" + options.help() );
      if (!parsed_options.count("physics"))
        throw std::string( "Missing Physics and Digitizer macro file\n\n" + options.help() );
      if (!parsed_options.count("seed"))
        throw std::string( "Missing simulation seed\n\n" + options.help() );
      
      const std::string& geometry_fn = parsed_options["geometry"].as<std::string>();
      const std::string& physics_fn = parsed_options["physics"].as<std::string>();

      simulation->setGeometryFile(geometry_fn);
      simulation->setPhysicsDigitizerFile(physics_fn);
      simulation->setSeed( parsed_options["seed"].as<uint32_t>() );
      simulation->setSourceType( parsed_options["source"].as<std::string>() );
      simulation->setEnergy( parsed_options["energy"].as<FloatType>() );
      if ( parsed_options.count("unstable") )
        simulation->setUnstable( parsed_options["unstable"].as<FloatType>() );
      if ( parsed_options.count("no-phantom") )
        simulation->setNoPhantom();
      if ( parsed_options.count("2D") )
        simulation->set2D();
      
      if (parsed_options.count("vis")) {
        uint32_t n_points = parsed_options["vis"].as<uint32_t>();
        simulation->setVisualization(n_points);
      }
      
      simulation->setAcquisitionLength(parsed_options["length"].as<float>());
      
      simulation->construct();
    }
    
    if (parsed_options.count("image")) {
      simulation->imageConstruction();
    }
    

  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (std::string error) {
    std::cout << error << std::endl;
    exit(1);
  }

  return EXIT_SUCCESS;
}

