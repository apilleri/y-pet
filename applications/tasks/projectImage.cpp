/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>

#include "cxxopts.hpp"

#include "dataTypes.h"
#include "f_utils.h"

#include "vProjectImage.h"

int main(int argc, char **argv) 
{
  try {
    cxxopts::Options options(argv[0], "Project Image");
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file", cxxopts::value<std::string>() )
    ("activate-vg", "Activate virtual geometry")
    ("fov", "Field of View", cxxopts::value<std::string>() )

    ("t, threads", "Number of threads", cxxopts::value<uint16_t>() )
    ("h, help", "Print help");
    
    options.add_options("Input/Output")
    ("i, input", "Input image", cxxopts::value<std::string>() )
    ("o, output", "Output histogram name", cxxopts::value<std::string>() )
    ("output-dir", "Output directory (default \"project\"",
     cxxopts::value<std::string>()->default_value("project") );
    
    options.add_options("Models")
    ("projector", "Projector for on-the-fly computation", cxxopts::value<std::string>() )
    ("use-sym", "Use symmetries")
    ("z, zmodel", "Normalization histogram", cxxopts::value<std::string>())
    ("g, gmodel", "Geometric Matrix Directory", cxxopts::value<std::string>())
    ("d, dmodel", "Detector Matrix Directory", cxxopts::value<std::string>())
    ("psf", "Apply image space resolution modelling with a "
    "space invariant 3D Gauss kernel: sigma,width",
     cxxopts::value< std::vector<FloatType> >() );
    
    cxxopts::ParseResult cl_options = options.parse(argc, argv);
    
    if ( cl_options.count("help") ) {
      std::cout << options.help() << std::endl;
      exit(0);
    }
    
    if (!cl_options.count("config"))
      throw "Missing Scanner Decription XML config file\n\n" + options.help();
    
    if (!cl_options.count("fov"))
      throw std::string( "Missing fov\n\n" + options.help() );
    
    if (!cl_options.count("input"))
      throw "Missing Input image\n\n" + options.help();
    
    if (!cl_options.count("output"))
      throw "Missing output histogram name\n\n" + options.help();
    
    const std::string& scanner_xml = cl_options["config"].as<std::string>();
//     const std::string& fov_tag = cl_options["fov"].as<std::string>();
//     const std::string& output_dir = cl_options["output-dir"].as<std::string>();
    
    vProjectImage* projector;
    
    // looking for which kind of algorithm to apply
    // 1 - full online: no options
    // 2 - sym stored: g option OR g and D options
    // 4 - sym mixed: only D (g on the fly)

    bool g_opt = cl_options.count("gmodel");
    bool p_opt = cl_options.count("projector");
    
    if (!g_opt && !p_opt) 
      throw std::string("One argument between gmodel or projector must be provided");
    
    bool d_opt = cl_options.count("dmodel");
    bool sym_opt = cl_options.count("use-sym");
    
    if ( !g_opt ) {
      if (sym_opt) {
        std::cout << "Using OnLine projector with symmetries..." << std::endl;
        projector = new ProjectImageSymOnLine(scanner_xml, cl_options);
      } else {
        if (d_opt) {
          std::string err = "Cannot use D model without symmetries";
          throw err + options.help();
        }
        std::cout << "Using OnLine projector..." << std::endl;
        projector = new ProjectImageFullOnLine(scanner_xml, cl_options);
      }
    } else {
      std::cout << "Using OffLine model..." << std::endl;
      projector = new ProjectImageSymOffLine(scanner_xml, cl_options);
    }
    
    
    projector->apply();
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nError: " << error << std::endl;
    exit(1);
  }
  
  return EXIT_SUCCESS;
}






