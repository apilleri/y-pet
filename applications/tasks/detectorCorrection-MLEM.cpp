/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>

#include "cxxopts.hpp"

#include "dataTypes.h"
#include "f_utils.h"

#include "vDetectorCorrection.h"

int main(int argc, char **argv) 
{
  try {
    cxxopts::Options options(argv[0], "Detector >Correction MLEM");
    
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file", cxxopts::value<std::string>() )
    ("fov", "Field of View", cxxopts::value<std::string>() )
    ("i, input", "Input histogram", cxxopts::value<std::string>() )
    ("o, output", "Output histogram", cxxopts::value<std::string>() )
    ("output-dir", "Output directory (default \"dc\"",
     cxxopts::value<std::string>()->default_value("dc") )
    
    ("d, dmodel", "Detector Matrix Directory", cxxopts::value<std::string>())
    ("fmodel", "Filter Matrix Directory", cxxopts::value<std::string>())
//     ("compute-sensitivity", "Compute sensitivty in place")
//     ("s, sensitivity", "Sensitivity raw file", cxxopts::value<std::string>() )
    ("z, normalization", "Normalization histogram", cxxopts::value<std::string>() )
    
    ("n, number", "Number of iterations", cxxopts::value<uint16_t>() )
    ("e, every",  "Save histogram every N iterations", cxxopts::value<uint16_t>() )
    
    ("t, threads", "Number of threads", cxxopts::value<uint16_t>() )
    ("h, help", "Print help");
    
    cxxopts::ParseResult cl_options = options.parse(argc, argv);
    
    if ( cl_options.count("help") ) {
      std::cout << options.help() << std::endl;
      exit(0);
    }
    
    if (!cl_options.count("config"))
      throw std::string( "Missing Scanner Decription XML config file\n\n" + options.help() );  
    
    if (!cl_options.count("fov"))
      throw std::string( "Missing fov\n\n" + options.help() );
    
    if (!cl_options.count("input"))
      throw std::string( "Missing Input acquisition histogram\n\n" + options.help() );
    
    if (!cl_options.count("output"))
      throw std::string( "Missing output histogram basename\n\n" + options.help() );
    
    if (!cl_options.count("dmodel"))
      throw std::string( "Missing Detector Matrix Directory\n\n" + options.help() );

    const std::string& scanner_xml = cl_options["config"].as<std::string>();
//     const std::string& fov_tag = cl_options["fov"].as<std::string>();
//     const std::string& output_dir = cl_options["output-dir"].as<std::string>();
    
    dcMLEM detectorCorrection(scanner_xml, cl_options);
    detectorCorrection.apply();
    
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nError: " << error << std::endl;
    exit(1);
  }
  
  return EXIT_SUCCESS;
}






