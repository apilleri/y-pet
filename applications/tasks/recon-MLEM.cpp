/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>

#include "cxxopts.hpp"
#include "dataTypes.h"
#include "f_utils.h"
#include "vMLEM.h"

int main(int argc, char **argv) 
{
  try {
    cxxopts::Options options(argv[0], "Image Reconstruction MLEM");
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file",
      cxxopts::value<std::string>() )
    ("activate-vg", "Activate virtual geometry")
    ("fov", "Field of View", cxxopts::value<std::string>() )
    ("tr", "Use transposed models")
    ("t, threads", "Number of threads", cxxopts::value<uint16_t>() )
    ("h, help", "Print help");
    
    options.add_options("Input/Output")
    ("i, input", "Input histogram(S)", cxxopts::value< StringVec >() )
    ("o, output", "Output image basename(s)", cxxopts::value< StringVec >() )
    ("output-dir", "Output directory (default \"recon\"",
     cxxopts::value<std::string>()->default_value("recon") );
   
    options.add_options("Corrections")
    ("a, attenuation", "Attenuation correction histogram(s)", cxxopts::value< StringVec >())
    ("r, random", "Random correction histogram(s)", cxxopts::value< StringVec >());
    
    options.add_options("Models")
    ("z, zmodel", "Normalization histogram", cxxopts::value<std::string>() )
    ("g, gmodel", "Geometric Matrix Directory", cxxopts::value<std::string>())
    ("d, dmodel", "Detector Matrix Directory", cxxopts::value<std::string>())
//     ("projector", "Projector for on-the-fly computation", cxxopts::value<std::string>() )
    ("psf", "Apply image space resolution modelling with a space invariant 3D"
    "Gauss kernel: sigma,width,type \ntype(0 - both, 1 - pre-only, 2 - post-only",
     cxxopts::value< std::vector<FloatType> >() );
    
    options.add_options("Sensitivity")
    ("compute-sensitivity", "Compute sensitivty in place")
    ("s, sensitivity", "Sensitivity raw file", cxxopts::value< StringVec >() );
    /* use this command to help adding more than one sens file:
     * `ls SCANNER_Sensitivity_1vs9_OSEM_Sym_G_SUB* |  tr '\n' ','`
     */ 
    
//     ("b, background", "Background histogram", cxxopts::value<std::string>() )
//     ("bgv", "Background value", cxxopts::value<FloatType>() )
    
    options.add_options("Reconstruction")
    ("start-image", "Start image", cxxopts::value<std::string>() )
    ("start-iter", "Start iteration number", cxxopts::value<uint16_t>() )
    ("n, number", "Number of iterations", cxxopts::value<uint16_t>() )
    ("e, every",  "Save image every N iterations", cxxopts::value<uint16_t>() );
    
    options.add_options("Regularization")
    ("f, filter", "Apply a gaussian filter: sigma,width",
     cxxopts::value< std::vector<FloatType> >() )
    ("wang-reg", "Apply the Wang Path-Based regularization (parameters: beta,delta)",
     cxxopts::value< std::vector<FloatType> >() );
    
    cxxopts::ParseResult cl_options = options.parse(argc, argv);
    
    if (cl_options.count("help")) {
      std::cout << options.help() << std::endl;
      exit(0);
    }
    
    checkCommandLine(cl_options, "config", options);
    checkCommandLine(cl_options, "fov", options);
    checkCommandLine(cl_options, "input", options);
    checkCommandLine(cl_options, "output", options);
    checkCommandLine(cl_options, "gmodel", options);
    checkCommandLine(cl_options, "dmodel", options);

    if (!cl_options.count("sensitivity") && !cl_options.count("compute-sensitivity")) {
      std::string err_str = "Missing (and not computing) sensitivity image\n\n";
      throw err_str + options.help();
    }
    
    if (cl_options.count("sensitivity") && cl_options.count("compute-sensitivity")) {
      std::string err_str = "Both computing and providing sensitivity\n\n";
      throw err_str + options.help();
    }
    
    if (cl_options.count("start-image") && !cl_options.count("start-iter")) {
      std::string err_str = "You must specify both start-image and start-iter\n\n";
      throw err_str + options.help();
    }
    
    if (!cl_options.count("start-image") && cl_options.count("start-iter")) {
      std::string err_str = "You must specify both start-image and start-iter\n\n";
      throw err_str + options.help();
    }
    
    size_t o_s = cl_options["output"].as< StringVec >().size();
    size_t i_s = cl_options["input"].as< StringVec >().size();
    size_t r_s = 0;
    if ( cl_options.count("random") )
      r_s = cl_options["random"].as< StringVec >().size();
    
    if ( o_s != i_s ) {
      std::string err_str = "You must provide the same number of input";
      err_str += " and output arguments\n\n" + options.help();
      throw err_str + options.help();
    }
    if (r_s) {
      if ( r_s != i_s) {
        std::string err_str = "You must provide the same number of input";
        err_str += " and random arguments\n\n" + options.help();
        throw err_str + options.help();
      }
    }
      
    const std::string& output_dir = cl_options["output-dir"].as<std::string>();
    createDirectory(output_dir);
    std::ofstream txt_file(output_dir + "/parameters.txt");
    if ( !txt_file.is_open() )
      throw std::string("Unable to open " + output_dir + "/parameters.txt");
    txt_file << "Reconstruction parameters: " << std::endl;
    if (cl_options.count("zmodel") ) {
      txt_file << "Using normalization model: ";
      txt_file << cl_options["zmodel"].as<std::string>() << std::endl;
    }
    txt_file << "Using Geometric model: ";
    txt_file << cl_options["gmodel"].as<std::string>() << std::endl;
    txt_file << "Using Detector model: ";
    txt_file << cl_options["dmodel"].as<std::string>() << std::endl;
    if (cl_options.count("psf") ) {
      txt_file << "Applying Image Space PSF convolution: ";
      txt_file << "sigma: " << cl_options["psf"].as<std::vector<FloatType>>()[0];
      txt_file << " width: " << cl_options["psf"].as<std::vector<FloatType>>()[1];
      txt_file << " type: " << cl_options["psf"].as<std::vector<FloatType>>()[2];
      txt_file << std::endl;
    }
    if (cl_options.count("filter") ) {
      txt_file << "Applying Gaussian filter: ";
      txt_file << "sigma: " << cl_options["filter"].as<std::vector<FloatType>>()[0];
      txt_file << std::endl;
      txt_file << "width: " << cl_options["filter"].as<std::vector<FloatType>>()[1];
      txt_file << std::endl;
    }
    txt_file.close();
    
    vMLEM* reconstruction;
    
    const std::string& scanner_xml = cl_options["config"].as<std::string>();
    if ( cl_options.count("tr") )
      reconstruction = new mlemSymDG(scanner_xml, cl_options);
    else
      reconstruction = new mlemSymDGnoT(scanner_xml, cl_options);
    
    reconstruction->apply();
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "ERROR parsing command line options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nERROR: " << error << std::endl;
    exit(1);
  }
  
  return EXIT_SUCCESS;
}





