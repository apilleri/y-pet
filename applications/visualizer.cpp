/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <chrono>

#include "cxxopts.hpp"

#include "ScannerVisualizer.h"
#include "dataTypes.h"
#include "f_utils.h"

#include "ScannerVisualizer.h"

typedef std::chrono::high_resolution_clock Clock;

int main(int argc, char **argv) 
{
  try {   
    cxxopts::Options options(argv[0], "Geometry Visualizer");
        
    options.add_options()
    ("c, config", "Scanner Decription XML config file", cxxopts::value<std::string>())
    ("activate-vg", "Activate virtual geometry")
    ("fov", "Field of View", cxxopts::value<std::string>() )
    ("q, quit", "Auto close at the end of the process")
    
    ("onLine",  "Show on line LOR, only G component")
    ("projector", "Projector for on-the-fly computation", cxxopts::value<std::string>() )
    ("offLine", "Show off line LOR, G and D compoenents")
    ("g, geometric", "Geometric Matrix Directory", cxxopts::value<std::string>())
    ("d, detector", "Detector Matrix Directory", cxxopts::value<std::string>())
    ("f, first",  "First crystal position",  cxxopts::value<std::vector<uint16_t>>())
    ("s, second", "Second crystal position", cxxopts::value<std::vector<uint16_t>>())
    ("p, print", "Print computed LOR")
    
    ("is-psf", "Apply image space resolution modelling with a "
    "space invariant 3D Gauss kernel: sigma,width",
     cxxopts::value< std::vector<FloatType> >() )
    
    ("cylinder", "Draw a cylinder (xc,yc,zc,h,r_min,r_max)", cxxopts::value<std::vector<FloatType>>())
    
    ("h, help", "Print help");

    auto cl_options = options.parse(argc, argv);
    
    if (!cl_options.count("config"))
      throw std::string( "Missing Scanner Decription XML config file\n\n" + options.help() );
    if (!cl_options.count("fov"))
      throw std::string( "Missing FOV\n\n" + options.help() );
    
    const std::string& scanner_xml = cl_options["config"].as<std::string>();
    const std::string& fov_tag = cl_options["fov"].as<std::string>();
    bool activate_vg = cl_options.count("activate-vg");
    
    ScannerVisualizer Visualizer(scanner_xml, activate_vg);
    
    Visualizer.initFOV(fov_tag);
    
    Visualizer.drawAxes();
    Visualizer.drawFov();
    Visualizer.drawFovPoints();
    Visualizer.drawGeometry();
    
    if ( cl_options.count("cylinder") ) {
      std::vector cyl_par = cl_options["cylinder"].as< std::vector<FloatType> >();
      ThreeVector center(cyl_par[0], cyl_par[1], cyl_par[2]);
      Visualizer.drawCylinder(center, cyl_par[3], cyl_par[4], cyl_par[5]);
    }
    
    if ( cl_options.count("onLine") || cl_options.count("offLine") ) {
    
      std::vector <uint16_t> first, second;
      if (!cl_options.count("first"))
        throw std::string( "Missing first crystal position (sector,module,block,crystal)\n\n" + options.help() );
      if (!cl_options.count("second"))
        throw std::string( "Missing second crystal position (sector,module,block,crystal)\n\n" + options.help() );
      first = cl_options["first"].as< std::vector<uint16_t> >();
      second = cl_options["second"].as< std::vector<uint16_t> >();
      
      std::vector<MatrixEntry> tor;
      
      if ( cl_options.count("onLine") ) {
        if ( !cl_options.count("projector") ) {
          std::string str_error = "No projector selected.";
          throw str_error;
        }
        const std::string& proj_tag = cl_options["projector"].as<std::string>();
        Visualizer.initProjector(proj_tag);
        tor = Visualizer.onLineLOR(first, second);
      }
      else if ( cl_options.count("offLine") ) {
        if (!cl_options.count("geometric"))
          throw std::string( "Missing Geometric Matrix Directory\n\n" + options.help() );
        if (!cl_options.count("detector"))
          throw std::string( "Missing Detector Matrix Directory\n\n" + options.help() );
        
        std::string g_dir = cl_options["geometric"].as<std::string>();
        std::string d_dir = cl_options["detector"].as<std::string>();
        
        tor = Visualizer.offLineLOR(first, second, g_dir, d_dir);
      }
      
      const FieldOfView* fov = Visualizer.fov();
      if (cl_options.count("print")) {
        for(uint16_t i=0; i<tor.size(); i++) {
          std::cout << std::setw(10) << tor[i].col
          << std::setw(4) << fov->getX(tor[i].col)
          << std::setw(4) << fov->getY(tor[i].col)
          << std::setw(4) << fov->getZ(tor[i].col) 
          << std::setw(15) << tor[i].v << std::endl;
        }
        
      }
      std::string name = Visualizer.scanner()->name() + "_";
      name += Visualizer.projector()->type() + "_";
      name += vec2str(first) + "_";
      name += vec2str(second);
      
      if (cl_options.count("is-psf") ) {
        name += "_PSF_";
        const std::vector<FloatType> par ;
        std::cout << "Applying Image Space PSF convolution: ";
        FloatType psf_sigma = cl_options["is-psf"].as<std::vector<FloatType>>()[0];
        std::cout << "sigma: " << psf_sigma;
        name += std::to_string(psf_sigma);
        FloatType psf_width = cl_options["is-psf"].as<std::vector<FloatType>>()[1];
        std::cout << " width: " << psf_width << std::endl;
        name += "_" + std::to_string(psf_width);
        
        ImageType image = fov->createInitImage(0);
        FloatType sum = 0;
        for (const MatrixEntry& e: tor) {
          image[e.col] = e.v;
          sum += e.v;
        }
        std::cout << "Sum before conv: " << sum << std::endl;
        
        fov->gaussFilter(image, psf_sigma, psf_width);
        
        tor.clear();
        for (uint32_t i=0; i<image.size(); i++)
          if (image[i])
            tor.push_back( MatrixEntry(i, image[i]) );
          
        sum = 0;
        for (const MatrixEntry& e: tor)
          sum += e.v;
        std::cout << "Sum after conv: " << sum << std::endl;
      }
      
      fov->saveLOR(name, tor);
      Visualizer.drawLOR(tor);
    }

    
    
    
    //DRAW DETECTOR MATRIX
//     
//     system.visualizer->drawDetectorMatrixColumn(dmatrix, unique1, unique2);
//     
    if ( !cl_options.count("quit") )
      Visualizer.wait();

  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << error << std::endl;
    exit(1);
  }

  return EXIT_SUCCESS;
}

    
