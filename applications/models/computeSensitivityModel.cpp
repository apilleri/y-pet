/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <cstdlib>
#include <string>

#include "omp.h"

#include "cxxopts.hpp"
#include "f_utils.h"
#include "HistogramsMap.h"
#include "vGeometry.h"
#include "PlanarDetector.h"

std::vector<FloatType> computePairSensitivity(const PlanarDetectorType& det1, 
                                              const PlanarDetectorType& det2,
                                              uint16_t n_points,
                                              const std::string& out_dir_name);

int main(int argc, char **argv) 
{
  try {
    cxxopts::Options options(argv[0], "Compute Sensitivity Model");
    
    options.add_options()
    ("c, config",   "Scanner Decription XML config file", cxxopts::value<std::string>())
    ("o, output", "Output Histogram name", cxxopts::value<std::string>() )
    ("overwrite",      "Overwrite output directory")
    ("n, points", "Number of points", cxxopts::value<uint16_t>())
    ("t, threads", "Number of threads", cxxopts::value<uint16_t>())
    ("h, help",     "Print help");
    
    auto cl_options = options.parse(argc, argv);
    
    if (cl_options.count("help")) {
      std::cout << options.help() << std::endl;
      exit(0);
    }

    checkCommandLine(cl_options, "config", options);
    checkCommandLine(cl_options, "output", options);
    checkCommandLine(cl_options, "points", options);

    uint16_t n_threads;
    if (cl_options.count("threads")) 
      n_threads = cl_options["threads"].as<uint16_t>();
    else
      n_threads = omp_get_max_threads();
    std::cout << "Using threads: " << n_threads << std::endl;
    
    std::string scanner_xml = cl_options["config"].as<std::string>();
    std::string output_name = cl_options["output"].as<std::string>();
    uint16_t n_points = cl_options["points"].as<uint16_t>();
    bool overwrite = cl_options.count("overwrite");
    
    vGeometry* scanner = vGeometry::initScanner(scanner_xml, false);
    
    std::string out_dir_name = scanner->name() + "_Sensitivity_model";
    if ( !fs::exists(out_dir_name) )
      fs::create_directory(out_dir_name);
    else if (!overwrite)
      throw std::string("Directory " + out_dir_name + " exists, use --overwrite");
    truncateFile(out_dir_name + "/log.txt");
    
    scanner->printGeometryInfo(out_dir_name);
    scanner->printSymInfo(out_dir_name);
    
    
    HistogramsMap out_hist;
    out_hist.allocate(scanner->getDetectorsList(),
                      scanner->getCoincidenceSchema(),
                      "LORs", 0);
    
    const GroupedCS& gcs = scanner->getGroupedCS();
    for (const std::vector<CoincidencePairSym>& group : gcs) {
      const CoincidencePairSym& pair = group.at(0);
      detID_t det1 = pair.id1();
      detID_t det2 = pair.id2();
      
      std::vector<FloatType> cur_pair_res;
      cur_pair_res = computePairSensitivity(scanner->getDetector(det1),
                                            scanner->getDetector(det2),
                                            n_points,
                                            out_dir_name);

      for (uint16_t c_sym=0; c_sym<group.size(); c_sym++) {
        for (uint32_t i=0; i<cur_pair_res.size(); i++) {
          detPair_t pair_id = group.at(c_sym).pairID();
          out_hist.data[pair_id][i] = cur_pair_res[i];
        }
      }
      
    }
    
    FloatType max = -1;
    
    for (detPair_t k : out_hist.keys()) {
      for (uint32_t lor=0; lor<out_hist.data[k].size(); lor++)
        max = std::max(max, out_hist.data[k][lor]);
    }
    
    for (detPair_t k : out_hist.keys()) {
      for (uint32_t lor=0; lor<out_hist.data[k].size(); lor++)
        out_hist.data[k][lor] /= max;
    }
  
    scanner->reflectHistograms(out_hist, "LORs");
    out_hist.save(output_name + ".hg");
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "ERROR parsing command line options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nERROR: " << error << std::endl;
    exit(1);
  }
  
  return EXIT_SUCCESS;
}

std::vector<FloatType> computePairSensitivity(const PlanarDetectorType& det1, 
                                              const PlanarDetectorType& det2,
                                              uint16_t n_points,
                                              const std::string& out_dir_name)
{
  std::string file_log_name = out_dir_name + "/log.txt";
  
  uint16_t id1 = det1.ID();
  uint16_t id2 = det2.ID();
  uint32_t n_ac1 = det1.aCryN();
  uint32_t n_ac2 = det2.aCryN();
  uint32_t n_LORs =  n_ac1 * n_ac2;
  
  std::ofstream file_log(file_log_name, std::ios::app);
  file_log << "computing pair: " << id1;
  file_log << " nC: " << n_ac1;
  file_log << " --- " << id2;
  file_log << " nC: " << n_ac2 << std::endl;


  SClock::time_point timeI = SClock::now();

  std::vector<FloatType> res(n_LORs, 0);
  
  SpaceType x_step = det1.xLen(0)/(n_points-1);
  std::vector<ThreeVector> points;
  
  //   SpaceType y_hsize = det1.yLen(0)/2;
  //   SpaceType z_hsize = det1.zLen(0)/2;
  //   uint16_t axial_p = 3;
  //   SpaceType y_step = det1.yLen(0)/(axial_p+1);
  //   SpaceType z_step = det1.zLen(0)/(axial_p+1);
  //   for (uint16_t i=1; i<=n_points; i++)
  //     for (uint16_t j=1; j<=axial_p; j++)
  //       for (uint16_t z=1; z<=axial_p; z++)
  //         points.push_back( ThreeVector(i*x_step, -y_hsize + j*y_step, -z_hsize + z*z_step) );
  
  //regular grid of five points
  SpaceType y_step = det1.yLen(0)/(4);
  SpaceType z_step = det1.zLen(0)/(4);
  for (uint16_t i=0; i<n_points; i++) {
    points.push_back( ThreeVector(i*x_step, -y_step, -z_step) );
    points.push_back( ThreeVector(i*x_step, -y_step, z_step) );
    points.push_back( ThreeVector(i*x_step, y_step, z_step) );
    points.push_back( ThreeVector(i*x_step, y_step, -z_step) );
    points.push_back( ThreeVector(i*x_step, 0, 0) );
  }
  
  #pragma omp parallel for
  for(uint32_t lor=0; lor<n_LORs; lor++) {
    uint16_t cry1 = lor/n_ac2;
    uint16_t cry2 = lor%n_ac2;

    //compute the sensitivity of the current lor.
    const ThreeVector& start_position = det1.aCrySurfaceCenter(cry1);
    const ThreeVector& stop_position = det2.aCrySurfaceCenter(cry2);
    AngleType start_ang = det1.zAngle();
    AngleType stop_ang = det2.zAngle(); 
    

    
    for (uint16_t c_p1 = 1; c_p1 <= points.size(); c_p1++) {
      ThreeVector start = start_position + points[c_p1-1].rotateZnotInPlace(start_ang);
      for (uint16_t c_p2 = 1; c_p2 <= points.size(); c_p2++) {
        ThreeVector stop = stop_position + points[c_p2-1].rotateZnotInPlace(stop_ang);
        
        ThreeVector ray = stop-start;
        ThreeVector ray_dir = ray.unit();
        
        SpaceType d1, d2, d3;
        FloatType prob1, prob2;
        
        if (!detectortIntersections(det1, cry1, stop, -ray_dir, d1, d2, d3) ) {
          #pragma omp critical 
          {
            std::cout << "warning dir " << c_p1 << " " << c_p2 << " " << det1.ID() << " " << cry1 << " " << det2.ID() << " " << cry2 << std::endl;
            std::cout << "\t\t" << start << " " << stop << std::endl;
          }
        }
//         prob1 = std::exp(-0.0833*(d2-d1))*(1-std::exp(-0.0833*(d3-d2)) );
        prob1 = std::exp(-0.0833*(ray.r()-d1));
        
        if (!detectortIntersections(det2, cry2, start, ray_dir, d1, d2, d3) ){
          #pragma omp critical 
          {
            std::cout << "warning -dir " << c_p1 << " " << c_p2 << " " << det1.ID() << " " << cry1 << " " << det2.ID() << " " << cry2 << std::endl;
            std::cout << "\t\t" << start << " " << stop << std::endl;
          }
        }
//         prob2 = std::exp(-0.0833*(d2-d1))*(1-std::exp(-0.0833*(d3-d2)) );
        prob2 = std::exp(-0.0833*(ray.r()-d1));
        
        res[lor] += prob1*prob2;
      }
    }
    
  }
  
//   #pragma omp parallel for
//   for(uint32_t lor=0; lor<n_LORs; lor++) {
//     res[lor] /= max;
//     res[lor] = 1/res[lor];
//   }
  
  SClock::time_point timeF = SClock::now();
  
  file_log << "\tcomputation time: ~ "
  << std::chrono::duration_cast<std::chrono::milliseconds>(timeF - timeI).count()
  << " ms" << std::endl;
  
  file_log.close();
  
  return res;
}
