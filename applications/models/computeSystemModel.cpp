/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>

#include "cxxopts.hpp"

#include "vProjector.h"
#include "vGeometry.h"
#include "systemModelBuilder.h"

int main(int argc, char **argv)
{
  try {
    cxxopts::Options options(argv[0], "Compute System Model");
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file", cxxopts::value<std::string>())
    ("fov", "Field of View", cxxopts::value<std::string>() )
    ("g, gmodel", "Geometric Matrix Directory", cxxopts::value<std::string>())
    ("d, dmodel", "Detector Matrix Directory", cxxopts::value<std::string>())
    ("s, splits", "Number of splits", cxxopts::value<uint16_t>())
    ("overwrite",      "Overwrite output directory")
    ("h, help", "Print help");
    
    auto cl_options = options.parse(argc, argv);
    
    if (cl_options.count("help")) {
      std::cout << options.help() << std::endl;
      exit(0);
    }
    
    
    checkCommandLine(cl_options, "config", options);
    checkCommandLine(cl_options, "fov", options);
    checkCommandLine(cl_options, "gmodel", options);
    checkCommandLine(cl_options, "dmodel", options);
    checkCommandLine(cl_options, "splits", options);
    
    
    const std::string& scanner_xml = cl_options["config"].as<std::string>();
    const std::string& fov_tag = cl_options["fov"].as<std::string>();
    const std::string& g_dir = cl_options["gmodel"].as<std::string>();
    const std::string& d_dir = cl_options["dmodel"].as<std::string>();
    uint16_t splits = cl_options["splits"].as<uint16_t>();
    bool overwrite = cl_options.count("overwrite");
    
    SystemModelBuilder builder(scanner_xml, fov_tag, g_dir, d_dir, overwrite);
    builder.setSplits(splits);
    builder.computeAndStoreFullModel();
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "ERROR parsing command line options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nERROR: " << error << std::endl;
    exit(1);
  }
  
  return EXIT_SUCCESS;
}


