/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <cstdlib>
#include <string>

#include "cxxopts.hpp"
#include "detectorModelBuilder.h"

int main(int argc, char **argv) 
{
  try {
    cxxopts::Options options(argv[0], "Compute Detector Model");
    
    options.add_options()
    ("c, config",      "Scanner Decription XML config file",  cxxopts::value<std::string>())
    ("d, simulations", "Simulation folder",  cxxopts::value<std::string>())
    ("t, threshold",   "Minimum probability threshold",  cxxopts::value<float>()->default_value("0.7"))
    ("m, max",         "Max number of LOFs per LOR",  cxxopts::value<uint16_t>()->default_value("50"))
    ("norm",           "Normalize the rows")
    ("overwrite",      "Overwrite output directory")
    ("h, help",        "Print help");
    
    auto cl_options = options.parse(argc, argv);
    
    if (cl_options.count("help")) {
      std::cout << options.help() << std::endl;
      exit(0);
    }
    
    
    checkCommandLine(cl_options, "config", options);
    checkCommandLine(cl_options, "simulations", options);
    
    std::string scanner_xml = cl_options["config"].as<std::string>();
    std::string simulations_dir = cl_options["simulations"].as<std::string>();
    bool overwrite = cl_options.count("overwrite");
    
    float threshold = cl_options["threshold"].as<float>();
    std::cout << "Using threshold: " << threshold << std::endl;
    uint16_t max = cl_options["max"].as<uint16_t>();
    std::cout << "Using Max number of LOFs per LOR: " << max << std::endl;
    bool norm = cl_options.count("norm");
    std::cout << "Normalizing rows: " << norm << std::endl;
    
    DetectorModelBuilder builder(scanner_xml, threshold, max, norm, overwrite);
    builder.setSimulationDir(simulations_dir);
    builder.computeAndStoreFullModel();
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "ERROR parsing command line options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nERROR: " << error << std::endl;
    exit(1);
  }
  
  return EXIT_SUCCESS;
}


