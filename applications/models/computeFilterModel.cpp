/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <cstdlib>
#include <string>

#include "cxxopts.hpp"
#include "filterModelBuilder.h"

int main(int argc, char **argv) 
{
  try {
    cxxopts::Options options(argv[0], "Compute Filter Model");
    
    options.add_options()
    ("c, config",      "Scanner Decription XML config file",  cxxopts::value<std::string>())
    ("w, width",       "Width of the kernel applied to the crystal amtrix", cxxopts::value<uint16_t>()->default_value("3"))
    ("m, max",         "Max number of LOFs per LOR",  cxxopts::value<uint16_t>()->default_value("20"))
    ("norm",           "Normalize the rows")
    ("overwrite",      "Overwrite output directory")
    ("h, help",        "Print help");
    
    auto result = options.parse(argc, argv);
    
    if (!result.count("config"))
      throw std::string( "Missing Scanner Decription XML config file\n\n" + options.help() );
    
    
    std::string scanner_xml = result["config"].as<std::string>();
    bool overwrite = false;
    if ( result.count("overwrite") )
      overwrite = true;

    uint16_t max = result["max"].as<uint16_t>();
    std::cout << "Using Max number of LOFs per LOR: " << max << std::endl;
    bool norm = false;
    if ( result.count("norm") )
      norm = true;
    
    FilterModelBuilder builder(scanner_xml, overwrite, max, norm);
    if ( result.count("overwrite") )
      builder.setOverwrite();

    builder.computeAndStoreFullModel();
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "ERROR parsing command line options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nERROR: " << error << std::endl;
    exit(1);
  }
  
  return EXIT_SUCCESS;
}


