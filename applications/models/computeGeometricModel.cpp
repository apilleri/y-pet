/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <cstdlib>
#include <string>

#include "cxxopts.hpp"
#include "geometricModelBuilder.h"

int main(int argc, char **argv) 
{
  try {
    cxxopts::Options options(argv[0], "Compute Geometric Model");
    
    options.add_options()
    ("c, config",   "Scanner Decription XML config file", cxxopts::value<std::string>())
    ("activate-vg", "Activate virtual geometry")
    ("fov", "Field of View", cxxopts::value<std::string>() )
    ("projector", "Projector for on-the-fly computation", cxxopts::value<std::string>() )
    ("transposed", "Compute Transposed model too")
    ("overwrite",      "Overwrite output directory")
    ("t, threads", "Number of threads", cxxopts::value<uint16_t>())
    ("h, help",     "Print help");

    auto cl_options = options.parse(argc, argv);

    if (cl_options.count("help")) {
      std::cout << options.help() << std::endl;
      exit(0);
    }
    
    checkCommandLine(cl_options, "config", options);
    checkCommandLine(cl_options, "fov", options);
    checkCommandLine(cl_options, "projector", options);
    
    uint16_t n_threads;
    if (cl_options.count("threads")) 
      n_threads = cl_options["threads"].as<uint16_t>();
    else
      n_threads = omp_get_max_threads();
    std::cout << "Using threads: " << n_threads << std::endl;
    
    std::string scanner_xml = cl_options["config"].as<std::string>();
    const std::string& fov_tag = cl_options["fov"].as<std::string>();
    const std::string& proj_tag = cl_options["projector"].as<std::string>();
    bool overwrite = cl_options.count("overwrite");
    bool activate_vg = cl_options.count("activate-vg");
    
    GeometricModelBuilder builder(scanner_xml, fov_tag, proj_tag, activate_vg, overwrite);
    
    if (cl_options.count("transposed"))
      builder.setTransposed();
    
    builder.computeAndStoreFullModel();
    

  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "ERROR parsing command line options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nERROR: " << error << std::endl;
    exit(1);
  }
  
  return EXIT_SUCCESS;
}

