/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <string>
#include <map>
#include <map>
#include <algorithm>

#include "cxxopts.hpp"
#include "f_utils.h"
#include "timer.hpp"
#include "PlanarGeometry.h"
#include "dataTypes.h"
#include "LmType.h"

uint16_t modMapA[9] = {0, 3, 6, 1, 4, 7, 2, 5, 8};
uint16_t modMapB[9] = {2, 5, 8, 1, 4, 7, 0, 3, 6};

#define N_CRYSTALS 529

void loadFromLm(std::string inputFile, PlanarGeometry *scanner, HistogramsMap &hg);

uint16_t getModuleID(uint16_t det);
uint16_t getCrystalID(uint16_t det, uint16_t cry);

int main(int argc, char *argv[])
{
  try {   
    cxxopts::Options options(argv[0], "Convert Data Dopet");
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file", cxxopts::value<std::string>())
    ("i, input", "Input data file", cxxopts::value<std::string>())
    ("o, output", "Output data file", cxxopts::value<std::string>())
    ("h, help", "Print help");

    auto result = options.parse(argc, argv);
    
    if (!result.count("input"))
      throw std::string( "Missing Input data file\n\n" + options.help() );
    
    if (!result.count("output"))
      throw std::string( "Missing Output data file\n\n" + options.help() );
    
    if (!result.count("config"))
      throw std::string( "Missing Scanner Decription XML config file\n\n" + options.help() );
    
    std::string input = result["input"].as<std::string>();
    std::string output = result["output"].as<std::string>();
    std::string scanner_xml = result["config"].as<std::string>();
    
    PlanarGeometry *Dopet = new PlanarGeometry(scanner_xml, false);
    
    HistogramsMap histogram;
    histogram.allocate(Dopet->getDetectorsList(),
                       Dopet->getCoincidenceSchema(),
                       "LORs", 0);

    loadFromLm(input, Dopet, histogram);
    histogram.save(output);
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nError: " << error << std::endl;
    exit(1);
  }
}


void loadFromLm(std::string inputFile, PlanarGeometry *scanner, HistogramsMap &hg)
{
  fs::path file_path = fs::current_path() / inputFile;
  uint32_t histSize = fs::file_size(file_path)/sizeof(LmType);
  //   uint32_t histSize = filesize(inputFile)/sizeof(LmType);
  
  std::cout << "histSize: " << histSize << std::endl;
  
  std::ifstream inputHistogramFile;
  inputHistogramFile.open(inputFile);
  if ( !inputHistogramFile.is_open() )
    throw std::string ( "Unable to open file " + inputFile + " for reading");
  
  LmType *lm_data;
    
  timer msecTimer("msec");
  msecTimer.start("Preparing Histograms", "", true);

  lm_data = new LmType[histSize];
  if (! inputHistogramFile.read(reinterpret_cast<char *>(lm_data), histSize*sizeof(LmType)) ) {
    delete[] lm_data;
    throw std::string ( "Unable to read from file " + inputFile);
  }
  
  inputHistogramFile.close();
  for (uint32_t ii=0; ii<histSize; ii++) {
    uint16_t sec1 = ((uint16_t)lm_data[ii].D1()-21)/10;
    uint16_t sec2 = ((uint16_t)lm_data[ii].D2()-21)/10;
    
    uint16_t mod1 = getModuleID( (uint16_t)lm_data[ii].D1() );
    uint16_t mod2 = getModuleID( (uint16_t)lm_data[ii].D2() );
    
    uint16_t cry1 = getCrystalID((uint16_t)lm_data[ii].D1(), (uint16_t)lm_data[ii].C1()  );
    uint16_t cry2 = getCrystalID((uint16_t)lm_data[ii].D2(), (uint16_t)lm_data[ii].C2()  );
    
    detID_t uniqueID1 = scanner->computeDetectorID( {sec1, mod1, 0} );
    detID_t uniqueID2 = scanner->computeDetectorID( {sec2, mod2, 0} );
    
    uint32_t szudzik_id = szudzikPair(uniqueID1, uniqueID2);
    uint32_t szudzik_id_swapped = szudzikPair(uniqueID2, uniqueID1);
    bool straight = true, swapped = true;
    
    
    if (hg.data.find(szudzik_id) == hg.data.end() )
      straight = false;
    if (hg.data.find(szudzik_id_swapped) == hg.data.end() ) 
      swapped = false;
    
    if (!straight && !swapped) {
      std::cout << " s1: " << std::setw(5) << sec1
      << " m1: " << std::setw(5) << mod1
      << " s2: " << std::setw(5) << sec2
      << " m2: " << std::setw(5) << mod2 << std::endl;
      std::cout << "uniqueID1: " << uniqueID1 << std::endl;
      std::cout << "uniqueID2: " << uniqueID2 << std::endl;
      std::cout << "szudzik_id: " << szudzik_id << std::endl;
      std::cout << "szudzik_id_swapped: " << szudzik_id_swapped << std::endl;
      throw std::string("Unable to find detector pair.");
    }
    
    
    if (swapped) {
      szudzik_id = szudzik_id_swapped;
      std::swap(cry1, cry2);
    }
    
    
    uint32_t lorN = cry1*N_CRYSTALS + cry2;

    hg.data[szudzik_id][lorN] = lm_data[ii].Count();
  }
  delete []lm_data;
  msecTimer.stop();
  
  std::cout << "checking..." << std::endl;
  for (data_type::iterator it = hg.data.begin(); it != hg.data.end(); ++it)
  {
    std::vector<uint16_t> hier1, hier2;
    detID_t det1, det2;
    std::tie(det1, det2) = szudzikUnPair(it->first);
//     hier1 = scanner->explodeDetectorID(det1);
//     hier2 = scanner->explodeDetectorID(det2);
    uint32_t count = 0, empty = 0;
    for (uint32_t i=0; i< 529*529; i++) {
      if ( (it->second)[i] )
        count ++;
      else
        empty ++;
    }
    
    std::cout
      << std::setw(5) << (*it).first
      << std::setw(3) << det1 //<< std::setw(3) << sec1 << std::setw(3) << mod1
      << std::setw(3) << det2 //<< std::setw(3) << sec2 << std::setw(3) << mod2
      << std::setw(10) << count
      << std::setw(10) << empty
      << std::endl;
  }

}

uint16_t getModuleID(uint16_t det)
{
  uint16_t sec = ((uint16_t)det-21)/10;
  int16_t mod;
  if (sec == 0) { //head A
    mod = det - 21;
    if (mod < 0 || mod > 8)
      throw std::string("mod: "+std::to_string(mod)+" should not be present");
    return modMapA[mod];
  }
  else if (sec == 1) { //head B
    mod = det - 31;
    if (mod < 0 || mod > 8)
      throw std::string("mod: "+std::to_string(mod)+" should not be present");
    return modMapB[mod];
  }
  else
    throw std::string("sec: "+std::to_string(sec)+" should not be present");
  
}

uint16_t getCrystalID(uint16_t det, uint16_t cry)
{
  uint16_t sec = ((uint16_t)det-21)/10;
  int16_t i, j;
  i = cry / 23;
  if (i<0 || i > 22)
    throw std::string("i: "+std::to_string(i)+" should not be present");
  
  if (sec == 1) { //head B
    j = cry%23;
    if (j<0 || j > 22)
      throw std::string("j: "+std::to_string(j)+" should not be present");
    
    return j*23 + (22-i);
  }
  else if (sec == 0) { //head A
    j = 22 - cry%23;
    if (j<0 || j > 22)
      throw std::string("j: "+std::to_string(j)+" should not be present");

    return j*23 + i;
  }
  else
    throw std::string("sec: "+std::to_string(sec)+" should not be present");
}
