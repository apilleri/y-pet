/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <string>
#include <map>
#include <map>
#include <algorithm>

#include "cxxopts.hpp"
#include "f_utils.h"
#include "timer.hpp"
#include "CylindricalGeometry.h"
#include "dataTypes.h"
#include "LmType.h"

#define N_CRYSTALS 702

void loadFromLm(std::string inputFile, CylindricalGeometry *scanner, HistogramsMap &hg);

uint16_t fixCrystal(uint16_t crystal)
{
  uint16_t row = static_cast<uint16_t>(crystal)%27;
  uint16_t col = static_cast<uint16_t>(crystal)/27;
  col = 25-col;
  return (col*27 + row);
}

int main(int argc, char *argv[])
{
  try {   
    cxxopts::Options options(argv[0], "Convert Data Iris");
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file", cxxopts::value<std::string>())
    ("i, input", "Input data file", cxxopts::value<std::string>())
    ("o, output", "Output data file", cxxopts::value<std::string>())
    ("h, help", "Print help");

    auto result = options.parse(argc, argv);
    
    if (!result.count("input"))
      throw std::string( "Missing Input data file\n\n" + options.help() );
    
    if (!result.count("output"))
      throw std::string( "Missing Output data file\n\n" + options.help() );
    
    if (!result.count("config"))
      throw std::string( "Missing Scanner Decription XML config file\n\n" + options.help() );
    
    std::string input = result["input"].as<std::string>();
    std::string output = result["output"].as<std::string>();
    std::string scanner_xml = result["config"].as<std::string>();
    
    CylindricalGeometry *Iris = new CylindricalGeometry(scanner_xml, false);
    
    HistogramsMap histogram;
    histogram.allocate(Iris->getDetectorsList(),
                       Iris->getCoincidenceSchema(),
                       "LORs", 0);

    loadFromLm(input, Iris, histogram);
    histogram.save(output);
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nError: " << error << std::endl;
    exit(1);
  }
}


void loadFromLm(std::string inputFile, CylindricalGeometry *scanner, HistogramsMap &hg)
{
  fs::path file_path = fs::current_path() / inputFile;
  uint32_t histSize = fs::file_size(file_path)/sizeof(LmType);
  //   uint32_t histSize = filesize(inputFile)/sizeof(LmType);
  
  std::cout << "histSize: " << histSize << std::endl;
  
  std::ifstream inputHistogramFile;
  inputHistogramFile.open(inputFile);
  if ( !inputHistogramFile.is_open() )
    throw std::string ( "Unable to open file " + inputFile + " for reading");
  
  LmType *lm_data;
    
  timer msecTimer("msec");
  msecTimer.start("Preparing Histograms", "", true);

  lm_data = new LmType[histSize];
  if (! inputHistogramFile.read(reinterpret_cast<char *>(lm_data), histSize*sizeof(LmType)) ) {
    delete[] lm_data;
    throw std::string ( "Unable to read from file " + inputFile);
  }
  
  inputHistogramFile.close();
  for (uint32_t ii=0; ii<histSize; ii++) {
    uint16_t sec1 = ((uint32_t)lm_data[ii].D1()-21)%10;
    uint16_t mod1 = 1-((uint32_t)lm_data[ii].D1()-21)/10;
    uint16_t sec2 = ((uint32_t)lm_data[ii].D2()-21)%10;
    uint16_t mod2 = 1-((uint32_t)lm_data[ii].D2()-21)/10;
    uint16_t cry1 = fixCrystal( lm_data[ii].C1() );
    uint16_t cry2 = fixCrystal( lm_data[ii].C2() );
    detID_t uniqueID1 = scanner->computeDetectorID( {sec1, mod1, 0} );
    detID_t uniqueID2 = scanner->computeDetectorID( {sec2, mod2, 0} );
    
    uint32_t szudzik_id = szudzikPair(uniqueID1, uniqueID2);
    uint32_t szudzik_id_swapped = szudzikPair(uniqueID2, uniqueID1);
    bool straight = true, swapped = true;
    
    
    if (hg.data.find(szudzik_id) == hg.data.end() )
      straight = false;
    if (hg.data.find(szudzik_id_swapped) == hg.data.end() ) 
      swapped = false;
    
    if (!straight && !swapped) {
      std::cout << " s1: " << std::setw(5) << sec1
      << " m1: " << std::setw(5) << mod1
      << " s2: " << std::setw(5) << sec2
      << " m2: " << std::setw(5) << mod2 << std::endl;
      std::cout << "uniqueID1: " << uniqueID1 << std::endl;
      std::cout << "uniqueID2: " << uniqueID2 << std::endl;
      std::cout << "szudzik_id: " << szudzik_id << std::endl;
      std::cout << "szudzik_id_swapped: " << szudzik_id_swapped << std::endl;
      throw std::string("Unable to find detector pair.");
    }
    
    
    if (swapped) {
      szudzik_id = szudzik_id_swapped;
      std::swap(cry1, cry2);
    }
    
    
    uint32_t lorN = cry1*N_CRYSTALS + cry2;

    hg.data[szudzik_id][lorN] += lm_data[ii].Count();
  }
  delete []lm_data;
  msecTimer.stop();
  
  hg.printStats();

}

