#ifndef INCLUDE_TRIMAGE_TYPES_H_
#define INCLUDE_TRIMAGE_TYPES_H_

// #define DEBUG_LOADTX_FLAG

#include <cstdint>
#include <array>
#include <cmath>
#include <iomanip>
#include <fstream>
#include <limits>

#define PROMPT_TIMING_WINDOW  2e-9L   // 2 ns
#define RANDOM_TIMING_WINDOW  100e-9L // 100 ns
#define TXs_NUMBER            2
#define ASICs_NUMBER          4
#define SPECTRON_NCRYSTALS    64

using uchar = unsigned char;
using number_t = uint64_t;

using coord_t = uchar;
using bit_t = bool;
using eventID_t = uint8_t;
using fraction_t = uint16_t;

using event_t = uchar;
using position_t = uint16_t;
using energy_t = uint16_t;
using timestamp_t = float;

using packetType_t = uchar;
using txID_t = position_t;
using asicID_t = position_t;

const timestamp_t maxTimestamp = static_cast<timestamp_t>( std::numeric_limits<uint32_t>::max() )*25e-9L
+ static_cast<timestamp_t>( 0b0000001111111111 )*24.4140625e-12L;
const timestamp_t overflowFactor = 3.0L/4.0L;


#define EVT_NULL     0b00000000
#define EVT_PROMPT   0b00000001
#define EVT_RANDOM   0b00000010
#define EVT_MULTIPLE 0b00000011
#define MAX_PARIAL_OVERFLOW 50

class inputType {
  uint16_t w0, w1, w2, w3, w4, w5;
  
public:
  // --- extraction of raw data
  
  packetType_t raw_packetType() const   { return ((w5 & 0b1111000000000000) >> 12); };
  bit_t delayed() const                 { return (w5 & 0b0000000000000100); };
  bit_t ce() const                      { return (w5 & 0b0000000000000010); };
  bit_t cb() const                      { return (w5 & 0b0000000000000001); };
  txID_t txID() const               { return ((w5 & 0b0000111110000000) >> 7); };
  asicID_t asicID() const           { return ((w5 & 0b0000000001111000) >> 3); };
  coord_t raw_X() const                 { return ((w4 & 0b1111111100000000) >> 8); };
  coord_t raw_Y() const                 { return (w4 & 0b0000000011111111); };
  bit_t gate3() const                   { return (w3 & 0b1000000000000000); };
  bit_t gate2() const                   { return (w3 & 0b0100000000000000); };
  bit_t gate1() const                   { return (w3 & 0b0010000000000000); };
  bit_t gate0() const                   { return (w3 & 0b0001000000000000); };
  uint16_t raw_energy() const           { return (w3 & 0b0000111111111111); };
  eventID_t raw_eventID() const         { return ((w0 & 0b1111110000000000) >> 10); };
  fraction_t raw_fraction() const       { return (w0 & 0b0000001111111111); }; 
  
  
  void setUnusable()
  {
    w5 = w5 & 0b0000111111111111;
  }
  
  // --- conversion from input to output data
  bool isSingle() const
  {
    return (!(raw_packetType() ^ 0b1110));
  }
  
  bool goodXY() const
  {
    return (raw_X() != 0 && raw_Y() != 0);
  }
  
  timestamp_t getTimestamp() const;
  
  std::tuple<position_t, energy_t> getPositionAndEnergy() const ;
  
  // --- other methods
  void print(std::ostream &) const;
};


class coincidenceTypeRaw {

protected:
  event_t evt_type;
  position_t position1;
  position_t position2;
  energy_t energy1;
  energy_t energy2;
  timestamp_t time1;
  timestamp_t time2;
  
public:
  coincidenceTypeRaw() :
  evt_type(0), position1(0), position2(0), energy1(0), energy2(0), time1(0), time2(0)
  {  }
  
  event_t getEvtType() const       { return evt_type; };
  
  position_t getPosition1() const  { return position1; };
  position_t getPosition2() const  { return position2; };
  
  energy_t getEnergy1() const  { return energy1; };
  energy_t getEnergy2() const  { return energy2; };
  
  timestamp_t getTime1() const  { return time1; };
  timestamp_t getTime2() const  { return time2; };
};

class coincidenceType : public coincidenceTypeRaw {
//   event_t evt_type;
//   position_t position1;
//   position_t position2;
//   energy_t energy1;
//   energy_t energy2;
//   timestamp_t time1;
//   timestamp_t time2;
  
public:
  // void constructor
  coincidenceType()
  { 
    evt_type = 0;
    position1 = 0;
    position2 = 0;
    energy1 = 0;
    energy2 = 0;
    time1 = 0;
    time2 = 0;
  }
//   
  // coincidence constructor  
  coincidenceType(const event_t&,
                  const inputType&,
                  const inputType&,
                  const timestamp_t&,
                  const timestamp_t&
  );
  

  
  void print(std::ofstream&) const;
};

#endif // INCLUDE_TRIMAGE_TYPES_H_
