/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <cstdint>
#include <map>
#include <atomic>

#include <omp.h>

// Root include
#include <TApplication.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>

#include "cxxopts.hpp"
#include "vGeometry.h"
#include "CylindricalGeometry.h"
#include "PlanarGeometry.h"
#include "f_utils.h"
#include "dataTypes.h"
#include "progress_bar.hpp"

#include "HistogramsMap.h"

#include <sys/types.h>
#include <dirent.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include <timer.hpp>


/*
 * From the simulation it is possible to extract the different types of
 * coincidences: true, scatter and randoms. The application allows to create
 * an histogram choosing which kind of coincidences to save.
 * A standard histogram is saved with extensions .hg and can contain every kind
 * of events. An histogram which contains only random counts has extension .r_hg
 * and an histogram which contains only scatter counts has extension .s_hg and
 * an histogram which contains only true counts has extension .t_hg
 *
*/

// #include <boost/progress.hpp>

class ROOTParser {
  
  vGeometry  *scanner;
  
  // contains the total number of counts without differentiating among the type
  // coincidences. It is used when extracting the "Coincidences" branch or 
  // when extracting the "delay" branch
  HistogramsMap histogram;
  HistogramsMap true_counts;
  HistogramsMap random_counts;
  HistogramsMap scatter_counts;
  
  uint16_t n_threads;
  
  // parameters
  bool delayed;
  bool staggered;
  bool print_scatter;
  
  FloatType upper_energy;
  FloatType lower_energy;
  
  ProgressBar *progress;
  
  // STATS
  uint64_t nentries = 0;
  std::atomic<uint64_t> Nbr_Coinc_Trues_Before = 0;
  std::atomic<uint64_t> Nbr_Coinc_Trues = 0;
  std::atomic<uint64_t> Nbr_Coinc_Random_Before = 0;
  std::atomic<uint64_t> Nbr_Coinc_Random = 0;
  std::atomic<uint64_t> Nbr_Coinc_Scatter_Before = 0;
  std::atomic<uint64_t> Nbr_Coinc_Scatter = 0;
  std::atomic<uint64_t> Nbr_Coinc_Scatter2_Before = 0;
  std::atomic<uint64_t> Nbr_Coinc_Scatter2 = 0;
  std::atomic<uint64_t> Nbr_out_energy_window = 0;
  std::map<std::string, uint64_t> scatter_regions;
  std::atomic<uint64_t> not_found = 0;
  
public:
  
  ROOTParser(std::string xml_config)
    : scanner(nullptr), delayed(false), staggered(false),
      print_scatter(false), upper_energy(0), lower_energy(0),
      n_threads(1), progress(nullptr)
  {
    scanner = vGeometry::initScanner(xml_config, false);
    scanner->printGeometryInfo("./");
    scanner->printSymInfo("./");
    
    histogram.allocate(scanner->getDetectorsList(),
                       scanner->getCoincidenceSchema(),
                       "LORs",
                       0);
    
    true_counts.allocate(scanner->getDetectorsList(),
                    scanner->getCoincidenceSchema(),
                    "LORs",
                    0);
    
    random_counts.allocate(scanner->getDetectorsList(),
                           scanner->getCoincidenceSchema(),
                           "LORs",
                           0);
    
    scatter_counts.allocate(scanner->getDetectorsList(),
                            scanner->getCoincidenceSchema(),
                            "LORs",
                            0);
    
  }
  

  const HistogramsMap& getHistogram() const { return histogram; }
  const HistogramsMap& getTrueHg() const { return true_counts; }
  const HistogramsMap& getRandomHg() const { return random_counts; }
  const HistogramsMap& getScatterHg() const { return scatter_counts; }

  void setDelayed() { delayed = true; }
  void setStaggered() { staggered = true; }
  void setPrintScatter() { print_scatter = true; }
  
  void setLowerEnergy(FloatType value) { lower_energy = value; }
  void setUpperEnergy(FloatType value) { upper_energy = value; }
  
  void setThreadsNumber(uint16_t nt)
  {
    n_threads = nt;
  }
  
  void parse(const std::vector<std::string>& files_list)
  {
    //getting total number of entries, used for the progress bar
    for (const std::string& ff : files_list) {
      TFile *t_file = TFile::Open(ff.c_str());
      if (!t_file)
        throw std::string("Unable to open: ") + ff;
      
      std::cout << "Openeng: " << ff << std::flush;
      
      TTree *Coincidences;
      if (!delayed) 
        Coincidences = (TTree*)gDirectory->Get("Coincidences");
      else
        Coincidences = (TTree*)gDirectory->Get("delay");
      
      nentries += Coincidences->GetEntries();
      t_file->Close();
      
      std::cout << " ok" << std::endl;
    }
    
    uint16_t n_files = files_list.size();
    
    std::cout << "Found " << n_files << " files with a total of ";
    std::cout << nentries << " entries" << std::endl;
    
    progress = new ProgressBar(nentries);
    
    namespace ch = std::chrono;
    ch::high_resolution_clock::time_point startTime, stopTime;
    
    startTime = ch::high_resolution_clock::now();
    omp_set_dynamic(0);
    #pragma omp parallel num_threads(n_threads)
    {
      #pragma omp single
      {
        std::cout << "Using " << omp_get_num_threads() << " threads" << std::endl;
      }
      
      #pragma omp for schedule(static, 1)
      for (uint16_t ff=0; ff<n_files; ff++)
        parseSingleFile(files_list[ff]);
      
    }
    
    stopTime = ch::high_resolution_clock::now();
    FloatType total_time = ch::duration_cast<ch::seconds>(stopTime - startTime).count();
    
    std::cout << "\nTotal time: " << total_time << " s";
  }
  
  void stats() const
  {
    std::cout << "\nBEFORE ENERGY WINDOW CUT" << std::endl;
    printStat("Total: ", nentries, nentries);
    printStat("Not found: ", not_found, nentries);
    printStat("Trues: ", Nbr_Coinc_Trues_Before, nentries);
    printStat("Random: ", Nbr_Coinc_Random_Before, nentries);
    printStat("Scatter: ", Nbr_Coinc_Scatter_Before, nentries);
    printStat("Scatter doppio: ", Nbr_Coinc_Scatter2_Before, nentries);
    
    std::cout << "\nAFTER ENERGY WINDOW CUT" << std::endl;
    uint64_t Nbr_Coinc_Saved = Nbr_Coinc_Trues + Nbr_Coinc_Random + Nbr_Coinc_Scatter;
    printStat("Total: ", Nbr_Coinc_Saved, nentries);
    printStat("Trues: ", Nbr_Coinc_Trues, Nbr_Coinc_Saved);
    printStat("Random: ", Nbr_Coinc_Random, Nbr_Coinc_Saved);
    printStat("Scatter: ", Nbr_Coinc_Scatter, Nbr_Coinc_Saved);
    printStat("Scatter doppio: ", Nbr_Coinc_Scatter2, Nbr_Coinc_Saved);
    
    
    if (print_scatter) {
      std::cout << "\nAFTER ENERGY WINDOW CUT: SCATTER PHANTOM" << std::endl;
      
      std::map<std::string,uint64_t>::const_iterator it=scatter_regions.begin();
      while ( it!=scatter_regions.end() ) {

        std::cout << "Volume: " << it->first;
        std::cout << " counts: " << it->second;
        uint64_t tot_scatter = Nbr_Coinc_Scatter+Nbr_Coinc_Scatter2;
        FloatType v = ((FloatType)it->second)/(tot_scatter)*100;
        std::cout << " (" << v  << "%)"<< std::endl;

        ++it;
      }
    }
  }
  
private:
  void printStat(std::string s, uint64_t value, uint64_t ref) const
  {
    float perc_v = ((float)value)/ref*100;
    std::string perc_s = "(" + std::to_string(perc_v) + "%)";
    std::cout << std::setw(17) << s;
    std::cout << std::setw(12) << value;
    std::cout << std::setw(15) << perc_s;
    std::cout << std::endl;
  }
  
public:
  void parseSingleFile(std::string root_file)
  {
    TFile *t_file = TFile::Open(root_file.c_str());
    if (!t_file)
      throw std::string("Unable to open: ") + root_file;
    
    Int_t           comptonPhantom1;
    Int_t           comptonPhantom2;
    Int_t           comptonCrystal1;
    Int_t           comptonCrystal2;
    Char_t          comptVolName1[40];
    Char_t          comptVolName2[40];
    Int_t           crystalID1;
    Int_t           crystalID2;
    Float_t         energy1;
    Float_t         energy2;
    Int_t           eventID1;
    Int_t           eventID2;
    Int_t           layerID1;
    Int_t           layerID2;
    Int_t           moduleID1;
    Int_t           moduleID2;
    Int_t           rsectorID1;
    Int_t           rsectorID2;
    Int_t           submoduleID1;
    Int_t           submoduleID2;
    
    //Set branch addresses - TTree Coincicences
    TTree *Coincidences;
    if (!delayed)
      Coincidences = (TTree*)gDirectory->Get("Coincidences");
    else
      Coincidences = (TTree*)gDirectory->Get("delay");

    Coincidences->SetBranchAddress("comptVolName1",&comptVolName1);
    Coincidences->SetBranchAddress("comptVolName2",&comptVolName2);
    Coincidences->SetBranchAddress("comptonPhantom1",&comptonPhantom1);
    Coincidences->SetBranchAddress("comptonPhantom2",&comptonPhantom2);
    Coincidences->SetBranchAddress("comptonCrystal1",&comptonCrystal1);
    Coincidences->SetBranchAddress("comptonCrystal2",&comptonCrystal2);
    Coincidences->SetBranchAddress("crystalID1",&crystalID1);
    Coincidences->SetBranchAddress("crystalID2",&crystalID2);
    Coincidences->SetBranchAddress("energy1",&energy1);
    Coincidences->SetBranchAddress("energy2",&energy2);
    Coincidences->SetBranchAddress("eventID1",&eventID1);
    Coincidences->SetBranchAddress("eventID2",&eventID2);
    Coincidences->SetBranchAddress("layerID1",&layerID1);
    Coincidences->SetBranchAddress("layerID2",&layerID2);
    Coincidences->SetBranchAddress("moduleID1",&moduleID1);
    Coincidences->SetBranchAddress("moduleID2",&moduleID2);
    Coincidences->SetBranchAddress("rsectorID1",&rsectorID1);
    Coincidences->SetBranchAddress("rsectorID2",&rsectorID2);
    Coincidences->SetBranchAddress("submoduleID1",&submoduleID1);
    Coincidences->SetBranchAddress("submoduleID2",&submoduleID2);
    
    
    uint64_t cur_nentries = Coincidences->GetEntries();
    uint64_t nbytes = 0;
    
    for (uint64_t i=0; i < cur_nentries;i++) {
      
      nbytes += Coincidences->GetEntry(i);
      
      if ( eventID1 != eventID2 )
        Nbr_Coinc_Random_Before++;
      if ( eventID1 == eventID2 && comptonPhantom1 == 0 && comptonPhantom2 == 0 ) 
        Nbr_Coinc_Trues_Before++;
      if ( eventID1 == eventID2 && (comptonPhantom1 != 0 || comptonPhantom2 != 0) ) {
        Nbr_Coinc_Scatter_Before++;
        if (comptonPhantom1 != 0 && comptonPhantom2 != 0)
          Nbr_Coinc_Scatter2_Before++;
      }
      
      
      bool inside_energy_window = true;
      if (energy1 > upper_energy || energy1 < lower_energy) { 
        Nbr_out_energy_window++;
        inside_energy_window = false;
      }
      if (energy2 > upper_energy || energy2 < lower_energy) {
        Nbr_out_energy_window++;
        inside_energy_window = false;
      }
      
      
      if (inside_energy_window) {
        uint16_t s, m, b;
        s = static_cast<uint16_t>(rsectorID1);
        m = static_cast<uint16_t>(moduleID1);
        b = static_cast<uint16_t>(submoduleID1);
        uint16_t detID1 = scanner->computeDetectorID( {s, m, b} );
        s = static_cast<uint16_t>(rsectorID2);
        m = static_cast<uint16_t>(moduleID2);
        b = static_cast<uint16_t>(submoduleID2);
        uint16_t detID2 = scanner->computeDetectorID( {s, m, b} );
        
        uint32_t szudzik_id = szudzikPair(detID1, detID2);
        uint32_t szudzik_id_swapped = szudzikPair(detID2, detID1);
        bool straight = true, swapped = true;
        
        if (histogram.data.find(szudzik_id) == histogram.data.end() )
          straight = false;
        if (histogram.data.find(szudzik_id_swapped) == histogram.data.end() ) 
          swapped = false;
        
        if (!straight && !swapped) {
          not_found++;
          continue;
        }
        
        
        if (swapped) {
          szudzik_id = szudzik_id_swapped;
          std::swap(layerID1, layerID2);
          std::swap(crystalID1, crystalID2);
        }
        
        uint16_t absolute_crystal1 = 0, absolute_crystal2 = 0;
        if (staggered) {
          absolute_crystal1 = layerID1;
          absolute_crystal2 = layerID2;
        }
        else {
          for (uint16_t layer=0; layer<layerID1; layer++)
            absolute_crystal1 += scanner->getDetector(detID1).layer(layer).nCrystals();
          absolute_crystal1 += crystalID1;
          for (uint16_t layer=0; layer<layerID2; layer++)
            absolute_crystal2 += scanner->getDetector(detID2).layer(layer).nCrystals();
          absolute_crystal2 += crystalID2;
        }
        
        uint32_t lor_id = absolute_crystal1*scanner->getDetector(detID2).rCryN() + absolute_crystal2;
        
        #pragma omp atomic update
        histogram.data[szudzik_id][lor_id]++; 
        
        if ( eventID1 != eventID2 ) {
          
          #pragma omp atomic update
          random_counts.data[szudzik_id][lor_id]++;
          Nbr_Coinc_Random++;
          
        } else {
          
          if ( comptonPhantom1 == 0 && comptonPhantom2 == 0 ) {
            
            #pragma omp atomic update
            true_counts.data[szudzik_id][lor_id]++;
            Nbr_Coinc_Trues++;
            
          } else {
            
            #pragma omp atomic update
            scatter_counts.data[szudzik_id][lor_id]++; 

            
            if (comptonPhantom1 != 0 && comptonPhantom2 != 0)
              Nbr_Coinc_Scatter2++;
            else 
              Nbr_Coinc_Scatter++;
            
            std::string v1(comptVolName1);
            std::string v2(comptVolName2);
            #pragma omp critical 
            {
              if (comptonPhantom1)
                scatter_regions[v1]++;
              if (comptonPhantom2)
                scatter_regions[v2]++;
            }
          }
          
        }
        

        
      }
      
      #pragma omp critical
      {
        ++(*progress);
      }

    }
    t_file->Close();
  }
  
};

// std::string parseRootFile(std::string fname, HistogramsMap& hist,
//                           float UpperEnergy, float LowerEnergy,
//                           vGeometry* scanner, bool staggered,
//                           bool p_scatter );

void printStat(std::string s, uint64_t value, uint64_t ref);


int main (int argc, char *argv[]) 
{
  try {
    cxxopts::Options options(argv[0], "Root to Histogram");
    
    options.add_options()
    ("h, help", "Print help");
    
    options.add_options("Configuration")
    ("c, config", "Scanner Decription XML config file",
     cxxopts::value<std::string>());
    
    options.add_options("Input/Output")
    ("f, file",   "Input root file", cxxopts::value<std::string>())
    ("d, dir",   "Input root files directory", cxxopts::value<std::string>())
    ("o, output",  "Output data file name (no extension)",
     cxxopts::value<std::string>())
    ("delayed", "Extract delayed branch")
    ("type", "Type of histograms x,x,x,x (all, true, random, scatter)",
     cxxopts::value<std::vector<uint8_t>>()->default_value("1,0,0,0") );
    
    options.add_options("Parameters")
    ("l, lower",   "Lower bound of the energy window (keV)",
     cxxopts::value<float>()->default_value("350"))
    ("u, upper",   "Upper bound of the energy window (keV)",
     cxxopts::value<float>()->default_value("650"))
    ("s, staggered", "Toggle staggered flag (due to Gate problems")
    ("print-scatter", "Print scatter phantom info")    
    ("t, threads", "Number of parallel threads (useful with more than one .root file)",  cxxopts::value<uint16_t>()->default_value("1"));
    
    auto cl_options = options.parse(argc, argv);
    
    if (!cl_options.count("config"))
      throw std::string( "Missing Scanner Decription XML config file\n\n" + options.help() );
    
    if ( (!cl_options.count("file") && !cl_options.count("dir")) ||
         (cl_options.count("file") && cl_options.count("dir")) )
      throw std::string( "You must specify one option (and only one) between file and dir\n\n" + options.help() );
    
    if (!cl_options.count("output"))
      throw std::string( "Missing Output data file\n\n" + options.help() );
    
    std::string scanner_xml = cl_options["config"].as<std::string>();
    std::vector<std::string> input;
    std::string output_fname = cl_options["output"].as<std::string>();
    
    float lower_bound = cl_options["lower"].as<float>() / 1000;
    float upper_bound = cl_options["upper"].as<float>() / 1000;
    
    uint16_t threads_number = cl_options["threads"].as<uint16_t>();

    const std::vector<uint8_t>& to_save = cl_options["type"].as<std::vector<uint8_t>>();
    
    if (to_save.size() != 4) {
      std::string err = "Provide type of histogram output in the form x,x,x,x";
      err += " where x is a binary digit";
      throw err;
    }

    std::string path("");
    if (cl_options.count("file") )
      input.push_back(cl_options["file"].as<std::string>());
    else {
      path = cl_options["dir"].as<std::string>();
      input = getRootFilenames(path);
    }
    

    
    ROOTParser parser(scanner_xml);
  
    std::string hg_extension = "hg";
    if ( cl_options.count("delayed") ) {
      parser.setDelayed();
      hg_extension = "dhg";
    }
    
    if ( cl_options.count("staggered") )
      parser.setStaggered();
    if ( cl_options.count("print-scatter") )
      parser.setPrintScatter();
    
    parser.setLowerEnergy(lower_bound);
    parser.setUpperEnergy(upper_bound);
  
    parser.setThreadsNumber(threads_number);
    ROOT::EnableThreadSafety();
    
    parser.parse(input);
    parser.stats();

    
    
    if (to_save[0] == 1) {
      const HistogramsMap& histogram = parser.getHistogram();
      histogram.save(output_fname + "." + hg_extension);
    }
    
    if (to_save[1] == 1) {
      const HistogramsMap& histogram = parser.getTrueHg();
//       histogram.printStats();
      histogram.save(output_fname + ".t_" + hg_extension);
    }
    
    if (to_save[2] == 1) {
      const HistogramsMap& histogram = parser.getRandomHg();
      histogram.save(output_fname + ".r_" + hg_extension);
    }
    
    if (to_save[3] == 1) {
      const HistogramsMap& histogram = parser.getScatterHg();
      histogram.save(output_fname + ".s_" + hg_extension);
    }
    
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nError: " << error << std::endl;
    exit(1);
  }
}
