/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <tuple>

#include "cxxopts.hpp"
#include "f_utils.h"
#include "timer.hpp"
#include "CylindricalGeometry.h"
#include "dataTypes.h"

#include "coincidenceType.h" // trimage preprocessing types

void loadCoincidences(std::string fname, std::vector<coincidenceTypeRaw>& out);

void parseData(const CylindricalGeometry * scanner,
               const std::vector<coincidenceTypeRaw>& data,
               uint16_t lb,
               uint16_t ub,
               HistogramsMap& hist,
               int16_t fix_sectors
              );

std::tuple<uint16_t, uint16_t, uint16_t, uint16_t> getGeometryPos(position_t pos, int16_t fix_sectors);
void computeCrystalsLUT();

uint16_t LUT_even[113];
uint16_t LUT_odd[113];

int main(int argc, char *argv[])
{
  try {   
    cxxopts::Options options(argv[0], "Convert Data IRIS");
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file", cxxopts::value<std::string>())
    ("i, input", "Input data file", cxxopts::value<std::string>())
    ("o, output", "Output data file", cxxopts::value<std::string>()->default_value(""))
    ("l, lower",   "Lower bound of the energy window (keV)",  cxxopts::value<float>()->default_value("350"))
    ("u, upper",   "Upper bound of the energy window (keV)",  cxxopts::value<float>()->default_value("650"))
    ("s, start",   "start index for fixing (REMOVE)",  cxxopts::value<int16_t>()->default_value("-1"))
    ("h, help", "Print help");
    
    auto result = options.parse(argc, argv);
    
    if (!result.count("input"))
      throw std::string( "Missing Input data file\n\n" + options.help() );
    
    if (!result.count("output"))
      throw std::string( "Missing Output data file\n\n" + options.help() );
    
    if (!result.count("config"))
      throw std::string( "Missing Scanner Decription XML config file\n\n" + options.help() );
    
    std::string input = result["input"].as<std::string>();
    std::string output = result["output"].as<std::string>();
    std::string scanner_xml = result["config"].as<std::string>();
    
    float lower_bound = result["lower"].as<float>();
    float upper_bound = result["upper"].as<float>();
    
    int16_t fix_sectors = result["start"].as<int16_t>();
    
    CylindricalGeometry *TRIMAGE = new CylindricalGeometry();
    TRIMAGE->setConfigFile(scanner_xml);
    TRIMAGE->init();
    
    HistogramsMap histogram;
    histogram.allocate(TRIMAGE->getFundamentalDetectorPairsList(),
                       TRIMAGE->getCoincidenceSchema(),
                       0);

    computeCrystalsLUT();
//     exit(1);
    std::vector<coincidenceTypeRaw> data;
    loadCoincidences(input, data);
    parseData(TRIMAGE, data, lower_bound, upper_bound, histogram, fix_sectors);
    
    histogram.save(output);
//     histogram.printStats();

    
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nError: " << error << std::endl;
    exit(1);
  }
}

void computeCrystalsLUT()
{
  for (uint16_t j=0; j<8; j++)
    for (uint16_t i=0; i<8; i++)
      LUT_odd[j*8 + i] = j + 8*i;
  
  for (uint16_t j=0; j<7; j++)
    for (uint16_t i=0; i<7; i++)
      LUT_odd[64 + j*7 + i] = 64 + j + 7*i;
  
  
  
  for (uint16_t j=0; j<8; j++)
    for (uint16_t i=0; i<8; i++)
      LUT_even[j*8 + i] = 63 - 8*i - j;
    
  for (uint16_t j=0; j<7; j++)
    for (uint16_t i=0; i<7; i++)
      LUT_even[64 + j*7 + i] = 112 - 7*i - j;
  
//   for (uint16_t i=0; i<113; i++)
//     std::cout << i << " : " << LUT_even[i] << std::endl;
}

void loadCoincidences(std::string fname, std::vector<coincidenceTypeRaw>& out)
{
  fs::path file_path = fs::current_path() / fname;
  uint64_t coinc_n = fs::file_size(file_path)/sizeof(coincidenceTypeRaw);
//   uint64_t coinc_n  = filesize(fname)/sizeof(coincidenceTypeRaw);
  
  std::ifstream input_file;
  input_file.open(fname);
  if ( !input_file.is_open() )
    throw std::string ( "Unable to open file " + fname + " for reading");
  
  std::cout << "Reading " << coinc_n << " coincidences." << std::endl;
  
  out.resize(coinc_n);
  if (! input_file.read(reinterpret_cast<char *>(out.data()), coinc_n*sizeof(coincidenceTypeRaw)) ) 
    throw std::string ( "Unable to read from file " + fname + " readed: " + std::to_string(input_file.tellg()) );
  
  
  input_file.close();
}


void parseData(const CylindricalGeometry * scanner,
               const std::vector<coincidenceTypeRaw>& data,
               uint16_t lb,
               uint16_t ub,
               HistogramsMap& hist,
               int16_t fix_sectors
              )
{
  uint64_t nTooClse = 0, nOutEnergyWindow = 0, nSaved = 0;
  uint16_t min_sector_difference = 9-scanner->getCoincidenceSectorStep();
  
  for (const coincidenceTypeRaw& entry : data) {
    
    uint16_t en1 = entry.getEnergy1();
    if (en1 < lb || en1 > ub) {
      nOutEnergyWindow++;
      continue;
    }
    
    uint16_t en2 = entry.getEnergy2();
    if (en2 < lb || en2 > ub) {
      nOutEnergyWindow++;
      continue;
    }
    
    uint16_t sec1, mod1, blk1, cry1;
    std::tie(sec1, mod1, blk1, cry1) = getGeometryPos(entry.getPosition1(), fix_sectors);
    
    uint16_t sec2, mod2, blk2, cry2;
    std::tie(sec2, mod2, blk2, cry2) = getGeometryPos(entry.getPosition2(), fix_sectors);
    
//     std::cout << sec1 << " " << mod1 << " " << cry1 << std::endl;
//     std::cout << sec2 << " " << mod2 << " " << cry2 << std::endl;
    
    if ( std::abs(sec1 - sec2)  < min_sector_difference || (18 - std::abs(sec1 - sec2)) < min_sector_difference)  {
//       std::cout << sec1 << " " << sec2 << std::endl;
      nTooClse++;
      continue;
    }
    
    detID_t uniqueID1 = scanner->computeDetectorID(sec1, mod1, blk1);
    detID_t uniqueID2 = scanner->computeDetectorID(sec2, mod2, blk2);
    
    uint32_t szudzik_id = szudzikPair(uniqueID1, uniqueID2);
    uint32_t szudzik_id_swapped = szudzikPair(uniqueID2, uniqueID1);
    bool straight = true, swapped = true;
    
    
    if (hist.data.find(szudzik_id) == hist.data.end() )
      straight = false;
    if (hist.data.find(szudzik_id_swapped) == hist.data.end() ) 
      swapped = false;
    
    if (!straight && !swapped) {
      std::cout << " s1: " << std::setw(5) << sec1
      << " m1: " << std::setw(5) << mod1
      << " s2: " << std::setw(5) << sec2
      << " m2: " << std::setw(5) << mod2 << std::endl;
      std::cout << "uniqueID1: " << uniqueID1 << std::endl;
      std::cout << "uniqueID2: " << uniqueID2 << std::endl;
      std::cout << "szudzik_id: " << szudzik_id << std::endl;
      std::cout << "szudzik_id_swapped: " << szudzik_id_swapped << std::endl;
      throw std::string("Unable to find detector pair.");
    }
    
    
    if (swapped) {
      szudzik_id = szudzik_id_swapped;
      std::swap(cry1, cry2);
    }
    
    
    uint32_t lorN = cry1*NCRYSTALS + cry2;
    
    nSaved++;
    hist.data[szudzik_id][lorN] ++ ;
  }
  
  std::cout << "nTooClose: " << nTooClse << std::endl;
  std::cout << "nOutEnergyWindow: " << nOutEnergyWindow << std::endl;
  std::cout << "nSaved: " << nSaved << std::endl;
}

std::tuple<uint16_t, uint16_t, uint16_t, uint16_t> getGeometryPos(position_t pos, int16_t fix_sectors)
{
  uint16_t sec = pos >> 11;
  // FIX per acquisizione 1vs3 con TX a caso
  if (fix_sectors >= 0) {
    switch (sec) {
      case 15:
        sec = fix_sectors;
        break;
      
      case 13:
        sec = (8+fix_sectors)%18;
        break;
      
      case 14:
        sec = (9+fix_sectors)%18;
        break;
        
      case 12:
        sec = (10+fix_sectors)%18;
        break;
    }
  }
  
  uint16_t asic = (pos & 0b0000011110000000) >> 7;
  uint16_t cry = pos & 0b0000000001111111;
  if (asic % 2) {
    cry = LUT_odd[cry];
    asic = 12 - asic; //dispari
  }
  else {
    cry = LUT_even[cry];
    asic = 10 - asic; //pari
  }

  uint16_t mod = asic/4;
  uint16_t blk = asic % 4;
  
  
  
  return std::make_tuple(sec, mod, blk, cry);
}

