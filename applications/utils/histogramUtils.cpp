/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <tuple>

#include "cxxopts.hpp"
#include "f_utils.h"
#include "timer.hpp"
#include "CylindricalGeometry.h"
#include "HistogramsMap.h"
#include "phantoms.h"

#include <sys/types.h>
#include <dirent.h>

void readDirectory(const std::string& name, std::vector<std::string>& v);

int main(int argc, char *argv[])
{
  try {   
    cxxopts::Options options(argv[0], "Histogram Utils");
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file", cxxopts::value<std::string>() )
    ("activate-vg", "Activate virtual geometry")
    ("h, help", "Print help");
    
    options.add_options("Histogram Operations")
    ("i, input", "Input Histogram",cxxopts::value<std::string>())
    ("o, output", "Output Histogram",cxxopts::value<std::string>())
    ("p, print", "Print Stats")
    ("d, dump",  "Dump2Txt")
    ("create", "Create histogram of ones")
    ("transform-to-exp", "Compute the value exp(-LOR_count)")
    ("factor", "Multiply LORS counts by a factor [use vlist]", cxxopts::value<FloatType>() )
    ("vlist", "detPair_t list", cxxopts::value< std::vector<detPair_t> >() );
    
    options.add_options("Binary Operations")
    ("add",   "Add two histograms (First + Second)")
    ("subtract",   "Subtract two histograms (First - Second)")
    ("multiply",   "Multiply two histograms (First * Second)")
    ("divide",   "Divide two histograms (First / Second)")
    ("first",   "First Histogram", cxxopts::value<std::string>() )
    ("second",   "Second Histogram", cxxopts::value<std::string>() );
    
    //     ("s, sum",   "Directory of the Histograms to add",
    //      cxxopts::value<std::string>() );
    //     ("m, merge", "Merge planar histograms (req. directory)",
    //      cxxopts::value<std::string>() )
    //     ("tag", "XML TAG to extract from the configuration file",
    //      cxxopts::value<std::string>())
    //     ("remove-random", "Remove random counts")
    
    auto cmd_line = options.parse(argc, argv);
    
    if ( cmd_line.count("help") ){
      std::cout << options.help() << std::endl;
      exit(1);
    }
      
    if (!cmd_line.count("config"))
      throw std::string( "Missing Scanner Decription XML config file\n\n" + options.help() );
    
    const std::string& scanner_xml = cmd_line["config"].as<std::string>();
    bool activate_vg = cmd_line.count("activate-vg");
    std::string hist_type;
    if (activate_vg)
      hist_type = "LOFs";
    else
      hist_type = "LORs";
    
    vGeometry *scanner = vGeometry::initScanner(scanner_xml, activate_vg);
    
    if (cmd_line.count("print") || cmd_line.count("dump") ||
      cmd_line.count("factor") || cmd_line.count("transform-to-exp") ) {
      
      if (!cmd_line.count("input"))
        throw std::string( "Missing input histogram\n\n" + options.help() );
      if (!cmd_line.count("output"))
        throw std::string( "Missing output histogram\n\n" + options.help() );
      
      HistogramsMap histogram;
      histogram.allocate(scanner->getDetectorsList(),
                        scanner->getCoincidenceSchema(),
                        hist_type, 0);
      
      if (!cmd_line.count("input"))
        throw std::string( "Missing input histogram\n\n" + options.help() );
      
      histogram.load( cmd_line["input"].as<std::string>() );
      
      if (cmd_line.count("print")) {
        const GroupedCS& gcs = scanner->getGroupedCS();
        
        std::cout << "Histograms statistics" << std::endl;
        float tot_sum=0;
        histogram.printHeader();
        for (const std::vector<CoincidencePairSym>& vec : gcs) {
          FloatType local_sum = 0;
          for (const CoincidencePairSym& pair : vec) {
            detPair_t id = pair.pairID();
            histogram.printPair(id);
            local_sum += std::accumulate(histogram.data[id].begin(),
                                         histogram.data[id].end(), 0.);
          }
          std::cout << "\t\tMean counts: " << std::setw(8) << local_sum/vec.size();
          std::cout << std::endl << std::endl;
          tot_sum += local_sum;
        }
        
        std::cout << "# detector pairs: " << histogram.data.size() << std::endl;
        std::cout << "tot_sum: " << tot_sum << std::endl;
      }
      
      else if (cmd_line.count("dump")) {

        histogram.dump2txt("dump");
        
      }
      
      else if (cmd_line.count("factor")) {
        
        if (!cmd_line.count("vlist"))
          throw std::string( "Missing detecor list\n\n" + options.help() );
        
        std::cout << "Input Histogram stats" << std::endl;
        histogram.printStats();
        
        std::vector<detPair_t> keys = cmd_line["vlist"].as<std::vector<detPair_t>>();
        
        for (detPair_t k : keys) {
          if ( histogram.data.find(k) == histogram.data.end() )
            std::cout << "unable to find key: " << k << std::endl;
          else {
            for (uint32_t c_lor=0; c_lor<histogram.data[k].size(); c_lor++)
              histogram.data[k][c_lor] *= cmd_line["factor"].as<FloatType>();
          }
        }
        
        histogram.save(cmd_line["output"].as<std::string>());
        std::cout << "Output Histogram stats" << std::endl;
        histogram.printStats();
        
      }
      
      else if (cmd_line.count("transform-to-exp")) {

        std::vector<detPair_t> keys = histogram.keys();
        
        for (detPair_t k : keys) {
          for (uint32_t c_lor=0; c_lor<histogram.data[k].size(); c_lor++)
            histogram.data[k][c_lor] = std::exp(-histogram.data[k][c_lor]);
        }
        
        histogram.save(cmd_line["output"].as<std::string>());
        std::cout << "Output Histogram stats" << std::endl;
        histogram.printStats();
      }
  
    }
    
    else if (cmd_line.count("create")) {
     
      if (!cmd_line.count("output"))
        throw std::string( "Missing output histogram\n\n" + options.help() );
      
      HistogramsMap histogram;
      histogram.allocate(scanner->getDetectorsList(),
                         scanner->getCoincidenceSchema(),
                         hist_type, 1);
      
      histogram.save(cmd_line["output"].as<std::string>());
      
    }
  
    else if ( cmd_line.count("add") || cmd_line.count("subtract") || 
      cmd_line.count("multiply") || cmd_line.count("divide") ) {
      
      HistogramsMap first, second;
      first.allocate(scanner->getDetectorsList(),
                    scanner->getCoincidenceSchema(),
                    hist_type, 0);
      second.allocate(scanner->getDetectorsList(),
                      scanner->getCoincidenceSchema(),
                      hist_type, 0);
      
      if (!cmd_line.count("first"))
        throw std::string( "Missing first histogram\n\n" + options.help() );
      if (!cmd_line.count("second"))
        throw std::string( "Missing second histogram\n\n" + options.help() );
      if (!cmd_line.count("output"))
        throw std::string( "Missing output histogram\n\n" + options.help() );
      
      first.load( cmd_line["first"].as<std::string>() );
      second.load( cmd_line["second"].as<std::string>() );
      
      HistogramsMap out;
      out.allocate(scanner->getDetectorsList(),
                   scanner->getCoincidenceSchema(),
                   hist_type,
                   0);
      
      out = first;
      if (cmd_line.count("add")) {
        out += second;
      }
      else if (cmd_line.count("subtract")) {
        out -= second;
      }
      else if (cmd_line.count("multiply")) {
        out *= second;
      }
      else if (cmd_line.count("divide")) {
        out /= second;
      }
      out.save(cmd_line["output"].as<std::string>());
    }
    
    
    else if ( cmd_line.count("factor") ) {
      
      if (!cmd_line.count("input"))
        throw std::string( "Missing Input Histogram\n\n" + options.help() );
      if (!cmd_line.count("output"))
        throw std::string( "Missing output Histogram\n\n" + options.help() );
      
    }
    
    else if ( cmd_line.count("transform-to-exp") ) {
      

    }
    
    else
      throw std::string( "No action specified\n\n" + options.help());

  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nError: " << error << std::endl;
    exit(1);
  }
}


void readDirectory(const std::string& name, std::vector<std::string>& v)
{
  DIR* dirp = opendir(name.c_str());
  struct dirent * dp;
  while ((dp = readdir(dirp)) != NULL) {
    std::string tmp(dp->d_name);
    if (tmp == "." || tmp == "..")
      continue;
    v.push_back(tmp);
  }
  closedir(dirp);
}
