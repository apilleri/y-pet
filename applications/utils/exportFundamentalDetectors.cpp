/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <fstream>
#include <string>


#include "cxxopts.hpp"
#include "vGeometry.h"
#include "f_utils.h"
#include "dataTypes.h"


int main (int argc, char *argv[]) 
{
  try {
    cxxopts::Options options(argv[0], "Export Fundamental Detectors");
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file",  cxxopts::value<std::string>())
    
    ("m, statistic",  "Max statistic of the eventual simulation",   cxxopts::value<uint16_t>())
    ("h, help", "Print help");
    
    auto cmd_line = options.parse(argc, argv);
    
    if (!cmd_line.count("config"))
      throw std::string("Missing Scanner Decription XML config file\n\n" + options.help() );
    
    std::string scanner_xml = cmd_line["config"].as<std::string>();
    vGeometry *scanner = vGeometry::initScanner(scanner_xml, false);
    
    uint16_t max_statistic = 0;
    if (cmd_line.count("statistic"))
      max_statistic = cmd_line["statistic"].as<uint16_t>();;
      
    std::string basename(scanner->name() + "_f_detectors/");
    createDirectory(basename);

    std::ofstream bash_txt(basename+"run_simulation.sh");
    bash_txt << "#!/bin/bash" << std::endl << std::endl;
    
    const CoincidenceSchema& cs = scanner->getCoincidenceSchema();
    for (const std::pair<detPair_t, CoincidencePairSym>& entry : cs) {
      const CoincidencePairSym& pair = entry.second;
      if ( pair.fundamental() ) {

        detID_t det1 = pair.id1();
        detID_t det2 = pair.id2();
        detPair_t szudzik_id = pair.pairID();
        
        std::string fn1 = std::to_string(szudzik_id) + "_";
        fn1 += std::to_string(det1) + ".detector";
        scanner->getDetector(det1).saveToDisk(basename+fn1);
        
        std::string fn2 = std::to_string(szudzik_id) + "_";
        fn2 += std::to_string(det2) + ".detector";
        scanner->getDetector(det2).saveToDisk(basename+fn2);
        
        std::string f_sim = std::to_string(szudzik_id) + "_";
        f_sim += std::to_string(det1) + "_";
        f_sim += std::to_string(det2) + ".simulation";
        bash_txt << "$MONTECARLO_DET";
        if (max_statistic)
          bash_txt << " -m " << max_statistic;
        bash_txt << " -s " + fn1 + " -e " + fn2 + " -o " + f_sim;
        bash_txt << " > log_" + f_sim + ".txt" << std::endl;
        
        f_sim = std::to_string(szudzik_id) + "_";
        f_sim += std::to_string(det2) + "_";
        f_sim += std::to_string(det1) + ".simulation";
        bash_txt << "$MONTECARLO_DET";
        if (max_statistic)
          bash_txt << " -m " << max_statistic;
        bash_txt << " -s " + fn2 + " -e " + fn1 + " -o " + f_sim;
        bash_txt << " > log_" + f_sim + ".txt" << std::endl;
      }
    }
    
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nError: " << error << std::endl;
    exit(1);
  }
}
