/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <chrono>
#include <cstdint>

#include "cxxopts.hpp"
#include "dataTypes.h"
#include "LmType.h"

uint16_t fixCrystal(uint16_t crystal)
{
//   return crystal;
  uint16_t row = static_cast<uint16_t>(crystal)%27;
  uint16_t col = static_cast<uint16_t>(crystal)/27;
  //     col = 26-col;
  row = 26-row;
  return (col*27 + row);
}

int main(int argc, char **argv) 
{
  try {
    cxxopts::Options options(argv[0], "Fix LM IRIS_XL");
    
    options.add_options()
    
    ("i, input", "Input image", cxxopts::value<std::string>())
    ("o, output", "Output image", cxxopts::value<std::string>())
    
    ("r, remove-heads", "remove heads aa,bb...", cxxopts::value< std::vector<uint16_t> >())
    
    ("h, help", "Print help");
    
    auto result = options.parse(argc, argv);
    
    if (!result.count("input"))
      throw std::string( "Missing input file\n\n" + options.help() );
    if (!result.count("output"))
      throw std::string( "Missing output file\n\n" + options.help() );
    const std::string& input = result["input"].as<std::string>();
    const std::string& output = result["output"].as<std::string>();
    
    uint64_t size = std::filesystem::file_size(input);
    uint64_t n_entries = size/(5*sizeof(float));
    
    std::vector<LmType> list_mode_in(n_entries);
    std::vector<LmType> list_mode_out;
    list_mode_out.reserve(n_entries);
    
    std::ifstream in_file(input);
    in_file.read( reinterpret_cast<char*>(list_mode_in.data()), n_entries*sizeof(LmType));
    in_file.close();
    
    bool remove_heads = false;
    std::vector<uint16_t> heads; 
    if ( result.count("remove-heads") ) {
      remove_heads = true;
      heads = result["remove-heads"].as<std::vector<uint16_t>>();
      std::cout << "removing: ";
      for (uint16_t head : heads)
        std::cout << head << " ";
      std::cout << std::endl;
    }
    
    for (uint64_t i=0; i<list_mode_in.size(); i++) {
      
      bool good_event = true;
      
      uint16_t d1 = list_mode_in[i].D1();
      uint16_t d2 = list_mode_in[i].D2();
      
      if (remove_heads) {
        for (uint16_t head : heads)
          if (d1 == head || d2 == head)
            good_event = false;
      }
      
      if (good_event) {
        float c = list_mode_in[i].Count();
        uint16_t c1 = fixCrystal( list_mode_in[i].C1() );
        uint16_t c2 = fixCrystal( list_mode_in[i].C2() );
        
        LmType temp(d1, d2, c1, c2, c);
        list_mode_out.push_back(temp);
      }
    }
    
    uint64_t n_to_save = list_mode_out.size();
    std::ofstream out_file(output);
    const char *p = reinterpret_cast<const char*>(list_mode_out.data());
    out_file.write(p, n_to_save*sizeof(LmType));
    out_file.close();
    
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << error << std::endl;
    exit(1);
  }
  
  return EXIT_SUCCESS;
}


