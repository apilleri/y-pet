/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <tuple>

#include "cxxopts.hpp"
#include "f_utils.h"
#include "timer.hpp"
#include "CylindricalGeometry.h"
#include "HistogramsMap.h"
#include "phantoms.h"

#include <sys/types.h>
#include <dirent.h>

size_t sanitizeEmpty(HistogramsMap& hist);
size_t sanitizeOutliners(HistogramsMap& hist);

int main(int argc, char *argv[])
{
  try {   
    cxxopts::Options options(argv[0], "Histogram Utils");
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file", cxxopts::value<std::string>() )
    ("activate-vg", "Activate virtual geometry")
    
    ("m, measured", "Input Histogram (measured)",cxxopts::value<std::string>())
    ("t, teoric", "Input Histogram (teoric)",cxxopts::value<std::string>())
    ("uniform", "Uniform teoric Histogram")
    ("sanitize", "Sanitize values over/under 5sigmas")
    
    ("o, output", "Output Histogram",cxxopts::value<std::string>())
    
    ("h, help", "Print help");
    
    auto cmd_line = options.parse(argc, argv);
    
    if ( cmd_line.count("help") ){
      std::cout << options.help() << std::endl;
      exit(1);
    }
    
    if (!cmd_line.count("config"))
      throw std::string( "Missing Scanner Decription XML config file\n\n" + options.help() );
    if (!cmd_line.count("measured"))
      throw std::string( "Missing measured histogram file\n\n" + options.help() );
    if (!cmd_line.count("uniform") && !cmd_line.count("teoric"))
      throw std::string( "Missing teoric histogram file\n\n" + options.help() );
    if (!cmd_line.count("output"))
      throw std::string( "Missing output histogram file\n\n" + options.help() );
    
    const std::string& scanner_xml = cmd_line["config"].as<std::string>();
    const std::string& meas_file = cmd_line["measured"].as<std::string>();
    const std::string& out_file = cmd_line["output"].as<std::string>();
    bool uniform = cmd_line.count("uniform");
    bool activate_vg = cmd_line.count("activate-vg");
    
    vGeometry *scanner = vGeometry::initScanner(scanner_xml, activate_vg);
    
    std::string input_hist_type;
    if ( cmd_line.count("activate-vg") ) 
      input_hist_type = "LOFs";
    else
      input_hist_type = "LORs";
    
    HistogramsMap meas_hg;
    meas_hg.allocate(scanner->getDetectorsList(),
                     scanner->getCoincidenceSchema(),
                     input_hist_type, 0);
    meas_hg.load( meas_file );
    meas_hg.printStats();
    
    HistogramsMap out_hg;
    out_hg.allocate(scanner->getDetectorsList(),
                    scanner->getCoincidenceSchema(),
                    input_hist_type, 0);
    
    std::vector<detPair_t> keys = out_hg.keys();
    
    std::cout << "measured n_nulls: " << sanitizeEmpty(meas_hg) << std::endl;
    
    HistogramsMap teo_hg;
    
    if (uniform) {
      teo_hg.allocate(scanner->getDetectorsList(),
                      scanner->getCoincidenceSchema(),
                      input_hist_type, 1);
    } else {
      const std::string& teo_file = cmd_line["teoric"].as<std::string>();
      teo_hg.allocate(scanner->getDetectorsList(),
                      scanner->getCoincidenceSchema(),
                      input_hist_type, 0);
      teo_hg.load( teo_file );
      teo_hg.printStats();
    }
    
    uint32_t n_nulls = 0;
    for (detPair_t k : keys) {
      for (uint32_t i=0; i < out_hg.data[k].size(); i++) {
        if ( teo_hg.data.at(k)[i] == 0) {
          n_nulls++;
          out_hg.data[k][i] = 1;
        } else
          out_hg.data[k][i] = meas_hg.data[k][i] / teo_hg.data[k][i];
      }
    }
    std::cout << "project n_nulls: " << n_nulls << std::endl;
    std::cout << "sanitized: " << sanitizeOutliners(out_hg) << std::endl;

    const GroupedCS& gcs = scanner->getGroupedCS();

    std::cout << "Histograms statistics" << std::endl;
    float tot_sum=0;
    out_hg.printHeader();
    for (const std::vector<CoincidencePairSym>& vec : gcs) {
      FloatType local_sum = 0;
      for (const CoincidencePairSym& pair : vec) {
        detPair_t id = pair.pairID();
        out_hg.printPair(id);
        local_sum += std::accumulate(out_hg.data[id].begin(),
                                    out_hg.data[id].end(), 0.);
      }
      std::cout << "\t\tMean counts: " << std::setw(8) << local_sum/vec.size();
      std::cout << std::endl << std::endl;
      tot_sum += local_sum;
    }
      
    std::cout << "# detector pairs: " << out_hg.data.size() << std::endl;
    std::cout << "tot_sum: " << tot_sum << std::endl;
          
    out_hg.save(out_file);
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nError: " << error << std::endl;
    exit(1);
  }
}

size_t sanitizeEmpty(HistogramsMap& hist)
{
  // the measured histogram is fixed in case of zero counts: the null LOR counts
  // are set to the minumum value present in that detector pair
  std::vector<detPair_t> keys = hist.keys();
  size_t fixed = 0;
  for (detPair_t k : keys) {
    FloatType min = std::numeric_limits<FloatType>::max();
    
    
    for (uint32_t i=0; i < hist.data[k].size(); i++) {
      if ( hist.data[k][i] > 0 && hist.data[k][i] < min )
        min = hist.data[k][i];
    }
    
    for (uint32_t i=0; i < hist.data[k].size(); i++) {
      if ( hist.data[k][i] == 0 ) {
        fixed++;
        hist.data[k][i] = min;
      }
    }
  }

  return fixed;
}

size_t sanitizeOutliners(HistogramsMap& hist)
{
  return 0;
}