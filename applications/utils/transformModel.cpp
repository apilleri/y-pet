/*
 * This file is part of Y-PET.
 *
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <cstdlib>
#include <iostream>
#include <string>

#include "cxxopts.hpp"
#include "f_utils.h"
#include "fieldOfView.h"
#include "modelT.h"
#include "timer.hpp"
#include "vGeometry.h"

class OldStorage {
 public:
  uint8_t x;
  uint8_t y;
  uint8_t z;
  uint8_t pad;
  float p;

  OldStorage() : x(0), y(0), z(0), pad(0), p(0) {}

  OldStorage(uint8_t xp, uint8_t yp, uint8_t zp, float pp)
      : x(xp), y(yp), z(zp), pad(0), p(pp) {}
};

int main(int argc, char** argv) {
  try {
    cxxopts::Options options(argv[0], "Transform Model to old storage");

    options.add_options()("c, config", "Scanner Decription XML config file",
                          cxxopts::value<std::string>())(
        "g, gmodel", "Geometric Matrix Directory",
        cxxopts::value<std::string>())("fov", "Field of View",
                                       cxxopts::value<std::string>())(
        "overwrite", "Overwrite output directory")("h, help", "Print help");

    auto cl_options = options.parse(argc, argv);

    if (cl_options.count("help")) {
      std::cout << options.help() << std::endl;
      exit(0);
    }

    checkCommandLine(cl_options, "config", options);
    checkCommandLine(cl_options, "gmodel", options);
    checkCommandLine(cl_options, "fov", options);

    std::string scanner_xml = cl_options["config"].as<std::string>();
    std::string model_dir = cl_options["gmodel"].as<std::string>();
    bool overwrite = cl_options.count("overwrite");

    std::string out_dir_name = model_dir;
    if (out_dir_name.back() == '/') {
      out_dir_name.pop_back();
    }

    out_dir_name += "_old_storage";

    if (!fs::exists(out_dir_name))
      fs::create_directory(out_dir_name);
    else if (!overwrite)
      throw std::string("Directory " + out_dir_name +
                        " exists, use --overwrite");

    timer msec_timer("msec");

    vGeometry* scanner;
    scanner = vGeometry::initScanner(scanner_xml, false);

    std::string fov_tag = cl_options["fov"].as<std::string>();
    FieldOfView fov(scanner_xml, fov_tag);

    using PairType = std::pair<detPair_t, CoincidencePairSym>;
    std::cout << "loading G model from directory " << model_dir << std::endl;
    for (const PairType& entry : scanner->getCoincidenceSchema()) {
      const CoincidencePairSym& pair = entry.second;
      if (pair.fundamental()) {
        detID_t id1 = pair.id1();
        detID_t id2 = pair.id2();
        std::string id_pair = std::to_string(id1) + "-" + std::to_string(id2);
        std::string filename = model_dir + "/" + id_pair;

        uint32_t n_ac1 = scanner->getDetector(id1).aCryN();
        uint32_t n_ac2 = scanner->getDetector(id2).aCryN();
        uint32_t n_LORs = n_ac1 * n_ac2;

        msec_timer.start("loading " + id_pair, "", true);
        SparseModel model(n_LORs, fov.nVoxels());
        // detPair_t sz_id = szudzikPair(id1, id2);|
        model.set(filename);
        msec_timer.stop();

        msec_timer.start("reverting to old format " + id_pair, "", true);
        std::ofstream file_model;
        std::string fname = out_dir_name + "/";
        fname += std::to_string(id1) + "-" + std::to_string(id2);
        file_model.open(fname + ".old_storage");
        if (!file_model.is_open())
          throw std::string("Unable to open file: " + fname + ".old_storage");

        uint32_t skipped = 0;
        for (uint32_t cur_row = 0; cur_row < n_LORs; cur_row++) {
			    uint32_t vx_num_check = model.rowElementsN(cur_row);

          if (vx_num_check > UINT16_MAX) {
            std::string error = "row " + std::to_string(cur_row);
            error += "has more than UINT16_MAX elements: ";
            error += std::to_string(vx_num_check);
            throw error;
          }

          uint16_t vx_num = model.rowElementsN(cur_row);
          file_model.write(reinterpret_cast<const char*>(&vx_num), sizeof(vx_num));

          std::vector<OldStorage> row;
          for (uint32_t i = 0; i < vx_num; i++) {
            uint32_t vx = model.rowPointer(cur_row)[i].col;
            FloatType p = model.rowPointer(cur_row)[i].v;
            uint16_t x = fov.getX(vx);
            uint16_t y = fov.getY(vx);
            uint16_t z = fov.getZ(vx);
            if (x > 255 || y > 255 || z > 255) {
              skipped++;
              continue;
            }
            row.push_back(
                OldStorage(static_cast<uint8_t>(x),
                            static_cast<uint8_t>(y),
                            static_cast<uint8_t>(z),
                            p));
          }
          file_model.write(reinterpret_cast<const char*>(row.data()),
                           row.size() * sizeof(OldStorage));
        }
        file_model.close();
        msec_timer.stop();
        std::cout << "Skipped " << skipped << " due to integer overflow" << std::endl;
      }
    }

  } catch (const cxxopts::OptionException& error) {
    std::cout << "ERROR parsing command line options: ";
    std::cout << error.what() << std::endl;
    exit(1);
  } catch (const std::string& error) {
    std::cout << "\nERROR: " << error << std::endl;
    exit(1);
  }

  return EXIT_SUCCESS;
}
