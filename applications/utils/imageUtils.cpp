/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <chrono>

#include "cxxopts.hpp"
#include "fieldOfView.h"
#include "dataTypes.h"

int main(int argc, char **argv) 
{
  try {
    cxxopts::Options options(argv[0], "Image Utils");
        
    options.add_options()
    ("c, config", "Scanner Decription XML config file",
      cxxopts::value<std::string>())
    ("fov", "Field of View", cxxopts::value<std::string>() )
    ("u, uniform",  "Value of the uniform background",
      cxxopts::value<FloatType>())
    ("z, zeros", "Remove negative values (set to zero) on input image")
    ("r, rotate", "Rotate image (angle, deg)",  cxxopts::value<AngleType>())
    
    ("i, input", "Input image", cxxopts::value<std::string>())
    ("o, output", "Output image", cxxopts::value<std::string>())
    
    ("h, help", "Print help");

    auto result = options.parse(argc, argv);
    
    if (!result.count("config"))
      throw std::string( "Missing Scanner Decription XML config file\n\n" + options.help() );
    if (!result.count("fov"))
      throw std::string( "Missing fov\n\n" + options.help() );
    if (!result.count("output"))
      throw std::string( "Missing output image\n\n" + options.help() );
    
    const std::string& scanner_config = result["config"].as<std::string>();
    const std::string& fov_tag = result["fov"].as<std::string>();
    const std::string& out_image = result["output"].as<std::string>();
    
    FieldOfView *FOV = new FieldOfView(scanner_config, fov_tag);

    ImageType result_image;
    
    if (result.count("uniform")) {
      
      FloatType value = result["uniform"].as<FloatType>();
      // UNIFORM
      result_image = FOV->createInitImage(value);
      
    } else if (result.count("rotate")) {
      
      if (!result.count("input"))
        throw std::string("Missing input image\n\n" + options.help() );
      const std::string& in_image = result["input"].as<std::string>();
      
      ImageType img = FOV->loadImage(in_image);
      AngleType angle = result["rotate"].as<AngleType>();
      
      result_image = FOV->createInitImage(0);
      
      FOV->rotateImage(img.data(), result_image.data(), angle, false);
      
    } else if (result.count("zeros")) {
      
      if (!result.count("input"))
        throw std::string("Missing input image\n\n" + options.help() );
      const std::string& in_image = result["input"].as<std::string>();
      
      ImageType img = FOV->loadImage(in_image);
      
      result_image = FOV->createInitImage(0);
      for (uint32_t vx=0; vx<result_image.size(); vx++) {
        if (img[vx]>0)
          result_image[vx] = img[vx];
      }
      
    } else
      throw std::string("No actiov provided\n\n" + options.help() );
    
    FOV->saveImage(out_image, result_image, false);

  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << error << std::endl;
    exit(1);
  }

  return EXIT_SUCCESS;
}

    
