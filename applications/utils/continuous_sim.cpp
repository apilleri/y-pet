/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <random>
#include <thread>
#include <mutex>

#include "cxxopts.hpp"
#include "HistogramsMap.h"
#include "vGeometry.h"

std::mutex g_mutex;

class ThreadJob {
  std::string _config;
  std::string _simulation;
  std::string _output;
  uint32_t _n;
  std::mt19937& _generator;

public:
  ThreadJob(const std::string& config,
            const std::string& simulation,
            const std::string& output,
            uint32_t n_realization,
            std::mt19937& generator):
    _config(config),
    _simulation(simulation),
    _output(output),
    _n(n_realization),
    _generator(generator)
  {}

  void operator()() {
    std::uniform_int_distribution<uint32_t> distrib(1, std::numeric_limits<uint32_t>::max());
    
    for (uint32_t sim=0; sim<_n; sim++) {
      uint32_t seed = distrib(_generator);
      std::string seed_s = std::to_string(seed);
      
      // copy simulation to a new file with the name
      std::string sim_filename = seed_s + ".mac";
      std::string command = "cp " + _simulation + " " + sim_filename;
      system(command.c_str());

      command = "sed -i s,^/gate/output/root/setFileName.*,/gate/output/root/setFileName\\ " + seed_s + ", " + sim_filename;
      system(command.c_str());

      command = "sed -i s,^/gate/random/setEngineSeed.*,/gate/random/setEngineSeed\\ " + seed_s + ", " + sim_filename;
      system(command.c_str());

      command = "Gate " + sim_filename + " > " + seed_s + ".txt";
      system(command.c_str());

      command = "./root2hist -c " + _config + " -f " + seed_s + ".root -o " + seed_s + " --type 0,1,0,0"
         + " >> " + seed_s + ".txt";
      system(command.c_str());


      // critical section
      {
        std::lock_guard<std::mutex> guard(g_mutex);
        command = "./histogramUtils -c " + _config + " --add --first " + _output + 
          " --second " + seed_s + ".t_hg" +
          " -o " + _output;
        system(command.c_str());

        std::cout << std::this_thread::get_id() << " done iteration " << sim << std::endl;
      }

      command = "rm " + sim_filename + " " + seed_s + ".root" + " " + seed_s + ".t_hg";
      system(command.c_str());
    }


  }
};

int main(int argc, char *argv[])
{
  try {   
    cxxopts::Options options(argv[0], "Continuous Simulation");
    
    options.add_options()
	  ("c, config", "Scanner Decription XML config file", cxxopts::value<std::string>())
    ("s, simulation", "Input Histogram",cxxopts::value<std::string>())
    ("o, output", "Output Histogram",cxxopts::value<std::string>())
	  ("t, threads", "Number of threads", cxxopts::value<uint32_t>()->default_value("1") )
    ("n, number", "Number of realizations for each thread", cxxopts::value<uint32_t>()->default_value("1") )
    
    ("h, help", "Print help");
    
    auto parsed_options = options.parse(argc, argv);
    
	  if ( !parsed_options.count("config") ) {
      std::string err_str = "Missing Scanner Decription XML config file\n\n";
      throw err_str + options.help();
    }

    if ( !parsed_options.count("simulation") ) {
      std::string err_str = "Missing simulation file\n\n";
      throw err_str + options.help();
    }
    
    if (!parsed_options.count("output")) {
      std::string err_str = "Missing Output Histogram\n\n";
      throw err_str + options.help();
    }
    
	  const std::string& config = parsed_options["config"].as<std::string>();
    const std::string& simulation = parsed_options["simulation"].as<std::string>();
    const std::string& output = parsed_options["output"].as<std::string>();
	  uint32_t n_threads = parsed_options["threads"].as<uint32_t>();
    uint32_t n_realizations = parsed_options["number"].as<uint32_t>();
    
    vGeometry *scanner = vGeometry::initScanner(config, false);
    
    if (not fs::exists(output)) {
      HistogramsMap output_hg;
      output_hg.allocate(scanner->getDetectorsList(),
                        scanner->getCoincidenceSchema(),
                        "LORs", 0);
      output_hg.save(output);
    }

    
	  std::thread t_pool[n_threads];
    std::random_device rd;  // a seed source for the random number engine
    std::mt19937 gen(rd()); // mersenne_twister_engine seeded with rd()

	  for (uint32_t cur_t=0; cur_t < n_threads; cur_t++) {
      t_pool[cur_t] = std::thread((ThreadJob(config, simulation, output, n_realizations, gen)));
	  }

    for (uint32_t cur_t=0; cur_t < n_threads; cur_t++) {
      t_pool[cur_t].join();
	  }
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nError: " << error << std::endl;
    exit(1);
  }
}

