/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <random>

#include "cxxopts.hpp"
#include "timer.hpp"
#include "HistogramsMap.h"
#include "vGeometry.h"

int main(int argc, char *argv[])
{
  try {   
    cxxopts::Options options(argv[0], "Add Noise to Histogram");
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file", cxxopts::value<std::string>())
    ("activate-vg", "Activate virtual geometry")
    ("i, input", "Input Histogram",cxxopts::value<std::string>())
    ("o, output", "Output basenameHistogram",cxxopts::value<std::string>())
    ("n, number", "Number of realizations", cxxopts::value<uint32_t>()->default_value("1") )
    
    ("h, help", "Print help");
    
    auto parsed_options = options.parse(argc, argv);
    
    if ( !parsed_options.count("config") ) {
      std::string err_str = "Missing Scanner Decription XML config file\n\n";
      throw err_str + options.help();
    }
    
    if (!parsed_options.count("input")) {
      std::string err_str = "Missing Input acquisition histogram(s)\n\n";
      throw err_str + options.help();
    }
    
    if (!parsed_options.count("output")) {
      std::string err_str = "Missing Outut acquisition histogram(s)\n\n";
      throw err_str + options.help();
    }
    

    
    const std::string& scanner_config = parsed_options["config"].as<std::string>();
    const std::string& input = parsed_options["input"].as<std::string>();
    const std::string& output = parsed_options["output"].as<std::string>();
    bool activate_vg = parsed_options.count("activate-vg");
    uint32_t n_realizations = parsed_options["number"].as<uint32_t>();
    
    vGeometry *scanner = vGeometry::initScanner(scanner_config, activate_vg);
    
    HistogramsMap input_hg;
    input_hg.allocate(scanner->getDetectorsList(),
                      scanner->getCoincidenceSchema(),
                      "LORs", 0);
    input_hg.load(input);
    
    HistogramsMap out_hg;
    out_hg.allocate(scanner->getDetectorsList(),
                    scanner->getCoincidenceSchema(),
                    "LORs", 0);
    

    
    const std::vector<detPair_t>& keys = input_hg.keys();
    
    for (uint32_t cur_hist=0; cur_hist < n_realizations; cur_hist++) {
      std::default_random_engine generator;
      generator.seed(cur_hist);
      
      out_hg.fill(0);
      for (detPair_t k : keys) {
        #pragma omp parallel for
        for (uint32_t lor=0; lor < input_hg.data[k].size(); lor++) {
          float value = input_hg.data[k][lor];
          std::poisson_distribution<uint32_t> distribution(value);
          out_hg.data[k][lor] = distribution(generator);
        }
      }
      
      out_hg.save(output + "_e" + std::to_string(cur_hist+1) + ".hg");
    }
    
    
    
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << "\nError: " << error << std::endl;
    exit(1);
  }
}

