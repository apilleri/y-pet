/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <chrono>
#include <cstdint>

#include "cxxopts.hpp"
#include "dataTypes.h"
#include "vGeometry.h"
#include "HistogramsMap.h"
#include "LmType.h"

#define N_CRYSTALS 2048 // 32*64

int main(int argc, char *argv[])
{
  try {    
    cxxopts::Options options(argv[0], "Convert IRIS_XL-64 histogram to LM");
    
    options.add_options()
    ("c, config", "Scanner Decription XML config file",
     cxxopts::value<std::string>())
    ("i, input", "Input histogram", cxxopts::value<std::string>())
    ("o, output", "Output LM file", cxxopts::value<std::string>())
    
    ("h, help", "Print help");
    
    auto cl_options = options.parse(argc, argv);
    
    if (!cl_options.count("config"))
      throw std::string( "Missing Scanner Decription XML config file\n\n" + options.help() );
    if (!cl_options.count("input"))
      throw std::string( "Missing input histogrm\n\n" + options.help() );
    if (!cl_options.count("output"))
      throw std::string( "Missing output filename\n\n" + options.help() );
    
    std::string scanner_xml = cl_options["config"].as<std::string>();
    const std::string& input = cl_options["input"].as<std::string>();
    const std::string& output = cl_options["output"].as<std::string>();
    
    HistogramsMap histogram;
    vGeometry *scanner = vGeometry::initScanner(scanner_xml, false);
    histogram.allocate(scanner->getDetectorsList(),
                       scanner->getCoincidenceSchema(),
                       "LORs",
                       0);
    histogram.load(input);
    
    std::vector<LmType> out_lm;
    out_lm.reserve( histogram.nLors() );
    
    //lookup table for the modules;
    std::map<uint32_t, uint16_t> modules_lut;
    modules_lut[0] = 28;
    modules_lut[1] = 27;
    modules_lut[2] = 26;
    modules_lut[3] = 25;
    modules_lut[4] = 34;
    modules_lut[5] = 33;
    modules_lut[6] = 32;
    modules_lut[7] = 31;
    modules_lut[8] = 24;
    modules_lut[9] = 23;
    modules_lut[10] = 22;
    modules_lut[11] = 21;
    modules_lut[12] = 38;
    modules_lut[13] = 37;
    modules_lut[14] = 36;
    modules_lut[15] = 35;
    
    for (detPair_t d_pair : histogram.keys() ) {
      detID_t d1, d2;
      std::tie(d1, d2) = szudzikUnPair(d_pair);
      d1 = modules_lut[d1];
      d2 = modules_lut[d2];
      for (uint32_t lor=0; lor < histogram.data[d_pair].size(); lor++) {
        uint32_t c1 = std::floor(lor/N_CRYSTALS);
        uint32_t c2 = lor%N_CRYSTALS;
        float value = histogram.data[d_pair][lor];
        out_lm.push_back( LmType(d1, d2, c1, c2, value) );
      }
    }

    uint64_t n_to_save = out_lm.size();
    std::ofstream out_file(output);
    out_file.write( reinterpret_cast<const char*>(out_lm.data()), n_to_save*sizeof(LmType));
    out_file.close();
    
  }
  catch (const cxxopts::OptionException& error) {
    std::cout << "error parsing options: " << error.what() << std::endl;
    exit(1);
  }
  catch (const std::string& error) {
    std::cout << error << std::endl;
    exit(1);
  }
  
  return EXIT_SUCCESS;
}
