/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "ScannerVisualizer.h"
#include "vProjector.h"
#include "modelT.h"
#include "timer.hpp"

void
ScannerVisualizer::drawLOR(const std::vector<MatrixEntry>& row) const
{
  SpaceType halfx = _fov->vxL(0)/2;
  SpaceType halfy = _fov->vxL(1)/2;
  SpaceType halfz = _fov->vxL(2)/2;
  
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  uint16_t n_voxels = row.size();
  
  //nothing to process
  if ( !n_voxels )
    return;
  
  SpaceType max_p = 0;
  for (MatrixEntry voxel : row) {
    SpaceType x = _fov->vx2pos(voxel.col).x();
    SpaceType y = _fov->vx2pos(voxel.col).y();
    SpaceType z = _fov->vx2pos(voxel.col).z();
    points->InsertNextPoint(x-halfx, y-halfy, z-halfz);
    points->InsertNextPoint(x+halfx, y-halfy, z-halfz);
    points->InsertNextPoint(x-halfx, y+halfy, z-halfz);
    points->InsertNextPoint(x+halfx, y+halfy, z-halfz);
    points->InsertNextPoint(x-halfx, y-halfy, z+halfz);
    points->InsertNextPoint(x+halfx, y-halfy, z+halfz);
    points->InsertNextPoint(x-halfx, y+halfy, z+halfz);
    points->InsertNextPoint(x+halfx, y+halfy, z+halfz);
    
    if (voxel.v > max_p)
      max_p = voxel.v;
  }

  //create the array of voxels of the whole module
  vtkVoxel * *voxels_list;
  voxels_list = new vtkVoxel*[n_voxels];
  for(uint16_t i=0; i<n_voxels; i++) {
    voxels_list[i] = vtkVoxel::New();
    voxels_list[i]->GetPointIds()->SetNumberOfIds(8); 
    //this maps internal id 0-7 to global point Id of the vtkPoints list
    for (uint16_t j = 0; j <8; j++) 
      voxels_list[i]->GetPointIds()->SetId(j,j+(i*8));
  }
 
  //create an unstructured grid for the voxels
  vtkSmartPointer<vtkUnstructuredGrid> unstructured_grid = vtkSmartPointer<vtkUnstructuredGrid>::New();
  unstructured_grid->Allocate(n_voxels);
  for(uint16_t i =0; i<n_voxels; i++) {
    unstructured_grid->InsertNextCell(voxels_list[i]->GetCellType(), voxels_list[i]->GetPointIds());
  }
  unstructured_grid->SetPoints(points);
  
  vtkSmartPointer<vtkDataSetSurfaceFilter> surfaceFilter = 
  vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
  surfaceFilter->SetInputData(unstructured_grid);
  surfaceFilter->Update();
  
  vtkSmartPointer<vtkPolyData> polydata =  vtkSmartPointer<vtkPolyData>::New();
  polydata->ShallowCopy(surfaceFilter->GetOutput());
  
  vtkSmartPointer<vtkUnsignedCharArray> cellData =
  vtkSmartPointer<vtkUnsignedCharArray>::New();
  cellData->SetNumberOfComponents(3);
  cellData->SetNumberOfTuples(n_voxels*8);

  for(uint32_t i=0; i<n_voxels*8; i++) {
    SpaceType rgb[3];
    rgb[0] = 0;
    rgb[1] = 0;
    rgb[2] = row[i/8].v/max_p * 255; 
    cellData->InsertTuple(i, rgb);
  }
  
  polydata->GetPointData()->SetScalars(cellData);
  
  
  vtkSmartPointer<vtkPolyDataMapper> grid_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  grid_mapper->SetInputData(polydata);
  
  vtkSmartPointer<vtkActor> grid_actor = vtkSmartPointer<vtkActor>::New();
  grid_actor->SetMapper(grid_mapper);
  
  //   grid_actor->RotateZ(rotation);
  //   grid_actor->GetProperty()->EdgeVisibilityOn();
  //   grid_actor->GetProperty()->SetEdgeColor(100.0/255, 0, 0);
  //   grid_actor->GetProperty()->SetColor(0,0,1);
  
  renderer->AddActor(grid_actor);
  
  for(uint16_t i=0; i<n_voxels; i++) 
    voxels_list[i]->Delete();
  delete[] voxels_list;
//   drawn_lor = grid_actor;
}



std::vector<MatrixEntry>
ScannerVisualizer::onLineLOR(const std::vector<uint16_t>& first,
                             const std::vector<uint16_t>& second) const
{
  timer musec_timer("musec");
  std::vector<uint16_t> hierarchy = {first[0], first[1], first[2]};
  detID_t det1_id = _scanner->computeDetectorID(hierarchy);
  uint16_t c1 = first[first.size()-1];
  hierarchy = {second[0], second[1], second[2]};
  detID_t det2_id = _scanner->computeDetectorID(hierarchy);
  uint16_t c2 = second[second.size()-1];
  
  const PlanarDetectorType& det1 = _scanner->getDetector(det1_id);
  const ThreeVector& start = det1.aCrySurfaceCenter(c1);
  const PlanarDetectorType& det2 = _scanner->getDetector(det2_id);
  const ThreeVector& stop = det2.aCrySurfaceCenter(c2);
  this->drawLine(start, stop, "red", 2);
  std::cout << start << std::endl;
  std::cout << stop << std::endl;
  
  std::cout << "type: " << _projector->type() << std::endl;

  if ( _projector->type().find("DepthMultiray") != std::string::npos ) {
  //   AngleType start_ang = det1.zAngle();
  //   AngleType stop_ang = det2.zAngle(); 
  //   const ProjInfo& p_info = _projector->info();
  //   std::vector<ThreeVector> points = 
  //     createPointsGrid(det1.xLen(c1), det1.yLen(c1), det1.zLen(c1),
  //       p_info.at("npx"), p_info.at("npy"), p_info.at("npz"));
  
  //   for (uint16_t c_p=0; c_p<points.size(); c_p++) {
  //     ThreeVector p1 = det1.rCryCenter(c1) + points[c_p].rotateZnotInPlace(start_ang);
  //     ThreeVector p2 = det2.rCryCenter(c2) + points[c_p].rotateZnotInPlace(stop_ang);
  //     this->drawPoint(p1, 0.1, "green");
  //     this->drawPoint(p2, 0.1, "green");
  //   }

    std::vector<ThreeVector> points;
    std::vector<double> weight;

    const ProjInfo& p_info = _projector->info();
    uint16_t npyz = p_info.at("npyz");
    uint16_t npx = p_info.at("npx");
    std::tie(points, weight) =
        det1.computeIntegrationPoints(c1, (stop-start).unit(), npyz, npx);
    for (uint16_t i = 0; i < points.size(); i++)  {
      this->drawPoint(points[i], npx*weight[i], "red");
      // std::cout << points[i] << " " << weight[i] << std::endl;
    }
    std::tie(points, weight) =
        det2.computeIntegrationPoints(c2, (stop - start).unit(), npyz, npx);
    for (uint16_t i = 0; i < points.size(); i++) {
      this->drawPoint(points[i], npx * weight[i], "green");
      // std::cout << points[i] << " " << weight[i] << std::endl;
    }
  } 

  std::vector<MatrixEntry> row;
  
  // std::vector<ThreeVector> points;
  // std::vector<double> weight;

  // const ProjInfo &p_info = _projector->info();
  // std::tie(points, weight) = det1.computeIntegrationPoints(c1, ThreeVector(0, 0, 0), p_info.at("npz"), p_info.at("npx"));
  // for (uint16_t i=0; i<points.size(); i++) {
  //   this->drawPoint(points[i], weight[i], "red");
  //   std::cout << weight[i] << std::endl;
  // }

  musec_timer.start("computation", "", true);
  _projector->project(det1, c1, det2, c2, row);
  musec_timer.stop();
  
  std::cout << "n crossed voxels: " << row.size() << std::endl;
  
  std::sort(row.begin(), row.end(),
            [&](const MatrixEntry& a, const MatrixEntry& b)
            {
              return a.col < b.col;
            }
  );
  

  return row;
}


std::vector<MatrixEntry>
ScannerVisualizer::offLineLOR(const std::vector<uint16_t>& first,
                              const std::vector<uint16_t>& second,
                              const std::string& g_dir,
                              const std::string& d_dir) const
{
  /*
  //TODO it doesn't work with reflection symetry.
  // reflection (like histograms) needed
  
  std::vector<uint16_t> hierarchy = {first[0], first[1], first[2]};
  detID_t det1_id = _scanner->computeDetectorID(hierarchy);
  uint16_t c1 = first[first.size()-1];
  hierarchy = {second[0], second[1], second[2]};
  detID_t det2_id = _scanner->computeDetectorID(hierarchy);
  uint16_t c2 = second[second.size()-1];
  
  const PlanarDetectorType& det1 = _scanner->getDetector(det1_id);
  const PlanarDetectorType& det2 = _scanner->getDetector(det2_id);
  AngleType angle1 = std::atan(det1.orientation()[1]/det1.orientation()[0]);
  AngleType angle2 = std::atan(det2.orientation()[1]/det2.orientation()[0]);
  
  std::cout << "detectors: " << det1_id << " " << det2_id << std::endl;
  //loading symmetric model
  const CoincidenceSchema& cs = _scanner->getCoincidenceSchema();
  CoincidenceSchema::const_iterator det_pair, det_pair_swapped;
  det_pair = cs.find( szudzikPair(det1_id, det2_id) );
  det_pair_swapped = cs.find( szudzikPair(det2_id, det1_id) );
  
  if ( det_pair_swapped != cs.end() ) {
    std::cout << "swapping" << std::endl;
    std::swap(det_pair, det_pair_swapped);
    std::swap(c1, c2);
  }
  
  if ( det_pair != cs.end() ) {
    timer msec_timer("msec");
    uint16_t n_rc1 = det1.rCryN();
    uint16_t n_rc2 = det2.rCryN();
    lorIDX_t n_LORs =  n_rc1 * n_rc2;
    
    uint16_t n_vc1 = det1.vCryN();
    uint16_t n_vc2 = det2.vCryN();
    lorIDX_t n_LOFs =  n_vc1 * n_vc2;
    
    detID_t f1_id, f2_id;
    std::tie(f1_id, f2_id) = szudzikUnPair( det_pair->second.symID() );
    std::string id_pair = std::to_string(f1_id) + "-" + std::to_string(f2_id);
    
    msec_timer.start("loading " + id_pair, "", true);
    SparseModel g_model_tr( _fov->nVoxels(), n_LOFs);
    g_model_tr.set(g_dir + "/" + id_pair + "_tr");
    SparseModel d_model(n_LORs, n_LOFs );
    d_model.set(d_dir + "/" + id_pair);
//     SparseModel d_model_tr(n_lors, n_lors );
//     d_model_tr.set(d_dir + "/" + id_pair + "_tr");
    msec_timer.stop();
    
    lorIDX_t lor_id = c1*n_rc2 + c2;
    MatrixEntry* d_pointer = d_model.rowPointer(lor_id);
    uint32_t n_el = d_model.rowElementsN(lor_id);
    std::vector<FloatType> d_row(d_model.cols(), 0);
    for (uint32_t i=0; i<n_el; i++) {
      std::cout << "LOF:" << std::setw(8) << d_pointer[i].col << std::setw(15) << d_pointer[i].v << std::endl;
      d_row[ d_pointer[i].col ] = d_pointer[i].v;
    }
    
    //print 
    std::cout << "d_row size: " << n_el << std::endl;
    
    //prodotto d_row e g_model, fare operatore;
    msec_timer.start("computing", "", true);
    std::vector<MatrixEntry> tor = d_row * g_model_tr;
    msec_timer.stop();
    
    std::cout << "n crossed voxels DG: " << tor.size() << std::endl;
    
    uint32_t lof_id = lor_id;
    uint16_t n_active_lofs = d_model.rowElementsN(lor_id);
    std::cout << "n active LOFs: " << n_active_lofs << std::endl;
    for (uint16_t i=0; i<n_active_lofs; i++) {
      MatrixEntry entry = *(d_model.rowPointer(lor_id) + i);
      uint16_t l_c1, l_c2;
      l_c1 = entry.col/n_vc2;
      l_c2 = entry.col%n_vc2;

      const ThreeVector& start = det1.aCrySurfaceCenter(l_c1);
      const ThreeVector& stop = det2.aCrySurfaceCenter(l_c2);
      
      drawBox(start, ThreeVector(1, 1, 1), "blue", angle1);
      drawBox(stop, ThreeVector(1, 1, 1), "blue", angle2);
      
      this->drawLine(start, stop, "blue", 2);
    }
    const ThreeVector& start = det1.rCrySurfaceCenter(c1);
    const ThreeVector& stop = det2.rCrySurfaceCenter(c2);
    
    drawBox(start, ThreeVector(1, 1, 1), "red", angle1);
    drawBox(stop, ThreeVector(1, 1, 1), "red", angle2);
    this->drawLine(start, stop, "red", 2);
    
    if ( !det_pair->second.fundamental() ) {
      //applying symmetry 
      ImageType temp1 = _fov->createInitImage(0);
      ImageType temp2 = _fov->createInitImage(0);
      for (const MatrixEntry& e : tor)
        temp1[e.col] = e.v;
      
      const SymmetryType& sym = det_pair->second.sym();
      AngleType zA = sym.zAngle();
      ThreeVector shift(0, sym.yShift(), sym.zShift() );
      bool xR = sym.xReflect();
      bool yR = sym.yReflect();
      bool zR = sym.zReflect();
      
      std::cout << zA << " " << shift << " " << xR << " " << yR << " " << zR << std::endl;
      _fov->flipImage(temp1.data(), temp2.data(), xR, yR, zR, false);
      std::fill(temp1.begin(), temp1.end(), 0);
      _fov->shiftImage(temp2.data(), temp1.data(), shift, true);
      std::fill(temp2.begin(), temp2.end(), 0);
      _fov->rotateImage(temp1.data(), temp2.data(), -zA, true);
      
      tor.clear();
      for (uint32_t vx=0; vx<temp2.size(); vx++) {
        if ( temp2[vx] )
          tor.push_back( MatrixEntry(vx, temp2[vx]) );
      }
    }
    
    return tor;
    
  } else
    throw std::string("ScannerVisualizer::offLineLOR unknown pair");*/
}

