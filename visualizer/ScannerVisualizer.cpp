/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#include "ScannerVisualizer.h"

#include <vtkCamera.h>

#include <vtkCallbackCommand.h>
#include <vtkCommand.h>

ScannerVisualizer::ScannerVisualizer(const std::string& config, bool activate_vg)
: xml_config(config), _scanner(nullptr), _fov(nullptr), _projector(nullptr)
{
  _scanner = vGeometry::initScanner(xml_config, activate_vg);
  _scanner->printGeometryInfo("./");
  _scanner->printSymInfo("./");
  
  renderer = vtkSmartPointer<vtkRenderer>::New();
  render_window = vtkSmartPointer<vtkRenderWindow>::New();
  render_window->AddRenderer(renderer);
  render_window_interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  render_window_interactor->SetRenderWindow(render_window);
  
//   vtkSmartPointer<vtkCamera> aCamera = vtkSmartPointer<vtkCamera>::New();
//   aCamera->SetFocalPoint (45, 0, 0);
//   renderer->SetActiveCamera(aCamera);
  
  renderer->SetBackground(0.2, 0.2, 0.2); 
  

//   vtkSmartPointer<vtkCallbackCommand> keypressCallback = vtkSmartPointer<vtkCallbackCommand>::New();
//   keypressCallback->SetCallback ( KeypressCallbackFunction );
//   render_window_interactor->AddObserver ( vtkCommand::KeyPressEvent, keypressCallback );
  
  
}


void ScannerVisualizer::initFOV(const std::string& tag_fov_p)
{
  std::cout << "Initializing FOV..." << std::flush;
  _fov = new FieldOfView(xml_config, tag_fov_p);
  _fov->printInfo("./" + _scanner->name() + "_" + tag_fov_p + ".txt");
  std::cout << "done." << std::endl;
}

void ScannerVisualizer::initProjector(const std::string& proj_tag_p)
{
  std::string fname("ScannerVisualizer::initProjector()");
  if (_fov == nullptr)
    throw fname + " FOV not initialized";
  
  _projector = vProjector::initProjector(xml_config, _fov, proj_tag_p);
}

void ScannerVisualizer::drawAxes() const
{
  vtkSmartPointer<vtkAxesActor> axes=vtkAxesActor::New();

  vtkSmartPointer< vtkTextProperty> property_X=vtkTextProperty::New();
  property_X->SetFontSize (14);
  property_X->SetFontFamilyToTimes();
  property_X->SetColor(1,0,0);
  // 	axes->SetCylinderRadius(axisLength);
  axes->SetXAxisLabelText("x");
  axes->GetXAxisCaptionActor2D()->GetTextActor()->SetTextScaleModeToNone();
  axes->GetXAxisCaptionActor2D()->SetCaptionTextProperty(property_X);
  axes->GetXAxisTipProperty ()->SetColor(1,0,0);
  axes->GetXAxisShaftProperty ()->SetColor(1,0,0);

  vtkSmartPointer< vtkTextProperty> property_Y=vtkTextProperty::New();
  property_Y->SetFontSize (14);
  property_Y->SetFontFamilyToTimes();
  property_Y->SetColor(0,1,0);
  axes->SetYAxisLabelText("y");
  axes->GetYAxisCaptionActor2D()->GetTextActor()->SetTextScaleModeToNone();
  axes->GetYAxisCaptionActor2D()->SetCaptionTextProperty(property_Y);
  axes->GetYAxisShaftProperty ()->SetColor(0,1,0);
  axes->GetYAxisTipProperty ()->SetColor(0,1,0);

  vtkSmartPointer< vtkTextProperty> property_Z=vtkTextProperty::New();
  property_Z->SetFontSize (14);
  property_Z->SetFontFamilyToTimes();
  property_Z->SetColor(0,0,1);
  axes->SetZAxisLabelText("z");
  axes->GetZAxisCaptionActor2D()->GetTextActor()->SetTextScaleModeToNone();
  axes->GetZAxisCaptionActor2D()->SetCaptionTextProperty(property_Z);
  axes->GetZAxisShaftProperty ()->SetColor(0,0,1);
  axes->GetZAxisTipProperty ()->SetColor(0,0,1);

  const ThreeVector& half_size = _scanner->getScannerSize()/2;
  axes->SetTotalLength(half_size.x() + 25, half_size.y() + 25, half_size.z() + 25);
  axes->SetConeRadius (0.1);
  axes->SetConeResolution(10);
  renderer->AddActor(axes);
}



void ScannerVisualizer::drawFov() const
{
  std::string fname("ScannerVisualizer::drawFov()");
  if (_fov == nullptr)
    throw fname + " FOV not initialized";
  
  SpaceType x, y, z;
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkStructuredGrid> structured_grid = vtkSmartPointer<vtkStructuredGrid>::New();
  vtkSmartPointer<vtkDataSetMapper> grid_mapper = vtkSmartPointer<vtkDataSetMapper>::New();
  vtkSmartPointer<vtkActor> grid_actor = vtkSmartPointer<vtkActor>::New();
  
  z = -(_fov->vxL(2)*_fov->vxN(2))/2;
  for(uint16_t iz = 0; iz <= _fov->vxN(2); iz++) {
    y = -(_fov->vxL(1)*_fov->vxN(1))/2;
    for(uint16_t iy = 0; iy <= _fov->vxN(1); iy++) {
      x = -(_fov->vxL(0)*_fov->vxN(0))/2;
      for(uint16_t ix = 0; ix <= _fov->vxN(0); ix++) {
        points->InsertNextPoint(x, y, z);
        x += _fov->vxL(0);
      }
      y += _fov->vxL(1);
    }
    z += _fov->vxL(2);
  }
  structured_grid->SetDimensions(_fov->vxN(0)+1, _fov->vxN(1)+1, _fov->vxN(2)+1);
  structured_grid->SetPoints(points);
  
  grid_mapper->SetInputData(structured_grid);
  grid_actor->GetProperty()->SetOpacity(0.2);
  grid_actor->SetMapper(grid_mapper);
  grid_actor->GetProperty()->EdgeVisibilityOn();
  grid_actor->GetProperty()->SetEdgeColor(100.0/255, 0, 0);
  
  renderer->AddActor(grid_actor);
  
}




void ScannerVisualizer::drawFovPoints() const
{
  const std::vector<ThreeVector>& vec = _scanner->CoincidenceSchemaMaxFOVPoints();
  for (uint16_t start=0; start<vec.size(); start++) {
    uint16_t stop = (start + 1) % vec.size();
    this->drawLine(vec[start], vec[stop], "green", 2);
  }
  
  const std::vector<ThreeVector>& fov_rot = _fov->polygonPoints();
  for (uint16_t start=0; start<fov_rot.size(); start++) {
    uint16_t stop = (start + 1) % vec.size();
    this->drawLine(fov_rot[start], fov_rot[stop], "yellow", 2);
  }
}



void ScannerVisualizer::drawGeometry() const
{
  const DetectorsList& det_list = _scanner->getDetectorsList();
  
  for (const std::pair<detID_t, PlanarDetectorType>& l_pair : det_list) {
    const PlanarDetectorType& det = l_pair.second;
    AngleType angle = std::atan(det.orientation()[1]/det.orientation()[0]);
    
    if ( _scanner->virtualGeometry() ) {
//       if ( !det.virtualLayerPresent() )
//         throw std::string("Virtual Geometry activated but no virtual layer is present");
      
      SpaceType ly = det.virtualLayer().crystalPitchY();
      SpaceType lz = det.virtualLayer().crystalPitchZ();
      drawPlanarLayer( ThreeVector(0.1, ly, lz),
                       det.vCryCenters(),
                       0,
                       det.vCryCenters().size(),
                       angle,
                       ThreeVector(200, 150, 10),
                       1);
    }
//     else {
      const uint16_t& n_layers = det.nLayers();
      
      uint16_t start_c=0, stop_c=0;
      for (uint16_t layer=0; layer<n_layers; layer++) {
        const PlanarLayerType& layer_s = det.layer(layer);
        ThreeVector crystal_lengths(layer_s.crystalLengthX(),
                                    layer_s.crystalLengthY(),
                                    layer_s.crystalLengthZ());
        uint16_t n_crystals_in_layer = det.layer(layer).nCrystals();
        start_c += stop_c;
        stop_c += n_crystals_in_layer;
        float true_det_opacity = 0.5;
        
        drawPlanarLayer( crystal_lengths,
                         det.rCryCenters(),
                         start_c,
                         stop_c,
                         angle,
                         ThreeVector(252, 252, 252),
                         true_det_opacity);
        //       drawPoints( det.rCryCenters() );
      }
//     }
    
  }
  
}


void ScannerVisualizer::drawPlanarLayer(
  const ThreeVector& crystal_lengths,
  const std::vector<ThreeVector>& crystal_centers,
  uint16_t start_idx,
  uint16_t stop_idx,
  const AngleType rotation,
  const ThreeVector& color,
  float opacity) const
{
  SpaceType halfx = crystal_lengths.x()/2;
  SpaceType halfy = crystal_lengths.y()/2;
  SpaceType halfz = crystal_lengths.z()/2;

  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  vtkHexahedron * *hexahedron_list;
  vtkSmartPointer<vtkUnstructuredGrid> unstructured_grid = vtkSmartPointer<vtkUnstructuredGrid>::New();
  vtkSmartPointer<vtkDataSetMapper> grid_mapper = vtkSmartPointer<vtkDataSetMapper>::New();
  vtkSmartPointer<vtkActor> grid_actor = vtkSmartPointer<vtkActor>::New();

  std::vector<ThreeVector> corners = crystalCorners(halfx, halfy, halfz, rotation);
  
  // Step through all crystals of the LAYER
  uint16_t n_crystals = stop_idx - start_idx;
  for (uint16_t idx=start_idx; idx<stop_idx; idx++) {
    SpaceType x = (crystal_centers[idx]).x();
    SpaceType y = (crystal_centers[idx]).y();
    SpaceType z = (crystal_centers[idx]).z();
    for (const ThreeVector& corner : corners)
      points->InsertNextPoint(x+corner.x(), y+corner.y(), z+corner.z());
  }
    
  //create the array of hexahedrons of the whole module
  hexahedron_list = new vtkHexahedron*[n_crystals];
  for(uint16_t i=0; i<n_crystals; i++) {
    hexahedron_list[i] = vtkHexahedron::New();
    hexahedron_list[i]->GetPointIds()->SetNumberOfIds(8);
  //this maps internal id 0-7 to global point Id of the vtkPoints list
    for (uint16_t j = 0; j <8; j++) 
      hexahedron_list[i]->GetPointIds()->SetId(j,j+(i*8));
  }
	
  //create an unstructured grid for the hexahedrons
  unstructured_grid->Allocate(n_crystals);
  for(uint16_t i =0; i<n_crystals; i++)
    unstructured_grid->InsertNextCell(hexahedron_list[i]->GetCellType(), hexahedron_list[i]->GetPointIds());
  unstructured_grid->SetPoints(points);

  //create a mapper and actor

  grid_mapper->SetInputData(unstructured_grid);
  grid_actor->SetMapper(grid_mapper);
  grid_actor->GetProperty()->SetOpacity(opacity);
  grid_actor->GetProperty()->EdgeVisibilityOn();
  grid_actor->GetProperty()->SetEdgeColor(150.0/255, 150.0/255, 150.0/255);
  grid_actor->GetProperty()->SetColor(color.x()/255, color.y()/255, color.z()/255);

  renderer->AddActor(grid_actor);

  for(uint16_t i=0; i<n_crystals; i++)
    hexahedron_list[i]->Delete();
  delete[] hexahedron_list;
  
}



void ScannerVisualizer::drawLine(
  const ThreeVector& startPoint,
  const ThreeVector& stopPoint,
  const std::string& color,
  SpaceType width) const
{
  vtkSmartPointer<vtkLineSource> line = vtkSmartPointer<vtkLineSource>::New();
  line->SetPoint1(startPoint.x(), startPoint.y(), startPoint.z());
  line->SetPoint2(stopPoint.x(), stopPoint.y(), stopPoint.z());
  // Visualize
  vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();

  vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(line->GetOutputPort());
  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);
  actor->GetProperty()->SetLineWidth(width);
  actor->GetProperty()->SetColor(colors->GetColor3d(color).GetData());

  renderer->AddActor(actor);
}


void ScannerVisualizer::drawPoint(const ThreeVector& point, SpaceType radius, std::string color) const
{
  vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();
  // Create a sphere
  vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
  sphereSource->SetCenter(point.x(), point.y(), point.z());
  sphereSource->SetRadius(radius);
  
  vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(sphereSource->GetOutputPort());
  
  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);
  actor->GetProperty()->SetColor(colors->GetColor3d(color).GetData());
  
  renderer->AddActor(actor);
}

void ScannerVisualizer::drawPoints(const std::vector<ThreeVector>& points, SpaceType radius) const
{
  vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();
  // Create a sphere
  for (const ThreeVector& p : points)
    drawPoint(p, radius, "Green");
}

#include <vtkDiskSource.h>
#include <vtkCylinderSource.h>

void
ScannerVisualizer::drawCylinder(const ThreeVector& center,
                                const SpaceType length,
                                const SpaceType r_min,
                                const SpaceType r_max
                               ) const
{
  vtkSmartPointer<vtkDiskSource> diskSource = vtkSmartPointer<vtkDiskSource>::New();
//   diskSource->SetCenter(center.x(), center.y(), center.z());
  diskSource->SetInnerRadius(r_min);
  diskSource->SetOuterRadius(r_max);
  diskSource->SetRadialResolution(100);
  diskSource->SetCircumferentialResolution(1000);
  
  // Create a mapper and actor
  vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(diskSource->GetOutputPort());
  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);
//   actor->RotateX(90);
  

  renderer->AddActor(actor);
}

void
ScannerVisualizer::drawBox(const ThreeVector& center,
                           const ThreeVector& size,
                           const std::string& color,
                           const AngleType rotation ) const
{
  
  vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();

  SpaceType halfx = size.x()/2;
  SpaceType halfy = size.y()/2;
  SpaceType halfz = size.z()/2;
  
  SpaceType x = center.x();
  SpaceType y = center.y();
  SpaceType z = center.z();
  
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  std::vector<ThreeVector> corners = crystalCorners(halfx, halfy, halfz, rotation);
  for (const ThreeVector& corner : corners)
    points->InsertNextPoint(x+corner.x(), y+corner.y(), z+corner.z());
  
  // Create a hexahedron from the points.
  vtkSmartPointer<vtkHexahedron> hex = vtkSmartPointer<vtkHexahedron>::New();
  hex->GetPointIds()->SetNumberOfIds(8);
  for (uint16_t i = 0; i < 8; ++i) {
    hex->GetPointIds()->SetId(i, i);
  }

  // Add the points and hexahedron to an unstructured grid.
  vtkSmartPointer<vtkUnstructuredGrid> uGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
  uGrid->SetPoints(points);
  uGrid->InsertNextCell(hex->GetCellType(), hex->GetPointIds());

  
  // Visualize.
  vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
  mapper->SetInputData(uGrid);

  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->GetProperty()->SetColor(colors->GetColor3d(color).GetData());
  actor->SetMapper(mapper);

  renderer->AddActor(actor);
  
}



void ScannerVisualizer::wait() const
{
  render_window_interactor->Start();
}

// void KeypressCallbackFunction ( vtkObject* caller, long unsigned int vtkNotUsed(eventId), void* vtkNotUsed(clientData), void* vtkNotUsed(callData) )
// {
//   std::cout << "Keypress callback" << std::endl;
//   
//   vtkRenderWindowInteractor *iren = 
//   static_cast<vtkRenderWindowInteractor*>(caller);
//   
//   std::cout << "Pressed: " << iren->GetKeySym() << std::endl;
//   
//   
// }


/*
 IF you have a 3-button mouse, point to the actor using mouse and then push middle button as you move. If you with 2-button mouse, pan is defined as <Shift>-Button 1.
  a.. Keypress j / Keypress t: toggle between joystick (position sensitive) and trackball (motion sensitive) styles. In joystick style, motion occurs continuously as long as a mouse button is pressed. In trackball style, motion occurs when the mouse button is pressed and the mouse pointer moves. 
  b.. Keypress c / Keypress a: toggle between camera and actor modes. In camera mode, mouse events affect the camera position and focal point. In actor mode, mouse events affect the actor that is under the mouse pointer. 
  c.. Button 1: rotate the camera around its focal point (if camera mode) or rotate the actor around its origin (if actor mode). The rotation is in the direction defined from the center of the renderer's viewport towards the mouse position. In joystick mode, the magnitude of the rotation is determined by the distance the mouse is from the center of the render window. 
  d.. Button 2: pan the camera (if camera mode) or translate the actor (if actor mode). In joystick mode, the direction of pan or translation is from the center of the viewport towards the mouse position. In trackball mode, the direction of motion is the direction the mouse moves. (Note: with 2-button mice, pan is defined as <Shift>-Button 1.) 
  e.. Button 3: zoom the camera (if camera mode) or scale the actor (if actor mode). Zoom in/increase scale if the mouse position is in the top half of the viewport; zoom out/decrease scale if the mouse position is in the bottom half. In joystick mode, the amount of zoom is controlled by the distance of the mouse pointer from the horizontal centerline of the window. 
  f.. Keypress 3: toggle the render window into and out of stereo mode. By default, red-blue stereo pairs are created. Some systems support Crystal Eyes LCD stereo glasses; you have to invoke SetStereoTypeToCrystalEyes() on the rendering window. 
  g.. Keypress e: exit the application. 
  h.. Keypress f: fly to the picked point 
  i.. Keypress p: perform a pick operation. The render window interactor has an internal instance of vtkCellPicker that it uses to pick. 
  j.. Keypress r: reset the camera view along the current view direction. Centers the actors and moves the camera so that all actors are visible. 
  k.. Keypress s: modify the representation of all actors so that they are surfaces. 
  l.. Keypress u: invoke the user-defined function. Typically, this keypress will bring up an interactor that you can type commands in. 
  m.. Keypress w: modify the representation of all actors so that they are wireframe.
  */

