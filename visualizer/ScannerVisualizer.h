/*
 * This file is part of Y-PET.
 * 
 * Y-PET is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Y-PET (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2017-2021 all Y-PET: Alessandro PILLERI
 */

#ifndef SCANNER_VISUALIZER_H
#define SCANNER_VISUALIZER_H

#include <vtkSmartPointer.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkMath.h>

#include <vtkAxesActor.h>
#include <vtkActor.h>
#include <vtkTextActor.h>
#include <vtkProperty.h>
#include <vtkTextProperty.h>
#include <vtkCaptionActor2D.h>

//FOV
#include <vtkStructuredGrid.h>
#include <vtkDataSetMapper.h>

//CylindricalScanner
#include <vtkUnstructuredGrid.h>
#include <vtkHexahedron.h>

//LOR
#include <vtkLineSource.h>
#include <vtkNamedColors.h>
#include <vtkPolyDataMapper.h>
#include <vtkVoxel.h>
#include <vtkPolyData.h>
#include <vtkUnsignedCharArray.h>
#include <vtkPointData.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkSphereSource.h>

#include "vGeometry.h"
#include "fieldOfView.h"
#include "vProjector.h"

/*! \class ScannerVisualizer
 *  \brief VTK scanner visualization.
 *
 *  This class is used to obtain a graphical representation of the scanner. It
 *  is possible to draw the Scanner, to draw the FOV, and to obtain a visualization
 *  of the FOV voxel crossed by a particular Line of Response (LOR).
 *
 */

class ScannerVisualizer {
  
  std::string xml_config;
  
  vGeometry* _scanner;
  
  FieldOfView* _fov;
  
  vProjector* _projector;
  
  vtkSmartPointer<vtkRenderer> renderer;
  vtkSmartPointer<vtkRenderWindow> render_window;
  vtkSmartPointer<vtkRenderWindowInteractor> render_window_interactor;
  
  vtkSmartPointer<vtkActor> drawn_lor;

public:
  //! Inizialize the VTK environment and objects.
  ScannerVisualizer(const std::string& config,  bool activate_vg);
  
  void initFOV(const std::string& tag_fov_p);
  
  void initProjector(const std::string& proj_tag_p);
  
  vGeometry* scanner() const { return _scanner; }
  
  const FieldOfView* fov() const { return _fov; }
  
  const vProjector* projector() const { return _projector; }
  
  //! Draw the axis of the Cartesian coordinate system
  void drawAxes() const;
  
  //! Visualization of the FOV
  void drawFov() const;
  
  void drawFovPoints() const;
  
  
  void drawGeometry() const;
  
  void drawPlanarLayer(const ThreeVector& crystal_lengths,
                       const std::vector<ThreeVector>& crystal_centers,
                       uint16_t start_idx,
                       uint16_t stop_idx,
                       const AngleType rotation,
                       const ThreeVector& color,
                       float opacity
                      ) const;
  
  /*! \brief Line drawing between two points in space.
   * 
   *  \param startPoint absolute position of the starting point
   *  \param stopPoint absolute position of the ending point
   */
  void drawLine(const ThreeVector& startPoint,
                const ThreeVector& stopPoint,
                const std::string& color,
                SpaceType width) const;
  
  void drawPoint(const ThreeVector& point, SpaceType radius, std::string color) const;              
                
  void drawPoints(const std::vector<ThreeVector>& points, SpaceType radius) const;
                
  void drawCylinder(const ThreeVector& center,
                    const SpaceType length,
                    const SpaceType r_min,
                    const SpaceType r_max
                   ) const;
                        
  void drawBox(const ThreeVector& center,
               const ThreeVector& size,
               const std::string& color, 
               const AngleType rotationDeg
              ) const ;
               
  void drawLOR(const std::vector<MatrixEntry> &row) const;
  
//   void drawSymmetricDetectors(const std::vector<SymPair>& list) const;
  
  void wait() const;
  
  std::vector<MatrixEntry>
    onLineLOR(const std::vector<uint16_t>& first,
              const std::vector<uint16_t>& second) const;
  
  std::vector<MatrixEntry>
    offLineLOR(const std::vector<uint16_t>& first,
              const std::vector<uint16_t>& second,
              const std::string& g_dir,
              const std::string& d_dir) const;
};



// void KeypressCallbackFunction (
//   vtkObject* caller,
//   long unsigned int eventId,
//   void* clientData,
//   void* callData );

#endif
